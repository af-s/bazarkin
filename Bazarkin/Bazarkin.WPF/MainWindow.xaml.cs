﻿using Bazarkin.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Bazarkin.WPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private EFDbContext db = new EFDbContext();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnAuthorization_Click(object sender, RoutedEventArgs e)
        {
            Authorization authorization = db.AuthorizationSet.Where(x => x.Login == tbPhone.Text && x.Password == tbPassword.Password).FirstOrDefault();

            if (authorization != null)
            {
                string key = authorization.Key;
                //DashboardWindow dashboardWindow = new DashboardWindow(key);
                this.Close();
                //dashboardWindow.Show();
            }
            else
            {
                lblInfo.Content = "Логин или пароль неверный";
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Touch.OrganizationWindow ow = new Touch.OrganizationWindow();
            ow.Show();
            this.Close();
        }
    }
}
