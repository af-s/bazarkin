﻿using Bazarkin.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Bazarkin.WPF.Touch
{
    /// <summary>
    /// Логика взаимодействия для Organization.xaml
    /// </summary>
    public partial class OrganizationWindow : Window
    {
        private EFDbContext db = new EFDbContext();
        private string type;

        private Organization organization;
        private OrganizationUnit organizationUnit;
        private Address address;
        private Phone phone;
        private Mail mail;

        public OrganizationWindow()
        {
            InitializeComponent();

            CollapsedAll();
            OrganizationList(null);
        }

        private void CollapsedAll()
        {
            spOrganizationList.Visibility = Visibility.Collapsed;
            spOrganization.Visibility = Visibility.Collapsed;
            spOrganizationUnitList.Visibility = Visibility.Collapsed;
            spOrganizationUnit.Visibility = Visibility.Collapsed;
            spAddressList.Visibility = Visibility.Collapsed;
            spPhoneList.Visibility = Visibility.Collapsed;
            spMailList.Visibility = Visibility.Collapsed;
            spResult.Visibility = Visibility.Collapsed;
            spSave.Visibility = Visibility.Collapsed;
            spAddressEdit.Visibility = Visibility.Collapsed;
            spPhoneEdit.Visibility = Visibility.Collapsed;
            spMailEdit.Visibility = Visibility.Collapsed;
        }

        private void ClearAll()
        {
            tbOrganizationSearh.Text = string.Empty;

            tbOrganizationName.Text = string.Empty;
            tbOrganizationDescription.Text = string.Empty;

            tbOrganizationUnitName.Text = string.Empty;
            tbOrganizationUnitDescription.Text = string.Empty;

            tbOrganizationInfo.Text = string.Empty;
            tbOrganizationSearh.Text = string.Empty;

            tbPhoneNumber.Text = string.Empty;
            tbPhoneDescription.Text = string.Empty;
            tbMailAddress.Text = string.Empty;
            tbMailDescription.Text = string.Empty;

            cbPhoneView.IsChecked = false;

            tbCountry.Text = string.Empty;
            tbRegion.Text = string.Empty;
            tbCity.Text = string.Empty;
            tbStreet.Text = string.Empty;
            tbBuilding.Text = string.Empty;
            tbStructure.Text = string.Empty;
            tbRoom.Text = string.Empty;
            tbShopingCenter.Text = string.Empty;
            tbSector.Text = string.Empty;
            tbRow.Text = string.Empty;
            tbPavilion.Text = string.Empty;
            tbIndex.Text = string.Empty;
            tbAddressDescription.Text = string.Empty;
            cbViewAddress.IsChecked = false;

            spOrganizationRowList.Children.Clear();
            spOrganizationUnitRowList.Children.Clear();

            spAddressRowList.Children.Clear();
            spPhoneRowList.Children.Clear();
            spMailRowList.Children.Clear();

            cbStatus.Items.Clear();
        }

        private void OrganizationList(List<Organization> OrganizationSet)
        {
            type = "organizationList";
            spOrganizationList.Visibility = Visibility.Visible;
            if (OrganizationSet == null)
            {
                OrganizationSet = db.OrganizationSet.Where(x => x.Id != 0).OrderBy(x => x.Name).ToList();
            }

            bool gray = true;
            foreach (var organization in OrganizationSet)
            {
                StackPanel spOrganizationRow = new StackPanel();
                spOrganizationRow.Orientation = Orientation.Horizontal;
                if (gray)
                {
                    spOrganizationRow.Background = new SolidColorBrush(Color.FromRgb(238, 238, 238));
                }
                gray = !gray;
                Label lblId = new Label();
                lblId.Width = 50;
                lblId.Foreground = new SolidColorBrush(Color.FromRgb(170, 170, 170));
                lblId.Content = organization.Id;

                Label lblName = new Label();
                lblName.Width = 540;
                lblName.Content = organization.Name;

                Label lblOrganizationUnitCount = new Label();
                lblOrganizationUnitCount.Width = 50;
                lblOrganizationUnitCount.Content = db.OrganizationUnitSet.Where(x => x.Organization.Id == organization.Id).Count();

                Button btnOrganizationUnitEdit = new Button();
                btnOrganizationUnitEdit.Width = 100;
                btnOrganizationUnitEdit.Content = "Редавктировать";
                btnOrganizationUnitEdit.Margin = new Thickness(10, 5, 5, 5);
                btnOrganizationUnitEdit.Click += new RoutedEventHandler(btnOrganizationEdit_Click);

                spOrganizationRow.Children.Add(lblId);
                spOrganizationRow.Children.Add(lblName);
                spOrganizationRow.Children.Add(lblOrganizationUnitCount);
                spOrganizationRow.Children.Add(btnOrganizationUnitEdit);

                spOrganizationRowList.Children.Add(spOrganizationRow);
            }
        }

        private void OrganizationAdd()
        {
            type = "organizationAdd";
            spOrganization.Visibility = Visibility.Visible;
            spSave.Visibility = Visibility.Visible;
        }

        private void OrganizationEdit()
        {
            //type = "organizationEdit";
            //spOrganization.Visibility = Visibility.Visible;
            //spOrganizationUnitList.Visibility = Visibility.Visible;
            //spResult.Visibility = Visibility.Visible;

            //spSave.Visibility = Visibility.Visible;

            //tbOrganizationName.Text = organization.Name;
            //tbOrganizationDescription.Text = organization.Description;

            //List<OrganizationUnit> organizationUnitSet = db.OrganizationUnitSet.Where(x => x.Organization.Id == organization.Id).ToList();
            //if (organizationUnitSet.Count == 0)
            //{
            //    spOrganizationUnitTable.Visibility = Visibility.Collapsed;
            //}
            //else
            //{
            //    spOrganizationUnitTable.Visibility = Visibility.Visible;

            //    bool gray = true;
            //    foreach (var organizationUnit in organizationUnitSet)
            //    {
            //        StackPanel spOrganizationUnitRow = new StackPanel();
            //        spOrganizationUnitRow.Orientation = Orientation.Horizontal;
            //        if (gray)
            //        {
            //            spOrganizationUnitRow.Background = new SolidColorBrush(Color.FromRgb(238, 238, 238));
            //        }
            //        gray = !gray;
            //        Label lblId = new Label();
            //        lblId.Width = 50;
            //        lblId.Foreground = new SolidColorBrush(Color.FromRgb(170, 170, 170));
            //        lblId.Content = organizationUnit.Id;

            //        Label lblName = new Label();
            //        lblName.Width = 590;
            //        lblName.Content = organizationUnit.Name;

            //        Button btnOrganizationEdit = new Button();
            //        btnOrganizationEdit.Width = 100;
            //        btnOrganizationEdit.Content = "Редактировать";
            //        btnOrganizationEdit.Margin = new Thickness(10, 5, 5, 5);
            //        btnOrganizationEdit.Click += new RoutedEventHandler(btnOrganizationUnitEdit_Click);

            //        spOrganizationUnitRow.Children.Add(lblId);
            //        spOrganizationUnitRow.Children.Add(lblName);
            //        spOrganizationUnitRow.Children.Add(btnOrganizationEdit);

            //        spOrganizationUnitRowList.Children.Add(spOrganizationUnitRow);
            //    }
            //}

            //List<OrganizationStatus> organizationStatusSet = db.OrganizationStatusSet.Where(x => x.Id != 0).OrderBy(x => x.Value).ToList();
            //OrganizationInfo organizationInfo = db.OrganizationInfoSet.Where(x => x.Organization.Id == organization.Id).FirstOrDefault();
            //foreach (var organizationStatus in organizationStatusSet)
            //{
            //    cbStatus.Items.Add(organizationStatus.Value);
            //}
            //if (organizationInfo != null)
            //{
            //    tbOrganizationInfo.Text = organizationInfo.Description;

            //    if (organizationInfo.OrganizationStatus != null)
            //    {
            //        OrganizationStatus organizationStatus = db.OrganizationStatusSet.Where(x => x.Id == organizationInfo.OrganizationStatus.Id).FirstOrDefault();
            //        if (organizationStatus != null)
            //        {
            //            cbStatus.SelectedItem = organizationInfo.OrganizationStatus.Value;
            //        }
            //    }
            //}
            //ContactGroup contactGroup = db.ContactGroupSet.Where(x => x.Id == organization.ContactGroup.Id).FirstOrDefault();
            //List<Address> addressSet = db.AddressSet.Where(x => x.ContactGroup.Id == contactGroup.Id).ToList();
            //AddressView(addressSet);
            //List<Phone> phoneSet = db.PhoneSet.Where(x => x.ContactGroup.Id == contactGroup.Id).ToList();
            //PhoneView(phoneSet);
            //List<Mail> mailSet = db.MailSet.Where(x => x.ContactGroup.Id == contactGroup.Id).ToList();
            //MailView(mailSet);
        }

        private void OrganizationUnitAdd()
        {
            type = "organizationUnitAdd";
            spOrganizationUnit.Visibility = Visibility.Visible;
            spSave.Visibility = Visibility.Visible;
        }

        private void OrganizationUnitEdit()
        {
            type = "organizationUnitEdit";
            spOrganizationUnit.Visibility = Visibility.Visible;

            spSave.Visibility = Visibility.Visible;

            //tbOrganizationUnitName.Text = organizationUnit.Name;
            tbOrganizationUnitDescription.Text = organizationUnit.Description;

            ContactGroup contactGroup = db.ContactGroupSet.Where(x => x.Id == organizationUnit.ContactGroup.Id).FirstOrDefault();
            List<Address> addressSet = db.AddressSet.Where(x => x.ContactGroup.Id == contactGroup.Id).ToList();
            AddressView(addressSet);
            List<Phone> phoneSet = db.PhoneSet.Where(x => x.ContactGroup.Id == contactGroup.Id).ToList();
            PhoneView(phoneSet);
            List<Mail> mailSet = db.MailSet.Where(x => x.ContactGroup.Id == contactGroup.Id).ToList();
            MailView(mailSet);
        }

        private void AddressEdit(ContactGroup contactGroup)
        {
            type = "addressEdit";
            spAddressEdit.Visibility = Visibility.Visible;

            tbCountry.Text = address.Country;
            tbRegion.Text = address.Region;
            tbCity.Text = address.City;
            tbStreet.Text = address.Street;
            tbBuilding.Text = address.Building;
            tbStructure.Text = address.Structure;
            tbRoom.Text = address.Room;
            tbShopingCenter.Text = address.ShoppingCenter;
            tbSector.Text = address.Sector;
            tbRow.Text = address.Row;
            tbPavilion.Text = address.Pavilion;
            tbIndex.Text = address.Index;
            tbAddressDescription.Text = address.Description;
            cbViewAddress.IsChecked = address.View;
        }

        private void PhoneEdit(ContactGroup contactGroup)
        {
            type = "phoneEdit";
            spPhoneEdit.Visibility = Visibility.Visible;

            tbPhoneNumber.Text = phone.Number;
            tbPhoneDescription.Text = phone.Description;
            cbPhoneView.IsChecked = phone.View;
        }

        private void MailEdit(ContactGroup contactGroup)
        {
            type = "mailEdit";
            spMailEdit.Visibility = Visibility.Visible;

            tbMailAddress.Text = mail.Value;
            tbMailDescription.Text = mail.Description;
        }

        private void btnOrganizationEdit_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            int id = Convert.ToInt32(((Label)((StackPanel)((Button)sender).Parent).Children[0]).Content);

            organization = db.OrganizationSet.Where(x => x.Id == id).FirstOrDefault();
            CollapsedAll();
            ClearAll();
            OrganizationEdit();
        }

        private void btnOrganizationAdd_Click(object sender, RoutedEventArgs e)
        {
            CollapsedAll();
            ClearAll();
            OrganizationAdd();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            //switch (type)
            //{
            //    case "organizationAdd":
            //        {
            //            organization = new Organization();
            //            organization.Name = tbOrganizationName.Text;
            //            organization.Description = tbOrganizationDescription.Text;
            //            organization.ContactGroup = new ContactGroup();

            //            db.OrganizationSet.Add(organization);
            //            db.SaveChanges();

            //            CollapsedAll();
            //            ClearAll();
            //            OrganizationEdit();
            //            break;
            //        }
            //    case "organizationEdit":
            //        {
            //            organization.Name = tbOrganizationName.Text;
            //            organization.Description = tbOrganizationDescription.Text;

            //            OrganizationInfo organizationInfo = db.OrganizationInfoSet.Where(x => x.Organization.Id == organization.Id).FirstOrDefault();
            //            if (organizationInfo == null)
            //            {
            //                organizationInfo = new OrganizationInfo();
            //                organizationInfo.Organization = organization;
            //            }
            //            organizationInfo.Description = tbOrganizationInfo.Text;

            //            if (cbStatus.SelectedItem != null)
            //            {
            //                OrganizationStatus organizationStatus = db.OrganizationStatusSet.Where(x => x.Value == cbStatus.SelectedItem.ToString()).FirstOrDefault();
            //                organizationInfo.OrganizationStatus = organizationStatus;
            //            }

            //            if (organizationInfo.Id == 0)
            //            {
            //                db.OrganizationInfoSet.Add(organizationInfo);
            //            }
            //            else
            //            {
            //                db.Entry(organizationInfo).State = EntityState.Modified;
            //            }

            //            db.Entry(organization).State = EntityState.Modified;
            //            db.SaveChanges();

            //            CollapsedAll();
            //            ClearAll();
            //            organization = null;
            //            OrganizationList(null);
            //            break;
            //        }
            //    case "organizationUnitAdd":
            //        {
            //            organizationUnit = new OrganizationUnit();
            //            organizationUnit.Name = tbOrganizationUnitName.Text;
            //            organizationUnit.Description = tbOrganizationUnitDescription.Text;
            //            organizationUnit.ContactGroup = new ContactGroup();
            //            organizationUnit.Organization = organization;

            //            db.OrganizationUnitSet.Add(organizationUnit);
            //            db.SaveChanges();

            //            CollapsedAll();
            //            ClearAll();
            //            OrganizationUnitEdit();
            //            break;
            //        }
            //    case "organizationUnitEdit":
            //        {
            //            organizationUnit.Name = tbOrganizationUnitName.Text;
            //            organizationUnit.Description = tbOrganizationUnitDescription.Text;

            //            db.Entry(organizationUnit).State = EntityState.Modified;
            //            db.SaveChanges();

            //            organizationUnit = null;

            //            CollapsedAll();
            //            ClearAll();
            //            OrganizationEdit();
            //            break;
            //        }
            //}
        }

        private void btnOrganizationClear_Click(object sender, RoutedEventArgs e)
        {
            OrganizationList(null);
        }

        private void btnOrganizationUnitAdd_Click(object sender, RoutedEventArgs e)
        {
            CollapsedAll();
            ClearAll();
            OrganizationUnitAdd();
        }

        private void btnOrganizationUnitEdit_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            int id = Convert.ToInt32(((Label)((StackPanel)((Button)sender).Parent).Children[0]).Content);

            organizationUnit = db.OrganizationUnitSet.Where(x => x.Id == id).FirstOrDefault();
            CollapsedAll();
            ClearAll();
            OrganizationUnitEdit();
        }

        private void AddressView(List<Address> addressSet)
        {
            spAddressList.Visibility = Visibility.Visible;
            if (addressSet.Count == 0)
            {
                spAddressTable.Visibility = Visibility.Collapsed;
            }
            else
            {
                spAddressTable.Visibility = Visibility.Visible;

                bool gray = true;
                foreach (var address in addressSet)
                {
                    StackPanel spAddressRow = new StackPanel();
                    spAddressRow.Orientation = Orientation.Horizontal;
                    if (gray)
                    {
                        spAddressRow.Background = new SolidColorBrush(Color.FromRgb(238, 238, 238));
                    }
                    gray = !gray;
                    Label lblId = new Label();
                    lblId.Width = 50;
                    lblId.Foreground = new SolidColorBrush(Color.FromRgb(170, 170, 170));
                    lblId.Content = address.Id;

                    Label lblAddress = new Label();
                    lblAddress.Width = 550;
                    lblAddress.Content = AddressCreate(address);

                    CheckBox cbView = new CheckBox();
                    cbView.Margin = new Thickness(13, 7, 10, 7);
                    cbView.Height = 16;
                    cbView.Width = 16;
                    cbView.IsEnabled = false;
                    cbView.IsChecked = address.View;

                    Button btnAddressEdit = new Button();
                    btnAddressEdit.Width = 100;
                    btnAddressEdit.Content = "Редактировать";
                    btnAddressEdit.Margin = new Thickness(10, 5, 5, 5);
                    btnAddressEdit.Click += new RoutedEventHandler(btnAddressEdit_Click);

                    spAddressRow.Children.Add(lblId);
                    spAddressRow.Children.Add(lblAddress);
                    spAddressRow.Children.Add(cbView);
                    spAddressRow.Children.Add(btnAddressEdit);

                    spAddressRowList.Children.Add(spAddressRow);
                }
            }
        }

        private void PhoneView(List<Phone> phoneSet)
        {
            spPhoneList.Visibility = Visibility.Visible;
            if (phoneSet.Count == 0)
            {
                spPhoneTable.Visibility = Visibility.Collapsed;
            }
            else
            {
                spPhoneTable.Visibility = Visibility.Visible;

                bool gray = true;
                foreach (var phone in phoneSet)
                {
                    StackPanel spPhoneRow = new StackPanel();
                    spPhoneRow.Orientation = Orientation.Horizontal;
                    if (gray)
                    {
                        spPhoneRow.Background = new SolidColorBrush(Color.FromRgb(238, 238, 238));
                    }
                    gray = !gray;
                    Label lblId = new Label();
                    lblId.Width = 50;
                    lblId.Foreground = new SolidColorBrush(Color.FromRgb(170, 170, 170));
                    lblId.Content = phone.Id;

                    Label lblPhone = new Label();
                    lblPhone.Width = 250;
                    lblPhone.Content = phone.Number;

                    Label lblDescription = new Label();
                    lblDescription.Width = 300;
                    lblDescription.Content = phone.Description;

                    CheckBox cbView = new CheckBox();
                    cbView.Margin = new Thickness(13, 7, 10, 7);
                    cbView.Height = 16;
                    cbView.Width = 16;
                    cbView.IsEnabled = false;
                    cbView.IsChecked = phone.View;

                    Button btnPhoneEdit = new Button();
                    btnPhoneEdit.Width = 100;
                    btnPhoneEdit.Content = "Редактировать";
                    btnPhoneEdit.Margin = new Thickness(10, 5, 5, 5);
                    btnPhoneEdit.Click += new RoutedEventHandler(btnPhoneEdit_Click);

                    spPhoneRow.Children.Add(lblId);
                    spPhoneRow.Children.Add(lblPhone);
                    spPhoneRow.Children.Add(lblDescription);
                    spPhoneRow.Children.Add(cbView);
                    spPhoneRow.Children.Add(btnPhoneEdit);

                    spPhoneRowList.Children.Add(spPhoneRow);
                }
            }
        }

        private void MailView(List<Mail> mailSet)
        {
            spMailList.Visibility = Visibility.Visible;
            if (mailSet.Count == 0)
            {
                spMailTable.Visibility = Visibility.Collapsed;
            }
            else
            {
                spMailTable.Visibility = Visibility.Visible;

                bool gray = true;
                foreach (var mail in mailSet)
                {
                    StackPanel spMailRow = new StackPanel();
                    spMailRow.Orientation = Orientation.Horizontal;
                    if (gray)
                    {
                        spMailRow.Background = new SolidColorBrush(Color.FromRgb(238, 238, 238));
                    }
                    gray = !gray;
                    Label lblId = new Label();
                    lblId.Width = 50;
                    lblId.Foreground = new SolidColorBrush(Color.FromRgb(170, 170, 170));
                    lblId.Content = mail.Id;

                    Label lblMail = new Label();
                    lblMail.Width = 250;
                    lblMail.Content = mail.Value;

                    Label lblDescription = new Label();
                    lblDescription.Width = 340;
                    lblDescription.Content = mail.Description;

                    Button btnPhoneEdit = new Button();
                    btnPhoneEdit.Width = 100;
                    btnPhoneEdit.Content = "Редактировать";
                    btnPhoneEdit.Margin = new Thickness(10, 5, 5, 5);
                    btnPhoneEdit.Click += new RoutedEventHandler(btnMailEdit_Click);

                    spMailRow.Children.Add(lblId);
                    spMailRow.Children.Add(lblMail);
                    spMailRow.Children.Add(lblDescription);
                    spMailRow.Children.Add(btnPhoneEdit);

                    spMailRowList.Children.Add(spMailRow);
                }
            }
        }

        private void btnAddressEdit_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            int id = Convert.ToInt32(((Label)((StackPanel)((Button)sender).Parent).Children[0]).Content);

            address = db.AddressSet.Where(x => x.Id == id).FirstOrDefault();
            ContactGroup contactGroup = db.ContactGroupSet.Where(x => x.Id == address.ContactGroup.Id).FirstOrDefault();
            CollapsedAll();
            ClearAll();
            AddressEdit(contactGroup);
        }

        private void btnPhoneEdit_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            int id = Convert.ToInt32(((Label)((StackPanel)((Button)sender).Parent).Children[0]).Content);

            phone = db.PhoneSet.Where(x => x.Id == id).FirstOrDefault();
            ContactGroup contactGroup = db.ContactGroupSet.Where(x => x.Id == phone.ContactGroup.Id).FirstOrDefault();
            CollapsedAll();
            ClearAll();
            PhoneEdit(contactGroup);
        }

        private void btnMailEdit_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            int id = Convert.ToInt32(((Label)((StackPanel)((Button)sender).Parent).Children[0]).Content);

            mail = db.MailSet.Where(x => x.Id == id).FirstOrDefault();
            ContactGroup contactGroup = db.ContactGroupSet.Where(x => x.Id == mail.ContactGroup.Id).FirstOrDefault();
            CollapsedAll();
            ClearAll();
            MailEdit(contactGroup);
        }

        private string AddressCreate(Address address)
        {
            string addressStr = string.Empty;
            if (address.Country != string.Empty && address.Country != null)
            {
                addressStr = addressStr + address.Country;
            }
            if (address.Region != string.Empty && address.Region != null)
            {
                if (addressStr != string.Empty)
                {
                    addressStr = addressStr + ", ";
                }
                addressStr = addressStr + address.Region;
            }
            if (address.City != string.Empty)
            {
                if (addressStr != string.Empty)
                {
                    addressStr = addressStr + ", ";
                }
                addressStr = addressStr + address.City;
            }
            if (address.Street != string.Empty)
            {
                if (addressStr != string.Empty)
                {
                    addressStr = addressStr + ", ";
                }
                addressStr = addressStr + address.Street;
            }
            if (address.Building != string.Empty)
            {
                if (addressStr != string.Empty)
                {
                    addressStr = addressStr + ", ";
                }
                addressStr = addressStr + address.Building;
            }
            if (address.Structure != string.Empty)
            {
                if (addressStr != string.Empty)
                {
                    addressStr = addressStr + ", ";
                }
                addressStr = addressStr + "строение " + address.Structure;
            }
            if (address.Room != string.Empty)
            {
                if (addressStr != string.Empty)
                {
                    addressStr = addressStr + ", ";
                }
                addressStr = addressStr + address.Room;
            }
            if (address.ShoppingCenter != string.Empty)
            {
                if (addressStr != string.Empty)
                {
                    addressStr = addressStr + ", ";
                }
                addressStr = addressStr + "торговый центр " + address.ShoppingCenter;
            }
            if (address.Sector != string.Empty)
            {
                if (addressStr != string.Empty)
                {
                    addressStr = addressStr + ", ";
                }
                addressStr = addressStr + "сектор " + address.Sector;
            }
            if (address.Row != string.Empty)
            {
                if (addressStr != string.Empty)
                {
                    addressStr = addressStr + ", ";
                }
                addressStr = addressStr + "ряд " + address.Row;
            }
            if (address.Pavilion != string.Empty)
            {
                if (addressStr != string.Empty)
                {
                    addressStr = addressStr + ", ";
                }
                addressStr = addressStr + "пивильон " + address.Pavilion;
            }
            return addressStr;
        }

        private void btnAddressSave_Click(object sender, RoutedEventArgs e)
        {
            address.Country = tbCountry.Text;
            address.Region = tbRegion.Text;
            address.City = tbCity.Text;
            address.Street = tbStreet.Text;
            address.Building = tbBuilding.Text;
            address.Structure = tbStructure.Text;
            address.Room = tbRoom.Text;
            address.ShoppingCenter = tbShopingCenter.Text;
            address.Sector = tbSector.Text;
            address.Row = tbRow.Text;
            address.Pavilion = tbPavilion.Text;
            address.Index = tbIndex.Text;
            address.Description = tbAddressDescription.Text;
            address.View = (bool)cbViewAddress.IsChecked;

            if (address.Id == 0)
            {
                db.AddressSet.Add(address);
            }
            else
            {
                db.Entry(organization).State = EntityState.Modified;
            }

            db.SaveChanges();
            address = null;
            CollapsedAll();
            ClearAll();

            if (organizationUnit == null)
            {
                organization = db.OrganizationSet.Where(x => x.Id == organization.Id).FirstOrDefault();
                OrganizationEdit();
            }
            else
            {
                organizationUnit = db.OrganizationUnitSet.Where(x => x.Id == organizationUnit.Id).FirstOrDefault();
                OrganizationUnitEdit();
            }
        }

        private void btnOrganizationAddressAdd_Click(object sender, RoutedEventArgs e)
        {
            address = new Address();

            ContactGroup contactGroup;
            if (organizationUnit == null)
            {
                contactGroup = db.ContactGroupSet.Where(x => x.Id == organization.ContactGroup.Id).FirstOrDefault();
            }
            else
            {
                contactGroup = db.ContactGroupSet.Where(x => x.Id == organizationUnit.ContactGroup.Id).FirstOrDefault();
            }

            address.ContactGroup = contactGroup;

            CollapsedAll();
            ClearAll();
            AddressEdit(contactGroup);
        }

        private void btnOrganizationPhoneAdd_Click(object sender, RoutedEventArgs e)
        {
            phone = new Phone();

            ContactGroup contactGroup;

            if (organizationUnit == null)
            {
                contactGroup = db.ContactGroupSet.Where(x => x.Id == organization.ContactGroup.Id).FirstOrDefault();
            }
            else
            {
                contactGroup = db.ContactGroupSet.Where(x => x.Id == organizationUnit.ContactGroup.Id).FirstOrDefault();
            }
            phone.ContactGroup = contactGroup;

            CollapsedAll();
            ClearAll();
            PhoneEdit(contactGroup);
        }

        private void btnPhoneSave_Click(object sender, RoutedEventArgs e)
        {
            phone.Number = tbPhoneNumber.Text;
            phone.Description = tbPhoneDescription.Text;
            phone.View = (bool)cbPhoneView.IsChecked;

            if (phone.Id == 0)
            {
                db.PhoneSet.Add(phone);
            }
            else
            {
                db.Entry(phone).State = EntityState.Modified;
            }

            db.SaveChanges();
            phone = null;
            CollapsedAll();
            ClearAll();

            if (organizationUnit == null)
            {
                organization = db.OrganizationSet.Where(x => x.Id == organization.Id).FirstOrDefault();
                OrganizationEdit();
            }
            else
            {
                organizationUnit = db.OrganizationUnitSet.Where(x => x.Id == organizationUnit.Id).FirstOrDefault();
                OrganizationUnitEdit();
            }
        }

        private void btnOrganizationMailAdd_Click(object sender, RoutedEventArgs e)
        {
            mail = new Mail();

            ContactGroup contactGroup;

            if (organizationUnit == null)
            {
                contactGroup = db.ContactGroupSet.Where(x => x.Id == organization.ContactGroup.Id).FirstOrDefault();
            }
            else
            {
                contactGroup = db.ContactGroupSet.Where(x => x.Id == organizationUnit.ContactGroup.Id).FirstOrDefault();
            }
            mail.ContactGroup = contactGroup;

            CollapsedAll();
            ClearAll();
            MailEdit(contactGroup);
        }
        private void btnMailSave_Click(object sender, RoutedEventArgs e)
        {
            mail.Value = tbMailAddress.Text;
            mail.Description = tbMailDescription.Text;

            if (mail.Id == 0)
            {
                db.MailSet.Add(mail);
            }
            else
            {
                db.Entry(mail).State = EntityState.Modified;
            }

            db.SaveChanges();
            mail = null;
            CollapsedAll();
            ClearAll();

            if (organizationUnit == null)
            {
                organization = db.OrganizationSet.Where(x => x.Id == organization.Id).FirstOrDefault();
                OrganizationEdit();
            }
            else
            {
                organizationUnit = db.OrganizationUnitSet.Where(x => x.Id == organizationUnit.Id).FirstOrDefault();
                OrganizationUnitEdit();
            }
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {


            if (type != "organizationList")
            {
                CollapsedAll();
                ClearAll();
            }

            if (type == "organizationAdd" || type == "organizationEdit")
            {
                organization = null;
                type = "organizationList";
                OrganizationList(null);
            }
            else if (type == "organizationUnitAdd" || type == "organizationUnitEdit")
            {
                organizationUnit = null;
                type = "organizationEdit";
                OrganizationEdit();
            }
            else if (type == "addressEdit" || type == "phoneEdit" || type == "mailEdit")
            {
                address = null;
                phone = null;
                mail = null;

                if (organizationUnit == null)
                {
                    organization = db.OrganizationSet.Where(x => x.Id == organization.Id).FirstOrDefault();
                    type = "organizationEdit";
                    OrganizationEdit();
                }
                else
                {
                    organizationUnit = db.OrganizationUnitSet.Where(x => x.Id == organizationUnit.Id).FirstOrDefault();
                    type = "organizationUnitEdit";
                    OrganizationUnitEdit();
                }
            }
        }

        private void btnOrganizationSearh_Click(object sender, RoutedEventArgs e)
        {
            string searchText = tbOrganizationSearh.Text;

            List<Organization> organizationSet = db.OrganizationSet.Where(x => x.Name.ToLower().Contains(searchText.ToLower())).ToList();

            CollapsedAll();
            ClearAll();
            OrganizationList(organizationSet);
        }
    }
}
