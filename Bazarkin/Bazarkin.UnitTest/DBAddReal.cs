﻿//using System;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Bazarkin.Domain;
//using System.Linq;
//using System.Data.Entity.Validation;
//using Bazarkin.Web.BusinessLogic;

//namespace Bazarkin.UnitTest
//{
//    [TestClass]
//    public class DBAddReal
//    {
//        EFDbContext db = new EFDbContext();

//        [TestMethod]
//        public void n01ManufacturerAdd()
//        {
//            Manufacturer manufacturer1 = new Manufacturer();
//            manufacturer1.Name = "CISA";
//            manufacturer1.Description = "Итальянская компания, является ведущим брендом в Европе в сфере производства замков и систем управления доступа. Имея 6 заводов и распространяя свою продукцию в более чем 70 странах, CISA играет важнейшую роль на мировом рынке. Более 30 000 наименований продукции CISA прошли сертификацию в соответствии с европейскими стандартами.";
//            manufacturer1.Alias =  Translit.RuToTranslite(manufacturer1.Name);
//            db.ManufacturerSet.Add(manufacturer1);

//            Manufacturer manufacturer2 = new Manufacturer();
//            manufacturer2.Name = "Kale Kilit";
//            manufacturer2.Description = "Турецкая компания по производству замочно-скобяных изделий Kale Kilit входит в число лидеров рынка. На сегодняшний момент производительность завода превышает 100 тысяч единиц изделий в день. История компании началась в 1953 году, когда была создана небольшая мастерская площадью всего 40 кв.м. С тех пор Kale Kilit постоянно наращивала объемы производства, расширяла ассортимент продукции. Kale Kilit сейчас – это завод площадью 21 тыс. кв.м, оснащенный по последнему слову техники.";
//            manufacturer2.Alias = Translit.RuToTranslite(manufacturer2.Name);
//            db.ManufacturerSet.Add(manufacturer2);

//            Manufacturer manufacturer3 = new Manufacturer();
//            manufacturer3.Name = "Apecks";
//            manufacturer3.Description = "Компания Apecs начала работать на российском рынке в 1992 году и продолжительное время занималась дистрибуцией замков отечественного и европейского производства. Именно тогда на основании опыта работы с российскими и европейскими заводами было принято решение о создании собственного бренда – Apecs. Тогда же были сформулированы основные принципы нового бренда: европейский дизайн, стабильное качество, разумная цена";
//            manufacturer3.Alias = Translit.RuToTranslite(manufacturer3.Name);
//            db.ManufacturerSet.Add(manufacturer3);

//            db.SaveChanges();
//        }

//        [TestMethod]
//        public void n02CategoryAdd()
//        {
//            //Category root = new Category();
//            //root.Name="root";
//            //root.Description=string.Empty;
//            //root.Alias=Translit.RuToTranslite(root.Name);
//            //root.Parent=null;
//            //db.CategorySet.Add(root);

//            //Category category1 = new Category();
//            //category1.Name = "Бытовая техника";
//            //category1.Description = "Описание категории Бытовая техника";
//            //category1.Alias = Translit.RuToTranslite(category1.Name);
//            //category1.Parent = root;
//            //category1.GoodHear = false;
//            //category1.Image = 1;
//            //db.CategorySet.Add(category1);

//            //Category category1v1 = new Category();
//            //category1v1.Name = "Холодильники";
//            //category1v1.Description = "Описание категории Холодильники";
//            //category1v1.Alias = Translit.RuToTranslite(category1v1.Name);
//            //category1v1.Parent = category1;
//            //category1v1.GoodHear = true;
//            //category1v1.Image = 2;
//            //db.CategorySet.Add(category1v1);

//            //Category category1v2 = new Category();
//            //category1v2.Name = "Стиральные машины";
//            //category1v2.Description = "Описание категории Стиральные машины";
//            //category1v2.Alias = Translit.RuToTranslite(category1v2.Name);
//            //category1v2.Parent = category1;
//            //category1v2.GoodHear = true;
//            //category1v2.Image = 3;
//            //db.CategorySet.Add(category1v2);

//            //Category category1v3 = new Category();
//            //category1v3.Name = "Духовые шкафы";
//            //category1v3.Description = "Описание категории Духовые шкафы";
//            //category1v3.Alias = Translit.RuToTranslite(category1v3.Name);
//            //category1v3.Parent = category1;
//            //category1v3.GoodHear = true;
//            //category1v3.Image = 4;
//            //db.CategorySet.Add(category1v3);

//            //Category category2 = new Category();
//            //category2.Name = "Электроника";
//            //category2.Description = "Описание категории Электроника";
//            //category2.Alias = Translit.RuToTranslite(category2.Name);
//            //category2.Parent = root;
//            //category2.GoodHear = false;
//            //category2.Image = 5;
//            //db.CategorySet.Add(category2);

//            //Category category3 = new Category();
//            //category3.Name = "Компьютерная техника";
//            //category3.Description = "Описание категории Компьютерная техника";
//            //category3.Alias = Translit.RuToTranslite(category3.Name);
//            //category3.Parent = root;
//            //category3.GoodHear = false;
//            //category3.Image = 6;
//            //db.CategorySet.Add(category3);

//            //Category category4 = new Category();
//            //category4.Name = "Детские товары";
//            //category4.Description = "Описание категории Детские товары";
//            //category4.Alias = Translit.RuToTranslite(category4.Name);
//            //category4.Parent = root;
//            //category4.GoodHear = false;
//            //category4.Image = 7;
//            //db.CategorySet.Add(category4);

//            //Category category5 = new Category();
//            //category5.Name = "Красоты и здоровье";
//            //category5.Description = "Описание категории Красоты и здоровье";
//            //category5.Alias = Translit.RuToTranslite(category5.Name);
//            //category5.Parent = root;
//            //category5.GoodHear = false;
//            //category5.Image = 8;
//            //db.CategorySet.Add(category5);

//            Category root = new Category();
//            root.Name = "root";
//            root.Description = string.Empty;
//            root.Alias = Translit.RuToTranslite(root.Name);
//            root.Parent = null;
//            db.CategorySet.Add(root);

//            Category category1 = new Category();
//            category1.Name = "Замки и фурнитура";
//            category1.Description = "Описание категории Замки и фурнитура";
//            category1.Alias = Translit.RuToTranslite(category1.Name);
//            category1.Parent = root;
//            category1.GoodHear = false;
//            category1.Image = 1;
//            db.CategorySet.Add(category1);

//            Category category1v1 = new Category();
//            category1v1.Name = "Врезной замок";
//            category1v1.Description = "Описание категории Врезной замок";
//            category1v1.Alias = Translit.RuToTranslite(category1v1.Name);
//            category1v1.Parent = category1;
//            category1v1.GoodHear = true;
//            category1v1.Image = 2;
//            db.CategorySet.Add(category1v1);

//            Category category1v2 = new Category();
//            category1v2.Name = "Накладной замок";
//            category1v2.Description = "Описание категории Накладной замок";
//            category1v2.Alias = Translit.RuToTranslite(category1v2.Name);
//            category1v2.Parent = category1;
//            category1v2.GoodHear = true;
//            category1v2.Image = 3;
//            db.CategorySet.Add(category1v2);

//            Category category1v3 = new Category();
//            category1v3.Name = "Личинка для замка";
//            category1v3.Description = "Описание категории Личинка для замка";
//            category1v3.Alias = Translit.RuToTranslite(category1v3.Name);
//            category1v3.Parent = category1;
//            category1v3.GoodHear = true;
//            category1v3.Image = 4;
//            db.CategorySet.Add(category1v3);

//            Category category2 = new Category();
//            category2.Name = "Лакокрасочные материалы";
//            category2.Description = "Описание категории Лако-красочные материалы";
//            category2.Alias = Translit.RuToTranslite(category2.Name);
//            category2.Parent = root;
//            category2.GoodHear = false;
//            category2.Image = 5;
//            db.CategorySet.Add(category2);

//            Category category3 = new Category();
//            category3.Name = "Напольные покрытия";
//            category3.Description = "Описание категории Напольные покрытия";
//            category3.Alias = Translit.RuToTranslite(category3.Name);
//            category3.Parent = root;
//            category3.GoodHear = false;
//            category3.Image = 6;
//            db.CategorySet.Add(category3);

//            Category category4 = new Category();
//            category4.Name = "Двери";
//            category4.Description = "Описание категории Двери";
//            category4.Alias = Translit.RuToTranslite(category4.Name);
//            category4.Parent = root;
//            category4.GoodHear = false;
//            category4.Image = 7;
//            db.CategorySet.Add(category4);

//            Category category5 = new Category();
//            category5.Name = "Обои";
//            category5.Description = "Описание категории Обои";
//            category5.Alias = Translit.RuToTranslite(category5.Name);
//            category5.Parent = root;
//            category5.GoodHear = false;
//            category5.Image = 8;
//            db.CategorySet.Add(category5);

//            db.SaveChanges();
//        }

//        [TestMethod]
//        public void n03PropertyTypeAdd()
//        {
//            PropertyType propertyType1 = new PropertyType();
//            propertyType1.Name = "Сантиметры";
//            propertyType1.Description = "Метрическая величина";
//            propertyType1.ValueFormat = "{0}см";
//            propertyType1.FilterType="interval";
//            db.PropertyTypeSet.Add(propertyType1);

//            PropertyType propertyType2 = new PropertyType();
//            propertyType2.Name = "Килолграмм";
//            propertyType2.Description = "Вес в килограммах";
//            propertyType2.ValueFormat = "{0}кг";
//            propertyType2.FilterType = "interval";
//            db.PropertyTypeSet.Add(propertyType2);

//            PropertyType propertyType3 = new PropertyType();
//            propertyType3.Name = "Значение";
//            propertyType3.Description = string.Empty;
//            propertyType3.ValueFormat = "{0}";
//            propertyType3.FilterType = "list";
//            db.PropertyTypeSet.Add(propertyType3);

//            db.SaveChanges();
//        }

//        [TestMethod]
//        public void n04PropertyGroupAdd()
//        {
//            PropertyGroup propertyGroup1 = new PropertyGroup();
//            propertyGroup1.Name = "Размер и вес";
//            propertyGroup1.Description = "Массово-габоритные характеристики";
//            db.PropertyGroupSet.Add(propertyGroup1);

//            PropertyGroup propertyGroup2 = new PropertyGroup();
//            propertyGroup2.Name = "Безопасность";
//            propertyGroup2.Description = "Данные группы свойств отвечают за функциональные качества товара";
//            db.PropertyGroupSet.Add(propertyGroup2);

//            PropertyGroup propertyGroup3 = new PropertyGroup();
//            propertyGroup3.Name = "Специальные";
//            propertyGroup3.Description = "";
//            db.PropertyGroupSet.Add(propertyGroup3);

//            PropertyGroup propertyGroup4 = new PropertyGroup();
//            propertyGroup4.Name = "Визуальные";
//            propertyGroup4.Description = "";
//            db.PropertyGroupSet.Add(propertyGroup4);

//            db.SaveChanges();
//        }

//        [TestMethod]
//        public void n05PropertyAdd()
//        {
//            Property property1 = new Property();
//            property1.Name = "Ширина";
//            property1.Description = "Ширина изделия";
//            property1.PropertyType = db.PropertyTypeSet.Where(x => x.Name == "Сантиметры").FirstOrDefault();
//            property1.PropertyGroup = db.PropertyGroupSet.Where(x => x.Name == "Размер и вес").FirstOrDefault();
//            db.PropertySet.Add(property1);

//            Property property2 = new Property();
//            property2.Name = "Высота";
//            property2.Description = "Высота изделия";
//            property2.PropertyType = db.PropertyTypeSet.Where(x => x.Name == "Сантиметры").FirstOrDefault();
//            property2.PropertyGroup = db.PropertyGroupSet.Where(x => x.Name == "Размер и вес").FirstOrDefault();
//            db.PropertySet.Add(property2);

//            Property property3 = new Property();
//            property3.Name = "Глубина";
//            property3.Description = "Глубина изделия";
//            property3.PropertyType = db.PropertyTypeSet.Where(x => x.Name == "Сантиметры").FirstOrDefault();
//            property3.PropertyGroup = db.PropertyGroupSet.Where(x => x.Name == "Размер и вес").FirstOrDefault();
//            db.PropertySet.Add(property3);

//            Property property4 = new Property();
//            property4.Name = "Вес";
//            property4.Description = "Вес изделия";
//            property4.PropertyType = db.PropertyTypeSet.Where(x => x.Name == "Килолграмм").FirstOrDefault();
//            property4.PropertyGroup = db.PropertyGroupSet.Where(x => x.Name == "Размер и вес").FirstOrDefault();
//            db.PropertySet.Add(property4);

//            //Property property5 = new Property();
//            //property5.Name = "Цвет";
//            //property5.Description = "Цвет изделия";
//            //property5.PropertyType = db.PropertyTypeSet.Where(x => x.Name == "Значение").FirstOrDefault();
//            //property5.PropertyGroup = db.PropertyGroupSet.Where(x => x.Name == "Внещний вид").FirstOrDefault();
//            //db.PropertySet.Add(property5);

//            Property property6 = new Property();
//            property6.Name = "Тип механизма секретности";
//            property6.Description = "";
//            property6.PropertyType = db.PropertyTypeSet.Where(x => x.Name == "Значение").FirstOrDefault();
//            property6.PropertyGroup = db.PropertyGroupSet.Where(x => x.Name == "Безопасность").FirstOrDefault();
//            db.PropertySet.Add(property6);

//            Property property7 = new Property();
//            property7.Name = "Тип цилиндрового механизма";
//            property7.Description = "";
//            property7.PropertyType = db.PropertyTypeSet.Where(x => x.Name == "Значение").FirstOrDefault();
//            property7.PropertyGroup = db.PropertyGroupSet.Where(x => x.Name == "Специальные").FirstOrDefault();
//            db.PropertySet.Add(property7);

//            Property property8 = new Property();
//            property8.Name = "Типоразмер цилиндра";
//            property8.Description = "";
//            property8.PropertyType = db.PropertyTypeSet.Where(x => x.Name == "Значение").FirstOrDefault();
//            property8.PropertyGroup = db.PropertyGroupSet.Where(x => x.Name == "Специальные").FirstOrDefault();
//            db.PropertySet.Add(property8);

//            Property property9 = new Property();
//            property9.Name = "Количество пинов";
//            property9.Description = "";
//            property9.PropertyType = db.PropertyTypeSet.Where(x => x.Name == "Значение").FirstOrDefault();
//            property9.PropertyGroup = db.PropertyGroupSet.Where(x => x.Name == "Безопасность").FirstOrDefault();
//            db.PropertySet.Add(property9);

//            Property property10 = new Property();
//            property10.Name = "Тип ключа";
//            property10.Description = "";
//            property10.PropertyType = db.PropertyTypeSet.Where(x => x.Name == "Значение").FirstOrDefault();
//            property10.PropertyGroup = db.PropertyGroupSet.Where(x => x.Name == "Безопасность").FirstOrDefault();
//            db.PropertySet.Add(property10);

//            Property property11 = new Property();
//            property11.Name = "Возможность перекодировки";
//            property11.Description = "";
//            property11.PropertyType = db.PropertyTypeSet.Where(x => x.Name == "Значение").FirstOrDefault();
//            property11.PropertyGroup = db.PropertyGroupSet.Where(x => x.Name == "Специальные").FirstOrDefault();
//            db.PropertySet.Add(property11);

//            Property property12 = new Property();
//            property12.Name = "Механизм постоянного ключа";
//            property12.Description = "";
//            property12.PropertyType = db.PropertyTypeSet.Where(x => x.Name == "Значение").FirstOrDefault();
//            property12.PropertyGroup = db.PropertyGroupSet.Where(x => x.Name == "Специальные").FirstOrDefault();
//            db.PropertySet.Add(property12);

//            Property property13 = new Property();
//            property13.Name = "Материал цилиндра";
//            property13.Description = "";
//            property13.PropertyType = db.PropertyTypeSet.Where(x => x.Name == "Значение").FirstOrDefault();
//            property13.PropertyGroup = db.PropertyGroupSet.Where(x => x.Name == "Визуальные").FirstOrDefault();
//            db.PropertySet.Add(property13);

//            Property property14 = new Property();
//            property14.Name = "Материал сердечника";
//            property14.Description = "";
//            property14.PropertyType = db.PropertyTypeSet.Where(x => x.Name == "Значение").FirstOrDefault();
//            property14.PropertyGroup = db.PropertyGroupSet.Where(x => x.Name == "Визуальные").FirstOrDefault();
//            db.PropertySet.Add(property14);

//            Property property15= new Property();
//            property15.Name = "Покрытие цилиндра";
//            property15.Description = "";
//            property15.PropertyType = db.PropertyTypeSet.Where(x => x.Name == "Значение").FirstOrDefault();
//            property15.PropertyGroup = db.PropertyGroupSet.Where(x => x.Name == "Визуальные").FirstOrDefault();
//            db.PropertySet.Add(property15);

//            db.SaveChanges();
//        }

//        [TestMethod]
//        public void n06PropertyCategoryAdd()
//        {
//            PropertyCategory propertyCategory1 = new PropertyCategory();
//            propertyCategory1.Category = db.CategorySet.Where(x => x.Name == "Личинка для замка").FirstOrDefault();
//            propertyCategory1.Index = 1;
//            propertyCategory1.Property = db.PropertySet.Where(x => x.Name == "Ширина").FirstOrDefault();
//            db.PropertyCategorySet.Add(propertyCategory1);

//            PropertyCategory propertyCategory2 = new PropertyCategory();
//            propertyCategory2.Category = db.CategorySet.Where(x => x.Name == "Личинка для замка").FirstOrDefault();
//            propertyCategory2.Index = 2;
//            propertyCategory2.Property = db.PropertySet.Where(x => x.Name == "Высота").FirstOrDefault();
//            db.PropertyCategorySet.Add(propertyCategory2);

//            PropertyCategory propertyCategory3 = new PropertyCategory();
//            propertyCategory3.Category = db.CategorySet.Where(x => x.Name == "Личинка для замка").FirstOrDefault();
//            propertyCategory3.Index = 3;
//            propertyCategory3.Property = db.PropertySet.Where(x => x.Name == "Глубина").FirstOrDefault();
//            db.PropertyCategorySet.Add(propertyCategory3);

//            PropertyCategory propertyCategory4 = new PropertyCategory();
//            propertyCategory4.Category = db.CategorySet.Where(x => x.Name == "Личинка для замка").FirstOrDefault();
//            propertyCategory4.Index = 4;
//            propertyCategory4.Property = db.PropertySet.Where(x => x.Name == "Вес").FirstOrDefault();
//            db.PropertyCategorySet.Add(propertyCategory4);

//            PropertyCategory propertyCategory5 = new PropertyCategory();
//            propertyCategory5.Category = db.CategorySet.Where(x => x.Name == "Личинка для замка").FirstOrDefault();
//            propertyCategory5.Index = 5;
//            propertyCategory5.Property = db.PropertySet.Where(x => x.Name == "Тип механизма секретности").FirstOrDefault();
//            db.PropertyCategorySet.Add(propertyCategory5);

//            PropertyCategory propertyCategory6 = new PropertyCategory();
//            propertyCategory6.Category = db.CategorySet.Where(x => x.Name == "Личинка для замка").FirstOrDefault();
//            propertyCategory6.Index = 5;
//            propertyCategory6.Property = db.PropertySet.Where(x => x.Name == "Тип цилиндрового механизма").FirstOrDefault();
//            db.PropertyCategorySet.Add(propertyCategory6);

//            PropertyCategory propertyCategory7 = new PropertyCategory();
//            propertyCategory7.Category = db.CategorySet.Where(x => x.Name == "Личинка для замка").FirstOrDefault();
//            propertyCategory7.Index = 5;
//            propertyCategory7.Property = db.PropertySet.Where(x => x.Name == "Типоразмер цилиндра").FirstOrDefault();
//            db.PropertyCategorySet.Add(propertyCategory7);

//            PropertyCategory propertyCategory8 = new PropertyCategory();
//            propertyCategory8.Category = db.CategorySet.Where(x => x.Name == "Личинка для замка").FirstOrDefault();
//            propertyCategory8.Index = 5;
//            propertyCategory8.Property = db.PropertySet.Where(x => x.Name == "Количество пинов").FirstOrDefault();
//            db.PropertyCategorySet.Add(propertyCategory8);

//            PropertyCategory propertyCategory9 = new PropertyCategory();
//            propertyCategory9.Category = db.CategorySet.Where(x => x.Name == "Личинка для замка").FirstOrDefault();
//            propertyCategory9.Index = 5;
//            propertyCategory9.Property = db.PropertySet.Where(x => x.Name == "Тип ключа").FirstOrDefault();
//            db.PropertyCategorySet.Add(propertyCategory9);

//            PropertyCategory propertyCategory10 = new PropertyCategory();
//            propertyCategory10.Category = db.CategorySet.Where(x => x.Name == "Личинка для замка").FirstOrDefault();
//            propertyCategory10.Index = 5;
//            propertyCategory10.Property = db.PropertySet.Where(x => x.Name == "Возможность перекодировки").FirstOrDefault();
//            db.PropertyCategorySet.Add(propertyCategory10);

//            PropertyCategory propertyCategory11 = new PropertyCategory();
//            propertyCategory11.Category = db.CategorySet.Where(x => x.Name == "Личинка для замка").FirstOrDefault();
//            propertyCategory11.Index = 5;
//            propertyCategory11.Property = db.PropertySet.Where(x => x.Name == "Механизм постоянного ключа").FirstOrDefault();
//            db.PropertyCategorySet.Add(propertyCategory11);

//            PropertyCategory propertyCategory12 = new PropertyCategory();
//            propertyCategory12.Category = db.CategorySet.Where(x => x.Name == "Личинка для замка").FirstOrDefault();
//            propertyCategory12.Index = 5;
//            propertyCategory12.Property = db.PropertySet.Where(x => x.Name == "Материал цилиндра").FirstOrDefault();
//            db.PropertyCategorySet.Add(propertyCategory12);

//            PropertyCategory propertyCategory13 = new PropertyCategory();
//            propertyCategory13.Category = db.CategorySet.Where(x => x.Name == "Личинка для замка").FirstOrDefault();
//            propertyCategory13.Index = 5;
//            propertyCategory13.Property = db.PropertySet.Where(x => x.Name == "Материал сердечника").FirstOrDefault();
//            db.PropertyCategorySet.Add(propertyCategory13);

//            PropertyCategory propertyCategory14 = new PropertyCategory();
//            propertyCategory14.Category = db.CategorySet.Where(x => x.Name == "Личинка для замка").FirstOrDefault();
//            propertyCategory14.Index = 5;
//            propertyCategory14.Property = db.PropertySet.Where(x => x.Name == "Покрытие цилиндра").FirstOrDefault();
//            db.PropertyCategorySet.Add(propertyCategory14);

//            db.SaveChanges();
//        }

//        [TestMethod]
//        public void n07GoodAdd()
//        {
//            Good good1 = new Good();
//            good1.Name = "GA-B489";
//            good1.Alias = Translit.RuToTranslite(good1.Name);
//            good1.Manufacturer = db.ManufacturerSet.Where(x => x.Name == "LG").FirstOrDefault();
//            good1.Description = "Красивое описание";
//            good1.Category = db.CategorySet.Where(x => x.Name == "Холодильники").FirstOrDefault();
//            db.GoodSet.Add(good1);

//            //Good good2 = new Good();
//            //good2.Name = "RL-57";
//            //good2.Alias = Translit.RuToTranslite(good2.Name);
//            //good2.Manufacturer = db.ManufacturerSet.Where(x => x.Name == "Samsung").FirstOrDefault();
//            //good2.Description = "Красивое описание";
//            //good2.Category = db.CategorySet.Where(x => x.Name == "Холодильники").FirstOrDefault();
//            //db.GoodSet.Add(good2);

//            //Good good3 = new Good();
//            //good3.Name = "RL-52";
//            //good3.Alias = Translit.RuToTranslite(good3.Name);
//            //good3.Manufacturer = db.ManufacturerSet.Where(x => x.Name == "Samsung").FirstOrDefault();
//            //good3.Description = "Красивое описание";
//            //good3.Category = db.CategorySet.Where(x => x.Name == "Холодильники").FirstOrDefault();
//            //db.GoodSet.Add(good3);

//            db.SaveChanges();
//        }

//        [TestMethod]
//        public void n08ContactGroupAdd()
//        {
//            ContactGroup contactGroup1 = new ContactGroup();
//            db.ContactGroupSet.Add(contactGroup1);

//            ContactGroup contactGroup2 = new ContactGroup();
//            db.ContactGroupSet.Add(contactGroup2);

//            ContactGroup contactGroup3 = new ContactGroup();
//            db.ContactGroupSet.Add(contactGroup3);

//            ContactGroup contactGroup4 = new ContactGroup();
//            db.ContactGroupSet.Add(contactGroup4);

//            ContactGroup contactGroup5 = new ContactGroup();
//            db.ContactGroupSet.Add(contactGroup5);

//            ContactGroup contactGroup6 = new ContactGroup();
//            db.ContactGroupSet.Add(contactGroup6);

//            db.SaveChanges();
//        }

//        [TestMethod]
//        public void n09PhoneAdd()
//        {
//            Phone phone1 = new Phone();
//            phone1.Number = "+78888888801";
//            phone1.Description = "Номер в оффисе";
//            phone1.ContactGroup = db.ContactGroupSet.Where(x => x.Id == 1).FirstOrDefault();
//            db.PhoneSet.Add(phone1);

//            Phone phone2 = new Phone();
//            phone2.Number = "+78888888802";
//            phone2.Description = "Номер магазина #1 торговая зона";
//            phone2.ContactGroup = db.ContactGroupSet.Where(x => x.Id == 2).FirstOrDefault();
//            db.PhoneSet.Add(phone2);

//            Phone phone3 = new Phone();
//            phone3.Number = "+78888888803";
//            phone3.Description = "Номер в оффисе #1 сервис";
//            phone3.ContactGroup = db.ContactGroupSet.Where(x => x.Id == 2).FirstOrDefault();
//            db.PhoneSet.Add(phone3);

//            Phone phone4 = new Phone();
//            phone4.Number = "+78888888804";
//            phone4.Description = "Номер магазина #2";
//            phone4.ContactGroup = db.ContactGroupSet.Where(x => x.Id == 3).FirstOrDefault();
//            db.PhoneSet.Add(phone4);

//            Phone phone5 = new Phone();
//            phone5.Number = "+78888888805";
//            phone5.Description = "Отдел закупок";
//            phone5.ContactGroup = db.ContactGroupSet.Where(x => x.Id == 5).FirstOrDefault();
//            db.PhoneSet.Add(phone5);

//            Phone phone6 = new Phone();
//            phone6.Number = "+78888888806";
//            phone6.Description = "Магазин";
//            phone6.ContactGroup = db.ContactGroupSet.Where(x => x.Id == 6).FirstOrDefault();
//            db.PhoneSet.Add(phone6);

//            db.SaveChanges();
//        }

//        [TestMethod]
//        public void n10AddressAdd()
//        {
//            Address address1 = new Address() { Index = "000000", Country = "Россия", Region = "Московская область", City = "Москва", Street = "Ленина", Building = "1", Room = "11" };
//            address1.Description = "Управляющий оффис";
//            address1.ContactGroup = db.ContactGroupSet.Where(x => x.Id == 1).FirstOrDefault();
//            db.AddressSet.Add(address1);

//            Address address2 = new Address() { Index = "000000", Country = "Россия", Region = "Московская область", City = "Москва", Street = "Пушкина", Building = "2", Room = "12" };
//            address2.Description = "На втором этаже";
//            address2.ContactGroup = db.ContactGroupSet.Where(x => x.Id == 2).FirstOrDefault();
//            db.AddressSet.Add(address2);

//            Address address3 = new Address() { Index = "000000", Country = "Россия", Region = "Московская область", City = "Москва", Street = "Свободы", Building = "3", Room = "13" };
//            address3.Description = "Вход с торца дома";
//            address3.ContactGroup = db.ContactGroupSet.Where(x => x.Id == 3).FirstOrDefault();
//            db.AddressSet.Add(address3);

//            Address address4 = new Address() { Index = "000000", Country = "Россия", Region = "Московская область", City = "Москва", Street = "Железнодорожная", Building = "4", Room = "14" };
//            address4.Description = "";
//            address4.ContactGroup = db.ContactGroupSet.Where(x => x.Id == 5).FirstOrDefault();
//            db.AddressSet.Add(address4);

//            Address address5 = new Address() { Index = "000000", Country = "Россия", Region = "Московская область", City = "Москва", Street = "Большая", Building = "5", Room = "15" };
//            address5.Description = "";
//            address5.ContactGroup = db.ContactGroupSet.Where(x => x.Id == 6).FirstOrDefault();
//            db.AddressSet.Add(address5);

//            db.SaveChanges();
//        }

//        [TestMethod]
//        public void n11OrganizationAdd()
//        {
//            Organization organization1 = new Organization();
//            organization1.Name = "ИП Иванов Иван Иванович";
//            organization1.Description = "Описание 1";
//            organization1.ContactGroup = db.ContactGroupSet.Where(x => x.Id == 1).FirstOrDefault();
//            db.OrganizationSet.Add(organization1);

//            Organization organization2 = new Organization();
//            organization2.Name = "ИП Петров Петр Петрович";
//            organization2.Description = "Описание 2";
//            organization2.ContactGroup = db.ContactGroupSet.Where(x => x.Id == 4).FirstOrDefault();
//            db.OrganizationSet.Add(organization2);

//            db.SaveChanges();
//        }

//        [TestMethod]
//        public void n12OrganizationUnitAdd()
//        {
//            OrganizationUnit organizationUnit1 = new OrganizationUnit();
//            organizationUnit1.Name = "Электроника";
//            organizationUnit1.Description = "Классный магазин";
//            organizationUnit1.PayerDate = DateTime.Now;
//            organizationUnit1.Organization = db.OrganizationSet.Where(x => x.Name == "ИП Иванов Иван Иванович").FirstOrDefault();
//            organizationUnit1.ContactGroup = db.ContactGroupSet.Where(x => x.Id == 2).FirstOrDefault();
//            db.OrganizationUnitSet.Add(organizationUnit1);

//            OrganizationUnit organizationUnit2 = new OrganizationUnit();
//            organizationUnit2.Name = "WHi-Fi";
//            organizationUnit2.Description = "";
//            organizationUnit2.PayerDate = DateTime.Now;
//            organizationUnit2.Organization = db.OrganizationSet.Where(x => x.Name == "ИП Иванов Иван Иванович").FirstOrDefault();
//            organizationUnit2.ContactGroup = db.ContactGroupSet.Where(x => x.Id == 3).FirstOrDefault();
//            db.OrganizationUnitSet.Add(organizationUnit2);

//            OrganizationUnit organizationUnit3 = new OrganizationUnit();
//            organizationUnit3.Name = "Техникс";
//            organizationUnit3.Description = "";
//            organizationUnit3.PayerDate = DateTime.Now;
//            organizationUnit3.Organization = db.OrganizationSet.Where(x => x.Name == "ИП Петров Петр Петрович").FirstOrDefault();
//            organizationUnit3.ContactGroup = db.ContactGroupSet.Where(x => x.Id == 5).FirstOrDefault();
//            db.OrganizationUnitSet.Add(organizationUnit3);

//            OrganizationUnit organizationUnit4 = new OrganizationUnit();
//            organizationUnit4.Name = "Техникс";
//            organizationUnit4.Description = "";
//            organizationUnit4.PayerDate = DateTime.Now;
//            organizationUnit4.Organization = db.OrganizationSet.Where(x => x.Name == "ИП Петров Петр Петрович").FirstOrDefault();
//            organizationUnit4.ContactGroup = db.ContactGroupSet.Where(x => x.Id == 6).FirstOrDefault();
//            db.OrganizationUnitSet.Add(organizationUnit4);

//            db.SaveChanges();
//        }

//        [TestMethod]
//        public void n13ModificationGoodAdd()
//        {
//            ModificationGood modificationGood1 = new ModificationGood();
//            modificationGood1.Good = db.GoodSet.Where(x => x.Name == "GA-B489").FirstOrDefault();
//            db.ModificationGoodSet.Add(modificationGood1);

//            ModificationGood modificationGood2 = new ModificationGood();
//            modificationGood2.Good = db.GoodSet.Where(x => x.Name == "GA-B489").FirstOrDefault();
//            db.ModificationGoodSet.Add(modificationGood2);

//            ModificationGood modificationGood3 = new ModificationGood();
//            modificationGood3.Good = db.GoodSet.Where(x => x.Name == "RL-57").FirstOrDefault();
//            db.ModificationGoodSet.Add(modificationGood3);

//            ModificationGood modificationGood4 = new ModificationGood();
//            modificationGood4.Good = db.GoodSet.Where(x => x.Name == "RL-57").FirstOrDefault();
//            db.ModificationGoodSet.Add(modificationGood4);

//            ModificationGood modificationGood5 = new ModificationGood();
//            modificationGood5.Good = db.GoodSet.Where(x => x.Name == "RL-52").FirstOrDefault();
//            db.ModificationGoodSet.Add(modificationGood5);

//            db.SaveChanges();
//        }

//        [TestMethod]
//        public void n14PropertyValueAdd()
//        {
//            PropertyValue propertyValue1 = new PropertyValue();
//            propertyValue1.Property = db.PropertySet.Where(x => x.Name == "Ширина").FirstOrDefault();
//            propertyValue1.Value = "59.5";
//            db.PropertyValueSet.Add(propertyValue1);

//            PropertyValue propertyValue2 = new PropertyValue();
//            propertyValue2.Property = db.PropertySet.Where(x => x.Name == "Высота").FirstOrDefault();
//            propertyValue2.Value = "200";
//            db.PropertyValueSet.Add(propertyValue2);

//            PropertyValue propertyValue3 = new PropertyValue();
//            propertyValue3.Property = db.PropertySet.Where(x => x.Name == "Глубина").FirstOrDefault();
//            propertyValue3.Value = "68.8";
//            db.PropertyValueSet.Add(propertyValue3);

//            PropertyValue propertyValue4 = new PropertyValue();
//            propertyValue4.Property = db.PropertySet.Where(x => x.Name == "Вес").FirstOrDefault();
//            propertyValue4.Value = "70.8";
//            db.PropertyValueSet.Add(propertyValue4);

//            PropertyValue propertyValue5 = new PropertyValue();
//            propertyValue5.Property = db.PropertySet.Where(x => x.Name == "Цвет").FirstOrDefault();
//            propertyValue5.Value = "золотистый";
//            db.PropertyValueSet.Add(propertyValue5);

//            PropertyValue propertyValue6 = new PropertyValue();
//            propertyValue6.Property = db.PropertySet.Where(x => x.Name == "Цвет").FirstOrDefault();
//            propertyValue6.Value = "серый";
//            db.PropertyValueSet.Add(propertyValue6);

//            //-----------------------------------------------------------

//            PropertyValue propertyValue7 = new PropertyValue();
//            propertyValue7.Property = db.PropertySet.Where(x => x.Name == "Ширина").FirstOrDefault();
//            propertyValue7.Value = "60";
//            db.PropertyValueSet.Add(propertyValue7);

//            //PropertyValue propertyValue8 = new PropertyValue();
//            //propertyValue8.Property = db.PropertySet.Where(x => x.Name == "Высота").FirstOrDefault();
//            //propertyValue8.Value = "200";
//            //db.PropertyValueSet.Add(propertyValue8);

//            PropertyValue propertyValue9 = new PropertyValue();
//            propertyValue9.Property = db.PropertySet.Where(x => x.Name == "Глубина").FirstOrDefault();
//            propertyValue9.Value = "65";
//            db.PropertyValueSet.Add(propertyValue9);

//            PropertyValue propertyValue10 = new PropertyValue();
//            propertyValue10.Property = db.PropertySet.Where(x => x.Name == "Вес").FirstOrDefault();
//            propertyValue10.Value = "88";
//            db.PropertyValueSet.Add(propertyValue10);

//            PropertyValue propertyValue11 = new PropertyValue();
//            propertyValue11.Property = db.PropertySet.Where(x => x.Name == "Цвет").FirstOrDefault();
//            propertyValue11.Value = "чёрный";
//            db.PropertyValueSet.Add(propertyValue11);

//            PropertyValue propertyValue12 = new PropertyValue();
//            propertyValue12.Property = db.PropertySet.Where(x => x.Name == "Цвет").FirstOrDefault();
//            propertyValue12.Value = "серебристый";
//            db.PropertyValueSet.Add(propertyValue12);

//            //-----------------------------------------------------------

//            //PropertyValue propertyValue13 = new PropertyValue();
//            //propertyValue13.Property = db.PropertySet.Where(x => x.Name == "Ширина").FirstOrDefault();
//            //propertyValue13.Value = "60";
//            //db.PropertyValueSet.Add(propertyValue13);

//            PropertyValue propertyValue14 = new PropertyValue();
//            propertyValue14.Property = db.PropertySet.Where(x => x.Name == "Высота").FirstOrDefault();
//            propertyValue14.Value = "192";
//            db.PropertyValueSet.Add(propertyValue14);

//            PropertyValue propertyValue15 = new PropertyValue();
//            propertyValue15.Property = db.PropertySet.Where(x => x.Name == "Глубина").FirstOrDefault();
//            propertyValue15.Value = "64.6";
//            db.PropertyValueSet.Add(propertyValue15);

//            PropertyValue propertyValue16 = new PropertyValue();
//            propertyValue16.Property = db.PropertySet.Where(x => x.Name == "Вес").FirstOrDefault();
//            propertyValue16.Value = "71";
//            db.PropertyValueSet.Add(propertyValue16);

//            //PropertyValue propertyValue17 = new PropertyValue();
//            //propertyValue17.Property = db.PropertySet.Where(x => x.Name == "Цвет").FirstOrDefault();
//            //propertyValue17.Value = "серебристый";
//            //db.PropertyValueSet.Add(propertyValue17);


//            db.SaveChanges();
//        }

//        [TestMethod]
//        public void n15PropertyValueGroup()
//        {
//            PropertyValueGroup propertyValueGroup1 = new PropertyValueGroup();
//            propertyValueGroup1.ModificationGood = db.ModificationGoodSet.Where(x=>x.Id==1).FirstOrDefault();
//            propertyValueGroup1.PropertyValue = db.PropertyValueSet.Where(x => x.Value == "59.5").FirstOrDefault();
//            db.PropertyValueGroupSet.Add(propertyValueGroup1);

//            PropertyValueGroup propertyValueGroup2 = new PropertyValueGroup();
//            propertyValueGroup2.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 1).FirstOrDefault();
//            propertyValueGroup2.PropertyValue = db.PropertyValueSet.Where(x => x.Value == "200").FirstOrDefault();
//            db.PropertyValueGroupSet.Add(propertyValueGroup2);

//            PropertyValueGroup propertyValueGroup3 = new PropertyValueGroup();
//            propertyValueGroup3.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 1).FirstOrDefault();
//            propertyValueGroup3.PropertyValue = db.PropertyValueSet.Where(x => x.Value == "68.8").FirstOrDefault();
//            db.PropertyValueGroupSet.Add(propertyValueGroup3);

//            PropertyValueGroup propertyValueGroup4 = new PropertyValueGroup();
//            propertyValueGroup4.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 1).FirstOrDefault();
//            propertyValueGroup4.PropertyValue = db.PropertyValueSet.Where(x => x.Value == "70.8").FirstOrDefault();
//            db.PropertyValueGroupSet.Add(propertyValueGroup4);

//            PropertyValueGroup propertyValueGroup5 = new PropertyValueGroup();
//            propertyValueGroup5.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 1).FirstOrDefault();
//            propertyValueGroup5.PropertyValue = db.PropertyValueSet.Where(x => x.Value == "золотистый").FirstOrDefault();
//            db.PropertyValueGroupSet.Add(propertyValueGroup5);

//            // ------------------------------------------------------------------

//            PropertyValueGroup propertyValueGroup6 = new PropertyValueGroup();
//            propertyValueGroup6.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 2).FirstOrDefault();
//            propertyValueGroup6.PropertyValue = db.PropertyValueSet.Where(x => x.Value == "59.5").FirstOrDefault();
//            db.PropertyValueGroupSet.Add(propertyValueGroup6);

//            PropertyValueGroup propertyValueGroup7 = new PropertyValueGroup();
//            propertyValueGroup7.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 2).FirstOrDefault();
//            propertyValueGroup7.PropertyValue = db.PropertyValueSet.Where(x => x.Value == "200").FirstOrDefault();
//            db.PropertyValueGroupSet.Add(propertyValueGroup7);

//            PropertyValueGroup propertyValueGroup8 = new PropertyValueGroup();
//            propertyValueGroup8.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 2).FirstOrDefault();
//            propertyValueGroup8.PropertyValue = db.PropertyValueSet.Where(x => x.Value == "68.8").FirstOrDefault();
//            db.PropertyValueGroupSet.Add(propertyValueGroup8);

//            PropertyValueGroup propertyValueGroup9 = new PropertyValueGroup();
//            propertyValueGroup9.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 2).FirstOrDefault();
//            propertyValueGroup9.PropertyValue = db.PropertyValueSet.Where(x => x.Value == "70.8").FirstOrDefault();
//            db.PropertyValueGroupSet.Add(propertyValueGroup9);

//            PropertyValueGroup propertyValueGroup10 = new PropertyValueGroup();
//            propertyValueGroup10.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 2).FirstOrDefault();
//            propertyValueGroup10.PropertyValue = db.PropertyValueSet.Where(x => x.Value == "серый").FirstOrDefault();
//            db.PropertyValueGroupSet.Add(propertyValueGroup10);

//            // ------------------------------------------------------------------

//            PropertyValueGroup propertyValueGroup11 = new PropertyValueGroup();
//            propertyValueGroup11.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 3).FirstOrDefault();
//            propertyValueGroup11.PropertyValue = db.PropertyValueSet.Where(x => x.Value == "60").FirstOrDefault();
//            db.PropertyValueGroupSet.Add(propertyValueGroup11);

//            PropertyValueGroup propertyValueGroup12 = new PropertyValueGroup();
//            propertyValueGroup12.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 3).FirstOrDefault();
//            propertyValueGroup12.PropertyValue = db.PropertyValueSet.Where(x => x.Value == "200").FirstOrDefault();
//            db.PropertyValueGroupSet.Add(propertyValueGroup12);

//            PropertyValueGroup propertyValueGroup13 = new PropertyValueGroup();
//            propertyValueGroup13.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 3).FirstOrDefault();
//            propertyValueGroup13.PropertyValue = db.PropertyValueSet.Where(x => x.Value == "65").FirstOrDefault();
//            db.PropertyValueGroupSet.Add(propertyValueGroup13);

//            PropertyValueGroup propertyValueGroup14 = new PropertyValueGroup();
//            propertyValueGroup14.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 3).FirstOrDefault();
//            propertyValueGroup14.PropertyValue = db.PropertyValueSet.Where(x => x.Value == "88").FirstOrDefault();
//            db.PropertyValueGroupSet.Add(propertyValueGroup14);

//            PropertyValueGroup propertyValueGroup15 = new PropertyValueGroup();
//            propertyValueGroup15.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 3).FirstOrDefault();
//            propertyValueGroup15.PropertyValue = db.PropertyValueSet.Where(x => x.Value == "чёрный").FirstOrDefault();
//            db.PropertyValueGroupSet.Add(propertyValueGroup15);

//            // ------------------------------------------------------------------

//            PropertyValueGroup propertyValueGroup16 = new PropertyValueGroup();
//            propertyValueGroup16.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 4).FirstOrDefault();
//            propertyValueGroup16.PropertyValue = db.PropertyValueSet.Where(x => x.Value == "60").FirstOrDefault();
//            db.PropertyValueGroupSet.Add(propertyValueGroup16);

//            PropertyValueGroup propertyValueGroup17 = new PropertyValueGroup();
//            propertyValueGroup17.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 4).FirstOrDefault();
//            propertyValueGroup17.PropertyValue = db.PropertyValueSet.Where(x => x.Value == "200").FirstOrDefault();
//            db.PropertyValueGroupSet.Add(propertyValueGroup17);

//            PropertyValueGroup propertyValueGroup18 = new PropertyValueGroup();
//            propertyValueGroup18.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 4).FirstOrDefault();
//            propertyValueGroup18.PropertyValue = db.PropertyValueSet.Where(x => x.Value == "65").FirstOrDefault();
//            db.PropertyValueGroupSet.Add(propertyValueGroup18);

//            PropertyValueGroup propertyValueGroup19 = new PropertyValueGroup();
//            propertyValueGroup19.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 4).FirstOrDefault();
//            propertyValueGroup19.PropertyValue = db.PropertyValueSet.Where(x => x.Value == "88").FirstOrDefault();
//            db.PropertyValueGroupSet.Add(propertyValueGroup19);

//            PropertyValueGroup propertyValueGroup20 = new PropertyValueGroup();
//            propertyValueGroup20.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 4).FirstOrDefault();
//            propertyValueGroup20.PropertyValue = db.PropertyValueSet.Where(x => x.Value == "серебристый").FirstOrDefault();
//            db.PropertyValueGroupSet.Add(propertyValueGroup20);

//            // ------------------------------------------------------------------

//            PropertyValueGroup propertyValueGroup21 = new PropertyValueGroup();
//            propertyValueGroup21.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 5).FirstOrDefault();
//            propertyValueGroup21.PropertyValue = db.PropertyValueSet.Where(x => x.Value == "60").FirstOrDefault();
//            db.PropertyValueGroupSet.Add(propertyValueGroup21);

//            PropertyValueGroup propertyValueGroup22 = new PropertyValueGroup();
//            propertyValueGroup22.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 5).FirstOrDefault();
//            propertyValueGroup22.PropertyValue = db.PropertyValueSet.Where(x => x.Value == "192").FirstOrDefault();
//            db.PropertyValueGroupSet.Add(propertyValueGroup22);

//            PropertyValueGroup propertyValueGroup23 = new PropertyValueGroup();
//            propertyValueGroup23.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 5).FirstOrDefault();
//            propertyValueGroup23.PropertyValue = db.PropertyValueSet.Where(x => x.Value == "64.6").FirstOrDefault();
//            db.PropertyValueGroupSet.Add(propertyValueGroup23);

//            PropertyValueGroup propertyValueGroup24 = new PropertyValueGroup();
//            propertyValueGroup24.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 5).FirstOrDefault();
//            propertyValueGroup24.PropertyValue = db.PropertyValueSet.Where(x => x.Value == "71").FirstOrDefault();
//            db.PropertyValueGroupSet.Add(propertyValueGroup24);

//            PropertyValueGroup propertyValueGroup25 = new PropertyValueGroup();
//            propertyValueGroup25.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 5).FirstOrDefault();
//            propertyValueGroup25.PropertyValue = db.PropertyValueSet.Where(x => x.Value == "серебристый").FirstOrDefault();
//            db.PropertyValueGroupSet.Add(propertyValueGroup25);

//            db.SaveChanges();
//        }

//        [TestMethod]
//        public void n16PriceAdd()
//        {
//            Price price1 = new Price();
//            price1.OrganizationUnit = db.OrganizationUnitSet.Where(x => x.Id == 1).FirstOrDefault();
//            price1.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 1).FirstOrDefault();
//            price1.Count = 15;
//            price1.Value = 49290;
//            db.PriceSet.Add(price1);

//            Price price2 = new Price();
//            price2.OrganizationUnit = db.OrganizationUnitSet.Where(x => x.Id == 1).FirstOrDefault();
//            price2.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 2).FirstOrDefault();
//            price2.Count = 50;
//            price2.Value = 41890;
//            db.PriceSet.Add(price2);

//            Price price3 = new Price();
//            price3.OrganizationUnit = db.OrganizationUnitSet.Where(x => x.Id == 1).FirstOrDefault();
//            price3.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 3).FirstOrDefault();
//            price3.Count = 3;
//            price3.Value = 63433;
//            db.PriceSet.Add(price3);

//            Price price4 = new Price();
//            price4.OrganizationUnit = db.OrganizationUnitSet.Where(x => x.Id == 1).FirstOrDefault();
//            price4.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 4).FirstOrDefault();
//            price4.Count = 5;
//            price4.Value = 53310;
//            db.PriceSet.Add(price4);

//            Price price5 = new Price();
//            price5.OrganizationUnit = db.OrganizationUnitSet.Where(x => x.Id == 1).FirstOrDefault();
//            price5.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 5).FirstOrDefault();
//            price5.Count = 8;
//            price5.Value = 54890;
//            db.PriceSet.Add(price5);

//            //-----------------------------------------------------------

//            Price price6 = new Price();
//            price6.OrganizationUnit = db.OrganizationUnitSet.Where(x => x.Id == 2).FirstOrDefault();
//            price6.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 1).FirstOrDefault();
//            price6.Count = 3;
//            price6.Value = 49290;
//            db.PriceSet.Add(price6);

//            Price price7 = new Price();
//            price7.OrganizationUnit = db.OrganizationUnitSet.Where(x => x.Id == 2).FirstOrDefault();
//            price7.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 2).FirstOrDefault();
//            price7.Count = 16;
//            price7.Value = 41890;
//            db.PriceSet.Add(price7);

//            Price price8 = new Price();
//            price8.OrganizationUnit = db.OrganizationUnitSet.Where(x => x.Id == 2).FirstOrDefault();
//            price8.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 3).FirstOrDefault();
//            price8.Count = 1;
//            price8.Value = 63433;
//            db.PriceSet.Add(price8);

//            Price price9 = new Price();
//            price9.OrganizationUnit = db.OrganizationUnitSet.Where(x => x.Id == 2).FirstOrDefault();
//            price9.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 4).FirstOrDefault();
//            price9.Count = 7;
//            price9.Value = 53310;
//            db.PriceSet.Add(price9);

//            Price price10 = new Price();
//            price10.OrganizationUnit = db.OrganizationUnitSet.Where(x => x.Id == 2).FirstOrDefault();
//            price10.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 5).FirstOrDefault();
//            price10.Count = 2;
//            price10.Value = 54890;
//            db.PriceSet.Add(price10);

//            //-----------------------------------------------------------

//            Price price11 = new Price();
//            price11.OrganizationUnit = db.OrganizationUnitSet.Where(x => x.Id == 3).FirstOrDefault();
//            price11.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 1).FirstOrDefault();
//            price11.Count = 5;
//            price11.Value = 21710;
//            db.PriceSet.Add(price11);

//            Price price12 = new Price();
//            price12.OrganizationUnit = db.OrganizationUnitSet.Where(x => x.Id == 3).FirstOrDefault();
//            price12.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 2).FirstOrDefault();
//            price12.Count = 4;
//            price12.Value = 38709;
//            db.PriceSet.Add(price12);

//            Price price13 = new Price();
//            price13.OrganizationUnit = db.OrganizationUnitSet.Where(x => x.Id == 3).FirstOrDefault();
//            price13.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 3).FirstOrDefault();
//            price13.Count = 6;
//            price13.Value = 66820;
//            db.PriceSet.Add(price8);

//            Price price14 = new Price();
//            price14.OrganizationUnit = db.OrganizationUnitSet.Where(x => x.Id == 4).FirstOrDefault();
//            price14.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 3).FirstOrDefault();
//            price14.Count = 2;
//            price14.Value = 72099;
//            db.PriceSet.Add(price14);

//            Price price15 = new Price();
//            price15.OrganizationUnit = db.OrganizationUnitSet.Where(x => x.Id == 4).FirstOrDefault();
//            price15.ModificationGood = db.ModificationGoodSet.Where(x => x.Id == 5).FirstOrDefault();
//            price15.Count = 3;
//            price15.Value = 43271;
//            db.PriceSet.Add(price15);
            
//            db.SaveChanges();
//        }

//        [TestMethod]
//        public void n17ImageGoodAdd()
//        {
//            ImageGood imageGood1 = new ImageGood();
//            imageGood1.Good = db.GoodSet.Where(x => x.Name == "GA-B489").FirstOrDefault();
//            imageGood1.Name = "1";
//            imageGood1.Index = 1;
//            db.ImageGoodSet.Add(imageGood1);

//            ImageGood imageGood2 = new ImageGood();
//            imageGood2.Good = db.GoodSet.Where(x => x.Name == "GA-B489").FirstOrDefault();
//            imageGood2.Name = "2";
//            imageGood2.Index = 2;
//            db.ImageGoodSet.Add(imageGood2);

//            ImageGood imageGood3 = new ImageGood();
//            imageGood3.Good = db.GoodSet.Where(x => x.Name == "GA-B489").FirstOrDefault();
//            imageGood3.Name = "3";
//            imageGood3.Index = 3;
//            db.ImageGoodSet.Add(imageGood3);

//            ImageGood imageGood4 = new ImageGood();
//            imageGood4.Good = db.GoodSet.Where(x => x.Name == "RL-57").FirstOrDefault();
//            imageGood4.Name = "4";
//            imageGood4.Index = 1;
//            db.ImageGoodSet.Add(imageGood4);

//            ImageGood imageGood5 = new ImageGood();
//            imageGood5.Good = db.GoodSet.Where(x => x.Name == "RL-57").FirstOrDefault();
//            imageGood5.Name = "5";
//            imageGood5.Index = 2;
//            db.ImageGoodSet.Add(imageGood5);

//            ImageGood imageGood6 = new ImageGood();
//            imageGood6.Good = db.GoodSet.Where(x => x.Name == "RL-57").FirstOrDefault();
//            imageGood6.Name = "6";
//            imageGood6.Index = 2;
//            db.ImageGoodSet.Add(imageGood6);

//            ImageGood imageGood7 = new ImageGood();
//            imageGood7.Good = db.GoodSet.Where(x => x.Name == "RL-52").FirstOrDefault();
//            imageGood7.Name = "7";
//            imageGood7.Index = 1;
//            db.ImageGoodSet.Add(imageGood7);

//            ImageGood imageGood8 = new ImageGood();
//            imageGood8.Good = db.GoodSet.Where(x => x.Name == "RL-52").FirstOrDefault();
//            imageGood8.Name = "8";
//            imageGood8.Index = 2;
//            db.ImageGoodSet.Add(imageGood8);

//            db.SaveChanges();
//        }

//        [TestMethod]
//        public void n18AuthorizationAdd()
//        {
//            Authorization authorization = new Authorization();
//            authorization.Login = "+79187856221";
//            authorization.Password = "123123";
//            authorization.Key = Guid.NewGuid().ToString();

//            db.AuthorizationSet.Add(authorization);
//            db.SaveChanges();
//        }

//        [TestMethod]
//        public void n19OrganizationStatusAdd()
//        {
//            OrganizationStatus organizationStatus1 = new OrganizationStatus();
//            organizationStatus1.Value = "Отказался от плтатной подписки";
//            organizationStatus1.Description = "Описание 1";
//            db.OrganizationStatusSet.Add(organizationStatus1);

//            OrganizationStatus organizationStatus2 = new OrganizationStatus();
//            organizationStatus2.Value = "Согласен на плтатную подписку";
//            organizationStatus2.Description = "Описание 2";
//            db.OrganizationStatusSet.Add(organizationStatus2);

//            db.SaveChanges();
//        }
//    }
//}
