﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bazarkin.Domain;
using Bazarkin.Web.BusinessLogic;
using System.Collections.Generic;

namespace Bazarkin.UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void DoubleValue()
        {
            EFDbContext db = new EFDbContext();

            // Свойства
            int idProperty = 232;
            // Значение свойства
            string value = "1500";
            // Категория
            int idCategory = 43;

            List<PropertyValue> pvList = db.PropertyValueSet.Where(x => x.Property.Id == idProperty && x.Value == value).ToList();

            if (pvList.Count > 1)
            {
                int idPvLast = pvList.Last().Id;

                List<PropertyValueGroup> pvgList = db.PropertyValueGroupSet.Where(x => x.PropertyValue.Id == idPvLast && x.ModificationGood.Good.Category.Id==idCategory).ToList();

                foreach(var pvg in pvgList)
                {
                    PropertyValueGroup pvgUpdate = db.PropertyValueGroupSet.Where(x => x.Id == pvg.Id).First();
                    int idPvFirst = pvList.First().Id;
                    PropertyValue pv = db.PropertyValueSet.Where(x => x.Id == idPvFirst).FirstOrDefault();
                    pvgUpdate.PropertyValue = pv;
                }

                db.SaveChanges();

                List<PropertyValueGroup> pvgNewList = db.PropertyValueGroupSet.Where(x => x.PropertyValue.Id == idPvLast && x.ModificationGood.Good.Category.Id == idCategory).ToList();
                if (pvgNewList.Count == 0)
                {
                    PropertyValue pvd = db.PropertyValueSet.Where(x => x.Id == idPvLast).FirstOrDefault();
                    db.PropertyValueSet.Remove(pvList.Last());
                    db.SaveChanges();
                }
            }
        }

        [TestMethod]
        public void EditValue()
        {
            EFDbContext db = new EFDbContext();

            // Свойства
            int idProperty = 179;
            // Значение свойства
            string valueOld = "Аккумулятор";
            string valueNew = "От аккумулятора";
            // Категория
            int idCategory = 43;

            PropertyValueGroup pvg = db.PropertyValueGroupSet.Where(x => x.Property.Id == idProperty &&
                                                                        x.ModificationGood.Good.Category.Id == idCategory &&
                                                                        x.PropertyValue.Value == valueOld).FirstOrDefault();

            PropertyValue pv = db.PropertyValueSet.Where(x => x.Id == pvg.PropertyValue.Id).FirstOrDefault();
            pv.Value = valueNew;

            db.SaveChanges();
        }
    }
}
