﻿using Bazarkin.ControlCenter.ViewModels;
using System.Windows;

namespace Bazarkin.ControlCenter.Views
{
    /// <summary>
    /// Логика взаимодействия для PropertyCategoryEditView.xaml
    /// </summary>
    public partial class PropertyCategoryEditView : Window
    {
        public PropertyCategoryEditView(int id)
        {
            InitializeComponent();
            DataContext = new PropertyCategoryEditViewModel(id);
        }
    }
}
