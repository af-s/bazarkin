﻿using Bazarkin.ControlCenter.ViewModels;
using System.Windows;

namespace Bazarkin.ControlCenter.Views
{
    /// <summary>
    /// Логика взаимодействия для PropertyTypeListView.xaml
    /// </summary>
    public partial class PropertyTypeListView : Window
    {
        public PropertyTypeListView()
        {
            InitializeComponent();
            DataContext = new PropertyTypeListViewModel();
        }
    }
}
