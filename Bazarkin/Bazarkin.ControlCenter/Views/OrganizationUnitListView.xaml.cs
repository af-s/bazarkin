﻿using Bazarkin.ControlCenter.ViewModels;
using System.Windows;

namespace Bazarkin.ControlCenter.Views
{
    /// <summary>
    /// Логика взаимодействия для OrganizationUnitListView.xaml
    /// </summary>
    public partial class OrganizationUnitListView : Window
    {
        public OrganizationUnitListView()
        {
            InitializeComponent();
            DataContext = new OrganizationUnitListViewModel();
        }
    }
}
