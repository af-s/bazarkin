﻿using Bazarkin.ControlCenter.ViewModels;
using System.Windows;

namespace Bazarkin.ControlCenter.Views
{
    /// <summary>
    /// Логика взаимодействия для OrganizationListView.xaml
    /// </summary>
    public partial class OrganizationListView : Window
    {
        public OrganizationListView()
        {
            InitializeComponent();
            DataContext = new OrganizationListViewModel();
        }
    }
}
