﻿using Bazarkin.ControlCenter.ViewModels;
using System.Windows;

namespace Bazarkin.ControlCenter.Views
{
    /// <summary>
    /// Логика взаимодействия для PriceModificationGoodView.xaml
    /// </summary>
    public partial class PriceModificationGoodView : Window
    {
        public PriceModificationGoodView(int id)
        {
            InitializeComponent();
            DataContext = new PriceModificationGoodViewModel(id);
        }
    }
}
