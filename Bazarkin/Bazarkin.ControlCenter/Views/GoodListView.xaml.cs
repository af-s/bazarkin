﻿using Bazarkin.ControlCenter.ViewModels;
using System.Windows;

namespace Bazarkin.ControlCenter.Views
{
    /// <summary>
    /// Логика взаимодействия для GoodListView.xaml
    /// </summary>
    public partial class GoodListView : Window
    {
        public GoodListView()
        {
            InitializeComponent();
            DataContext = new GoodListViewModel();
        }
    }
}
