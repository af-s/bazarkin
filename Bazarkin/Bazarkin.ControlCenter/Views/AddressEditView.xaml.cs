﻿using Bazarkin.ControlCenter.ViewModels;
using System.Windows;

namespace Bazarkin.ControlCenter.Views
{
    /// <summary>
    /// Логика взаимодействия для AddressEditView.xaml
    /// </summary>
    public partial class AddressEditView : Window
    {
        public AddressEditView(int? idAddress, int idContactGroup)
        {
            InitializeComponent();
            DataContext = new AddressEditViewModel(idAddress, idContactGroup);
        }
    }
}
