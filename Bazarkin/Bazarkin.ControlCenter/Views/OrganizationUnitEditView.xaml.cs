﻿using Bazarkin.ControlCenter.ViewModels;
using System.Windows;

namespace Bazarkin.ControlCenter.Views
{
    /// <summary>
    /// Логика взаимодействия для OrganizationUnitEditView.xaml
    /// </summary>
    public partial class OrganizationUnitEditView : Window
    {
        public OrganizationUnitEditView(int? id)
        {
            InitializeComponent();
            DataContext = new OrganizationUnitEditViewModel(id);
        }
    }
}
