﻿using Bazarkin.ControlCenter.ViewModels;
using System.Windows;

namespace Bazarkin.ControlCenter.Views
{
    /// <summary>
    /// Логика взаимодействия для PhoneEditView.xaml
    /// </summary>
    public partial class PhoneEditView : Window
    {
        public PhoneEditView(int? idPhone, int idContactGroup)
        {
            InitializeComponent();
            DataContext = new PhoneEditViewModel(idPhone, idContactGroup);
        }
    }
}
