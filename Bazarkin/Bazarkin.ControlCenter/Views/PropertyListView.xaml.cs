﻿using Bazarkin.ControlCenter.ViewModels;
using System.Windows;

namespace Bazarkin.ControlCenter.Views
{
    /// <summary>
    /// Логика взаимодействия для PropertyListView.xaml
    /// </summary>
    public partial class PropertyListView : Window
    {
        public PropertyListView()
        {
            InitializeComponent();
            DataContext = new PropertyListViewModel();
        }
    }
}
