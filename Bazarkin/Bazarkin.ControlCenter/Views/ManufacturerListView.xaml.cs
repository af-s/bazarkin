﻿using Bazarkin.ControlCenter.ViewModels;
using System.Windows;

namespace Bazarkin.ControlCenter.Views
{
    /// <summary>
    /// Логика взаимодействия для ManufacturerListView.xaml
    /// </summary>
    public partial class ManufacturerListView : Window
    {
        public ManufacturerListView()
        {
            InitializeComponent();
            DataContext = new ManufacturerListViewModel();
        }
    }
}
