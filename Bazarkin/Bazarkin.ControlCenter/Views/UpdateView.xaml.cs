﻿using Bazarkin.ControlCenter.ViewModels;
using System.Windows;

namespace Bazarkin.ControlCenter.Views
{
    /// <summary>
    /// Логика взаимодействия для UpdateView.xaml
    /// </summary>
    public partial class UpdateView : Window
    {
        public UpdateView(string versionActual)
        {
            InitializeComponent();
            DataContext = new UpdateViewModel(versionActual);
        }
    }
}
