﻿using Bazarkin.ControlCenter.ViewModels;
using System.Windows;

namespace Bazarkin.ControlCenter.Views
{
    /// <summary>
    /// Логика взаимодействия для ModificationGoodEditView.xaml
    /// </summary>
    public partial class ModificationGoodEditView : Window
    {
        public ModificationGoodEditView(int idGood, int? idModification)
        {
            InitializeComponent();
            DataContext = new ModificationGoodEditViewModel(idGood, idModification);
        }
    }
}
