﻿using Bazarkin.ControlCenter.ViewModels;
using System.Windows;

namespace Bazarkin.ControlCenter.Views
{
    /// <summary>
    /// Логика взаимодействия для CategoryListView.xaml
    /// </summary>
    public partial class CategoryListView : Window
    {
        public CategoryListView()
        {
            InitializeComponent();
            DataContext = new CategoryListViewModel();
        }
    }
}
