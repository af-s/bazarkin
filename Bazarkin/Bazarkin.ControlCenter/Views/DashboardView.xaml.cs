﻿using Bazarkin.ControlCenter.ViewModels;
using System.Windows;

namespace Bazarkin.ControlCenter.Views
{
    /// <summary>
    /// Логика взаимодействия для DashboardView.xaml
    /// </summary>
    public partial class DashboardView : Window
    {
        public DashboardView()
        {
            InitializeComponent();
            DataContext = new DashboardViewModel();
        }

        private void btnHelp_Click(object sender, RoutedEventArgs e)
        {
            var window = new HelpView();
            window.Show();
        }

        private void btnCategory_Click(object sender, RoutedEventArgs e)
        {
            var window = new CategoryListView();
            window.Show();
        }

        private void btnOrganization_Click(object sender, RoutedEventArgs e)
        {
            var window = new OrganizationListView();
            window.Show();
        }

        private void btnPropertyType_Click(object sender, RoutedEventArgs e)
        {
            var window = new PropertyTypeListView();
            window.Show();
        }

        private void btnPropertyGroup_Click(object sender, RoutedEventArgs e)
        {
            var window = new PropertyGroupListView();
            window.Show();
        }

        private void btnProperty_Click(object sender, RoutedEventArgs e)
        {
            var window = new PropertyListView();
            window.Show();
        }

        private void btnManufacturer_Click(object sender, RoutedEventArgs e)
        {
            var window = new ManufacturerListView();
            window.Show();
        }

        private void btnGood_Click(object sender, RoutedEventArgs e)
        {
            var window = new GoodListView();
            window.Show();
        }

        private void btnOrganizationUnit_Click(object sender, RoutedEventArgs e)
        {
            var window = new OrganizationUnitListView();
            window.Show();
        }
    }
}
