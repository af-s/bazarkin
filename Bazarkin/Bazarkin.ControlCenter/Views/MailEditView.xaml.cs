﻿using Bazarkin.ControlCenter.ViewModels;
using System.Windows;

namespace Bazarkin.ControlCenter.Views
{
    /// <summary>
    /// Логика взаимодействия для MailEditView.xaml
    /// </summary>
    public partial class MailEditView : Window
    {
        public MailEditView(int? idMail, int idContactGroup)
        {
            InitializeComponent();
            DataContext = new MailEditViewModel(idMail, idContactGroup);
        }
    }
}
