﻿using Bazarkin.ControlCenter.ViewModels;
using System.Windows;

namespace Bazarkin.ControlCenter.Views
{
    /// <summary>
    /// Логика взаимодействия для CategoryEditView.xaml
    /// </summary>
    public partial class CategoryEditView : Window
    {
        public CategoryEditView(int? id)
        {
            InitializeComponent();
            DataContext = new CategoryEditViewModel(id);
        }
    }
}
