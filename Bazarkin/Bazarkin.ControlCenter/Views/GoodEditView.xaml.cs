﻿using Bazarkin.ControlCenter.ViewModels;
using System.Windows;

namespace Bazarkin.ControlCenter.Views
{
    /// <summary>
    /// Логика взаимодействия для GoodEditView.xaml
    /// </summary>
    public partial class GoodEditView : Window
    {
        public GoodEditView(int? id)
        {
            InitializeComponent();
            DataContext = new GoodEditViewModel(id);
        }
    }
}
