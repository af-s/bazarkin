﻿using Bazarkin.ControlCenter.ViewModels;
using System.Windows;

namespace Bazarkin.ControlCenter.Views
{
    /// <summary>
    /// Логика взаимодействия для OrganizationEditView.xaml
    /// </summary>
    public partial class OrganizationEditView : Window
    {
        public OrganizationEditView(int? id)
        {
            InitializeComponent();
            DataContext = new OrganizationEditViewModel(id);
        }
    }
}
