﻿using Bazarkin.ControlCenter.ViewModels;
using System.Windows;

namespace Bazarkin.ControlCenter.Views
{
    /// <summary>
    /// Логика взаимодействия для PropertyGroupListView.xaml
    /// </summary>
    public partial class PropertyGroupListView : Window
    {
        public PropertyGroupListView()
        {
            InitializeComponent();
            DataContext = new PropertyGroupListViewModel();
        }
    }
}
