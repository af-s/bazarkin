﻿using Bazarkin.ControlCenter.ViewModels;
using System.Windows;

namespace Bazarkin.ControlCenter.Views
{
    /// <summary>
    /// Логика взаимодействия для PropertyTypeEditView.xaml
    /// </summary>
    public partial class PropertyTypeEditView : Window
    {
        public PropertyTypeEditView(int? id)
        {
            InitializeComponent();
            DataContext = new PropertyTypeEditViewModel(id);
        }
    }
}
