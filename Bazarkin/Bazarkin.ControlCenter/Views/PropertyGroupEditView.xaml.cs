﻿using Bazarkin.ControlCenter.ViewModels;
using System.Windows;

namespace Bazarkin.ControlCenter.Views
{
    /// <summary>
    /// Логика взаимодействия для PropertyGroupEditView.xaml
    /// </summary>
    public partial class PropertyGroupEditView : Window
    {
        public PropertyGroupEditView(int? id)
        {
            InitializeComponent();
            DataContext = new PropertyGroupEditViewModel(id);
        }
    }
}
