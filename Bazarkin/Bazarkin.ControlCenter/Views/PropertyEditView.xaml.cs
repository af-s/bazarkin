﻿using Bazarkin.ControlCenter.ViewModels;
using System.Windows;

namespace Bazarkin.ControlCenter.Views
{
    /// <summary>
    /// Логика взаимодействия для PropertyEditView.xaml
    /// </summary>
    public partial class PropertyEditView : Window
    {
        public PropertyEditView(int? id)
        {
            InitializeComponent();
            DataContext = new PropertyEditViewModel(id);
        }
    }
}
