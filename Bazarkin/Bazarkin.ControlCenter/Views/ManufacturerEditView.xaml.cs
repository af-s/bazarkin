﻿using Bazarkin.ControlCenter.ViewModels;
using System.Windows;

namespace Bazarkin.ControlCenter.Views
{
    /// <summary>
    /// Логика взаимодействия для ManufacturerEditView.xaml
    /// </summary>
    public partial class ManufacturerEditView : Window
    {
        public ManufacturerEditView(int? id)
        {
            InitializeComponent();
            DataContext = new ManufacturerEditViewModel(id);
        }
    }
}
