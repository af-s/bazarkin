﻿using Bazarkin.ClassLibrary;
using Bazarkin.ControlCenter.Views;
using Bazarkin.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Bazarkin.ControlCenter.ViewModels
{
    class CategoryEditViewModel : MVVMRelated
    {
        #region Constructors
        public CategoryEditViewModel(int? id)
        {
            EFDbContext db = new EFDbContext();

            Category category;
            if (id == null)
            {
                flagSaved = false;
                category = new Category();
            }
            else
            {
                flagSaved = true;
                category = db.CategorySet.Where(x => x.Id == id).FirstOrDefault();

                this.id = category.Id;
                name = category.Name;
                description = category.Description;
                alias = category.Alias;
                image = category.Image;

                if (category.Parent != null)
                {
                    idParent = db.CategorySet.Where(x => x.Id == category.Parent.Id).FirstOrDefault().Id;
                }

                List<PropertyCategory> propertyCategoryList = db.PropertyCategorySet.Where(x => x.Category.Id == category.Id).ToList();
                List<Property> propertyList = new List<Property>();
                foreach (var propertyCategory in propertyCategoryList)
                {
                    propertyList.Add(db.PropertySet.Where(x => x.Id == propertyCategory.Property.Id).FirstOrDefault());
                }

                List<PropertyGroup> propertyGroupList = new List<PropertyGroup>();
                foreach (var property in propertyList)
                {
                    propertyGroupList.Add(db.PropertyGroupSet.Where(x => x.Id == property.PropertyGroup.Id).FirstOrDefault());
                }
                propertyGroupList = propertyGroupList.Distinct().OrderBy(x => x.Name).ToList();

                propertyGroupAndPropertyList = new Dictionary<string, string>();
                foreach (var propertyGroup in propertyGroupList)
                {
                    string propertyListString = string.Empty;

                    List<Property> propertiesInGrouplist = propertyList.Where(x => x.PropertyGroup.Id == propertyGroup.Id).OrderBy(x => x.Name).ToList();

                    foreach (var propertiesInGroup in propertiesInGrouplist)
                    {
                        if (propertyListString != string.Empty)
                        {
                            propertyListString += ", ";
                        }
                        propertyListString += propertiesInGroup.Name;
                    }

                    propertyGroupAndPropertyList.Add(propertyGroup.Name, propertyListString);
                }
            }

            categoryList = new List<Category>();
            categoryList.Add(new Category() { Id = -1 });
            categoryList.AddRange(db.CategorySet.Where(x => x.Id != id).OrderBy(x => x.Name).ToList());
        }
        #endregion

        #region Properties
        private int _id;
        public int id
        {
            get { return _id; }
            set
            {
                _id = value;
                RaisePropertyChanged();
            }
        }

        private string _name;
        public string name
        {
            get { return _name; }
            set
            {
                _name = value;
                alias = Translit.RuToTranslite(value);
                RaisePropertyChanged();
            }
        }

        private string _description;
        public string description
        {
            get { return _description; }
            set
            {
                _description = value;
                RaisePropertyChanged();
            }
        }

        private string _alias;
        public string alias
        {
            get { return _alias; }
            set
            {
                _alias = value;
                RaisePropertyChanged();
            }
        }

        private int? _image;
        public int? image
        {
            get { return _image; }
            set
            {
                _image = value;
                RaisePropertyChanged();
            }
        }

        private int? _idParent;
        public int? idParent
        {
            get { return _idParent; }
            set
            {
                _idParent = value;
                RaisePropertyChanged();
            }
        }

        private List<Category> _categoryList;
        public List<Category> categoryList
        {
            get { return _categoryList; }
            set
            {
                _categoryList = value;
                RaisePropertyChanged();
            }
        }

        private Dictionary<string, string> _propertyGroupAndPropertyList;
        public Dictionary<string, string> propertyGroupAndPropertyList
        {
            get { return _propertyGroupAndPropertyList; }
            set
            {
                _propertyGroupAndPropertyList = value;
                RaisePropertyChanged();
            }
        }

        private bool _flagSaved;
        public bool flagSaved
        {
            get { return _flagSaved; }
            set
            {
                _flagSaved = value;
                RaisePropertyChanged();
            }
        }

        #endregion

        #region Comands
        private AsyncDelegateCommand _propertyCategoryEditCommand;
        public ICommand ButtonClickEditPropertyCategory
        {
            get
            {
                if (_propertyCategoryEditCommand == null)
                {
                    _propertyCategoryEditCommand = new AsyncDelegateCommand(PropertyCategoryEdit);
                }
                return _propertyCategoryEditCommand;
            }
        }

        private AsyncDelegateCommand _categorySaveCommand;
        public ICommand ButtonClickCategorySave
        {
            get
            {
                if (_categorySaveCommand == null)
                {
                    _categorySaveCommand = new AsyncDelegateCommand(CategorySave);
                }
                return _categorySaveCommand;
            }
        }
        #endregion

        #region Methods
        private async Task PropertyCategoryEdit(object o)
        {
            await Task.Delay(0);
            var window = new PropertyCategoryEditView(id);
            window.Show();
        }

        private async Task CategorySave(object o)
        {
            EFDbContext db = new EFDbContext();
            await Task.Delay(0);
            Category category;
            if (id == 0)
            {
                category = new Category();
            }
            else
            {
                category = db.CategorySet.Where(x => x.Id == id).FirstOrDefault();
            }
            category.Name = name;
            category.Description = description;
            category.Alias = alias;
            category.Image = image;

            if (idParent == -1)
            {
                category.Parent = null;
            }
            else
            {
                category.Parent = db.CategorySet.Where(x => x.Id == idParent).FirstOrDefault();
            }
            
            if (category.Id == 0)
            {
                db.CategorySet.Add(category);
            }

            db.SaveChanges();
            id = category.Id;
            flagSaved = true;
        }
        #endregion
    }
}
