﻿using Bazarkin.ClassLibrary;
using Bazarkin.ControlCenter.Views;
using Bazarkin.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Bazarkin.ControlCenter.ViewModels
{
    class OrganizationEditViewModel : MVVMRelated
    {
        EFDbContext db = new EFDbContext();

        #region Constructors
        public OrganizationEditViewModel(int? id)
        {
            EFDbContext db = new EFDbContext();
            Organization organization;
            if (id == null)
            {
                organization = new Organization();
            }
            else
            {
                organization = db.OrganizationSet.Where(x => x.Id == id).FirstOrDefault();

                idOrganization = organization.Id;
                name = organization.Name;
                description = organization.Description;
                active = organization.Active;

                ContactGroup contactGroup = db.ContactGroupSet.Where(x => x.Id == organization.ContactGroup.Id).FirstOrDefault();
                idContactGroup = contactGroup.Id;

                List<Phone> phoneList = db.PhoneSet.Where(x => x.ContactGroup.Id == contactGroup.Id).ToList();
                phoneRowList = new List<PhoneRow>();
                foreach (var phone in phoneList)
                {
                    phoneRowList.Add(new PhoneRow(phone, contactGroup.Id));
                }

                List<Address> addressList = db.AddressSet.Where(x => x.ContactGroup.Id == contactGroup.Id).ToList();
                addressRowList = new List<AddressRow>();
                foreach (var address in addressList)
                {
                    addressRowList.Add(new AddressRow(address, contactGroup.Id));
                }

                List<Mail> mailList = db.MailSet.Where(x => x.ContactGroup.Id == contactGroup.Id).ToList();
                mailRowList = new List<MailRow>();
                foreach (var mail in mailList)
                {
                    mailRowList.Add(new MailRow(db, mail, contactGroup.Id));
                }
            }
            idClient = 0;
        }
        #endregion

        #region Properties
        private int _idOrganization;
        public int idOrganization
        {
            get { return _idOrganization; }
            set
            {
                _idOrganization = value;
                RaisePropertyChanged();
            }
        }

        private int _idContactGroup;
        public int idContactGroup
        {
            get { return _idContactGroup; }
            set
            {
                _idContactGroup = value;
                RaisePropertyChanged();
            }
        }

        private int _idClient;
        public int idClient
        {
            get { return _idClient; }
            set
            {
                _idClient = value;
                RaisePropertyChanged();
            }
        }

        private string _name;
        public string name
        {
            get { return _name; }
            set
            {
                _name = value;
                RaisePropertyChanged();
            }
        }

        private string _description;
        public string description
        {
            get { return _description; }
            set
            {
                _description = value;
                RaisePropertyChanged();
            }
        }

        private bool _active;
        public bool active
        {
            get { return _active; }
            set
            {
                _active = value;
                RaisePropertyChanged();
            }
        }

        private List<PhoneRow> _phoneRowList;
        public List<PhoneRow> phoneRowList
        {
            get { return _phoneRowList; }
            set
            {
                _phoneRowList = value;
                RaisePropertyChanged();
            }
        }

        private List<AddressRow> _addressRowList;
        public List<AddressRow> addressRowList
        {
            get { return _addressRowList; }
            set
            {
                _addressRowList = value;
                RaisePropertyChanged();
            }
        }

        private List<MailRow> _mailRowList;
        public List<MailRow> mailRowList
        {
            get { return _mailRowList; }
            set
            {
                _mailRowList = value;
                RaisePropertyChanged();
            }
        }
        #endregion

        #region Classes
        #region PhoneRow
        public class PhoneRow : MVVMRelated
        {
            #region Constructors
            public PhoneRow(Phone phone, int idContactGroup)
            {
                id = phone.Id;
                number = phone.Number;
                description = phone.Description;
                view = phone.View;

                this.idContactGroup = idContactGroup;
            }
            #endregion

            #region Properties
            private int _id;
            public int id
            {
                get { return _id; }
                set
                {
                    _id = value;
                    RaisePropertyChanged();
                }
            }

            private string _number;
            public string number
            {
                get { return _number; }
                set
                {
                    _number = value;
                    RaisePropertyChanged();
                }
            }

            private string _description;
            public string description
            {
                get { return _description; }
                set
                {
                    _description = value;
                    RaisePropertyChanged();
                }
            }

            private bool _view;
            public bool view
            {
                get { return _view; }
                set
                {
                    _view = value;
                    RaisePropertyChanged();
                }
            }

            private int _idContactGroup;
            public int idContactGroup
            {
                get { return _idContactGroup; }
                set
                {
                    _idContactGroup = value;
                    RaisePropertyChanged();
                }
            }
            #endregion

            #region Comands
            private AsyncDelegateCommand _editPhoneCommand;
            public ICommand ButtonClickEditPhone
            {
                get
                {
                    if (_editPhoneCommand == null)
                    {
                        _editPhoneCommand = new AsyncDelegateCommand(EditPhone);
                    }
                    return _editPhoneCommand;
                }
            }
            #endregion

            #region Methods
            private async Task EditPhone(object o)
            {
                await Task.Delay(0);
                var window = new PhoneEditView(id, idContactGroup);
                window.Show();
            }
            #endregion
        }
        #endregion
        #region AddressRow
        public class AddressRow : MVVMRelated
        {
            #region Constructors
            public AddressRow(Address address, int idContactGroup)
            {
                id = address.Id;
                addressString = AddressLib.ObjectToString(address);
                description = address.Description;
                view = address.View;

                this.idContactGroup = idContactGroup;
            }
            #endregion

            #region Properties
            private int _id;
            public int id
            {
                get { return _id; }
                set
                {
                    _id = value;
                    RaisePropertyChanged();
                }
            }

            private string _addressString;
            public string addressString
            {
                get { return _addressString; }
                set
                {
                    _addressString = value;
                    RaisePropertyChanged();
                }
            }

            private string _description;
            public string description
            {
                get { return _description; }
                set
                {
                    _description = value;
                    RaisePropertyChanged();
                }
            }

            private bool _view;
            public bool view
            {
                get { return _view; }
                set
                {
                    _view = value;
                    RaisePropertyChanged();
                }
            }

            private int _idContactGroup;
            public int idContactGroup
            {
                get { return _idContactGroup; }
                set
                {
                    _idContactGroup = value;
                    RaisePropertyChanged();
                }
            }
            #endregion

            #region Comands
            private AsyncDelegateCommand _editAddressCommand;
            public ICommand ButtonClickEditAddress
            {
                get
                {
                    if (_editAddressCommand == null)
                    {
                        _editAddressCommand = new AsyncDelegateCommand(EditAddress);
                    }
                    return _editAddressCommand;
                }
            }
            #endregion

            #region Methods
            private async Task EditAddress(object o)
            {
                await Task.Delay(0);
                var window = new AddressEditView(id, idContactGroup);
                window.Show();
            }
            #endregion
        }
        #endregion
        #region MailRow
        public class MailRow : MVVMRelated
        {
            #region Constructors
            public MailRow(EFDbContext db, Mail mail, int idContactGroup)
            {
                id = mail.Id;
                value = mail.Value;
                description = mail.Description;

                this.idContactGroup = idContactGroup;
            }
            #endregion

            #region Properties
            private int _id;
            public int id
            {
                get { return _id; }
                set
                {
                    _id = value;
                    RaisePropertyChanged();
                }
            }

            private string _value;
            public string value
            {
                get { return _value; }
                set
                {
                    _value = value;
                    RaisePropertyChanged();
                }
            }

            private string _description;
            public string description
            {
                get { return _description; }
                set
                {
                    _description = value;
                    RaisePropertyChanged();
                }
            }

            private int _idContactGroup;
            public int idContactGroup
            {
                get { return _idContactGroup; }
                set
                {
                    _idContactGroup = value;
                    RaisePropertyChanged();
                }
            }
            #endregion

            #region Comands
            private AsyncDelegateCommand _editMailCommand;
            public ICommand ButtonClickEditMail
            {
                get
                {
                    if (_editMailCommand == null)
                    {
                        _editMailCommand = new AsyncDelegateCommand(EditMail);
                    }
                    return _editMailCommand;
                }
            }
            #endregion

            #region Methods
            private async Task EditMail(object o)
            {
                await Task.Delay(0);
                var window = new MailEditView(id, idContactGroup);
                window.Show();
            }
            #endregion
        }
        #endregion
        #endregion

        #region Comands
        private AsyncDelegateCommand _newPhoneCommand;
        public ICommand ButtonClickNewPhone
        {
            get
            {
                if (_newPhoneCommand == null)
                {
                    _newPhoneCommand = new AsyncDelegateCommand(NewPhone);
                }
                return _newPhoneCommand;
            }
        }

        private AsyncDelegateCommand _newAddressCommand;
        public ICommand ButtonClickNewAddress
        {
            get
            {
                if (_newAddressCommand == null)
                {
                    _newAddressCommand = new AsyncDelegateCommand(NewAddress);
                }
                return _newAddressCommand;
            }
        }

        private AsyncDelegateCommand _newMailCommand;
        public ICommand ButtonClickNewMail
        {
            get
            {
                if (_newMailCommand == null)
                {
                    _newMailCommand = new AsyncDelegateCommand(NewMail);
                }
                return _newMailCommand;
            }
        }

        private AsyncDelegateCommand _saveOrganizationCommand;
        public ICommand ButtonClickSaveOrganization
        {
            get
            {
                if (_saveOrganizationCommand == null)
                {
                    _saveOrganizationCommand = new AsyncDelegateCommand(SaveOrganization);
                }
                return _saveOrganizationCommand;
            }
        }
        #endregion

        #region Methods
        private async Task NewPhone(object o)
        {
            await Task.Delay(0);
            var window = new PhoneEditView(null, idContactGroup);
            window.Show();
        }

        private async Task NewAddress(object o)
        {
            await Task.Delay(0);
            var window = new AddressEditView(null, idContactGroup);
            window.Show();
        }

        private async Task NewMail(object o)
        {
            await Task.Delay(0);
            var window = new MailEditView(null, idContactGroup);
            window.Show();
        }

        private async Task SaveOrganization(object o)
        {
            await Task.Delay(0);
            Organization organization;
            if (idOrganization == 0)
            {
                organization = new Organization();
                organization.ContactGroup = new ContactGroup();
            }
            else
            {
                organization = db.OrganizationSet.Where(x => x.Id == idOrganization).FirstOrDefault();
            }

            organization.Name = name;
            organization.Description = description;
            organization.Active = active;

            if (organization.Id == 0)
            {
                db.OrganizationSet.Add(organization);
            }

            db.SaveChanges();
        }
        #endregion
    }
}
