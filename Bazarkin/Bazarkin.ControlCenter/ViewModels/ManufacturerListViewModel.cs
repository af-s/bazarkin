﻿using Bazarkin.ControlCenter.Views;
using Bazarkin.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Bazarkin.ControlCenter.ViewModels
{
    class ManufacturerListViewModel : MVVMRelated
    {
        #region Constructors
        public ManufacturerListViewModel()
        {
            EFDbContext db = new EFDbContext();
            List<Manufacturer> manufacturerList = db.ManufacturerSet.OrderBy(x => x.Name).ToList();
            manufacturerRowList = new List<ManufacturerRow>();
            foreach (var manufacturer in manufacturerList)
            {
                manufacturerRowList.Add(new ManufacturerRow(manufacturer));
            }
        }
        #endregion

        #region Properties
        private List<ManufacturerRow> _manufacturerRowList;
        public List<ManufacturerRow> manufacturerRowList
        {
            get { return _manufacturerRowList; }
            set
            {
                _manufacturerRowList = value;
                RaisePropertyChanged();
            }
        }
        #endregion

        #region Classes
        public class ManufacturerRow : MVVMRelated
        {
            #region Constructors
            public ManufacturerRow(Manufacturer manufacturer)
            {
                id = manufacturer.Id;
                name = manufacturer.Name;
                description = manufacturer.Description;
                alias = manufacturer.Alias;
            }
            #endregion

            #region Properties
            private int _id;
            public int id
            {
                get { return _id; }
                set
                {
                    _id = value;
                    RaisePropertyChanged();
                }
            }

            private string _name;
            public string name
            {
                get { return _name; }
                set
                {
                    _name = value;
                    RaisePropertyChanged();
                }
            }

            private string _description;
            public string description
            {
                get { return _description; }
                set
                {
                    _description = value;
                    RaisePropertyChanged();
                }
            }

            private string _alias;
            public string alias
            {
                get { return _alias; }
                set
                {
                    _alias = value;
                    RaisePropertyChanged();
                }
            }
            #endregion

            #region Comands
            private AsyncDelegateCommand _editManufacturerCommand;
            public ICommand ButtonClickEditManufacturer
            {
                get
                {
                    if (_editManufacturerCommand == null)
                    {
                        _editManufacturerCommand = new AsyncDelegateCommand(EditManufacturer);
                    }
                    return _editManufacturerCommand;
                }
            }
            #endregion

            #region Methods
            private async Task EditManufacturer(object o)
            {
                await Task.Delay(0);
                var window = new ManufacturerEditView(id);
                window.Show();
            }
            #endregion
        }
        #endregion

        #region Comands
        private AsyncDelegateCommand _newManufacturerCommand;
        public ICommand ButtonClickNewManufacturer
        {
            get
            {
                if (_newManufacturerCommand == null)
                {
                    _newManufacturerCommand = new AsyncDelegateCommand(NewManufacturer);
                }
                return _newManufacturerCommand;
            }
        }
        #endregion

        #region Methods
        private async Task NewManufacturer(object o)
        {
            await Task.Delay(0);
            var window = new ManufacturerEditView(null);
            window.Show();
        }
        #endregion
    }
}
