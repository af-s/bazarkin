﻿using Bazarkin.ClassLibrary;
using Bazarkin.ControlCenter.Views;
using Bazarkin.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Bazarkin.ControlCenter.ViewModels
{
    class OrganizationUnitEditViewModel : MVVMRelated
    {
        EFDbContext db = new EFDbContext();

        #region Constructors
        public OrganizationUnitEditViewModel(int? id)
        {
            EFDbContext db = new EFDbContext();
            OrganizationUnit organizationUnit;
            if (id == null)
            {
                flagValidation = false;
                flagSaved = false;
                organizationUnit = new OrganizationUnit();
            }
            else
            {
                flagValidation = true;
                flagSaved = true;

                organizationUnit = db.OrganizationUnitSet.Where(x => x.Id == id).FirstOrDefault();
                idOrganizationUnit = organizationUnit.Id;
                idOrganization = organizationUnit.Organization.Id;
                description = organizationUnit.Description;
                active = organizationUnit.Active;

                ContactGroup contactGroup = db.ContactGroupSet.Where(x => x.Id == organizationUnit.ContactGroup.Id).FirstOrDefault();
                idContactGroup = contactGroup.Id;

                List<Phone> phoneList = db.PhoneSet.Where(x => x.ContactGroup.Id == contactGroup.Id).ToList();
                phoneRowList = new List<PhoneRow>();
                foreach (var phone in phoneList)
                {
                    phoneRowList.Add(new PhoneRow(phone, contactGroup.Id));
                }

                List<Address> addressList = db.AddressSet.Where(x => x.ContactGroup.Id == contactGroup.Id).ToList();
                addressRowList = new List<AddressRow>();
                foreach (var address in addressList)
                {
                    addressRowList.Add(new AddressRow(address, contactGroup.Id));
                }
            }
            organizationList = new List<Organization>();
            organizationList.Add(new Organization());
            organizationList.AddRange(db.OrganizationSet.OrderBy(x => x.Name).ToList());
        }
        #endregion

        #region Properties
        private int _idOrganizationUnit;
        public int idOrganizationUnit
        {
            get { return _idOrganizationUnit; }
            set
            {
                _idOrganizationUnit = value;
                RaisePropertyChanged();
            }
        }

        private int _idOrganization;
        public int idOrganization
        {
            get { return _idOrganization; }
            set
            {
                _idOrganization = value;
                Validation();
                RaisePropertyChanged();
            }
        }

        private int _idContactGroup;
        public int idContactGroup
        {
            get { return _idContactGroup; }
            set
            {
                _idContactGroup = value;
                RaisePropertyChanged();
            }
        }

        private List<Organization> _organizationList;
        public List<Organization> organizationList
        {
            get { return _organizationList; }
            set
            {
                _organizationList = value;
                RaisePropertyChanged();
            }
        }

        private string _description;
        public string description
        {
            get { return _description; }
            set
            {
                _description = value;
                RaisePropertyChanged();
            }
        }

        private bool _active;
        public bool active
        {
            get { return _active; }
            set
            {
                _active = value;
                RaisePropertyChanged();
            }
        }

        private List<PhoneRow> _phoneRowList;
        public List<PhoneRow> phoneRowList
        {
            get { return _phoneRowList; }
            set
            {
                _phoneRowList = value;
                RaisePropertyChanged();
            }
        }

        private List<AddressRow> _addressRowList;
        public List<AddressRow> addressRowList
        {
            get { return _addressRowList; }
            set
            {
                _addressRowList = value;
                RaisePropertyChanged();
            }
        }

        private bool _flagSaved;
        public bool flagSaved
        {
            get { return _flagSaved; }
            set
            {
                _flagSaved = value;
                RaisePropertyChanged();
            }
        }

        private bool _flagValidation;
        public bool flagValidation
        {
            get { return _flagValidation; }
            set
            {
                _flagValidation = value;
                RaisePropertyChanged();
            }
        }
        #endregion

        #region Classes
        #region PhoneRow
        public class PhoneRow : MVVMRelated
        {
            #region Constructors
            public PhoneRow(Phone phone, int idContactGroup)
            {
                id = phone.Id;
                number = phone.Number;
                description = phone.Description;
                view = phone.View;

                this.idContactGroup = idContactGroup;
            }
            #endregion

            #region Properties
            private int _id;
            public int id
            {
                get { return _id; }
                set
                {
                    _id = value;
                    RaisePropertyChanged();
                }
            }

            private string _number;
            public string number
            {
                get { return _number; }
                set
                {
                    _number = value;
                    RaisePropertyChanged();
                }
            }

            private string _description;
            public string description
            {
                get { return _description; }
                set
                {
                    _description = value;
                    RaisePropertyChanged();
                }
            }

            private bool _view;
            public bool view
            {
                get { return _view; }
                set
                {
                    _view = value;
                    RaisePropertyChanged();
                }
            }

            private int _idContactGroup;
            public int idContactGroup
            {
                get { return _idContactGroup; }
                set
                {
                    _idContactGroup = value;
                    RaisePropertyChanged();
                }
            }
            #endregion

            #region Comands
            private AsyncDelegateCommand _editPhoneCommand;
            public ICommand ButtonClickEditPhone
            {
                get
                {
                    if (_editPhoneCommand == null)
                    {
                        _editPhoneCommand = new AsyncDelegateCommand(EditPhone);
                    }
                    return _editPhoneCommand;
                }
            }
            #endregion

            #region Methods
            private async Task EditPhone(object o)
            {
                await Task.Delay(0);
                var window = new PhoneEditView(id, idContactGroup);
                window.Show();
            }
            #endregion
        }
        #endregion
        #region AddressRow
        public class AddressRow : MVVMRelated
        {
            #region Constructors
            public AddressRow(Address address, int idContactGroup)
            {
                id = address.Id;
                addressString = AddressLib.ObjectToString(address);
                description = address.Description;
                view = address.View;

                this.idContactGroup = idContactGroup;
            }
            #endregion

            #region Properties
            private int _id;
            public int id
            {
                get { return _id; }
                set
                {
                    _id = value;
                    RaisePropertyChanged();
                }
            }

            private string _addressString;
            public string addressString
            {
                get { return _addressString; }
                set
                {
                    _addressString = value;
                    RaisePropertyChanged();
                }
            }

            private string _description;
            public string description
            {
                get { return _description; }
                set
                {
                    _description = value;
                    RaisePropertyChanged();
                }
            }

            private bool _view;
            public bool view
            {
                get { return _view; }
                set
                {
                    _view = value;
                    RaisePropertyChanged();
                }
            }

            private int _idContactGroup;
            public int idContactGroup
            {
                get { return _idContactGroup; }
                set
                {
                    _idContactGroup = value;
                    RaisePropertyChanged();
                }
            }
            #endregion

            #region Comands
            private AsyncDelegateCommand _editAddressCommand;
            public ICommand ButtonClickEditAddress
            {
                get
                {
                    if (_editAddressCommand == null)
                    {
                        _editAddressCommand = new AsyncDelegateCommand(EditAddress);
                    }
                    return _editAddressCommand;
                }
            }
            #endregion

            #region Methods
            private async Task EditAddress(object o)
            {
                await Task.Delay(0);
                var window = new AddressEditView(id, idContactGroup);
                window.Show();
            }
            #endregion
        }
        #endregion
        #endregion

        #region Comands
        private AsyncDelegateCommand _newPhoneCommand;
        public ICommand ButtonClickNewPhone
        {
            get
            {
                if (_newPhoneCommand == null)
                {
                    _newPhoneCommand = new AsyncDelegateCommand(NewPhone);
                }
                return _newPhoneCommand;
            }
        }

        private AsyncDelegateCommand _newAddressCommand;
        public ICommand ButtonClickNewAddress
        {
            get
            {
                if (_newAddressCommand == null)
                {
                    _newAddressCommand = new AsyncDelegateCommand(NewAddress);
                }
                return _newAddressCommand;
            }
        }

        private AsyncDelegateCommand _saveOrganizationUnitCommand;
        public ICommand ButtonClickSaveOrganizationUnit
        {
            get
            {
                if (_saveOrganizationUnitCommand == null)
                {
                    _saveOrganizationUnitCommand = new AsyncDelegateCommand(SaveOrganizationUnit);
                }
                return _saveOrganizationUnitCommand;
            }
        }

        private AsyncDelegateCommand _editPriceModificationGood;
        public ICommand ButtonClickEditPriceModificationGood
        {
            get
            {
                if (_editPriceModificationGood == null)
                {
                    _editPriceModificationGood = new AsyncDelegateCommand(EditPriceModificationGood);
                }
                return _editPriceModificationGood;
            }
        }
        #endregion

        #region Methods
        private async Task NewPhone(object o)
        {
            await Task.Delay(0);
            var window = new PhoneEditView(null, idContactGroup);
            window.Show();
        }

        private async Task NewAddress(object o)
        {
            await Task.Delay(0);
            var window = new AddressEditView(null, idContactGroup);
            window.Show();
        }

        private async Task EditPriceModificationGood(object o)
        {
            await Task.Delay(0);
            var window = new PriceModificationGoodView(idOrganizationUnit);
            window.Show();
        }

        private async Task SaveOrganizationUnit(object o)
        {
            await Task.Delay(0);
            OrganizationUnit organizationUnit;
            if (idOrganizationUnit == 0)
            {
                organizationUnit = new OrganizationUnit();
                organizationUnit.ContactGroup = new ContactGroup();
            }
            else
            {
                organizationUnit = db.OrganizationUnitSet.Where(x => x.Id == idOrganizationUnit).FirstOrDefault();
            }

            organizationUnit.Organization = db.OrganizationSet.Where(x => x.Id == idOrganization).FirstOrDefault();
            organizationUnit.Description = description;
            organizationUnit.Active = active;

            if (organizationUnit.Id == 0)
            {
                db.OrganizationUnitSet.Add(organizationUnit);
            }

            db.SaveChanges();

            idOrganizationUnit = organizationUnit.Id;
            idContactGroup = organizationUnit.ContactGroup.Id;
            flagSaved = true;
        }

        private void Validation()
        {
            if (idOrganization != 0)
            {
                flagValidation = true;
            }
            else
            {
                flagValidation = false;
            }
        }
        #endregion
    }
}
