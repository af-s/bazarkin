﻿using Bazarkin.ControlCenter.Views;
using Bazarkin.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Bazarkin.ControlCenter.ViewModels
{
    class OrganizationListViewModel : MVVMRelated
    {
        #region Constructors
        public OrganizationListViewModel()
        {
            EFDbContext db = new EFDbContext();
            List<Organization> organizationList = db.OrganizationSet.OrderBy(x => x.Name).ToList();
            organizationRowList = new List<OrganizationRow>();
            foreach (var organization in organizationList)
            {
                organizationRowList.Add(new OrganizationRow(db, organization));
            }
        }
        #endregion

        #region Properties
        private List<OrganizationRow> _organizationRowList;
        public List<OrganizationRow> organizationRowList
        {
            get { return _organizationRowList; }
            set
            {
                _organizationRowList = value;
                RaisePropertyChanged();
            }
        }
        #endregion

        #region Classes
        public class OrganizationRow : MVVMRelated
        {
            #region Constructors
            public OrganizationRow(EFDbContext db, Organization organization)
            {
                id = organization.Id;
                idClient = 0;
                name = organization.Name;
                organizationUnitCount = "-";
                active = organization.Active;
            }
            #endregion

            #region Properties
            private int _id;
            public int id
            {
                get { return _id; }
                set
                {
                    _id = value;
                    RaisePropertyChanged();
                }
            }

            private int _idClient;
            public int idClient
            {
                get { return _idClient; }
                set
                {
                    _idClient = value;
                    RaisePropertyChanged();
                }
            }

            private string _name;
            public string name
            {
                get { return _name; }
                set
                {
                    _name = value;
                    RaisePropertyChanged();
                }
            }

            private string _organizationUnitCount;
            public string organizationUnitCount
            {
                get { return _organizationUnitCount; }
                set
                {
                    _organizationUnitCount = value;
                    RaisePropertyChanged();
                }
            }

            private bool _active;
            public bool active
            {
                get { return _active; }
                set
                {
                    _active = value;
                    RaisePropertyChanged();
                }
            }
            #endregion

            #region Comands
            private AsyncDelegateCommand _editOrganizationCommand;
            public ICommand ButtonClickEditOrganization
            {
                get
                {
                    if (_editOrganizationCommand == null)
                    {
                        _editOrganizationCommand = new AsyncDelegateCommand(EditOrganization);
                    }
                    return _editOrganizationCommand;
                }
            }
            #endregion

            #region Methods
            private async Task EditOrganization(object o)
            {
                await Task.Delay(0);
                var window = new OrganizationEditView(id);
                window.Show();
            }
            #endregion
        }
        #endregion

        #region Comands
        private AsyncDelegateCommand _newOrganizationCommand;
        public ICommand ButtonClickNewOrganization
        {
            get
            {
                if (_newOrganizationCommand == null)
                {
                    _newOrganizationCommand = new AsyncDelegateCommand(NewOrganization);
                }
                return _newOrganizationCommand;
            }
        }
        #endregion

        #region Methods
        private async Task NewOrganization(object o)
        {
            await Task.Delay(0);
            var window = new OrganizationEditView(null);
            window.Show();
        }
        #endregion
    }
}
