﻿using Bazarkin.Domain;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Bazarkin.ControlCenter.ViewModels
{
    class PhoneEditViewModel : MVVMRelated
    {
        EFDbContext db = new EFDbContext();

        #region Constructors
        public PhoneEditViewModel(int? idPhone, int idContactGroup)
        {
            Phone phone;
            if (idPhone==null)
            {
                phone = new Phone();
            }
            else
            {
                phone = db.PhoneSet.Where(x => x.Id == idPhone).FirstOrDefault();
            }
            this.idPhone = phone.Id;
            this.idContactGroup = idContactGroup;
            number= phone.Number;
            description= phone.Description;
            view= phone.View;
        }
        #endregion

        #region Properties
        private int _idPhone;
        public int idPhone
        {
            get { return _idPhone; }
            set
            {
                _idPhone = value;
                RaisePropertyChanged();
            }
        }

        private int _idContactGroup;
        public int idContactGroup
        {
            get { return _idContactGroup; }
            set
            {
                _idContactGroup = value;
                RaisePropertyChanged();
            }
        }

        private string _number;
        public string number
        {
            get { return _number; }
            set
            {
                _number = value;
                RaisePropertyChanged();
            }
        }

        private string _description;
        public string description
        {
            get { return _description; }
            set
            {
                _description = value;
                RaisePropertyChanged();
            }
        }

        private bool _view;
        public bool view
        {
            get { return _view; }
            set
            {
                _view = value;
                RaisePropertyChanged();
            }
        }
        #endregion

        #region Comands
        private AsyncDelegateCommand _savePhoneCommand;
        public ICommand ButtonClickSavePhone
        {
            get
            {
                if (_savePhoneCommand == null)
                {
                    _savePhoneCommand = new AsyncDelegateCommand(SavePhone);
                }
                return _savePhoneCommand;
            }
        }
        #endregion

        #region Methods
        private async Task SavePhone(object o)
        {
            await Task.Delay(0);
            Phone phone;
            if (idPhone==0)
            {
                phone = new Phone();
                phone.ContactGroup = db.ContactGroupSet.Where(x => x.Id == idContactGroup).FirstOrDefault();
            }
            else
            {
                phone = db.PhoneSet.Where(x => x.Id == idPhone).FirstOrDefault();
            }

            phone.Number = number;
            phone.Description = description;
            phone.View = view;

            if (phone.Id == 0)
            {
                db.PhoneSet.Add(phone);
            }

            db.SaveChanges();
        }
        #endregion
    }
}
