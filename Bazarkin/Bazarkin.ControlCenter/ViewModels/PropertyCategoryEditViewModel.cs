﻿using Bazarkin.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Bazarkin.ControlCenter.ViewModels
{
    class PropertyCategoryEditViewModel : MVVMRelated
    {
        EFDbContext db = new EFDbContext();

        #region Constructors
        public PropertyCategoryEditViewModel(int id)
        {
            this.id = id;
            Category category = db.CategorySet.Where(x=>x.Id==id).FirstOrDefault();

            List<PropertyGroup> propertyGroupList = db.PropertyGroupSet.OrderBy(x => x.Name).ToList();
            propertyGroupAndPropertiesList = new List<PropertyGroupAndProperties>();
            foreach(var propertyGroup in propertyGroupList)
            {
                propertyGroupAndPropertiesList.Add(new PropertyGroupAndProperties(db, category, propertyGroup));
            }
        }
        #endregion

        #region Properties
        private int _id;
        public int id
        {
            get { return _id; }
            set
            {
                _id = value;
                RaisePropertyChanged();
            }
        }

        private List<PropertyGroupAndProperties> _propertyGroupAndPropertiesList;
        public List<PropertyGroupAndProperties> propertyGroupAndPropertiesList
        {
            get { return _propertyGroupAndPropertiesList; }
            set
            {
                _propertyGroupAndPropertiesList = value;
                RaisePropertyChanged();
            }
        }
        #endregion

        #region Classes
        #region PropertyGroupAndProperties
        public class PropertyGroupAndProperties : MVVMRelated
        {
            #region Constructors
            public PropertyGroupAndProperties(EFDbContext db, Category category, PropertyGroup propertyGroup)
            {
                name = propertyGroup.Name;
                List<Property> propertyList = db.PropertySet.Where(x => x.PropertyGroup.Id == propertyGroup.Id).OrderBy(x => x.Name).ToList();
                propertyAndCheckList = new List<PropertyAndCheck>();
                foreach (var property in propertyList)
                {
                    propertyAndCheckList.Add(new PropertyAndCheck(db, category, property));
                }
            }
            #endregion

            #region Properties
            private string _name;
            public string name
            {
                get { return _name; }
                set
                {
                    _name = value;
                    RaisePropertyChanged();
                }
            }

            private List<PropertyAndCheck> _propertyAndCheckList;
            public List<PropertyAndCheck> propertyAndCheckList
            {
                get { return _propertyAndCheckList; }
                set
                {
                    _propertyAndCheckList = value;
                    RaisePropertyChanged();
                }
            }
            #endregion
        }
        #endregion
        #region PropertyAndCheck
        public class PropertyAndCheck : MVVMRelated
        {
            #region Constructors
            public PropertyAndCheck(EFDbContext db, Category category, Property property)
            {
                id = property.Id;
                name = property.Name;
                description = property.Description;
                propertyType = db.PropertyTypeSet.Where(x => x.Id == property.PropertyType.Id).FirstOrDefault().Name;
                PropertyCategory propertyCategory = db.PropertyCategorySet.Where(x => x.Category.Id == category.Id && x.Property.Id == property.Id).FirstOrDefault();
                if (propertyCategory != null)
                {
                    check = true;
                }
            }
            #endregion

            #region Properties
            private int _id;
            public int id
            {
                get { return _id; }
                set
                {
                    _id = value;
                    RaisePropertyChanged();
                }
            }

            private string _name;
            public string name
            {
                get { return _name; }
                set
                {
                    _name = value;
                    RaisePropertyChanged();
                }
            }

            private string _description;
            public string description
            {
                get { return _description; }
                set
                {
                    _description = value;
                    RaisePropertyChanged();
                }
            }

            private string _propertyType;
            public string propertyType
            {
                get { return _propertyType; }
                set
                {
                    _propertyType = value;
                    RaisePropertyChanged();
                }
            }

            private bool _check;
            public bool check
            {
                get { return _check; }
                set
                {
                    _check = value;
                    RaisePropertyChanged();
                }
            }
            #endregion
        }
        #endregion
        #endregion

        #region Comands
        private AsyncDelegateCommand _savePropertyCategoryCommand;
        public ICommand ButtonClickSavePropertyCategory
        {
            get
            {
                if (_savePropertyCategoryCommand == null)
                {
                    _savePropertyCategoryCommand = new AsyncDelegateCommand(SavePropertyCategory);
                }
                return _savePropertyCategoryCommand;
            }
        }
        #endregion

        #region Methods
        private async Task SavePropertyCategory(object o)
        {
            await Task.Delay(0);
            foreach(var propertyGroupAndProperties in propertyGroupAndPropertiesList)
            {
                foreach(var propertyAndCheck in propertyGroupAndProperties.propertyAndCheckList)
                {
                    PropertyCategory propertyCategory = db.PropertyCategorySet.Where(x => x.Category.Id == id && x.Property.Id == propertyAndCheck.id).FirstOrDefault();
                    if(propertyAndCheck.check && propertyCategory==null)
                    {
                        propertyCategory = new PropertyCategory();
                        propertyCategory.Category = db.CategorySet.Where(x => x.Id == id).FirstOrDefault();
                        propertyCategory.Property = db.PropertySet.Where(x => x.Id == propertyAndCheck.id).FirstOrDefault();
                        db.PropertyCategorySet.Add(propertyCategory);
                    }
                    else if(!propertyAndCheck.check && propertyCategory != null)
                    {
                        db.PropertyCategorySet.Remove(propertyCategory);
                    }
                }
            }
            db.SaveChanges();
        }
        #endregion
    }
}
