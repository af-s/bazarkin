﻿using Bazarkin.ControlCenter.Views;
using Bazarkin.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Bazarkin.ControlCenter.ViewModels
{
    class DashboardViewModel : MVVMRelated
    {
        EFDbContext db = new EFDbContext();
        #region Constructors
        public DashboardViewModel()
        {
            categoryListCount = db.CategorySet.Count() + " шт";
            goodListCount = db.GoodSet.Count() + " шт";
            organizationListCount = db.OrganizationSet.Count() + " шт";
            organizationUnitListCount = db.OrganizationUnitSet.Count() + " шт";
            manufacturerListCount = db.ManufacturerSet.Count() + " шт";
            propertyListCount = db.PropertySet.Count() + " шт";
            propertyGroupListCount = db.PropertyGroupSet.Count() + " шт";
            propertyTypeListCount =db.PropertyTypeSet.Count() + " шт";
            shoppingCenterListCount = 0 + " шт";  // Получить из БД
            userListCount = 0 + " шт";  // Получить из БД
            taskListCount = 0 + " шт";  // Получить из БД
        }
        #endregion

        #region Properties
        private string _categoryListCount;
        public string categoryListCount
        {
            get { return _categoryListCount; }
            set
            {
                _categoryListCount = value;
                RaisePropertyChanged();
            }
        }

        private string _goodListCount;
        public string goodListCount
        {
            get { return _goodListCount; }
            set
            {
                _goodListCount = value;
                RaisePropertyChanged();
            }
        }

        private string _organizationListCount;
        public string organizationListCount
        {
            get { return _organizationListCount; }
            set
            {
                _organizationListCount = value;
                RaisePropertyChanged();
            }
        }

        private string _organizationUnitListCount;
        public string organizationUnitListCount
        {
            get { return _organizationUnitListCount; }
            set
            {
                _organizationUnitListCount = value;
                RaisePropertyChanged();
            }
        }

        private string _manufacturerListCount;
        public string manufacturerListCount
        {
            get { return _manufacturerListCount; }
            set
            {
                _manufacturerListCount = value;
                RaisePropertyChanged();
            }
        }

        private string _propertyListCount;
        public string propertyListCount
        {
            get { return _propertyListCount; }
            set
            {
                _propertyListCount = value;
                RaisePropertyChanged();
            }
        }

        private string _propertyGroupListCount;
        public string propertyGroupListCount
        {
            get { return _propertyGroupListCount; }
            set
            {
                _propertyGroupListCount = value;
                RaisePropertyChanged();
            }
        }

        private string _propertyTypeListCount;
        public string propertyTypeListCount
        {
            get { return _propertyTypeListCount; }
            set
            {
                _propertyTypeListCount = value;
                RaisePropertyChanged();
            }
        }

        private string _shoppingCenterListCount;
        public string shoppingCenterListCount
        {
            get { return _shoppingCenterListCount; }
            set
            {
                _shoppingCenterListCount = value;
                RaisePropertyChanged();
            }
        }

        private string _userListCount;
        public string userListCount
        {
            get { return _userListCount; }
            set
            {
                _userListCount = value;
                RaisePropertyChanged();
            }
        }

        private string _taskListCount;
        public string taskListCount
        {
            get { return _taskListCount; }
            set
            {
                _taskListCount = value;
                RaisePropertyChanged();
            }
        }
        #endregion

        #region Methods
        #endregion

        #region Comands
        #endregion
    }
}
