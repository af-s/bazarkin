﻿using Bazarkin.ClassLibrary;
using Bazarkin.ControlCenter.Views;
using Bazarkin.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Bazarkin.ControlCenter.ViewModels
{
    class OrganizationUnitListViewModel : MVVMRelated
    {
        #region Constructors
        public OrganizationUnitListViewModel()
        {
            EFDbContext db = new EFDbContext();
            List<OrganizationUnit> organizationUnitList = db.OrganizationUnitSet.OrderBy(x => x.Organization.Name).ToList();
            organizationUnitRowList = new List<OrganizationUnitRow>();
            foreach (var organizationUnit in organizationUnitList)
            {
                organizationUnitRowList.Add(new OrganizationUnitRow(db, organizationUnit));
            }
        }
        #endregion

        #region Properties
        private List<OrganizationUnitRow> _organizationUnitRowList;
        public List<OrganizationUnitRow> organizationUnitRowList
        {
            get { return _organizationUnitRowList; }
            set
            {
                _organizationUnitRowList = value;
                RaisePropertyChanged();
            }
        }
        #endregion

        #region Classes
        public class OrganizationUnitRow : MVVMRelated
        {
            #region Constructors
            public OrganizationUnitRow(EFDbContext db, OrganizationUnit organizationUnit)
            {
                id = organizationUnit.Id;
                Address address = db.AddressSet.Where(x => x.ContactGroup.Id == organizationUnit.ContactGroup.Id).FirstOrDefault();
                if (address != null)
                {
                    addressStr = AddressLib.ObjectToString(address);
                }
                organizationName = db.OrganizationSet.Where(x => x.Id == organizationUnit.Organization.Id).FirstOrDefault().Name;
                active = organizationUnit.Active;
            }
            #endregion

            #region Properties
            private int _id;
            public int id
            {
                get { return _id; }
                set
                {
                    _id = value;
                    RaisePropertyChanged();
                }
            }

            private string _organizationName;
            public string organizationName
            {
                get { return _organizationName; }
                set
                {
                    _organizationName = value;
                    RaisePropertyChanged();
                }
            }

            private string _addressStr;
            public string addressStr
            {
                get { return _addressStr; }
                set
                {
                    _addressStr = value;
                    RaisePropertyChanged();
                }
            }

            private bool _active;
            public bool active
            {
                get { return _active; }
                set
                {
                    _active = value;
                    RaisePropertyChanged();
                }
            }
            #endregion

            #region Comands
            private AsyncDelegateCommand _editOrganizationUnitCommand;
            public ICommand ButtonClickEditOrganizationUnit
            {
                get
                {
                    if (_editOrganizationUnitCommand == null)
                    {
                        _editOrganizationUnitCommand = new AsyncDelegateCommand(EditOrganizationUnit);
                    }
                    return _editOrganizationUnitCommand;
                }
            }
            #endregion

            #region Methods
            private async Task EditOrganizationUnit(object o)
            {
                await Task.Delay(0);
                var window = new OrganizationUnitEditView(id);
                window.Show();
            }
            #endregion
        }
        #endregion

        #region Comands
        private AsyncDelegateCommand _newOrganizationUnitCommand;
        public ICommand ButtonClickNewOrganizationUnit
        {
            get
            {
                if (_newOrganizationUnitCommand == null)
                {
                    _newOrganizationUnitCommand = new AsyncDelegateCommand(NewOrganizationUnit);
                }
                return _newOrganizationUnitCommand;
            }
        }
        #endregion

        #region Methods
        private async Task NewOrganizationUnit(object o)
        {
            await Task.Delay(0);
            var window = new OrganizationUnitEditView(null);
            window.Show();
        }
        #endregion
    }
}
