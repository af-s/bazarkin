﻿using Bazarkin.ControlCenter.Views;
using Bazarkin.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Bazarkin.ControlCenter.ViewModels
{
    class PropertyGroupListViewModel : MVVMRelated
    {
        #region Constructors
        public PropertyGroupListViewModel()
        {
            EFDbContext db = new EFDbContext();
            List<PropertyGroup> propertyGroupeList = db.PropertyGroupSet.OrderBy(x => x.Name).ToList();
            propertyGroupRowList = new List<PropertyGroupRow>();
            foreach (var propertyGroupe in propertyGroupeList)
            {
                propertyGroupRowList.Add(new PropertyGroupRow(db, propertyGroupe));
            }
        }
        #endregion

        #region Properties
        private List<PropertyGroupRow> _propertyGroupRowList;
        public List<PropertyGroupRow> propertyGroupRowList
        {
            get { return _propertyGroupRowList; }
            set
            {
                _propertyGroupRowList = value;
                RaisePropertyChanged();
            }
        }
        #endregion

        #region Classes
        public class PropertyGroupRow : MVVMRelated
        {
            #region Constructors
            public PropertyGroupRow(EFDbContext db, PropertyGroup propertyGroup)
            {
                id = propertyGroup.Id;
                name = propertyGroup.Name;
                description = propertyGroup.Description;
                List<Property> propertyList = db.PropertySet.Where(x => x.PropertyGroup.Id == propertyGroup.Id).ToList();
                if (propertyList != null)
                {
                    propertyCount = propertyList.Count();
                }
                else
                {
                    propertyCount = 0;
                }
            }
            #endregion

            #region Properties
            private int _id;
            public int id
            {
                get { return _id; }
                set
                {
                    _id = value;
                    RaisePropertyChanged();
                }
            }

            private string _name;
            public string name
            {
                get { return _name; }
                set
                {
                    _name = value;
                    RaisePropertyChanged();
                }
            }

            private string _description;
            public string description
            {
                get { return _description; }
                set
                {
                    _description = value;
                    RaisePropertyChanged();
                }
            }

            private int _propertyCount;
            public int propertyCount
            {
                get { return _propertyCount; }
                set
                {
                    _propertyCount = value;
                    RaisePropertyChanged();
                }
            }
            #endregion

            #region Comands
            private AsyncDelegateCommand _editPropertyGroupCommand;
            public ICommand ButtonClickEditPropertyGroup
            {
                get
                {
                    if (_editPropertyGroupCommand == null)
                    {
                        _editPropertyGroupCommand = new AsyncDelegateCommand(EditPropertyGroup);
                    }
                    return _editPropertyGroupCommand;
                }
            }
            #endregion

            #region Methods
            private async Task EditPropertyGroup(object o)
            {
                await Task.Delay(0);
                var window = new PropertyGroupEditView(id);
                window.Show();
            }
            #endregion
        }
        #endregion

        #region Comands
        private AsyncDelegateCommand _newPropertyGroupCommand;
        public ICommand ButtonClickNewPropertyGroup
        {
            get
            {
                if (_newPropertyGroupCommand == null)
                {
                    _newPropertyGroupCommand = new AsyncDelegateCommand(NewPropertyGroup);
                }
                return _newPropertyGroupCommand;
            }
        }
        #endregion

        #region Methods
        private async Task NewPropertyGroup(object o)
        {
            await Task.Delay(0);
            var window = new PropertyGroupEditView(null);
            window.Show();
        }
        #endregion
    }
}
