﻿using Bazarkin.ControlCenter.Views;
using Bazarkin.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Bazarkin.ControlCenter.ViewModels
{
    class CategoryListViewModel : MVVMRelated
    {
        #region Constructors
        public CategoryListViewModel()
        {
            EFDbContext db = new EFDbContext();
            List<Category> categoryList = db.CategorySet.OrderBy(x => x.Name).ToList();
            categoryRowList = new List<CategoryRow>();
            foreach (var category in categoryList)
            {
                categoryRowList.Add(new CategoryRow(db, category));
            }
        }
        #endregion

        #region Properties
        private List<CategoryRow> _categoryRowList;
        public List<CategoryRow> categoryRowList
        {
            get { return _categoryRowList; }
            set
            {
                _categoryRowList = value;
                RaisePropertyChanged();
            }
        }
        #endregion

        #region Classes
        public class CategoryRow : MVVMRelated
        {
            #region Constructors
            public CategoryRow(EFDbContext db, Category category)
            {
                id = category.Id;
                name = category.Name;
                description = category.Description;
                categoryCount = db.CategorySet.Where(x=>x.Parent.Id==category.Id).Count();
                goodCount = db.GoodSet.Where(x => x.Category.Id == category.Id).Count();
                if(category.Parent != null && category.Parent.Id != null)
                {
                    parent = db.CategorySet.Where(x => x.Id == category.Parent.Id).FirstOrDefault().Name;
                }
                else
                {
                    parent = string.Empty;
                }
            }
            #endregion

            #region Properties
            private int _id;
            public int id
            {
                get { return _id; }
                set
                {
                    _id = value;
                    RaisePropertyChanged();
                }
            }

            private string _name;
            public string name
            {
                get { return _name; }
                set
                {
                    _name = value;
                    RaisePropertyChanged();
                }
            }

            private string _description;
            public string description
            {
                get { return _description; }
                set
                {
                    _description = value;
                    RaisePropertyChanged();
                }
            }

            private int _categoryCount;
            public int categoryCount
            {
                get { return _categoryCount; }
                set
                {
                    _categoryCount = value;
                    RaisePropertyChanged();
                }
            }

            private int _goodCount;
            public int goodCount
            {
                get { return _goodCount; }
                set
                {
                    _goodCount = value;
                    RaisePropertyChanged();
                }
            }

            private string _parent;
            public string parent
            {
                get { return _parent; }
                set
                {
                    _parent = value;
                    RaisePropertyChanged();
                }
            }
            #endregion

            #region Comands
            private AsyncDelegateCommand _editCategoryCommand;
            public ICommand ButtonClickEditCategory
            {
                get
                {
                    if (_editCategoryCommand == null)
                    {
                        _editCategoryCommand = new AsyncDelegateCommand(EditCategory);
                    }
                    return _editCategoryCommand;
                }
            }
            #endregion

            #region Methods
            private async Task EditCategory(object o)
            {
                await Task.Delay(0);
                var window = new CategoryEditView(id);
                window.Show();
            }
            #endregion
        }
        #endregion

        #region Comands
        private AsyncDelegateCommand _newCategoryCommand;
        public ICommand ButtonClickNewCategory
        {
            get
            {
                if (_newCategoryCommand == null)
                {
                    _newCategoryCommand = new AsyncDelegateCommand(NewCategory);
                }
                return _newCategoryCommand;
            }
        }
        #endregion

        #region Methods
        private async Task NewCategory(object o)
        {
            await Task.Delay(0);
            var window = new CategoryEditView(null);
            window.Show();
        }
        #endregion
    }
}
