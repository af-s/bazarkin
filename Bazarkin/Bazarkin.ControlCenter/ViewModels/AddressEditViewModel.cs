﻿using Bazarkin.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Bazarkin.ControlCenter.ViewModels
{
    class AddressEditViewModel : MVVMRelated
    {
        EFDbContext db = new EFDbContext();

        #region Constructors
        public AddressEditViewModel(int? idAddress, int idContactGroup)
        {
            Address address;
            if (idAddress == null)
            {
                address = new Address();
            }
            else
            {
                address = db.AddressSet.Where(x => x.Id == idAddress).FirstOrDefault();
            }
            this.idAddress = address.Id;
            this.idContactGroup = idContactGroup;

            country = address.Country;
            region = address.Region;
            city = address.City;
            street = address.Street;
            building = address.Building;
            structure = address.Structure;
            room = address.Room;
            shoppingCenter = address.ShoppingCenter;
            floor=0;
            row = address.Row;
            sector = address.Sector;
            pavilion = address.Pavilion;
            index = address.Index;

            description = address.Description;
            view = address.View;
        }
        #endregion

        #region Properties
        private int _idAddress;
        public int idAddress
        {
            get { return _idAddress; }
            set
            {
                _idAddress = value;
                RaisePropertyChanged();
            }
        }

        private int _idContactGroup;
        public int idContactGroup
        {
            get { return _idContactGroup; }
            set
            {
                _idContactGroup = value;
                RaisePropertyChanged();
            }
        }

        private string _country;
        public string country
        {
            get { return _country; }
            set
            {
                _country = value;
                RaisePropertyChanged();
            }
        }

        private string _region;
        public string region
        {
            get { return _region; }
            set
            {
                _region = value;
                RaisePropertyChanged();
            }
        }

        private string _city;
        public string city
        {
            get { return _city; }
            set
            {
                _city = value;
                RaisePropertyChanged();
            }
        }

        private string _street;
        public string street
        {
            get { return _street; }
            set
            {
                _street = value;
                RaisePropertyChanged();
            }
        }

        private string _building;
        public string building
        {
            get { return _building; }
            set
            {
                _building = value;
                RaisePropertyChanged();
            }
        }

        private string _structure;
        public string structure
        {
            get { return _structure; }
            set
            {
                _structure = value;
                RaisePropertyChanged();
            }
        }

        private string _room;
        public string room
        {
            get { return _room; }
            set
            {
                _room = value;
                RaisePropertyChanged();
            }
        }

        private string _shoppingCenter;
        public string shoppingCenter
        {
            get { return _shoppingCenter; }
            set
            {
                _shoppingCenter = value;
                RaisePropertyChanged();
            }
        }

        private int _floor;
        public int floor
        {
            get { return _floor; }
            set
            {
                _floor = value;
                RaisePropertyChanged();
            }
        }

        private string _row;
        public string row
        {
            get { return _row; }
            set
            {
                _row = value;
                RaisePropertyChanged();
            }
        }

        private string _sector;
        public string sector
        {
            get { return _sector; }
            set
            {
                _sector = value;
                RaisePropertyChanged();
            }
        }

        private string _pavilion;
        public string pavilion
        {
            get { return _pavilion; }
            set
            {
                _pavilion = value;
                RaisePropertyChanged();
            }
        }

        private string _index;
        public string index
        {
            get { return _index; }
            set
            {
                _index = value;
                RaisePropertyChanged();
            }
        }

        private string _description;
        public string description
        {
            get { return _description; }
            set
            {
                _description = value;
                RaisePropertyChanged();
            }
        }

        private bool _view;
        public bool view
        {
            get { return _view; }
            set
            {
                _view = value;
                RaisePropertyChanged();
            }
        }
        #endregion

        #region Comands
        private AsyncDelegateCommand _saveAddressCommand;
        public ICommand ButtonClickSaveAddress
        {
            get
            {
                if (_saveAddressCommand == null)
                {
                    _saveAddressCommand = new AsyncDelegateCommand(SaveAddress);
                }
                return _saveAddressCommand;
            }
        }
        #endregion

        #region Methods
        private async Task SaveAddress(object o)
        {
            await Task.Delay(0);
            Address address;
            if (idAddress == 0)
            {
                address = new Address();
                address.ContactGroup = db.ContactGroupSet.Where(x => x.Id == idContactGroup).FirstOrDefault();
            }
            else
            {
                address = db.AddressSet.Where(x => x.Id == idAddress).FirstOrDefault();
            }

            address.Country=country;
            address.Region = region;
            address.City = city;
            address.Street =street;
            address.Building = building;
            address.Structure = structure;
            address.Room = room;
            address.ShoppingCenter = shoppingCenter;
            //address.Floor = floor;
            address.Row = row;
            address.Sector = sector;
            address.Pavilion = pavilion;
            address.Index = index;

            address.Description = description;
            address.View = view;

            if (address.Id == 0)
            {
                db.AddressSet.Add(address);
            }

            db.SaveChanges();
        }
        #endregion
    }
}
