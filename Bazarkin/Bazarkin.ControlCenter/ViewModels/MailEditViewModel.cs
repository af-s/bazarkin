﻿using Bazarkin.Domain;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Bazarkin.ControlCenter.ViewModels
{
    class MailEditViewModel : MVVMRelated
    {
        EFDbContext db = new EFDbContext();

        #region Constructors
        public MailEditViewModel(int? idMail, int idContactGroup)
        {
            Mail mail;
            if (idMail == null)
            {
                mail = new Mail();
            }
            else
            {
                mail = db.MailSet.Where(x => x.Id == idMail).FirstOrDefault();
            }
            this.idMail = mail.Id;
            this.idContactGroup = idContactGroup;
            value = mail.Value;
            description = mail.Description;
        }
        #endregion

        #region Properties
        private int _idMail;
        public int idMail
        {
            get { return _idMail; }
            set
            {
                _idMail = value;
                RaisePropertyChanged();
            }
        }

        private int _idContactGroup;
        public int idContactGroup
        {
            get { return _idContactGroup; }
            set
            {
                _idContactGroup = value;
                RaisePropertyChanged();
            }
        }

        private string _value;
        public string value
        {
            get { return _value; }
            set
            {
                _value = value;
                RaisePropertyChanged();
            }
        }

        private string _description;
        public string description
        {
            get { return _description; }
            set
            {
                _description = value;
                RaisePropertyChanged();
            }
        }
        #endregion

        #region Comands
        private AsyncDelegateCommand _saveMailCommand;
        public ICommand ButtonClickSaveMail
        {
            get
            {
                if (_saveMailCommand == null)
                {
                    _saveMailCommand = new AsyncDelegateCommand(SaveMail);
                }
                return _saveMailCommand;
            }
        }
        #endregion

        #region Methods
        private async Task SaveMail(object o)
        {
            await Task.Delay(0);
            Mail mail;
            if (idMail == 0)
            {
                mail = new Mail();
                mail.ContactGroup = db.ContactGroupSet.Where(x => x.Id == idContactGroup).FirstOrDefault();
            }
            else
            {
                mail = db.MailSet.Where(x => x.Id == idMail).FirstOrDefault();
            }

            mail.Value = value;
            mail.Description = description;

            if (mail.Id == 0)
            {
                db.MailSet.Add(mail);
            }

            db.SaveChanges();
        }
        #endregion
    }
}
