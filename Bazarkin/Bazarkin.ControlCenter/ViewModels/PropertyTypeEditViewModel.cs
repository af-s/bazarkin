﻿using Bazarkin.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Bazarkin.ControlCenter.ViewModels
{
    class PropertyTypeEditViewModel : MVVMRelated
    {
        EFDbContext db = new EFDbContext();

        #region Constructors
        public PropertyTypeEditViewModel(int? id)
        {
            PropertyType propertyType;
            if (id == null)
            {
                propertyType = new PropertyType();
                propertyType.FilterType = "list";
            }
            else
            {
                propertyType = db.PropertyTypeSet.Where(x => x.Id == id).FirstOrDefault();
            }
            this.id = propertyType.Id;
            name = propertyType.Name;
            description = propertyType.Description;
            valueFormat = propertyType.ValueFormat;
            filterType = propertyType.FilterType;

            filterTypeList = new Dictionary<string, string>();
            filterTypeList.Add("list", "Список");
            filterTypeList.Add("interval", "Интервал");
        }
        #endregion

        #region Properties
        private int _id;
        public int id
        {
            get { return _id; }
            set
            {
                _id = value;
                RaisePropertyChanged();
            }
        }

        private string _name;
        public string name
        {
            get { return _name; }
            set
            {
                _name = value;
                RaisePropertyChanged();
            }
        }

        private string _description;
        public string description
        {
            get { return _description; }
            set
            {
                _description = value;
                RaisePropertyChanged();
            }
        }

        private string _valueFormat;
        public string valueFormat
        {
            get { return _valueFormat; }
            set
            {
                _valueFormat = value;
                RaisePropertyChanged();
            }
        }

        private string _filterType;
        public string filterType
        {
            get { return _filterType; }
            set
            {
                _filterType = value;
                RaisePropertyChanged();
            }
        }

        private Dictionary<string, string> _filterTypeList;
        public Dictionary<string, string> filterTypeList
        {
            get { return _filterTypeList; }
            set
            {
                _filterTypeList = value;
                RaisePropertyChanged();
            }
        }
        #endregion

        #region Comands
        private AsyncDelegateCommand _savePropertyTypeCommand;
        public ICommand ButtonClickSavePropertyType
        {
            get
            {
                if (_savePropertyTypeCommand == null)
                {
                    _savePropertyTypeCommand = new AsyncDelegateCommand(SavePropertyType);
                }
                return _savePropertyTypeCommand;
            }
        }
        #endregion

        #region Methods
        private async Task SavePropertyType(object o)
        {
            await Task.Delay(0);
            PropertyType propertyType;
            if (id == 0)
            {
                propertyType = new PropertyType();
            }
            else
            {
                propertyType = db.PropertyTypeSet.Where(x => x.Id == id).FirstOrDefault();
            }

            propertyType.Name = name;
            propertyType.Description = description;
            propertyType.ValueFormat = valueFormat;
            propertyType.FilterType = filterType;

            if (propertyType.Id == 0)
            {
                db.PropertyTypeSet.Add(propertyType);
            }

            db.SaveChanges();
        }
        #endregion
    }
}
