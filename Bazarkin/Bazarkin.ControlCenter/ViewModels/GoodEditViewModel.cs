﻿using Bazarkin.ClassLibrary;
using Bazarkin.ControlCenter.Views;
using Bazarkin.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Bazarkin.ControlCenter.ViewModels
{
    class GoodEditViewModel : MVVMRelated
    {
        EFDbContext db = new EFDbContext();

        #region Constructors
        public GoodEditViewModel(int? id)
        {
            Good good;
            if (id == null)
            {
                flagSaved = false;
                good = new Good();
            }
            else
            {
                flagSaved = true;
                good = db.GoodSet.Where(x => x.Id == id).FirstOrDefault();
                idManufacturer = good.Manufacturer.Id;
                idCategory = good.Category.Id;

                Dictionary<int, string> modificationNameDict = GoodLib.GetModificationNameDict(good);

                List<ModificationGood> modificationGoodList = db.ModificationGoodSet.Where(x => x.Good.Id == good.Id).ToList();
                modificationGoodRowList = new List<ModificationGoodRow>();
                foreach (var modificationGood in modificationGoodList)
                {
                    modificationGoodRowList.Add(new ModificationGoodRow(db, good, modificationGood, modificationNameDict.Where(x => x.Key == modificationGood.Id).FirstOrDefault().Value));
                }
            }

            this.id = good.Id;
            name = good.Name;
            description = good.Description;
            active = good.Active;

            manufacturerList = db.ManufacturerSet.OrderBy(x=>x.Name).ToList();
            categoryList = db.CategorySet.OrderBy(x => x.Name).ToList();
        }
        #endregion

        #region Properties
        private int _id;
        public int id
        {
            get { return _id; }
            set
            {
                _id = value;
                RaisePropertyChanged();
            }
        }

        private string _name;
        public string name
        {
            get { return _name; }
            set
            {
                _name = value;
                RaisePropertyChanged();
            }
        }

        private string _description;
        public string description
        {
            get { return _description; }
            set
            {
                _description = value;
                RaisePropertyChanged();
            }
        }

        private bool _active;
        public bool active
        {
            get { return _active; }
            set
            {
                _active = value;
                RaisePropertyChanged();
            }
        }

        private int _idManufacturer;
        public int idManufacturer
        {
            get { return _idManufacturer; }
            set
            {
                _idManufacturer = value;
                RaisePropertyChanged();
            }
        }

        private List<Manufacturer> _manufacturerList;
        public List<Manufacturer> manufacturerList
        {
            get { return _manufacturerList; }
            set
            {
                _manufacturerList = value;
                RaisePropertyChanged();
            }
        }

        private int _idCategory;
        public int idCategory
        {
            get { return _idCategory; }
            set
            {
                _idCategory = value;
                RaisePropertyChanged();
            }
        }

        private List<Category> _categoryList;
        public List<Category> categoryList
        {
            get { return _categoryList; }
            set
            {
                _categoryList = value;
                RaisePropertyChanged();
            }
        }

        private List<ModificationGoodRow> _modificationGoodRowList;
        public List<ModificationGoodRow> modificationGoodRowList
        {
            get { return _modificationGoodRowList; }
            set
            {
                _modificationGoodRowList = value;
                RaisePropertyChanged();
            }
        }

        private bool _flagSaved;
        public bool flagSaved
        {
            get { return _flagSaved; }
            set
            {
                _flagSaved = value;
                RaisePropertyChanged();
            }
        }
        #endregion

        #region Classes
        public class ModificationGoodRow : MVVMRelated
        {
            #region Constructors
            public ModificationGoodRow(EFDbContext db, Good good, ModificationGood modificationGood, string modificationName)
            {
                idModificationGood = modificationGood.Id;
                idGood = good.Id;
                modificationGoodString = modificationName;
            }
            #endregion

            #region Properties
            private int _idModificationGood;
            public int idModificationGood
            {
                get { return _idModificationGood; }
                set
                {
                    _idModificationGood = value;
                    RaisePropertyChanged();
                }
            }

            private int _idGood;
            public int idGood
            {
                get { return _idGood; }
                set
                {
                    _idGood = value;
                    RaisePropertyChanged();
                }
            }

            private string _modificationGoodString;
            public string modificationGoodString
            {
                get { return _modificationGoodString; }
                set
                {
                    _modificationGoodString = value;
                    RaisePropertyChanged();
                }
            }
            #endregion

            #region Comands
            private AsyncDelegateCommand _editModificationGoodCommand;
            public ICommand ButtonClickEditModificationGood
            {
                get
                {
                    if (_editModificationGoodCommand == null)
                    {
                        _editModificationGoodCommand = new AsyncDelegateCommand(EditModificationGood);
                    }
                    return _editModificationGoodCommand;
                }
            }
            #endregion

            #region Methods
            private async Task EditModificationGood(object o)
            {
                await Task.Delay(0);
                var window = new ModificationGoodEditView(idGood, idModificationGood);
                window.Show();
            }
            #endregion
        }
        #endregion

        #region Comands
        private AsyncDelegateCommand _newModificationGoodCommand;
        public ICommand ButtonClickNewModificationGood
        {
            get
            {
                if (_newModificationGoodCommand == null)
                {
                    _newModificationGoodCommand = new AsyncDelegateCommand(NewModificationGood);
                }
                return _newModificationGoodCommand;
            }
        }

        private AsyncDelegateCommand _saveGoodCommand;
        public ICommand ButtonClickSaveGood
        {
            get
            {
                if (_saveGoodCommand == null)
                {
                    _saveGoodCommand = new AsyncDelegateCommand(SaveGood);
                }
                return _saveGoodCommand;
            }
        }
        #endregion

        #region Methods
        private async Task NewModificationGood(object o)
        {
            await Task.Delay(0);
            var window = new ModificationGoodEditView(id, null);
            window.Show();
        }

        private async Task SaveGood(object o)
        {
            await Task.Delay(0);
            Good good;
            if (id == 0)
            {
                good = new Good();
            }
            else
            {
                good = db.GoodSet.Where(x => x.Id == id).FirstOrDefault();
            }

            good.Name = name;
            good.Description = description;
            good.Active = active;

            good.Manufacturer = db.ManufacturerSet.Where(x => x.Id == idManufacturer).FirstOrDefault();
            good.Category = db.CategorySet.Where(x => x.Id == idCategory).FirstOrDefault();

            if (good.Id == 0)
            {
                db.GoodSet.Add(good);
            }

            db.SaveChanges();
            id = good.Id;
            flagSaved = true;
        }
        #endregion
    }
}
