﻿using Bazarkin.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Bazarkin.ControlCenter.ViewModels
{
    class ModificationGoodEditViewModel : MVVMRelated
    {
        EFDbContext db = new EFDbContext();

        #region Constructors
        public ModificationGoodEditViewModel(int idGood, int? idModification)
        {
            this.idGood = idGood;
            status = string.Empty;
            flagSave = true;
            ModificationGood modificationGood;
            if (idModification == null)
            {
                modificationGood = new ModificationGood();
            }
            else
            {
                modificationGood = db.ModificationGoodSet.Where(x => x.Id == idModification).FirstOrDefault();
            }
            this.idModification = modificationGood.Id;

            Good good = db.GoodSet.Where(x => x.Id == idGood).FirstOrDefault();
            Category category = db.CategorySet.Where(x => x.Id == good.Category.Id).FirstOrDefault();
            idCategory = category.Id;

            List<PropertyCategory> propertyCategoryList = db.PropertyCategorySet.Where(x => x.Category.Id == category.Id).ToList();
            List<PropertyGroup> propertyGroupList = new List<PropertyGroup>();
            foreach(var propertyCategory in propertyCategoryList)
            {
                propertyGroupList.Add(db.PropertyGroupSet.Where(x => x.Id == propertyCategory.Property.PropertyGroup.Id).FirstOrDefault());
            }
            propertyGroupList = propertyGroupList.Distinct().ToList();

            propertyGroupAndPropertiesList = new List<PropertyGroupAndProperties>();
            foreach(var propertyGroup in propertyGroupList)
            {
                propertyGroupAndPropertiesList.Add(new PropertyGroupAndProperties(db, category, modificationGood, propertyGroup));
            }
        }
        #endregion

        #region Properties
        private int _idCategory;
        public int idCategory
        {
            get { return _idCategory; }
            set
            {
                _idCategory = value;
                RaisePropertyChanged();
            }
        }

        private int _idGood;
        public int idGood
        {
            get { return _idGood; }
            set
            {
                _idGood = value;
                RaisePropertyChanged();
            }
        }

        private int? _idModification;
        public int? idModification
        {
            get { return _idModification; }
            set
            {
                _idModification = value;
                RaisePropertyChanged();
            }
        }

        private List<PropertyGroupAndProperties> _propertyGroupAndPropertiesList;
        public List<PropertyGroupAndProperties> propertyGroupAndPropertiesList
        {
            get { return _propertyGroupAndPropertiesList; }
            set
            {
                _propertyGroupAndPropertiesList = value;
                RaisePropertyChanged();
            }
        }

        private bool _flagSave;
        public bool flagSave
        {
            get { return _flagSave; }
            set
            {
                _flagSave = value;
                RaisePropertyChanged();
            }
        }

        private string _status;
        public string status
        {
            get { return _status; }
            set
            {
                _status = value;
                RaisePropertyChanged();
            }
        }
        #endregion

        #region Classes
        #region PropertyGroupAndProperties
        public class PropertyGroupAndProperties : MVVMRelated
        {
            #region Constructors
            public PropertyGroupAndProperties(EFDbContext db, Category category, ModificationGood modificationGood, PropertyGroup propertyGroup)
            {
                name = propertyGroup.Name;
                List<PropertyCategory> propertyCategoryList = db.PropertyCategorySet.Where(x => x.Category.Id == category.Id).ToList();
                List<Property> propertyList = new List<Property>();
                foreach(var propertyCategory in propertyCategoryList)
                {
                    propertyList.Add(db.PropertySet.Where(x => x.Id == propertyCategory.Property.Id).FirstOrDefault());
                }
                propertyList = propertyList.Where(x => x.PropertyGroup.Id == propertyGroup.Id).OrderBy(x=>x.Name).ToList();

                propertyAndPropertyValueList = new List<PropertyAndPropertyValue>();
                foreach (var property in propertyList)
                {
                    propertyAndPropertyValueList.Add(new PropertyAndPropertyValue(db, category, modificationGood, property));
                }
            }
            #endregion

            #region Properties
            private string _name;
            public string name
            {
                get { return _name; }
                set
                {
                    _name = value;
                    RaisePropertyChanged();
                }
            }

            private List<PropertyAndPropertyValue> _propertyAndPropertyValueList;
            public List<PropertyAndPropertyValue> propertyAndPropertyValueList
            {
                get { return _propertyAndPropertyValueList; }
                set
                {
                    _propertyAndPropertyValueList = value;
                    RaisePropertyChanged();
                }
            }
            #endregion
        }
        #endregion
        #region PropertyAndPropertyValue
        public class PropertyAndPropertyValue : MVVMRelated
        {
            #region Constructors
            public PropertyAndPropertyValue(EFDbContext db, Category category, ModificationGood modificationGood, Property property)
            {
                id = property.Id;
                name = property.Name;
                description = property.Description;
                propertyType = db.PropertyTypeSet.Where(x => x.Id == property.PropertyType.Id).FirstOrDefault().Name;

                if (modificationGood.Id != 0)
                {
                    PropertyValueGroup propertyValueGroup = db.PropertyValueGroupSet.Where(x => x.ModificationGood.Id == modificationGood.Id && x.PropertyValue.Property.Id == property.Id).FirstOrDefault();
                    if(propertyValueGroup==null)
                    {
                        idPropertyValue = 0;
                    }
                    else
                    {
                        idPropertyValue = db.PropertyValueSet.Where(x => x.Id == propertyValueGroup.PropertyValue.Id).FirstOrDefault().Id;
                    }
                }

                List<Good> goodList = db.GoodSet.Where(x => x.Category.Id == category.Id).ToList();
                List<ModificationGood> modificationGoodList = new List<ModificationGood>();
                foreach(var good in goodList)
                {
                    modificationGoodList.AddRange(db.ModificationGoodSet.Where(x => x.Good.Id == good.Id));
                }
                propertyValueList = new List<PropertyValue>();
                foreach(var modificationGoodItem in modificationGoodList)
                {
                    PropertyValueGroup propertyValueGroupItem = db.PropertyValueGroupSet.Where(x => x.ModificationGood.Id == modificationGoodItem.Id && x.PropertyValue.Property.Id == property.Id).FirstOrDefault();
                    if (propertyValueGroupItem != null)
                    {
                        propertyValueList.Add(db.PropertyValueSet.Where(x => x.Id == propertyValueGroupItem.PropertyValue.Id).FirstOrDefault());
                    }
                }
                propertyValueList.Add(new PropertyValue());
                propertyValueList = propertyValueList.Distinct().OrderBy(x => x.Value).ToList();
            }
            #endregion

            #region Properties
            private int _id;
            public int id
            {
                get { return _id; }
                set
                {
                    _id = value;
                    RaisePropertyChanged();
                }
            }

            private string _name;
            public string name
            {
                get { return _name; }
                set
                {
                    _name = value;
                    RaisePropertyChanged();
                }
            }

            private string _description;
            public string description
            {
                get { return _description; }
                set
                {
                    _description = value;
                    RaisePropertyChanged();
                }
            }

            private string _propertyType;
            public string propertyType
            {
                get { return _propertyType; }
                set
                {
                    _propertyType = value;
                    RaisePropertyChanged();
                }
            }

            private List<PropertyValue> _propertyValueList;
            public List<PropertyValue> propertyValueList
            {
                get { return _propertyValueList; }
                set
                {
                    _propertyValueList = value;
                    RaisePropertyChanged();
                }
            }

            private int _idPropertyValue;
            public int idPropertyValue
            {
                get { return _idPropertyValue; }
                set
                {
                    _idPropertyValue = value;
                    RaisePropertyChanged();
                }
            }

            private string _value;
            public string value
            {
                get { return _value; }
                set
                {
                    _value = value;
                    RaisePropertyChanged();
                }
            }
            #endregion
        }
        #endregion
        #endregion

        #region Comands
        private AsyncDelegateCommand _saveModificationGoodCommand;
        public ICommand ButtonClickSaveModificationGood
        {
            get
            {
                if (_saveModificationGoodCommand == null)
                {
                    _saveModificationGoodCommand = new AsyncDelegateCommand(SaveModificationGood);
                }
                return _saveModificationGoodCommand;
            }
        }
        #endregion

        #region Methods
        private async Task SaveModificationGood(object o)
        {
            await Task.Delay(0);
            flagSave = false;
            status = "[Процесс] Сохранение...";

            ModificationGood modificationGood;
            if (idModification == 0)
            {
                modificationGood = new ModificationGood();
                modificationGood.Good = db.GoodSet.Where(x => x.Id == idGood).FirstOrDefault();
            }
            else
            {
                modificationGood = db.ModificationGoodSet.Where(x=>x.Id==idModification).FirstOrDefault();
            }
            
            foreach (var propertyGroupAndProperties in propertyGroupAndPropertiesList)
            {
                foreach (var propertyAndPropertyValue in propertyGroupAndProperties.propertyAndPropertyValueList)
                {
                    PropertyValueGroup propertyValueGroup = db.PropertyValueGroupSet.Where(x => x.ModificationGood.Id == idModification && x.PropertyValue.Property.Id == propertyAndPropertyValue.id).FirstOrDefault();

                    Property property = db.PropertySet.Where(x=>x.Id== propertyAndPropertyValue.id).FirstOrDefault();
                    PropertyValue propertyValue;

                    if (propertyValueGroup == null)
                    {
                        propertyValueGroup = new PropertyValueGroup();
                        propertyValueGroup.ModificationGood = modificationGood;
                        propertyValue = new PropertyValue();
                        propertyValueGroup.PropertyValue = propertyValue;
                        propertyValueGroup.PropertyValue.Property = property;
                        propertyValueGroup.Property = property;
                    }
                    else
                    {
                        propertyValue = db.PropertyValueSet.Where(x => x.Id == propertyValueGroup.PropertyValue.Id).FirstOrDefault();
                    }

                    if (propertyAndPropertyValue.idPropertyValue != 0)
                    {
                        // Запись из комбобокса
                        propertyValueGroup.PropertyValue = db.PropertyValueSet.Where(x => x.Id == propertyAndPropertyValue.idPropertyValue).FirstOrDefault();
                        if (propertyValueGroup.Id == 0)
                        {
                            db.PropertyValueGroupSet.Add(propertyValueGroup);
                        }
                    }
                    else if(propertyAndPropertyValue.idPropertyValue == 0 && propertyAndPropertyValue.value!=null && propertyAndPropertyValue.value.Trim() != string.Empty)
                    {
                        // Запись из поля
                        propertyValueGroup.PropertyValue.Value = propertyAndPropertyValue.value.Trim();
                        if (propertyValueGroup.Id == 0)
                        {
                            db.PropertyValueGroupSet.Add(propertyValueGroup);
                        }
                    }
                    else
                    {
                        // Удаление связи со значением
                        if (propertyValueGroup.Id != 0)
                        {
                            db.PropertyValueGroupSet.Remove(propertyValueGroup);
                        }
                        // Удаление значения
                        if(propertyValue.Id!=0 && db.PropertyValueGroupSet.Where(x=>x.PropertyValue.Id== propertyValue.Id).Count()<=1)
                        {
                            db.PropertyValueSet.Remove(propertyValue);
                        }
                    }
                }
            }
            db.SaveChanges();
            status = "[Результат] Сохранено";
            idModification = modificationGood.Id;
        }
        #endregion
    }
}
