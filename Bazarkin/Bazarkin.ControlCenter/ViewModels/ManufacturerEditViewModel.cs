﻿using Bazarkin.ClassLibrary;
using Bazarkin.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Bazarkin.ControlCenter.ViewModels
{
    class ManufacturerEditViewModel : MVVMRelated
    {
        EFDbContext db = new EFDbContext();

        #region Constructors
        public ManufacturerEditViewModel(int? id)
        {
            Manufacturer manufacturer;
            if (id == null)
            {
                manufacturer = new Manufacturer();
            }
            else
            {
                manufacturer = db.ManufacturerSet.Where(x => x.Id == id).FirstOrDefault();

                this.id = manufacturer.Id;
                name = manufacturer.Name;
                alias = manufacturer.Alias;
                description = manufacturer.Description;
                avatar = manufacturer.Avatar;
            }
        }
        #endregion

        #region Properties
        private int _id;
        public int id
        {
            get { return _id; }
            set
            {
                _id = value;
                RaisePropertyChanged();
            }
        }

        private string _name;
        public string name
        {
            get { return _name; }
            set
            {
                _name = value;
                alias = Translit.RuToTranslite(_name);
                RaisePropertyChanged();
            }
        }

        private string _alias;
        public string alias
        {
            get { return _alias; }
            set
            {
                _alias = value;
                RaisePropertyChanged();
            }
        }

        private string _description;
        public string description
        {
            get { return _description; }
            set
            {
                _description = value;
                RaisePropertyChanged();
            }
        }

        private string _avatar;
        public string avatar
        {
            get { return _avatar; }
            set
            {
                _avatar = value;
                RaisePropertyChanged();
            }
        }
        #endregion

        #region Comands
        private AsyncDelegateCommand _saveManufacturerCommand;
        public ICommand ButtonClickSaveManufacturer
        {
            get
            {
                if (_saveManufacturerCommand == null)
                {
                    _saveManufacturerCommand = new AsyncDelegateCommand(SaveManufacturer);
                }
                return _saveManufacturerCommand;
            }
        }
        #endregion

        #region Methods
        private async Task SaveManufacturer(object o)
        {
            await Task.Delay(0);
            Manufacturer manufacturer;
            if (id == 0)
            {
                manufacturer = new Manufacturer();
            }
            else
            {
                manufacturer = db.ManufacturerSet.Where(x => x.Id == id).FirstOrDefault();
            }

            manufacturer.Name = name;
            manufacturer.Alias = alias;
            manufacturer.Description = description;
            manufacturer.Avatar = avatar;

            if (manufacturer.Id == 0)
            {
                db.ManufacturerSet.Add(manufacturer);
            }

            db.SaveChanges();
        }
        #endregion
    }
}
