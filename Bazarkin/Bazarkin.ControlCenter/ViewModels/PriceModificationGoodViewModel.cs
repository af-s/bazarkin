﻿using Bazarkin.ClassLibrary;
using Bazarkin.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Bazarkin.ControlCenter.ViewModels
{
    class PriceModificationGoodViewModel : MVVMRelated
    {
        EFDbContext db = new EFDbContext();

        #region Constructors
        public PriceModificationGoodViewModel(int id)
        {
            this.id = id;
            OrganizationUnit organizationUnit = db.OrganizationUnitSet.Where(x => x.Id == id).FirstOrDefault();

            List<Category> categoryList = db.CategorySet.OrderBy(x => x.Name).ToList();

            categoryAndGoodAndModificationGoodAndPriceList = new List<CategoryAndGoodAndModificationGoodAndPrice>();
            foreach(var category in categoryList)
            {
                if (db.GoodSet.Where(x => x.Category.Id == category.Id).Count() > 0)
                {
                    categoryAndGoodAndModificationGoodAndPriceList.Add(new CategoryAndGoodAndModificationGoodAndPrice(db, organizationUnit, category));
                }
            }
        }
        #endregion

        #region Properties
        private int _id;
        public int id
        {
            get { return _id; }
            set
            {
                _id = value;
                RaisePropertyChanged();
            }
        }

        private List<CategoryAndGoodAndModificationGoodAndPrice> _categoryAndGoodAndModificationGoodAndPriceList;
        public List<CategoryAndGoodAndModificationGoodAndPrice> categoryAndGoodAndModificationGoodAndPriceList
        {
            get { return _categoryAndGoodAndModificationGoodAndPriceList; }
            set
            {
                _categoryAndGoodAndModificationGoodAndPriceList = value;
                RaisePropertyChanged();
            }
        }
        #endregion

        #region Classes
        #region CategoryAndGoodAndModificationGoodAndPrice
        public class CategoryAndGoodAndModificationGoodAndPrice : MVVMRelated
        {
            #region Constructors
            public CategoryAndGoodAndModificationGoodAndPrice(EFDbContext db, OrganizationUnit organizationUnit, Category category)
            {
                name = category.Name;

                goodAndModificationGoodList = new List<GoodAndModificationGood>();
                List<Good> goodList = db.GoodSet.Where(x => x.Category.Id == category.Id).OrderBy(x => x.Name).ToList();
                foreach(var good in goodList)
                {
                    goodAndModificationGoodList.Add(new GoodAndModificationGood(db, organizationUnit, good));
                }
            }
            #endregion

            #region Properties
            private string _name;
            public string name
            {
                get { return _name; }
                set
                {
                    _name = value;
                    RaisePropertyChanged();
                }
            }

            private List<GoodAndModificationGood> _goodAndModificationGoodList;
            public List<GoodAndModificationGood> goodAndModificationGoodList
            {
                get { return _goodAndModificationGoodList; }
                set
                {
                    _goodAndModificationGoodList = value;
                    RaisePropertyChanged();
                }
            }
            #endregion
        }
        #endregion
        #region GoodAndModificationGood
        public class GoodAndModificationGood : MVVMRelated
        {
            #region Constructors
            public GoodAndModificationGood(EFDbContext db, OrganizationUnit organizationUnit, Good good)
            {
                id = good.Id;
                Manufacturer manufacturer = db.ManufacturerSet.Where(x => x.Id == good.Manufacturer.Id).FirstOrDefault();
                name = string.Format("{0} {1}", manufacturer.Name, good.Name);

                List<ModificationGood> modificationGoodList = db.ModificationGoodSet.Where(x => x.Good.Id == good.Id).ToList();
                Dictionary<int, string> modificationNameDict = GoodLib.GetModificationNameDict(good);
                modificationGoodAndPriceList = new List<ModificationGoodAndPrice>();
                foreach (var modificationGood in modificationGoodList)
                {
                    modificationGoodAndPriceList.Add(new ModificationGoodAndPrice(db, organizationUnit, modificationGood, modificationNameDict.Where(x => x.Key == modificationGood.Id).FirstOrDefault().Value));
                }
            }
            #endregion

            #region Properties
            private int _id;
            public int id
            {
                get { return _id; }
                set
                {
                    _id = value;
                    RaisePropertyChanged();
                }
            }

            private string _name;
            public string name
            {
                get { return _name; }
                set
                {
                    _name = value;
                    RaisePropertyChanged();
                }
            }

            private List<ModificationGoodAndPrice> _modificationGoodAndPriceList;
            public List<ModificationGoodAndPrice> modificationGoodAndPriceList
            {
                get { return _modificationGoodAndPriceList; }
                set
                {
                    _modificationGoodAndPriceList = value;
                    RaisePropertyChanged();
                }
            }
            #endregion
        }
        #endregion
        #region ModificationGoodAndPrice
        public class ModificationGoodAndPrice : MVVMRelated
        {
            #region Constructors
            public ModificationGoodAndPrice(EFDbContext db, OrganizationUnit organizationUnit, ModificationGood modificationGood, string name)
            {
                id = modificationGood.Id;
                this.name = name;
                List<Price> priceList = db.PriceSet.Where(x => x.ModificationGood.Id == modificationGood.Id && x.OrganizationUnit.Id == organizationUnit.Id).ToList();
                if (priceList.Count != 0)
                {
                    price = priceList.Last().Value;
                }
            }
            #endregion

            #region Properties
            private int _id;
            public int id
            {
                get { return _id; }
                set
                {
                    _id = value;
                    RaisePropertyChanged();
                }
            }

            private string _name;
            public string name
            {
                get { return _name; }
                set
                {
                    _name = value;
                    RaisePropertyChanged();
                }
            }

            private decimal? _price;
            public decimal? price
            {
                get { return _price; }
                set
                {
                    _price = value;
                    RaisePropertyChanged();
                }
            }
            #endregion
        }
        #endregion
        #endregion

        #region Comands
        private AsyncDelegateCommand _savePriceModificationGoodCommand;
        public ICommand ButtonClickSavePriceModificationGood
        {
            get
            {
                if (_savePriceModificationGoodCommand == null)
                {
                    _savePriceModificationGoodCommand = new AsyncDelegateCommand(SavePriceModificationGood);
                }
                return _savePriceModificationGoodCommand;
            }
        }
        #endregion

        #region Methods
        private async Task SavePriceModificationGood(object o)
        {
            await Task.Delay(0);

            OrganizationUnit organizationUnit = db.OrganizationUnitSet.Where(x => x.Id == id).FirstOrDefault();

            foreach (var categoryAndGoodAndModificationGoodAndPrice in categoryAndGoodAndModificationGoodAndPriceList)
            {
                foreach(var goodAndModificationGood in categoryAndGoodAndModificationGoodAndPrice.goodAndModificationGoodList)
                {
                    foreach(var modificationGoodAndPrice in goodAndModificationGood.modificationGoodAndPriceList)
                    {
                        ModificationGood modificationGood = db.ModificationGoodSet.Where(x => x.Id == modificationGoodAndPrice.id).FirstOrDefault();
                        Price priceNew = new Price();
                        List<Price> priceOldList = db.PriceSet.Where(x => x.ModificationGood.Id == modificationGoodAndPrice.id && x.OrganizationUnit.Id == id).ToList();
                        Price priceOld;
                        if (priceOldList.Count != 0)
                        {
                            priceOld = priceOldList.LastOrDefault();
                        }
                        else
                        {
                            priceOld = null;
                        }
                        if (modificationGoodAndPrice.price == null)
                        {
                            if(priceOld != null && priceOld.Value != null)
                            {
                                priceNew.ModificationGood = modificationGood;
                                priceNew.OrganizationUnit = organizationUnit;
                                priceNew.Value = null;
                                priceNew.DateTime = DateTime.Now;

                                db.PriceSet.Add(priceNew);
                            }
                        }
                        else
                        {
                            if(priceOld != null && priceOld.Value == modificationGoodAndPrice.price)
                            {

                            }
                            else
                            {
                                priceNew.ModificationGood = modificationGood;
                                priceNew.OrganizationUnit = organizationUnit;
                                priceNew.Value = Convert.ToDecimal(modificationGoodAndPrice.price);
                                priceNew.DateTime = DateTime.Now;

                                db.PriceSet.Add(priceNew);
                            }
                        }
                    }
                }
            }

            if (db.PriceSet.Local.Count() > 0)
            {
                db.SaveChanges();
            }
        }
        #endregion
    }
}