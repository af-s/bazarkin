﻿using Bazarkin.ControlCenter.Views;
using Bazarkin.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Bazarkin.ControlCenter.ViewModels
{
    class PropertyTypeListViewModel : MVVMRelated
    {
        #region Constructors
        public PropertyTypeListViewModel()
        {
            EFDbContext db = new EFDbContext();
            List<PropertyType> propertyTypeList = db.PropertyTypeSet.OrderBy(x => x.Name).ToList();
            propertyTypeRowList = new List<PropertyTypeRow>();
            foreach (var propertyType in propertyTypeList)
            {
                propertyTypeRowList.Add(new PropertyTypeRow(db, propertyType));
            }
        }
        #endregion

        #region Properties
        private List<PropertyTypeRow> _propertyTypeRowList;
        public List<PropertyTypeRow> propertyTypeRowList
        {
            get { return _propertyTypeRowList; }
            set
            {
                _propertyTypeRowList = value;
                RaisePropertyChanged();
            }
        }
        #endregion

        #region Classes
        public class PropertyTypeRow : MVVMRelated
        {
            #region Constructors
            public PropertyTypeRow(EFDbContext db, PropertyType propertyType)
            {
                id = propertyType.Id;
                name = propertyType.Name;
                description = propertyType.Description;
                valueFormat = propertyType.ValueFormat;
                filterType = propertyType.FilterType;
            }
            #endregion

            #region Properties
            private int _id;
            public int id
            {
                get { return _id; }
                set
                {
                    _id = value;
                    RaisePropertyChanged();
                }
            }

            private string _name;
            public string name
            {
                get { return _name; }
                set
                {
                    _name = value;
                    RaisePropertyChanged();
                }
            }

            private string _description;
            public string description
            {
                get { return _description; }
                set
                {
                    _description = value;
                    RaisePropertyChanged();
                }
            }

            private string _valueFormat;
            public string valueFormat
            {
                get { return _valueFormat; }
                set
                {
                    _valueFormat = value;
                    RaisePropertyChanged();
                }
            }

            private string _filterType;
            public string filterType
            {
                get { return _filterType; }
                set
                {
                    _filterType = value;
                    RaisePropertyChanged();
                }
            }
            #endregion

            #region Comands
            private AsyncDelegateCommand _editPropertyTypeCommand;
            public ICommand ButtonClickEditPropertyType
            {
                get
                {
                    if (_editPropertyTypeCommand == null)
                    {
                        _editPropertyTypeCommand = new AsyncDelegateCommand(EditPropertyType);
                    }
                    return _editPropertyTypeCommand;
                }
            }
            #endregion

            #region Methods
            private async Task EditPropertyType(object o)
            {
                await Task.Delay(0);
                var window = new PropertyTypeEditView(id);
                window.Show();
            }
            #endregion
        }
        #endregion

        #region Comands
        private AsyncDelegateCommand _newPropertyTypeCommand;
        public ICommand ButtonClickNewPropertyType
        {
            get
            {
                if (_newPropertyTypeCommand == null)
                {
                    _newPropertyTypeCommand = new AsyncDelegateCommand(NewPropertyType);
                }
                return _newPropertyTypeCommand;
            }
        }
        #endregion

        #region Methods
        private async Task NewPropertyType(object o)
        {
            await Task.Delay(0);
            var window = new PropertyTypeEditView(null);
            window.Show();
        }
        #endregion
    }
}
