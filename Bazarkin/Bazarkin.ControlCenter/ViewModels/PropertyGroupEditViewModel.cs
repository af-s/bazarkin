﻿using Bazarkin.Domain;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Bazarkin.ControlCenter.ViewModels
{
    class PropertyGroupEditViewModel : MVVMRelated
    {
        EFDbContext db = new EFDbContext();

        #region Constructors
        public PropertyGroupEditViewModel(int? id)
        {
            PropertyGroup propertyGroup;
            if (id == null)
            {
                propertyGroup = new PropertyGroup();
            }
            else
            {
                propertyGroup = db.PropertyGroupSet.Where(x => x.Id == id).FirstOrDefault();
            }
            this.id = propertyGroup.Id;
            name = propertyGroup.Name;
            description = propertyGroup.Description;
        }
        #endregion

        #region Properties
        private int _id;
        public int id
        {
            get { return _id; }
            set
            {
                _id = value;
                RaisePropertyChanged();
            }
        }

        private string _name;
        public string name
        {
            get { return _name; }
            set
            {
                _name = value;
                RaisePropertyChanged();
            }
        }

        private string _description;
        public string description
        {
            get { return _description; }
            set
            {
                _description = value;
                RaisePropertyChanged();
            }
        }
        #endregion

        #region Comands
        private AsyncDelegateCommand _savePropertyGroupCommand;
        public ICommand ButtonClickSavePropertyGroup
        {
            get
            {
                if (_savePropertyGroupCommand == null)
                {
                    _savePropertyGroupCommand = new AsyncDelegateCommand(SavePropertyGroup);
                }
                return _savePropertyGroupCommand;
            }
        }
        #endregion

        #region Methods
        private async Task SavePropertyGroup(object o)
        {
            await Task.Delay(0);
            PropertyGroup propertyGroup;
            if (id == 0)
            {
                propertyGroup = new PropertyGroup();
            }
            else
            {
                propertyGroup = db.PropertyGroupSet.Where(x => x.Id == id).FirstOrDefault();
            }

            propertyGroup.Name = name;
            propertyGroup.Description = description;

            if (propertyGroup.Id == 0)
            {
                db.PropertyGroupSet.Add(propertyGroup);
            }

            db.SaveChanges();
        }
        #endregion
    }
}
