﻿using Bazarkin.ControlCenter.Views;
using Bazarkin.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Bazarkin.ControlCenter.ViewModels
{
    class GoodListViewModel : MVVMRelated
    {
        #region Constructors
        public GoodListViewModel()
        {
            EFDbContext db = new EFDbContext();
            List<Good> goodList = db.GoodSet.OrderBy(x => x.Name).ToList();
            goodRowList = new List<GoodRow>();
            foreach (var good in goodList)
            {
                goodRowList.Add(new GoodRow(db, good));
            }
        }
        #endregion

        #region Properties
        private List<GoodRow> _goodRowList;
        public List<GoodRow> goodRowList
        {
            get { return _goodRowList; }
            set
            {
                _goodRowList = value;
                RaisePropertyChanged();
            }
        }
        #endregion

        #region Classes
        public class GoodRow : MVVMRelated
        {
            #region Constructors
            public GoodRow(EFDbContext db, Good good)
            {
                id = good.Id;
                name = good.Name;
                description = good.Description;
                modificationCount = db.ModificationGoodSet.Where(x=>x.Good.Id==good.Id).Count();
                category = db.CategorySet.Where(x=>x.Id==good.Category.Id).FirstOrDefault().Name;
                manufacturer=db.ManufacturerSet.Where(x=>x.Id==good.Manufacturer.Id).FirstOrDefault().Name;
                active = good.Active;
            }
            #endregion

            #region Properties
            private int _id;
            public int id
            {
                get { return _id; }
                set
                {
                    _id = value;
                    RaisePropertyChanged();
                }
            }

            private string _name;
            public string name
            {
                get { return _name; }
                set
                {
                    _name = value;
                    RaisePropertyChanged();
                }
            }

            private string _description;
            public string description
            {
                get { return _description; }
                set
                {
                    _description = value;
                    RaisePropertyChanged();
                }
            }

            private int _modificationCount;
            public int modificationCount
            {
                get { return _modificationCount; }
                set
                {
                    _modificationCount = value;
                    RaisePropertyChanged();
                }
            }

            private string _category;
            public string category
            {
                get { return _category; }
                set
                {
                    _category = value;
                    RaisePropertyChanged();
                }
            }

            private string _manufacturer;
            public string manufacturer
            {
                get { return _manufacturer; }
                set
                {
                    _manufacturer = value;
                    RaisePropertyChanged();
                }
            }

            private bool _active;
            public bool active
            {
                get { return _active; }
                set
                {
                    _active = value;
                    RaisePropertyChanged();
                }
            }
            #endregion

            #region Comands
            private AsyncDelegateCommand _editGoodCommand;
            public ICommand ButtonClickEditGood
            {
                get
                {
                    if (_editGoodCommand == null)
                    {
                        _editGoodCommand = new AsyncDelegateCommand(EditGood);
                    }
                    return _editGoodCommand;
                }
            }
            #endregion

            #region Methods
            private async Task EditGood(object o)
            {
                await Task.Delay(0);
                var window = new GoodEditView(id);
                window.Show();
            }
            #endregion
        }
        #endregion

        #region Comands
        private AsyncDelegateCommand _newGoodCommand;
        public ICommand ButtonClickNewGood
        {
            get
            {
                if (_newGoodCommand == null)
                {
                    _newGoodCommand = new AsyncDelegateCommand(NewGood);
                }
                return _newGoodCommand;
            }
        }
        #endregion

        #region Methods
        private async Task NewGood(object o)
        {
            await Task.Delay(0);
            var window = new GoodEditView(null);
            window.Show();
        }
        #endregion
    }
}
