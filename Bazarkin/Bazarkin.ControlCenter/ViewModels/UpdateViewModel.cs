﻿using Bazarkin.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Bazarkin.ControlCenter.ViewModels
{
    class UpdateViewModel : MVVMRelated
    {
        public UpdateViewModel(string versionActual)
        {
            version = string.Format("Версия установленная: {0}", Constants.version);
            this.versionActual = string.Format("Версия актуальная: {0}", versionActual);
        }

        private string _version;
        public string version
        {
            get { return _version; }
            set
            {
                _version = value;
                RaisePropertyChanged();
            }
        }

        private string _versionActual;
        public string versionActual
        {
            get { return _versionActual; }
            set
            {
                _versionActual = value;
                RaisePropertyChanged();
            }
        }

        private AsyncDelegateCommand _updateNowCommand;
        public ICommand ButtonClickUpdateNow
        {
            get
            {
                if (_updateNowCommand == null)
                {
                    _updateNowCommand = new AsyncDelegateCommand(UpdateNow);
                }
                return _updateNowCommand;
            }
        }

        private async Task UpdateNow(object o)
        {
            // Ссылка на скачивание новой версии
            await Task.Delay(0);
            versionActual += ".X";
        }
    }
}
