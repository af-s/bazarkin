﻿using Bazarkin.ControlCenter.Views;
using Bazarkin.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Bazarkin.ControlCenter.ViewModels
{
    class PropertyListViewModel : MVVMRelated
    {
        #region Constructors
        public PropertyListViewModel()
        {
            EFDbContext db = new EFDbContext();
            List<Property> propertyList = db.PropertySet.OrderBy(x => x.Name).ToList();
            propertyRowList = new List<PropertyRow>();
            foreach (var property in propertyList)
            {
                propertyRowList.Add(new PropertyRow(db, property));
            }
        }
        #endregion

        #region Properties
        private List<PropertyRow> _propertyRowList;
        public List<PropertyRow> propertyRowList
        {
            get { return _propertyRowList; }
            set
            {
                _propertyRowList = value;
                RaisePropertyChanged();
            }
        }
        #endregion

        #region Classes
        public class PropertyRow : MVVMRelated
        {
            #region Constructors
            public PropertyRow(EFDbContext db, Property property)
            {
                id = property.Id;
                name = property.Name;
                description = property.Description;
                if (property.PropertyGroup != null)
                {
                    PropertyGroup propertyGroup = db.PropertyGroupSet.Where(x => x.Id == property.PropertyGroup.Id).FirstOrDefault();
                    propertyGroupName = propertyGroup.Name;
                }
                if (property.PropertyType != null)
                {
                    PropertyType propertyType = db.PropertyTypeSet.Where(x => x.Id == property.PropertyType.Id).FirstOrDefault();
                    propertyTypeName = propertyType.Name;
                }
            }
            #endregion

            #region Properties
            private int _id;
            public int id
            {
                get { return _id; }
                set
                {
                    _id = value;
                    RaisePropertyChanged();
                }
            }

            private string _name;
            public string name
            {
                get { return _name; }
                set
                {
                    _name = value;
                    RaisePropertyChanged();
                }
            }

            private string _description;
            public string description
            {
                get { return _description; }
                set
                {
                    _description = value;
                    RaisePropertyChanged();
                }
            }

            private string _propertyGroupName;
            public string propertyGroupName
            {
                get { return _propertyGroupName; }
                set
                {
                    _propertyGroupName = value;
                    RaisePropertyChanged();
                }
            }

            private string _propertyTypeName;
            public string propertyTypeName
            {
                get { return _propertyTypeName; }
                set
                {
                    _propertyTypeName = value;
                    RaisePropertyChanged();
                }
            }
            #endregion

            #region Comands
            private AsyncDelegateCommand _editPropertyCommand;
            public ICommand ButtonClickEditProperty
            {
                get
                {
                    if (_editPropertyCommand == null)
                    {
                        _editPropertyCommand = new AsyncDelegateCommand(EditProperty);
                    }
                    return _editPropertyCommand;
                }
            }
            #endregion

            #region Methods
            private async Task EditProperty(object o)
            {
                await Task.Delay(0);
                var window = new PropertyEditView(id);
                window.Show();
            }
            #endregion
        }
        #endregion

        #region Comands
        private AsyncDelegateCommand _newPropertyCommand;
        public ICommand ButtonClickNewProperty
        {
            get
            {
                if (_newPropertyCommand == null)
                {
                    _newPropertyCommand = new AsyncDelegateCommand(NewProperty);
                }
                return _newPropertyCommand;
            }
        }
        #endregion

        #region Methods
        private async Task NewProperty(object o)
        {
            await Task.Delay(0);
            var window = new PropertyEditView(null);
            window.Show();
        }
        #endregion
    }
}
