﻿using Bazarkin.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Bazarkin.ControlCenter.ViewModels
{
    class PropertyEditViewModel : MVVMRelated
    {
        EFDbContext db = new EFDbContext();

        #region Constructors
        public PropertyEditViewModel(int? id)
        {
            Property property;
            if (id == null)
            {
                property = new Property();
            }
            else
            {
                property = db.PropertySet.Where(x => x.Id == id).FirstOrDefault();
                propertyGroupId = property.PropertyGroup.Id;
                propertyTypeId = property.PropertyType.Id;
            }
            this.id = property.Id;
            name = property.Name;
            description = property.Description;

            propertyGroupList = db.PropertyGroupSet.OrderBy(x => x.Name).ToList();
            propertyTypeList = db.PropertyTypeSet.OrderBy(x => x.Name).ToList();
        }
        #endregion

        #region Properties
        private int _id;
        public int id
        {
            get { return _id; }
            set
            {
                _id = value;
                RaisePropertyChanged();
            }
        }

        private string _name;
        public string name
        {
            get { return _name; }
            set
            {
                _name = value;
                RaisePropertyChanged();
            }
        }

        private string _description;
        public string description
        {
            get { return _description; }
            set
            {
                _description = value;
                RaisePropertyChanged();
            }
        }

        private int _propertyGroupId;
        public int propertyGroupId
        {
            get { return _propertyGroupId; }
            set
            {
                _propertyGroupId = value;
                RaisePropertyChanged();
            }
        }

        private int _propertyTypeId;
        public int propertyTypeId
        {
            get { return _propertyTypeId; }
            set
            {
                _propertyTypeId = value;
                RaisePropertyChanged();
            }
        }

        private List<PropertyGroup> _propertyGroupList;
        public List<PropertyGroup> propertyGroupList
        {
            get { return _propertyGroupList; }
            set
            {
                _propertyGroupList = value;
                RaisePropertyChanged();
            }
        }

        private List<PropertyType> _propertyTypeList;
        public List<PropertyType> propertyTypeList
        {
            get { return _propertyTypeList; }
            set
            {
                _propertyTypeList = value;
                RaisePropertyChanged();
            }
        }
        #endregion

        #region Comands
        private AsyncDelegateCommand _savePropertyCommand;
        public ICommand ButtonClickSaveProperty
        {
            get
            {
                if (_savePropertyCommand == null)
                {
                    _savePropertyCommand = new AsyncDelegateCommand(SaveProperty);
                }
                return _savePropertyCommand;
            }
        }
        #endregion

        #region Methods
        private async Task SaveProperty(object o)
        {
            await Task.Delay(0);
            Property property;
            if (id == 0)
            {
                property = new Property();
            }
            else
            {
                property = db.PropertySet.Where(x => x.Id == id).FirstOrDefault();
            }

            property.Name = name;
            property.Description = description;
            property.PropertyGroup = db.PropertyGroupSet.Where(x => x.Id == propertyGroupId).FirstOrDefault();
            property.PropertyType = db.PropertyTypeSet.Where(x => x.Id == propertyTypeId).FirstOrDefault();

            if (property.Id == 0)
            {
                db.PropertySet.Add(property);
            }

            db.SaveChanges();
        }
        #endregion
    }
}
