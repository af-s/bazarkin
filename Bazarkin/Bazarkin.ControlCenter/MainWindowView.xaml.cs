﻿using Bazarkin.ControlCenter.Views;
using System.Windows;

namespace Bazarkin.ControlCenter
{
    /// <summary>
    /// Логика взаимодействия для MainWindowView.xaml
    /// </summary>
    public partial class MainWindowView : Window
    {
        public MainWindowView()
        {
            string versionActual = "0.0.2"; // Подгрузка с базы данных
            if (Constants.version== versionActual)
            {
                var updateView = new DashboardView();
                updateView.Show();
                Close();
            }
            else
            {
                var updateView = new UpdateView(versionActual);
                updateView.Show();
                Close();
            }
            InitializeComponent();
        }
    }
}
