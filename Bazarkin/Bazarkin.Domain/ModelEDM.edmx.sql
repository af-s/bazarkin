
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 11/03/2015 13:57:06
-- Generated from EDMX file: E:\Projects\BazarkinRepo\Bazarkin\Bazarkin.Domain\ModelEDM.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [BazarkinDB];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_GoodManufacturer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GoodSet] DROP CONSTRAINT [FK_GoodManufacturer];
GO
IF OBJECT_ID(N'[dbo].[FK_GoodCategory]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GoodSet] DROP CONSTRAINT [FK_GoodCategory];
GO
IF OBJECT_ID(N'[dbo].[FK_PropertyCategoryCategory]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PropertyCategorySet] DROP CONSTRAINT [FK_PropertyCategoryCategory];
GO
IF OBJECT_ID(N'[dbo].[FK_PropertyPropertyType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PropertySet] DROP CONSTRAINT [FK_PropertyPropertyType];
GO
IF OBJECT_ID(N'[dbo].[FK_PropertyPropertyValue]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PropertyValueSet] DROP CONSTRAINT [FK_PropertyPropertyValue];
GO
IF OBJECT_ID(N'[dbo].[FK_PropertyGroupProperty]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PropertySet] DROP CONSTRAINT [FK_PropertyGroupProperty];
GO
IF OBJECT_ID(N'[dbo].[FK_CartPrice]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CartSet] DROP CONSTRAINT [FK_CartPrice];
GO
IF OBJECT_ID(N'[dbo].[FK_OrganizationUnitPrice]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PriceSet] DROP CONSTRAINT [FK_OrganizationUnitPrice];
GO
IF OBJECT_ID(N'[dbo].[FK_PhoneContactGroup]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PhoneSet] DROP CONSTRAINT [FK_PhoneContactGroup];
GO
IF OBJECT_ID(N'[dbo].[FK_AddressContactGroup]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AddressSet] DROP CONSTRAINT [FK_AddressContactGroup];
GO
IF OBJECT_ID(N'[dbo].[FK_ClientContactGroup]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserSet] DROP CONSTRAINT [FK_ClientContactGroup];
GO
IF OBJECT_ID(N'[dbo].[FK_OrganizationContactGroup]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OrganizationSet] DROP CONSTRAINT [FK_OrganizationContactGroup];
GO
IF OBJECT_ID(N'[dbo].[FK_OrganizationUnitContactGroup]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OrganizationUnitSet] DROP CONSTRAINT [FK_OrganizationUnitContactGroup];
GO
IF OBJECT_ID(N'[dbo].[FK_ModificationGoodGood]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ModificationGoodSet] DROP CONSTRAINT [FK_ModificationGoodGood];
GO
IF OBJECT_ID(N'[dbo].[FK_PriceModificationGood]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PriceSet] DROP CONSTRAINT [FK_PriceModificationGood];
GO
IF OBJECT_ID(N'[dbo].[FK_PropertyValueGroupModificationGood]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PropertyValueGroupSet] DROP CONSTRAINT [FK_PropertyValueGroupModificationGood];
GO
IF OBJECT_ID(N'[dbo].[FK_PropertyValueGroupPropertyValue]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PropertyValueGroupSet] DROP CONSTRAINT [FK_PropertyValueGroupPropertyValue];
GO
IF OBJECT_ID(N'[dbo].[FK_MailContactGroup]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MailSet] DROP CONSTRAINT [FK_MailContactGroup];
GO
IF OBJECT_ID(N'[dbo].[FK_OrganizationUnitOrganization]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OrganizationUnitSet] DROP CONSTRAINT [FK_OrganizationUnitOrganization];
GO
IF OBJECT_ID(N'[dbo].[FK_PropertyValueGroupProperty]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PropertyValueGroupSet] DROP CONSTRAINT [FK_PropertyValueGroupProperty];
GO
IF OBJECT_ID(N'[dbo].[FK_OrganizationImageHeader]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[OrganizationSet] DROP CONSTRAINT [FK_OrganizationImageHeader];
GO
IF OBJECT_ID(N'[dbo].[FK_ViewedGood]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ViewedSet] DROP CONSTRAINT [FK_ViewedGood];
GO
IF OBJECT_ID(N'[dbo].[FK_СompareGood]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[СompareSet] DROP CONSTRAINT [FK_СompareGood];
GO
IF OBJECT_ID(N'[dbo].[FK_FavoriteGood]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[FavoriteSet] DROP CONSTRAINT [FK_FavoriteGood];
GO
IF OBJECT_ID(N'[dbo].[FK_UserAuthorization]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserSet] DROP CONSTRAINT [FK_UserAuthorization];
GO
IF OBJECT_ID(N'[dbo].[FK_HistoryHistory]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[HistorySet] DROP CONSTRAINT [FK_HistoryHistory];
GO
IF OBJECT_ID(N'[dbo].[FK_UserFunctionUser]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserFunctionSet] DROP CONSTRAINT [FK_UserFunctionUser];
GO
IF OBJECT_ID(N'[dbo].[FK_UserFunctionFunction]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserFunctionSet] DROP CONSTRAINT [FK_UserFunctionFunction];
GO
IF OBJECT_ID(N'[dbo].[FK_ImageGoodGood]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ImageGoodSet] DROP CONSTRAINT [FK_ImageGoodGood];
GO
IF OBJECT_ID(N'[dbo].[FK_ImageModificationGoodImageGood]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ImageModificationGoodSet] DROP CONSTRAINT [FK_ImageModificationGoodImageGood];
GO
IF OBJECT_ID(N'[dbo].[FK_ImageModificationGoodModificationGood]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ImageModificationGoodSet] DROP CONSTRAINT [FK_ImageModificationGoodModificationGood];
GO
IF OBJECT_ID(N'[dbo].[FK_CartUser]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CartSet] DROP CONSTRAINT [FK_CartUser];
GO
IF OBJECT_ID(N'[dbo].[FK_ViewedUser]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ViewedSet] DROP CONSTRAINT [FK_ViewedUser];
GO
IF OBJECT_ID(N'[dbo].[FK_СompareUser]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[СompareSet] DROP CONSTRAINT [FK_СompareUser];
GO
IF OBJECT_ID(N'[dbo].[FK_FavoriteUser]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[FavoriteSet] DROP CONSTRAINT [FK_FavoriteUser];
GO
IF OBJECT_ID(N'[dbo].[FK_CategoryCategory]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CategorySet] DROP CONSTRAINT [FK_CategoryCategory];
GO
IF OBJECT_ID(N'[dbo].[FK_PropertyCategoryProperty]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PropertyCategorySet] DROP CONSTRAINT [FK_PropertyCategoryProperty];
GO
IF OBJECT_ID(N'[dbo].[FK_HistoryUser]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[HistorySet] DROP CONSTRAINT [FK_HistoryUser];
GO
IF OBJECT_ID(N'[dbo].[FK_ImageOrganizationUnitOrganizationUnit]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ImageOrganizationUnitSet] DROP CONSTRAINT [FK_ImageOrganizationUnitOrganizationUnit];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[ManufacturerSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ManufacturerSet];
GO
IF OBJECT_ID(N'[dbo].[GoodSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GoodSet];
GO
IF OBJECT_ID(N'[dbo].[CategorySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CategorySet];
GO
IF OBJECT_ID(N'[dbo].[PropertyCategorySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PropertyCategorySet];
GO
IF OBJECT_ID(N'[dbo].[PropertyGroupSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PropertyGroupSet];
GO
IF OBJECT_ID(N'[dbo].[PropertySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PropertySet];
GO
IF OBJECT_ID(N'[dbo].[PropertyTypeSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PropertyTypeSet];
GO
IF OBJECT_ID(N'[dbo].[PropertyValueSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PropertyValueSet];
GO
IF OBJECT_ID(N'[dbo].[PriceSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PriceSet];
GO
IF OBJECT_ID(N'[dbo].[OrganizationUnitSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[OrganizationUnitSet];
GO
IF OBJECT_ID(N'[dbo].[OrganizationSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[OrganizationSet];
GO
IF OBJECT_ID(N'[dbo].[CartSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CartSet];
GO
IF OBJECT_ID(N'[dbo].[FavoriteSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[FavoriteSet];
GO
IF OBJECT_ID(N'[dbo].[СompareSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[СompareSet];
GO
IF OBJECT_ID(N'[dbo].[ViewedSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ViewedSet];
GO
IF OBJECT_ID(N'[dbo].[UserSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UserSet];
GO
IF OBJECT_ID(N'[dbo].[PhoneSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PhoneSet];
GO
IF OBJECT_ID(N'[dbo].[AddressSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AddressSet];
GO
IF OBJECT_ID(N'[dbo].[AuthorizationSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AuthorizationSet];
GO
IF OBJECT_ID(N'[dbo].[ContactGroupSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ContactGroupSet];
GO
IF OBJECT_ID(N'[dbo].[ModificationGoodSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ModificationGoodSet];
GO
IF OBJECT_ID(N'[dbo].[ImageGoodSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ImageGoodSet];
GO
IF OBJECT_ID(N'[dbo].[PropertyValueGroupSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PropertyValueGroupSet];
GO
IF OBJECT_ID(N'[dbo].[MailSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MailSet];
GO
IF OBJECT_ID(N'[dbo].[ImageHeaderSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ImageHeaderSet];
GO
IF OBJECT_ID(N'[dbo].[ConfigurationSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ConfigurationSet];
GO
IF OBJECT_ID(N'[dbo].[HistorySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[HistorySet];
GO
IF OBJECT_ID(N'[dbo].[UserFunctionSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UserFunctionSet];
GO
IF OBJECT_ID(N'[dbo].[FunctionSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[FunctionSet];
GO
IF OBJECT_ID(N'[dbo].[ImageModificationGoodSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ImageModificationGoodSet];
GO
IF OBJECT_ID(N'[dbo].[ImageOrganizationUnitSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ImageOrganizationUnitSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'ManufacturerSet'
CREATE TABLE [dbo].[ManufacturerSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [Description] nvarchar(max)  NULL,
    [Avatar] nvarchar(max)  NULL,
    [Alias] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'GoodSet'
CREATE TABLE [dbo].[GoodSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [Active] bit  NOT NULL,
    [Description] nvarchar(max)  NOT NULL,
    [Manufacturer_Id] int  NOT NULL,
    [Category_Id] int  NOT NULL
);
GO

-- Creating table 'CategorySet'
CREATE TABLE [dbo].[CategorySet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [Description] nvarchar(max)  NULL,
    [Alias] nvarchar(max)  NOT NULL,
    [Image] int  NULL,
    [Parent_Id] int  NULL
);
GO

-- Creating table 'PropertyCategorySet'
CREATE TABLE [dbo].[PropertyCategorySet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [OrderIndex] int  NULL,
    [Category_Id] int  NOT NULL,
    [Property_Id] int  NOT NULL
);
GO

-- Creating table 'PropertyGroupSet'
CREATE TABLE [dbo].[PropertyGroupSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NULL
);
GO

-- Creating table 'PropertySet'
CREATE TABLE [dbo].[PropertySet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NULL,
    [PropertyType_Id] int  NOT NULL,
    [PropertyGroup_Id] int  NOT NULL
);
GO

-- Creating table 'PropertyTypeSet'
CREATE TABLE [dbo].[PropertyTypeSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NULL,
    [ValueFormat] nvarchar(max)  NOT NULL,
    [FilterType] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'PropertyValueSet'
CREATE TABLE [dbo].[PropertyValueSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Value] nvarchar(max)  NOT NULL,
    [Property_Id] int  NOT NULL
);
GO

-- Creating table 'PriceSet'
CREATE TABLE [dbo].[PriceSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Value] decimal(18,0)  NULL,
    [DateTime] datetime  NOT NULL,
    [OrganizationUnit_Id] int  NOT NULL,
    [ModificationGood_Id] int  NOT NULL
);
GO

-- Creating table 'OrganizationUnitSet'
CREATE TABLE [dbo].[OrganizationUnitSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Description] nvarchar(max)  NULL,
    [PayerDate] datetime  NULL,
    [Active] bit  NOT NULL,
    [ContactGroup_Id] int  NOT NULL,
    [Organization_Id] int  NULL
);
GO

-- Creating table 'OrganizationSet'
CREATE TABLE [dbo].[OrganizationSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NULL,
    [Active] bit  NOT NULL,
    [IdClient] int  NOT NULL,
    [ContactGroup_Id] int  NOT NULL,
    [ImageHeader_Id] int  NULL
);
GO

-- Creating table 'CartSet'
CREATE TABLE [dbo].[CartSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Date] datetime  NOT NULL,
    [Count] nvarchar(max)  NOT NULL,
    [Price_Id] int  NOT NULL,
    [User_Id] int  NOT NULL
);
GO

-- Creating table 'FavoriteSet'
CREATE TABLE [dbo].[FavoriteSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Date] datetime  NOT NULL,
    [Good_Id] int  NOT NULL,
    [User_Id] int  NOT NULL
);
GO

-- Creating table 'СompareSet'
CREATE TABLE [dbo].[СompareSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Date] datetime  NOT NULL,
    [Good_Id] int  NOT NULL,
    [User_Id] int  NOT NULL
);
GO

-- Creating table 'ViewedSet'
CREATE TABLE [dbo].[ViewedSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Date] datetime  NOT NULL,
    [Good_Id] int  NOT NULL,
    [User_Id] int  NOT NULL
);
GO

-- Creating table 'UserSet'
CREATE TABLE [dbo].[UserSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Family] nvarchar(max)  NOT NULL,
    [Patronymic] nvarchar(max)  NULL,
    [ContactGroup_Id] int  NOT NULL,
    [Authorization_Id] int  NOT NULL
);
GO

-- Creating table 'PhoneSet'
CREATE TABLE [dbo].[PhoneSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Number] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NULL,
    [View] bit  NOT NULL,
    [OrderIndex] int  NULL,
    [ContactGroup_Id] int  NOT NULL
);
GO

-- Creating table 'AddressSet'
CREATE TABLE [dbo].[AddressSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Country] nvarchar(max)  NULL,
    [Street] nvarchar(max)  NULL,
    [Building] nvarchar(max)  NULL,
    [Structure] nvarchar(max)  NULL,
    [Room] nvarchar(max)  NULL,
    [Index] nvarchar(max)  NULL,
    [Region] nvarchar(max)  NULL,
    [City] nvarchar(max)  NULL,
    [Description] nvarchar(max)  NULL,
    [ShoppingCenter] nvarchar(max)  NULL,
    [Row] nvarchar(max)  NULL,
    [Sector] nvarchar(max)  NULL,
    [Pavilion] nvarchar(max)  NULL,
    [View] bit  NOT NULL,
    [Floor] nvarchar(max)  NULL,
    [OrderIndex] int  NULL,
    [ContactGroup_Id] int  NOT NULL
);
GO

-- Creating table 'AuthorizationSet'
CREATE TABLE [dbo].[AuthorizationSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Login] nvarchar(max)  NOT NULL,
    [Password] nvarchar(max)  NOT NULL,
    [Key] nvarchar(max)  NULL
);
GO

-- Creating table 'ContactGroupSet'
CREATE TABLE [dbo].[ContactGroupSet] (
    [Id] int IDENTITY(1,1) NOT NULL
);
GO

-- Creating table 'ModificationGoodSet'
CREATE TABLE [dbo].[ModificationGoodSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Model] nvarchar(max)  NULL,
    [Good_Id] int  NOT NULL
);
GO

-- Creating table 'ImageGoodSet'
CREATE TABLE [dbo].[ImageGoodSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [OrderIndex] int  NULL,
    [Good_Id] int  NOT NULL
);
GO

-- Creating table 'PropertyValueGroupSet'
CREATE TABLE [dbo].[PropertyValueGroupSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ModificationGood_Id] int  NOT NULL,
    [PropertyValue_Id] int  NOT NULL,
    [Property_Id] int  NOT NULL
);
GO

-- Creating table 'MailSet'
CREATE TABLE [dbo].[MailSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Value] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NULL,
    [ContactGroup_Id] int  NOT NULL
);
GO

-- Creating table 'ImageHeaderSet'
CREATE TABLE [dbo].[ImageHeaderSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Unique] bit  NOT NULL,
    [Description] nvarchar(max)  NULL
);
GO

-- Creating table 'ConfigurationSet'
CREATE TABLE [dbo].[ConfigurationSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Version] nvarchar(max)  NOT NULL,
    [OrganizationIdClientfinal] int  NOT NULL
);
GO

-- Creating table 'HistorySet'
CREATE TABLE [dbo].[HistorySet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [DateTime] datetime  NOT NULL,
    [Info] nvarchar(max)  NOT NULL,
    [ObjectType] nvarchar(max)  NOT NULL,
    [ObjectId] nvarchar(max)  NOT NULL,
    [Parent_Id] int  NULL,
    [User_Id] int  NOT NULL
);
GO

-- Creating table 'UserFunctionSet'
CREATE TABLE [dbo].[UserFunctionSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [User_Id] int  NOT NULL,
    [Function_Id] int  NOT NULL
);
GO

-- Creating table 'FunctionSet'
CREATE TABLE [dbo].[FunctionSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Key] nvarchar(max)  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NULL
);
GO

-- Creating table 'ImageModificationGoodSet'
CREATE TABLE [dbo].[ImageModificationGoodSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ImageGood_Id] int  NOT NULL,
    [ModificationGood_Id] int  NOT NULL
);
GO

-- Creating table 'ImageOrganizationUnitSet'
CREATE TABLE [dbo].[ImageOrganizationUnitSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [OrderIndex] nvarchar(max)  NOT NULL,
    [OrganizationUnit_Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'ManufacturerSet'
ALTER TABLE [dbo].[ManufacturerSet]
ADD CONSTRAINT [PK_ManufacturerSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'GoodSet'
ALTER TABLE [dbo].[GoodSet]
ADD CONSTRAINT [PK_GoodSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CategorySet'
ALTER TABLE [dbo].[CategorySet]
ADD CONSTRAINT [PK_CategorySet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PropertyCategorySet'
ALTER TABLE [dbo].[PropertyCategorySet]
ADD CONSTRAINT [PK_PropertyCategorySet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PropertyGroupSet'
ALTER TABLE [dbo].[PropertyGroupSet]
ADD CONSTRAINT [PK_PropertyGroupSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PropertySet'
ALTER TABLE [dbo].[PropertySet]
ADD CONSTRAINT [PK_PropertySet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PropertyTypeSet'
ALTER TABLE [dbo].[PropertyTypeSet]
ADD CONSTRAINT [PK_PropertyTypeSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PropertyValueSet'
ALTER TABLE [dbo].[PropertyValueSet]
ADD CONSTRAINT [PK_PropertyValueSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PriceSet'
ALTER TABLE [dbo].[PriceSet]
ADD CONSTRAINT [PK_PriceSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'OrganizationUnitSet'
ALTER TABLE [dbo].[OrganizationUnitSet]
ADD CONSTRAINT [PK_OrganizationUnitSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'OrganizationSet'
ALTER TABLE [dbo].[OrganizationSet]
ADD CONSTRAINT [PK_OrganizationSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CartSet'
ALTER TABLE [dbo].[CartSet]
ADD CONSTRAINT [PK_CartSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'FavoriteSet'
ALTER TABLE [dbo].[FavoriteSet]
ADD CONSTRAINT [PK_FavoriteSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'СompareSet'
ALTER TABLE [dbo].[СompareSet]
ADD CONSTRAINT [PK_СompareSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ViewedSet'
ALTER TABLE [dbo].[ViewedSet]
ADD CONSTRAINT [PK_ViewedSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'UserSet'
ALTER TABLE [dbo].[UserSet]
ADD CONSTRAINT [PK_UserSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PhoneSet'
ALTER TABLE [dbo].[PhoneSet]
ADD CONSTRAINT [PK_PhoneSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AddressSet'
ALTER TABLE [dbo].[AddressSet]
ADD CONSTRAINT [PK_AddressSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AuthorizationSet'
ALTER TABLE [dbo].[AuthorizationSet]
ADD CONSTRAINT [PK_AuthorizationSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ContactGroupSet'
ALTER TABLE [dbo].[ContactGroupSet]
ADD CONSTRAINT [PK_ContactGroupSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ModificationGoodSet'
ALTER TABLE [dbo].[ModificationGoodSet]
ADD CONSTRAINT [PK_ModificationGoodSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ImageGoodSet'
ALTER TABLE [dbo].[ImageGoodSet]
ADD CONSTRAINT [PK_ImageGoodSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PropertyValueGroupSet'
ALTER TABLE [dbo].[PropertyValueGroupSet]
ADD CONSTRAINT [PK_PropertyValueGroupSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'MailSet'
ALTER TABLE [dbo].[MailSet]
ADD CONSTRAINT [PK_MailSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ImageHeaderSet'
ALTER TABLE [dbo].[ImageHeaderSet]
ADD CONSTRAINT [PK_ImageHeaderSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ConfigurationSet'
ALTER TABLE [dbo].[ConfigurationSet]
ADD CONSTRAINT [PK_ConfigurationSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'HistorySet'
ALTER TABLE [dbo].[HistorySet]
ADD CONSTRAINT [PK_HistorySet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'UserFunctionSet'
ALTER TABLE [dbo].[UserFunctionSet]
ADD CONSTRAINT [PK_UserFunctionSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'FunctionSet'
ALTER TABLE [dbo].[FunctionSet]
ADD CONSTRAINT [PK_FunctionSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ImageModificationGoodSet'
ALTER TABLE [dbo].[ImageModificationGoodSet]
ADD CONSTRAINT [PK_ImageModificationGoodSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ImageOrganizationUnitSet'
ALTER TABLE [dbo].[ImageOrganizationUnitSet]
ADD CONSTRAINT [PK_ImageOrganizationUnitSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Manufacturer_Id] in table 'GoodSet'
ALTER TABLE [dbo].[GoodSet]
ADD CONSTRAINT [FK_GoodManufacturer]
    FOREIGN KEY ([Manufacturer_Id])
    REFERENCES [dbo].[ManufacturerSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GoodManufacturer'
CREATE INDEX [IX_FK_GoodManufacturer]
ON [dbo].[GoodSet]
    ([Manufacturer_Id]);
GO

-- Creating foreign key on [Category_Id] in table 'GoodSet'
ALTER TABLE [dbo].[GoodSet]
ADD CONSTRAINT [FK_GoodCategory]
    FOREIGN KEY ([Category_Id])
    REFERENCES [dbo].[CategorySet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GoodCategory'
CREATE INDEX [IX_FK_GoodCategory]
ON [dbo].[GoodSet]
    ([Category_Id]);
GO

-- Creating foreign key on [Category_Id] in table 'PropertyCategorySet'
ALTER TABLE [dbo].[PropertyCategorySet]
ADD CONSTRAINT [FK_PropertyCategoryCategory]
    FOREIGN KEY ([Category_Id])
    REFERENCES [dbo].[CategorySet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PropertyCategoryCategory'
CREATE INDEX [IX_FK_PropertyCategoryCategory]
ON [dbo].[PropertyCategorySet]
    ([Category_Id]);
GO

-- Creating foreign key on [PropertyType_Id] in table 'PropertySet'
ALTER TABLE [dbo].[PropertySet]
ADD CONSTRAINT [FK_PropertyPropertyType]
    FOREIGN KEY ([PropertyType_Id])
    REFERENCES [dbo].[PropertyTypeSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PropertyPropertyType'
CREATE INDEX [IX_FK_PropertyPropertyType]
ON [dbo].[PropertySet]
    ([PropertyType_Id]);
GO

-- Creating foreign key on [Property_Id] in table 'PropertyValueSet'
ALTER TABLE [dbo].[PropertyValueSet]
ADD CONSTRAINT [FK_PropertyPropertyValue]
    FOREIGN KEY ([Property_Id])
    REFERENCES [dbo].[PropertySet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PropertyPropertyValue'
CREATE INDEX [IX_FK_PropertyPropertyValue]
ON [dbo].[PropertyValueSet]
    ([Property_Id]);
GO

-- Creating foreign key on [PropertyGroup_Id] in table 'PropertySet'
ALTER TABLE [dbo].[PropertySet]
ADD CONSTRAINT [FK_PropertyGroupProperty]
    FOREIGN KEY ([PropertyGroup_Id])
    REFERENCES [dbo].[PropertyGroupSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PropertyGroupProperty'
CREATE INDEX [IX_FK_PropertyGroupProperty]
ON [dbo].[PropertySet]
    ([PropertyGroup_Id]);
GO

-- Creating foreign key on [Price_Id] in table 'CartSet'
ALTER TABLE [dbo].[CartSet]
ADD CONSTRAINT [FK_CartPrice]
    FOREIGN KEY ([Price_Id])
    REFERENCES [dbo].[PriceSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CartPrice'
CREATE INDEX [IX_FK_CartPrice]
ON [dbo].[CartSet]
    ([Price_Id]);
GO

-- Creating foreign key on [OrganizationUnit_Id] in table 'PriceSet'
ALTER TABLE [dbo].[PriceSet]
ADD CONSTRAINT [FK_OrganizationUnitPrice]
    FOREIGN KEY ([OrganizationUnit_Id])
    REFERENCES [dbo].[OrganizationUnitSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_OrganizationUnitPrice'
CREATE INDEX [IX_FK_OrganizationUnitPrice]
ON [dbo].[PriceSet]
    ([OrganizationUnit_Id]);
GO

-- Creating foreign key on [ContactGroup_Id] in table 'PhoneSet'
ALTER TABLE [dbo].[PhoneSet]
ADD CONSTRAINT [FK_PhoneContactGroup]
    FOREIGN KEY ([ContactGroup_Id])
    REFERENCES [dbo].[ContactGroupSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PhoneContactGroup'
CREATE INDEX [IX_FK_PhoneContactGroup]
ON [dbo].[PhoneSet]
    ([ContactGroup_Id]);
GO

-- Creating foreign key on [ContactGroup_Id] in table 'AddressSet'
ALTER TABLE [dbo].[AddressSet]
ADD CONSTRAINT [FK_AddressContactGroup]
    FOREIGN KEY ([ContactGroup_Id])
    REFERENCES [dbo].[ContactGroupSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AddressContactGroup'
CREATE INDEX [IX_FK_AddressContactGroup]
ON [dbo].[AddressSet]
    ([ContactGroup_Id]);
GO

-- Creating foreign key on [ContactGroup_Id] in table 'UserSet'
ALTER TABLE [dbo].[UserSet]
ADD CONSTRAINT [FK_ClientContactGroup]
    FOREIGN KEY ([ContactGroup_Id])
    REFERENCES [dbo].[ContactGroupSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ClientContactGroup'
CREATE INDEX [IX_FK_ClientContactGroup]
ON [dbo].[UserSet]
    ([ContactGroup_Id]);
GO

-- Creating foreign key on [ContactGroup_Id] in table 'OrganizationSet'
ALTER TABLE [dbo].[OrganizationSet]
ADD CONSTRAINT [FK_OrganizationContactGroup]
    FOREIGN KEY ([ContactGroup_Id])
    REFERENCES [dbo].[ContactGroupSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_OrganizationContactGroup'
CREATE INDEX [IX_FK_OrganizationContactGroup]
ON [dbo].[OrganizationSet]
    ([ContactGroup_Id]);
GO

-- Creating foreign key on [ContactGroup_Id] in table 'OrganizationUnitSet'
ALTER TABLE [dbo].[OrganizationUnitSet]
ADD CONSTRAINT [FK_OrganizationUnitContactGroup]
    FOREIGN KEY ([ContactGroup_Id])
    REFERENCES [dbo].[ContactGroupSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_OrganizationUnitContactGroup'
CREATE INDEX [IX_FK_OrganizationUnitContactGroup]
ON [dbo].[OrganizationUnitSet]
    ([ContactGroup_Id]);
GO

-- Creating foreign key on [Good_Id] in table 'ModificationGoodSet'
ALTER TABLE [dbo].[ModificationGoodSet]
ADD CONSTRAINT [FK_ModificationGoodGood]
    FOREIGN KEY ([Good_Id])
    REFERENCES [dbo].[GoodSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ModificationGoodGood'
CREATE INDEX [IX_FK_ModificationGoodGood]
ON [dbo].[ModificationGoodSet]
    ([Good_Id]);
GO

-- Creating foreign key on [ModificationGood_Id] in table 'PriceSet'
ALTER TABLE [dbo].[PriceSet]
ADD CONSTRAINT [FK_PriceModificationGood]
    FOREIGN KEY ([ModificationGood_Id])
    REFERENCES [dbo].[ModificationGoodSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PriceModificationGood'
CREATE INDEX [IX_FK_PriceModificationGood]
ON [dbo].[PriceSet]
    ([ModificationGood_Id]);
GO

-- Creating foreign key on [ModificationGood_Id] in table 'PropertyValueGroupSet'
ALTER TABLE [dbo].[PropertyValueGroupSet]
ADD CONSTRAINT [FK_PropertyValueGroupModificationGood]
    FOREIGN KEY ([ModificationGood_Id])
    REFERENCES [dbo].[ModificationGoodSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PropertyValueGroupModificationGood'
CREATE INDEX [IX_FK_PropertyValueGroupModificationGood]
ON [dbo].[PropertyValueGroupSet]
    ([ModificationGood_Id]);
GO

-- Creating foreign key on [PropertyValue_Id] in table 'PropertyValueGroupSet'
ALTER TABLE [dbo].[PropertyValueGroupSet]
ADD CONSTRAINT [FK_PropertyValueGroupPropertyValue]
    FOREIGN KEY ([PropertyValue_Id])
    REFERENCES [dbo].[PropertyValueSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PropertyValueGroupPropertyValue'
CREATE INDEX [IX_FK_PropertyValueGroupPropertyValue]
ON [dbo].[PropertyValueGroupSet]
    ([PropertyValue_Id]);
GO

-- Creating foreign key on [ContactGroup_Id] in table 'MailSet'
ALTER TABLE [dbo].[MailSet]
ADD CONSTRAINT [FK_MailContactGroup]
    FOREIGN KEY ([ContactGroup_Id])
    REFERENCES [dbo].[ContactGroupSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MailContactGroup'
CREATE INDEX [IX_FK_MailContactGroup]
ON [dbo].[MailSet]
    ([ContactGroup_Id]);
GO

-- Creating foreign key on [Organization_Id] in table 'OrganizationUnitSet'
ALTER TABLE [dbo].[OrganizationUnitSet]
ADD CONSTRAINT [FK_OrganizationUnitOrganization]
    FOREIGN KEY ([Organization_Id])
    REFERENCES [dbo].[OrganizationSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_OrganizationUnitOrganization'
CREATE INDEX [IX_FK_OrganizationUnitOrganization]
ON [dbo].[OrganizationUnitSet]
    ([Organization_Id]);
GO

-- Creating foreign key on [Property_Id] in table 'PropertyValueGroupSet'
ALTER TABLE [dbo].[PropertyValueGroupSet]
ADD CONSTRAINT [FK_PropertyValueGroupProperty]
    FOREIGN KEY ([Property_Id])
    REFERENCES [dbo].[PropertySet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PropertyValueGroupProperty'
CREATE INDEX [IX_FK_PropertyValueGroupProperty]
ON [dbo].[PropertyValueGroupSet]
    ([Property_Id]);
GO

-- Creating foreign key on [ImageHeader_Id] in table 'OrganizationSet'
ALTER TABLE [dbo].[OrganizationSet]
ADD CONSTRAINT [FK_OrganizationImageHeader]
    FOREIGN KEY ([ImageHeader_Id])
    REFERENCES [dbo].[ImageHeaderSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_OrganizationImageHeader'
CREATE INDEX [IX_FK_OrganizationImageHeader]
ON [dbo].[OrganizationSet]
    ([ImageHeader_Id]);
GO

-- Creating foreign key on [Good_Id] in table 'ViewedSet'
ALTER TABLE [dbo].[ViewedSet]
ADD CONSTRAINT [FK_ViewedGood]
    FOREIGN KEY ([Good_Id])
    REFERENCES [dbo].[GoodSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ViewedGood'
CREATE INDEX [IX_FK_ViewedGood]
ON [dbo].[ViewedSet]
    ([Good_Id]);
GO

-- Creating foreign key on [Good_Id] in table 'СompareSet'
ALTER TABLE [dbo].[СompareSet]
ADD CONSTRAINT [FK_СompareGood]
    FOREIGN KEY ([Good_Id])
    REFERENCES [dbo].[GoodSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_СompareGood'
CREATE INDEX [IX_FK_СompareGood]
ON [dbo].[СompareSet]
    ([Good_Id]);
GO

-- Creating foreign key on [Good_Id] in table 'FavoriteSet'
ALTER TABLE [dbo].[FavoriteSet]
ADD CONSTRAINT [FK_FavoriteGood]
    FOREIGN KEY ([Good_Id])
    REFERENCES [dbo].[GoodSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_FavoriteGood'
CREATE INDEX [IX_FK_FavoriteGood]
ON [dbo].[FavoriteSet]
    ([Good_Id]);
GO

-- Creating foreign key on [Authorization_Id] in table 'UserSet'
ALTER TABLE [dbo].[UserSet]
ADD CONSTRAINT [FK_UserAuthorization]
    FOREIGN KEY ([Authorization_Id])
    REFERENCES [dbo].[AuthorizationSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserAuthorization'
CREATE INDEX [IX_FK_UserAuthorization]
ON [dbo].[UserSet]
    ([Authorization_Id]);
GO

-- Creating foreign key on [Parent_Id] in table 'HistorySet'
ALTER TABLE [dbo].[HistorySet]
ADD CONSTRAINT [FK_HistoryHistory]
    FOREIGN KEY ([Parent_Id])
    REFERENCES [dbo].[HistorySet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_HistoryHistory'
CREATE INDEX [IX_FK_HistoryHistory]
ON [dbo].[HistorySet]
    ([Parent_Id]);
GO

-- Creating foreign key on [User_Id] in table 'UserFunctionSet'
ALTER TABLE [dbo].[UserFunctionSet]
ADD CONSTRAINT [FK_UserFunctionUser]
    FOREIGN KEY ([User_Id])
    REFERENCES [dbo].[UserSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserFunctionUser'
CREATE INDEX [IX_FK_UserFunctionUser]
ON [dbo].[UserFunctionSet]
    ([User_Id]);
GO

-- Creating foreign key on [Function_Id] in table 'UserFunctionSet'
ALTER TABLE [dbo].[UserFunctionSet]
ADD CONSTRAINT [FK_UserFunctionFunction]
    FOREIGN KEY ([Function_Id])
    REFERENCES [dbo].[FunctionSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserFunctionFunction'
CREATE INDEX [IX_FK_UserFunctionFunction]
ON [dbo].[UserFunctionSet]
    ([Function_Id]);
GO

-- Creating foreign key on [Good_Id] in table 'ImageGoodSet'
ALTER TABLE [dbo].[ImageGoodSet]
ADD CONSTRAINT [FK_ImageGoodGood]
    FOREIGN KEY ([Good_Id])
    REFERENCES [dbo].[GoodSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ImageGoodGood'
CREATE INDEX [IX_FK_ImageGoodGood]
ON [dbo].[ImageGoodSet]
    ([Good_Id]);
GO

-- Creating foreign key on [ImageGood_Id] in table 'ImageModificationGoodSet'
ALTER TABLE [dbo].[ImageModificationGoodSet]
ADD CONSTRAINT [FK_ImageModificationGoodImageGood]
    FOREIGN KEY ([ImageGood_Id])
    REFERENCES [dbo].[ImageGoodSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ImageModificationGoodImageGood'
CREATE INDEX [IX_FK_ImageModificationGoodImageGood]
ON [dbo].[ImageModificationGoodSet]
    ([ImageGood_Id]);
GO

-- Creating foreign key on [ModificationGood_Id] in table 'ImageModificationGoodSet'
ALTER TABLE [dbo].[ImageModificationGoodSet]
ADD CONSTRAINT [FK_ImageModificationGoodModificationGood]
    FOREIGN KEY ([ModificationGood_Id])
    REFERENCES [dbo].[ModificationGoodSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ImageModificationGoodModificationGood'
CREATE INDEX [IX_FK_ImageModificationGoodModificationGood]
ON [dbo].[ImageModificationGoodSet]
    ([ModificationGood_Id]);
GO

-- Creating foreign key on [User_Id] in table 'CartSet'
ALTER TABLE [dbo].[CartSet]
ADD CONSTRAINT [FK_CartUser]
    FOREIGN KEY ([User_Id])
    REFERENCES [dbo].[UserSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CartUser'
CREATE INDEX [IX_FK_CartUser]
ON [dbo].[CartSet]
    ([User_Id]);
GO

-- Creating foreign key on [User_Id] in table 'ViewedSet'
ALTER TABLE [dbo].[ViewedSet]
ADD CONSTRAINT [FK_ViewedUser]
    FOREIGN KEY ([User_Id])
    REFERENCES [dbo].[UserSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ViewedUser'
CREATE INDEX [IX_FK_ViewedUser]
ON [dbo].[ViewedSet]
    ([User_Id]);
GO

-- Creating foreign key on [User_Id] in table 'СompareSet'
ALTER TABLE [dbo].[СompareSet]
ADD CONSTRAINT [FK_СompareUser]
    FOREIGN KEY ([User_Id])
    REFERENCES [dbo].[UserSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_СompareUser'
CREATE INDEX [IX_FK_СompareUser]
ON [dbo].[СompareSet]
    ([User_Id]);
GO

-- Creating foreign key on [User_Id] in table 'FavoriteSet'
ALTER TABLE [dbo].[FavoriteSet]
ADD CONSTRAINT [FK_FavoriteUser]
    FOREIGN KEY ([User_Id])
    REFERENCES [dbo].[UserSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_FavoriteUser'
CREATE INDEX [IX_FK_FavoriteUser]
ON [dbo].[FavoriteSet]
    ([User_Id]);
GO

-- Creating foreign key on [Parent_Id] in table 'CategorySet'
ALTER TABLE [dbo].[CategorySet]
ADD CONSTRAINT [FK_CategoryCategory]
    FOREIGN KEY ([Parent_Id])
    REFERENCES [dbo].[CategorySet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CategoryCategory'
CREATE INDEX [IX_FK_CategoryCategory]
ON [dbo].[CategorySet]
    ([Parent_Id]);
GO

-- Creating foreign key on [Property_Id] in table 'PropertyCategorySet'
ALTER TABLE [dbo].[PropertyCategorySet]
ADD CONSTRAINT [FK_PropertyCategoryProperty]
    FOREIGN KEY ([Property_Id])
    REFERENCES [dbo].[PropertySet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PropertyCategoryProperty'
CREATE INDEX [IX_FK_PropertyCategoryProperty]
ON [dbo].[PropertyCategorySet]
    ([Property_Id]);
GO

-- Creating foreign key on [User_Id] in table 'HistorySet'
ALTER TABLE [dbo].[HistorySet]
ADD CONSTRAINT [FK_HistoryUser]
    FOREIGN KEY ([User_Id])
    REFERENCES [dbo].[UserSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_HistoryUser'
CREATE INDEX [IX_FK_HistoryUser]
ON [dbo].[HistorySet]
    ([User_Id]);
GO

-- Creating foreign key on [OrganizationUnit_Id] in table 'ImageOrganizationUnitSet'
ALTER TABLE [dbo].[ImageOrganizationUnitSet]
ADD CONSTRAINT [FK_ImageOrganizationUnitOrganizationUnit]
    FOREIGN KEY ([OrganizationUnit_Id])
    REFERENCES [dbo].[OrganizationUnitSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ImageOrganizationUnitOrganizationUnit'
CREATE INDEX [IX_FK_ImageOrganizationUnitOrganizationUnit]
ON [dbo].[ImageOrganizationUnitSet]
    ([OrganizationUnit_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------