﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Bazarkin.AdminPanel.ViewModels;

namespace Bazarkin.AdminPanel.Views
{
    public partial class AuthorizationView : UserControl
    {
        public AuthorizationView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Велосипед используется для занесения данных о пароле во ViewModel данного окна.
        /// PasswordBox не имеет свойств привязки по соображениям безопастности нашей любимой Microsoft =).
        /// Так же смотри описание вызываемого метода SetPassword.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PasswordBox_OnPasswordChanged(object sender, RoutedEventArgs e)
        {
            var context = this.DataContext as AuthorizationViewVM;
            if (context != null)
                context.SetPassword(PasswordBox.Password);
        }
    }
}
