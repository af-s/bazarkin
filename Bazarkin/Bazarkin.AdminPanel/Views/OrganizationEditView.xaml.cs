﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Bazarkin.AdminPanel.Interfaces;
using Bazarkin.AdminPanel.ViewModels;

namespace Bazarkin.AdminPanel.Views
{
    /// <summary>
    /// Interaction logic for OrganizationEditView.xaml
    /// </summary>
    public partial class OrganizationEditView : Window
    {
        public OrganizationEditView(IEditWindowClosed editVm)
        {
            editVm.CloseWindowAction += this.Close;
            this.DataContext = editVm;
            InitializeComponent();
        }
    }
}
