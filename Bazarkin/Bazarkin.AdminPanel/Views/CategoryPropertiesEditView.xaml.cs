﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Bazarkin.AdminPanel.Interfaces;
using Bazarkin.AdminPanel.ViewModels;

namespace Bazarkin.AdminPanel.Views
{
    /// <summary>
    /// Interaction logic for CategoryPropertiesEditView.xaml
    /// </summary>
    public partial class CategoryPropertiesEditView : Window
    {
        public CategoryPropertiesEditView(IEditWindowClosed editVm)
        {
            editVm.CloseWindowAction += this.Close;
            this.DataContext = editVm;
            InitializeComponent();
        }

        private new void PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }

        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9]+");
            return !regex.IsMatch(text);
        }

        private void UIElement_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = e.Key == Key.Space;
        }
    }
}
