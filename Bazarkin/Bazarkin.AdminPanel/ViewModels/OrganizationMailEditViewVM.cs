﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bazarkin.AdminPanel.Classes;
using Bazarkin.AdminPanel.CommonClasses;
using Bazarkin.AdminPanel.CommonViewModels;
using Bazarkin.AdminPanel.EventArgs;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Bazarkin.AdminPanel.ViewModels
{
    public class OrganizationMailEditViewVM : CommonEditViewVM<MailItem>
    {
        public OrganizationMailEditViewVM(MailItem editedItem)
            : base(editedItem) { }

        public OrganizationMailEditViewVM()
            : base() { }
    }
}
