﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bazarkin.AdminPanel.Classes;
using Bazarkin.AdminPanel.CommonViewModels;
using Bazarkin.AdminPanel.EventArgs;
using Bazarkin.AdminPanel.Interfaces;
using Bazarkin.AdminPanel.Views;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Bazarkin.AdminPanel.ViewModels
{
    public class UsersViewVM : CommonMainItemVM<UsersViewItem>
    {
        #region Constructors

        public UsersViewVM()
            : base("Пользователи")
        {

        }

        #endregion

        #region Methods

        protected override void FillDisplayedCollection()
        {
            DisplayedCollection = new ObservableCollection<UsersViewItem>();
            bool b = false;
            for (int x = 0; x < 20; x++)
            {
                var item =new UsersViewItem
                {
                    ID = x,
                    Name = "Name",
                    Patronymic = "Patronymic",
                    Surname = "Surname",
                    Login = "+7xxxxxx" + x,
                    Password = "Password" + x,
                    Key = "Key" + x,
                    Rights = new ObservableCollection<RightsItem>(),
                    Status = b
                };
                for (int i = 0; i < 5; i++)
                {
                    item.Rights.Add(new RightsItem
                    {
                        ID = i,
                        Name = "Name"+i
                    });
                }
                DisplayedCollection.Add(item);
                b = !b;
            }
        }

        protected override void EditDialogCallback(object sender, EditWindowClosedArgs e)
        {
            
        }

        #endregion

        #region Handlers

        protected override void OnEditCommand()
        {
            var editVm = new UserEditViewVM(SelectedItem);
            editVm.DialogCallback += EditDialogCallback;
            new UserEditView(editVm).ShowDialog();
        }



        protected override void OnAddCommand()
        {
            var editVm = new UserEditViewVM();
            editVm.DialogCallback += EditDialogCallback;
            new UserEditView(editVm).ShowDialog();
        }

        protected override void OnFindCommand()
        {
            
        }


        #endregion


    }
}
