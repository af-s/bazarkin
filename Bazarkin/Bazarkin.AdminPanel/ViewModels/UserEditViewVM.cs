﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bazarkin.AdminPanel.Classes;
using Bazarkin.AdminPanel.CommonViewModels;
using Bazarkin.AdminPanel.EventArgs;
using Bazarkin.AdminPanel.Interfaces;
using Bazarkin.AdminPanel.Views;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Bazarkin.AdminPanel.ViewModels
{
    public class UserEditViewVM : CommonEditViewVM<UsersViewItem>
    {
        #region Properties

        #region Commands

        public RelayCommand ResetCommand { get; set; }
        public RelayCommand EditCommand { get; set; }

        #endregion

        #endregion

        #region Constructors

        public UserEditViewVM(UsersViewItem editedItem)
            : base(editedItem) { }
        public UserEditViewVM()
            : base() { }

        #endregion

        #region Handlers

        /// <summary>
        /// Обработчик события кнопки "Редактировать права пользователя"
        /// </summary>
        private void OnEditCommand()
        {
            var editVm = new RightsEditViewVM(EditedItem.Rights);
            editVm.DialogCallback += EditDialogCallback;
            new RightsEditView(editVm).ShowDialog();
        }


        /// <summary>
        /// Обработчик события кнопки "Сбросить ключ"
        /// </summary>
        private void OnResetCommand()
        {
            EditedItem.Key = string.Empty;
        }

        #endregion

        #region Methods

        protected override void InitializeCommands()
        {
            ResetCommand = new RelayCommand(OnResetCommand);
            EditCommand = new RelayCommand(OnEditCommand);
            base.InitializeCommands();
        }


        private void EditDialogCallback(object sender, EditWindowClosedArgs e)
        {
            if (e.DialogResult)
                EditedItem.Rights = e.EditedItem;
        }

        #endregion
    }
}
