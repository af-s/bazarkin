﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bazarkin.AdminPanel.Classes;
using Bazarkin.AdminPanel.CommonClasses;
using Bazarkin.AdminPanel.CommonViewModels;
using Bazarkin.AdminPanel.EventArgs;
using Bazarkin.AdminPanel.Interfaces;
using Bazarkin.AdminPanel.Views;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Bazarkin.Domain;

namespace Bazarkin.AdminPanel.ViewModels
{
    public class OrganizationsViewVM :CommonMainItemVM<OrganizationsViewItem>
    {
        #region Constructors

        public OrganizationsViewVM()
            : base("Организации")
        {
        }

        #endregion

        #region Methods

        protected override void FillDisplayedCollection()
        {
            EFDbContext db = new EFDbContext();
            List<Organization> organizationSet = db.OrganizationSet.OrderBy(x => x.Name).ToList();

            DisplayedCollection = new ObservableCollection<OrganizationsViewItem>();

            foreach (var organization in organizationSet)
            {
                var item = new OrganizationsViewItem();
                item.ID = organization.Id;
                item.Name = organization.Name;
                item.Description = organization.Description;
                item.IsActive = organization.Active;

                DisplayedCollection.Add(item);
            }

            #region Демоданные
            //var b = false;
            //for (int x = 0; x < 20; x++)
            //{
            //    var item = new OrganizationsViewItem
            //    {
            //        #region Страсти
            //        ID = x,
            //        Name = "Name" + x,
            //        Description = "Description" + x,
            //        IsActive = b,
            //        Points = new ObservableCollection<TradePointItem>(),
            //        Phones = new ObservableCollection<PhoneItem>(),
            //        Addresses = new ObservableCollection<OrganizationAddressItem>(),
            //        Mails = new ObservableCollection<MailItem>()
            //        #endregion
            //    };
            //    var c = false;
            //    for (int i = 0; i < 5; i++)
            //    {
            //        var tradeItem = new TradePointItem
            //        {
            //            ID = i,
            //            IsActive = c,
            //            Address = "Address" + i,
            //            PaidTo = DateTime.Now,
            //            Description = "Description" + i,
            //            Name = "Name" + i,
            //            Phones = new ObservableCollection<PhoneItem>(),
            //        };
            //        var s = false;
            //        for (int k = 0; k < 5; k++)
            //        {
            //            tradeItem.Phones.Add(new PhoneItem
            //            {
            //                ID = k,
            //                Description = "Description" + k,
            //                Number = "+7xxxxxxxxx" + k,
            //                IsVisible = s
            //            });
            //            s = !s;
            //        }
            //        item.Points.Add(tradeItem);
            //        c = !c;
            //    }
            //    for (int i = 0; i < 5; i++)
            //    {
            //        item.Mails.Add(new MailItem
            //        {
            //            ID = i,
            //            Description = "Description" + i,
            //            Mail = "Mail" + i,
            //        });
            //    }
            //    for (int i = 0; i < 5; i++)
            //    {
            //        item.Phones.Add(new PhoneItem
            //        {
            //            ID = i,
            //            Description = "Description" + i,
            //            Number = "+7xxxxxxxxx" + i,
            //            IsVisible = c
            //        });
            //        c = !c;
            //    }
            //    for (int i = 0; i < 5; i++)
            //    {
            //        item.Addresses.Add(new OrganizationAddressItem
            //        {
            //            ID = i,
            //            Country = "Country" + i,
            //            Region = "Region" + i,
            //            Description = "Description" + i,
            //            Street = "Street" + i,
            //            Row = "Row" + i,
            //            Apartment = "Apartment" + i,
            //            Sector = "Sector" + i,
            //            ShoppingCenter = "ShoppingCenter" + i,
            //            Pavilion = "Pavilion" + i,
            //            Building = "Building" + i,
            //            City = "City" + i,
            //            Structure = "Structure" + i,
            //            Index = i * i,
            //            IsVisible = c
            //        });
            //        c = !c;
            //    }
            //    DisplayedCollection.Add(item);
            //    b = !b;
            //}
            #endregion
        }

        protected override void EditDialogCallback(object sender, EditWindowClosedArgs e)
        {
            
        }

        #endregion

        #region Handlers

        protected override void OnAddCommand()
        {
            var editVm = new OrganizationEditViewVM();
            editVm.DialogCallback += EditDialogCallback;
            new OrganizationEditView(editVm).ShowDialog();
        }

        protected override void OnFindCommand()
        {
            
        }

        protected override void OnEditCommand()
        {
            var editVm = new OrganizationEditViewVM(SelectedItem);
            editVm.DialogCallback += EditDialogCallback;
            new OrganizationEditView(editVm).ShowDialog();
        }

        #endregion


    }
}
