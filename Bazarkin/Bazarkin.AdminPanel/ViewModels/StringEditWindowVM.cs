﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bazarkin.AdminPanel.EventArgs;
using Bazarkin.AdminPanel.Interfaces;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Bazarkin.AdminPanel.ViewModels
{
    public class StringEditWindowVM : ViewModelBase, IEditWindowClosed
    {
        #region Properties

        #region EditedItem

        private string mEditedItem;

        /// <summary>
        /// Редактируемая запись
        /// </summary>
        public string EditedItem
        {
            get { return mEditedItem; }
            set
            {
                if (mEditedItem == value)
                    return;
                mEditedItem = value;
                RaisePropertyChanged(() => EditedItem);
            }
        }

        #endregion

        #region Comment

        private string mComment;

        /// <summary>
        /// Коментарий
        /// </summary>
        public string Comment
        {
            get { return mComment; }
            set
            {
                if (mComment == value)
                    return;
                mComment = value;
                RaisePropertyChanged(() => Comment);
            }
        }

        #endregion

        public Action CloseWindowAction { get; set; }
        public EventHandler<EditWindowClosedArgs> DialogCallback { get; set; }

        #region Commands

        public RelayCommand OkCommand { get; set; }
        public RelayCommand CancelCommand { get; set; }

        #endregion

        #endregion

        #region Constructors

        public StringEditWindowVM(string editedstring, string comment)
        {
            EditedItem = editedstring;
            Comment = comment;
            InitializeCommands();
        }

        #endregion

        #region Methods

        private void InitializeCommands()
        {
            OkCommand = new RelayCommand(OnOkCommand);
            CancelCommand = new RelayCommand(OnCancelCommand);
        }

        private EditWindowClosedArgs GetDialogResultArgs(bool dialogResult)
        {
            return dialogResult ? new EditWindowClosedArgs(dialogResult, EditedItem) : new EditWindowClosedArgs(dialogResult);
        }

        #endregion


        #region Handlers

        private void OnCancelCommand()
        {
            DialogCallback.Invoke(this, GetDialogResultArgs(false));
            CloseWindowAction.Invoke();
        }

        private void OnOkCommand()
        {
            DialogCallback.Invoke(this, GetDialogResultArgs(true));
            CloseWindowAction.Invoke();
        } 

        #endregion
    }
}
