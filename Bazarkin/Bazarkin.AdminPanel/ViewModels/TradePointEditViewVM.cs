﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bazarkin.AdminPanel.Classes;
using Bazarkin.AdminPanel.CommonClasses;
using Bazarkin.AdminPanel.CommonViewModels;
using Bazarkin.AdminPanel.EventArgs;
using Bazarkin.AdminPanel.Views;
using GalaSoft.MvvmLight.Command;

namespace Bazarkin.AdminPanel.ViewModels
{
    public class TradePointEditViewVM : CommonEditViewVM<TradePointItem>
    {
        #region Properties

        #region SelectedTradePointPhone

        private PhoneItem mSelectedTradePointPhone;

        /// <summary>
        /// Выбранный телефон
        /// </summary>
        public PhoneItem SelectedTradePointPhone
        {
            get { return mSelectedTradePointPhone; }
            set
            {
                if (mSelectedTradePointPhone == value)
                    return;
                mSelectedTradePointPhone = value;
                RaisePropertyChanged(() => SelectedTradePointPhone);
            }
        }

        #endregion

        #region Commands

        public RelayCommand AddPhoneCommand { get; set; }
        public RelayCommand EditPhoneCommand { get; set; }
        public RelayCommand DeletePhoneCommand { get; set; }
        public RelayCommand EditAddress { get; set; }
        
        #endregion

        #endregion

        #region Constructors

        public TradePointEditViewVM(TradePointItem editedItem)
            : base(editedItem) { }
        public TradePointEditViewVM()
            : base() { }

        #endregion

        #region Methods

        protected override void InitializeCommands()
        {
            AddPhoneCommand = new RelayCommand(OnAddPhoneCommand);
            EditPhoneCommand= new RelayCommand(OnEditPhoneCommand);
            DeletePhoneCommand = new RelayCommand(OnDeletePhoneCommand);
            EditAddress = new RelayCommand(OnEditAddress);
            base.InitializeCommands();
        }


        /// <summary>
        /// Срабатывает по завершении редактирования записи
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditDialogCallback(object sender, EditWindowClosedArgs e)
        {
            if(e.DialogResult==false) return;
            if (e.EditedItem is PhoneItem)
            {
                
            }
            if (e.EditedItem is AddressItem)
                EditedItem.Addresses.Add(e.EditedItem);
        }

        #endregion

        #region Handlers

        private void OnAddPhoneCommand()
        {
            var editVm = new PhoneEditViewVM();
            editVm.DialogCallback += EditDialogCallback;
            new PhoneEditView(editVm).ShowDialog();
        }

        private void OnDeletePhoneCommand()
        {
            EditedItem.Phones.Remove(SelectedTradePointPhone);
        }

        private void OnEditPhoneCommand()
        {
            var editVm = new PhoneEditViewVM(SelectedTradePointPhone);
            editVm.DialogCallback += EditDialogCallback;
            new PhoneEditView(editVm).ShowDialog();
        }

        private void OnEditAddress()
        {
            var editVm = new OrganizationAddressEditViewVM();
            editVm.DialogCallback += EditDialogCallback;
            new OrganizationAddressEditView(editVm).ShowDialog();
        }


        #endregion
    }
}
