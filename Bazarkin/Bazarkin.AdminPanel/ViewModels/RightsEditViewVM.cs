﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bazarkin.AdminPanel.Classes;
using Bazarkin.AdminPanel.CommonViewModels;
using Bazarkin.AdminPanel.EventArgs;
using Bazarkin.AdminPanel.Interfaces;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Bazarkin.AdminPanel.ViewModels
{
    public class RightsEditViewVM : ViewModelBase, IEditWindowClosed
    {
        #region Properties

        public Action CloseWindowAction { get; set; }
        public EventHandler<EditWindowClosedArgs> DialogCallback { get; set; }

        #region DisplayedCollection

        private ObservableCollection<RightsItem> mDisplayedCollection;

        /// <summary>
        /// Коллекция отображаемая в окне редактирования прав
        /// </summary>
        public ObservableCollection<RightsItem> DisplayedCollection
        {
            get { return mDisplayedCollection; }
            set
            {
                if (mDisplayedCollection == value)
                    return;
                mDisplayedCollection = value;
                RaisePropertyChanged(() => DisplayedCollection);
            }
        }

        #endregion

        ObservableCollection<RightsItem> UserCollection { get; set; }

        #region Commands

        public RelayCommand OkCommand { get; set; }
        public RelayCommand CancelCommand { get; set; }

        #endregion

        #endregion

        #region Constructors

        public RightsEditViewVM(IEnumerable<RightsItem> userCollection)
        {
            InitializeCommands();
            this.UserCollection = new ObservableCollection<RightsItem>(userCollection);
            FillDisplayedCollection();
        }

        #endregion

        #region Handlers

        private void OnCancelCommand()
        {
            DialogCallback.Invoke(this, GetDialogResultArgs(false));
            CloseWindowAction.Invoke();
        }

        private void OnOkCommand()
        {
            UserCollection.Clear();
            foreach (var item in DisplayedCollection.Where(x => x.IsActive))
                UserCollection.Add(item);
            
            DialogCallback.Invoke(this, GetDialogResultArgs(true));
            CloseWindowAction.Invoke();
        }

        #endregion

        #region Methods

        private void FillDisplayedCollection()
        {
            DisplayedCollection= new ObservableCollection<RightsItem>();
            for (int i = 0; i < 20; i++)
            {
                var item = new RightsItem
                {
                    ID = i,
                    Name = "Name" + i
                };
                DisplayedCollection.Add(item);
            }
            CompareCollections();
  
        }


        /// <summary>
        /// Отмечает галочками те которые привязаны к пользователю
        /// </summary>
        private void CompareCollections()
        {
            var existed = UserCollection.Select(x => x.ID).ToList();
            foreach (var item in DisplayedCollection.Where(item => existed.Contains(item.ID)))
                item.IsActive = true;
        }


        private void InitializeCommands()
        {
            OkCommand = new RelayCommand(OnOkCommand);
            CancelCommand = new RelayCommand(OnCancelCommand);
        }

        private EditWindowClosedArgs GetDialogResultArgs(bool dialogResult)
        {
            return dialogResult ? new EditWindowClosedArgs(dialogResult, UserCollection) : new EditWindowClosedArgs(dialogResult);
        }

        #endregion

    }
}
