﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bazarkin.AdminPanel.Classes;
using Bazarkin.AdminPanel.CommonViewModels;
using Bazarkin.AdminPanel.EventArgs;
using Bazarkin.AdminPanel.Views;
using Bazarkin.Domain;

namespace Bazarkin.AdminPanel.ViewModels
{
    public class PropertyGroupsViewVM : CommonMainItemVM<PropertyGroupItem>
    {
        #region Constructors

        public PropertyGroupsViewVM()
            : base("Группы свойств")
        {

        }

        #endregion

        #region Methods

        protected override void FillDisplayedCollection()
        {
            EFDbContext db = new EFDbContext();
            List<PropertyGroup> propertyGroupSet = db.PropertyGroupSet.OrderBy(x => x.Name).ToList();

            DisplayedCollection = new ObservableCollection<PropertyGroupItem>();

            foreach(var propertyGroup in propertyGroupSet)
            {
                var item = new PropertyGroupItem
                {
                    ID = propertyGroup.Id,
                    Description = propertyGroup.Description,
                    Name = propertyGroup.Name,
                    Properties = new ObservableCollection<PropertyItem>()
                };

                DisplayedCollection.Add(item);
            }

            //for (int x = 0; x < 20; x++)
            //{
            //    var item = new PropertyGroupItem
            //    {
            //        ID = x,
            //        Description = "Description" + x,
            //        Name = "Name" + x,
            //        Properties = new ObservableCollection<PropertyItem>()
            //    };
            //    for (int k = 0; k < 5; k++)
            //    {
            //        item.Properties.Add(new PropertyItem
            //        {
            //            ID = k,
            //            Name = "Name"+k,
            //            Description = "Description"+k,
            //            PropertyGroup = x,
            //            PropertyType = k
            //        });
            //    }
            //    DisplayedCollection.Add(item);
            //}
        }

        protected override void EditDialogCallback(object sender, EditWindowClosedArgs e)
        {
            
        }
        #endregion

        #region Handlers

        protected override void OnAddCommand()
        {
            var editVm = new PropertyGroupsEditViewVM();
            editVm.DialogCallback += EditDialogCallback;
            new PropertyGroupsEditView(editVm).ShowDialog();
        }

        protected override void OnFindCommand()
        {
            
        }

        protected override void OnEditCommand()
        {
            var editVm = new PropertyGroupsEditViewVM(SelectedItem);
            editVm.DialogCallback += EditDialogCallback;
            new PropertyGroupsEditView(editVm).ShowDialog();
        }

        #endregion
    }
}
