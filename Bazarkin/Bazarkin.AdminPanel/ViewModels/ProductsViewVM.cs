﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bazarkin.AdminPanel.Classes;
using Bazarkin.AdminPanel.CommonClasses;
using Bazarkin.AdminPanel.CommonViewModels;
using Bazarkin.AdminPanel.EventArgs;
using Bazarkin.AdminPanel.Interfaces;
using Bazarkin.AdminPanel.Views;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Bazarkin.AdminPanel.ViewModels
{
    public class ProductsViewVM : CommonMainItemVM<ProductItem>
    {
        #region Constructors

        public ProductsViewVM()
            : base("Товары")
        {

        }

        #endregion

        #region Methods

        protected override void FillDisplayedCollection()
        {
            DisplayedCollection = new ObservableCollection<ProductItem>();
            bool b = false;
            for (int x = 0; x < 20; x++)
            {
                var productItem = new ProductItem
                {
                    ID = x,
                    IsActive = b,
                    Description = "Description" + x,
                    Name = "Name" + x,
                    Modifications = new ObservableCollection<ProductModificationItem>()

                };
                for (int k = 0; k < 5; k++)
                {
                    productItem.Modifications.Add(new ProductModificationItem
                    {
                        ID = k,
                        Modification = "Modification" + k,
                        Article = "Article" + k
                    });
                }
                DisplayedCollection.Add(productItem);
                b = !b;
            }
        }

        protected override void EditDialogCallback(object sender, EditWindowClosedArgs e)
        {
            
        }

        #endregion

        #region Handlers

        protected override void OnEditCommand()
        {
            var editVm = new ProductsEditViewVM(SelectedItem);
            editVm.DialogCallback += EditDialogCallback;
            new ProductsEditView(editVm).ShowDialog();
        }

        protected override void OnAddCommand()
        {
            var editVm = new ProductsEditViewVM();
            editVm.DialogCallback += EditDialogCallback;
            new ProductsEditView(editVm).ShowDialog();
        }

        protected override void OnFindCommand()
        {
            
        }

        #endregion
    }
}
