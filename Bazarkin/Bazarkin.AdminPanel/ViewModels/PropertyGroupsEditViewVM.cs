﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bazarkin.AdminPanel.Classes;
using Bazarkin.AdminPanel.CommonViewModels;
using Bazarkin.AdminPanel.EventArgs;
using Bazarkin.AdminPanel.Views;
using GalaSoft.MvvmLight.Command;
using Bazarkin.Domain;

namespace Bazarkin.AdminPanel.ViewModels
{
    public class PropertyGroupsEditViewVM : CommonEditViewVM<PropertyGroupItem>
    {
        #region Properties

        #region SelectedProperty

        private PropertyItem mSelectedProperty;

        /// <summary>
        /// Выбранное свойство
        /// </summary>
        public PropertyItem SelectedProperty
        {
            get { return mSelectedProperty; }
            set
            {
                if (mSelectedProperty == value)
                    return;
                mSelectedProperty = value;
                RaisePropertyChanged(() => SelectedProperty);
            }
        }

        #endregion

        #region Commands

        public RelayCommand EditCommand { get; set; }
        public RelayCommand DeleteCommand { get; set; }

        public RelayCommand AddProperty { get; set; }

        #endregion 

        #endregion
         #region Constructors

        public PropertyGroupsEditViewVM(PropertyGroupItem editedItem)
            : base(editedItem) { }
        public PropertyGroupsEditViewVM()
            : base() { }

        #endregion

        #region Methods

        protected override void InitializeCommands()
        {
            EditCommand = new RelayCommand(OnEditCommand);
            DeleteCommand = new RelayCommand(OnDeleteCommand);
            AddProperty = new RelayCommand(OnAddProperty);
            base.InitializeCommands();
        }



        private void EditDialogCallback(object sender, EditWindowClosedArgs e)
        {

        }

        #endregion

        #region Handlers


        private void OnDeleteCommand()
        {
            EditedItem.Properties.Remove(SelectedProperty);
        }

        private void OnEditCommand()
        {
            var editVm = new PropertyEditViewVM(SelectedProperty);
            editVm.DialogCallback += EditDialogCallback;
            new PropertyEditView(editVm).ShowDialog();
        }

        private void OnAddProperty()
        {
            var editVm = new PropertyEditViewVM();
            editVm.DialogCallback += EditDialogCallback;
            new PropertyEditView(editVm).ShowDialog();
        }

        #endregion

        protected override void OnOkCommand()
        {
            EFDbContext db = new EFDbContext();

            PropertyGroup propertyGroup;
            if (EditedItem.ID == 0)
            {
                propertyGroup = new PropertyGroup();
            }
            else
            {
                propertyGroup = db.PropertyGroupSet.Where(x => x.Id == EditedItem.ID).FirstOrDefault();
            }

            propertyGroup.Name = EditedItem.Name.Trim();
            propertyGroup.Description = (EditedItem.Description == null || EditedItem.Description.Trim() == string.Empty) ? null : EditedItem.Description.Trim();

            if (propertyGroup.Id == 0)
            {
                db.PropertyGroupSet.Add(propertyGroup);
            }

            db.SaveChanges();

            CloseWindowAction.Invoke();
        }
    }
}
