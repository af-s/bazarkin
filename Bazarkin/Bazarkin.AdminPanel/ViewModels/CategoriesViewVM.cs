﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bazarkin.AdminPanel.Classes;
using Bazarkin.AdminPanel.CommonViewModels;
using Bazarkin.AdminPanel.EventArgs;
using Bazarkin.AdminPanel.Interfaces;
using Bazarkin.AdminPanel.Views;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Bazarkin.Domain;

namespace Bazarkin.AdminPanel.ViewModels
{
    public class CategoriesViewVM : CommonMainItemVM<CategoriesViewItem>
    {
        EFDbContext db = new EFDbContext();

        #region Constructors

        public CategoriesViewVM()
            : base("Категории")
        {

        }

        #endregion

        #region Methods

        protected override void FillDisplayedCollection()
        {            
            List<Category> categorySet = db.CategorySet.OrderBy(x => x.Name).ToList();

            DisplayedCollection = new ObservableCollection<CategoriesViewItem>();
            //bool b = false;
            //for (int x = 0; x < 20; x++)
            //{
            //    var itm = new CategoriesViewItem();
            //    if (b)
            //    {
            //        itm.ID = x;
            //        itm.Name = "Name" + x;
            //        itm.Description =
            //            "A ListView control contains a collection of items that you can view in different ways";
            //        itm.IsFinal = true;
            //        itm.Parent = null;
            //    }
            //    else
            //    {
            //        itm.ID = x;
            //        itm.Name = "Parent" + x;
            //        itm.Description =
            //            "A GridView is an object that can display the data in columns.";
            //        itm.IsFinal = false;
            //        itm.Parent = (x - 1) < 0 ? 0 : (x - 1);
            //    }
            //    SetCategoryProperties(itm);
            //    DisplayedCollection.Add(itm);
            //    b = !b;
            //}
            foreach(var category in categorySet)
            {
                var categoriesViewItem = new CategoriesViewItem();
                categoriesViewItem.ID = category.Id;
                categoriesViewItem.Name = category.Name;
                categoriesViewItem.Description = category.Description;
                //categoriesViewItem.IsFinal = category.GoodHear;
                // categoriesViewItem.Parent = (category.Parent == null) ? "Корневая" : db.CategorySet.Where(x => x.Id == category.Parent.Id).FirstOrDefault().Name;
                categoriesViewItem.Parent = (category.Parent == null) ? 0 : category.Parent.Id;

                SetCategoryProperties(categoriesViewItem);
                DisplayedCollection.Add(categoriesViewItem);
            }
        }

        /// <summary>
        /// Заполняем свойства для категории
        /// </summary>
        /// <param name="itm"></param>
        private void SetCategoryProperties(CategoriesViewItem itm)
        {
             itm.Properties = new ObservableCollection<CategoryPropertiesTreeViewItem>();
            for (int i = 0; i < 10; i++)
            {
                var item = new CategoryPropertiesTreeViewItem
                {
                    Name = "Parent Name " + i,
                    Childs = new ObservableCollection<CategoryPropertiesChildTreeViewItem>()
                };
                var b = false;
                for (int j = 0; j < 5; j++)
                {
                    var child = new CategoryPropertiesChildTreeViewItem
                    {
                        IsActive = b,
                        Description = "Description" + j,
                        Value = j,
                        Name = "Child Name " + j,
                    };
                    /*Обрати внимание, подписка этого св-ва обязательна что бы менять отображаемые значения в верхних элементах дерева*/
                    child.ValueChangedEvent += item.ChildPropertyChanged;
                    item.Childs.Add(child);
                    b = !b;
                }
                itm.Properties.Add(item);
            }
        }

        protected override void EditDialogCallback(object sender, EditWindowClosedArgs e)
        {
                
        }

        #endregion

        #region Handlers

        protected override void OnAddCommand()
        {
            var editVm = new CategoryEditViewVM(DisplayedCollection);
            editVm.DialogCallback += EditDialogCallback;
            new CategoryEditView(editVm).ShowDialog();
        }

        protected override void OnFindCommand()
        {

        }

        protected override void OnEditCommand()
        {
            var editVm = new CategoryEditViewVM(SelectedItem, DisplayedCollection);
            editVm.DialogCallback += EditDialogCallback;
            new CategoryEditView(editVm).ShowDialog();
        }

        #endregion

    }
}
