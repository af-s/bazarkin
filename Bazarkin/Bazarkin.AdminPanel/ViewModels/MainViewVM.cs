﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Bazarkin.AdminPanel.Classes;
using Bazarkin.AdminPanel.Enums;
using Bazarkin.AdminPanel.EventArgs;
using Bazarkin.AdminPanel.Views;
using GalaSoft.MvvmLight;

namespace Bazarkin.AdminPanel.ViewModels
{
    public class MainViewVM : ViewModelBase
    {
        #region Properties

        public EventHandler<DisplayViewArgs> DisplayView;

        #region Elements

        private ObservableCollection<MainViewElement> mElements;

        /// <summary>
        /// Коллекцкия элементы отображаемые в главном меню
        /// </summary>
        public ObservableCollection<MainViewElement> Elements
        {
            get { return mElements; }
            set
            {
                if (mElements == value)
                    return;
                mElements = value;
                RaisePropertyChanged(() => Elements);
            }
        }

        #endregion

        #endregion

        #region Constructors

        public MainViewVM()
        {
            AddMainViewElements();
        }

        #endregion

        #region Handlers

        /// <summary>
        /// Обработчик события клика по элементу в "Главном меню"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ElementClicked(object sender, MainViewElementArgs e)
        {
            /*метод на случай если нужно к переданным View или ViewModel добавить каких нибудь расширений.
             во всех остальных случаях просто отображаем на форме то что пришло*/

            if (DisplayView == null) return;
            if (e.ElementView == null || e.ElementViewModel == null) return;
            var args = new DisplayViewArgs(e.ElementView, e.ElementViewModel, new EventHandler((sender1, e1) => RefreshView()));
            DisplayView(this, args);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Метод запланирован для добавления элементов в главное меню (Заполнен в качестве примера)
        /// Генерируем кодом т.к. количество плиток заранее неизвестно
        /// </summary>
        public void AddMainViewElements()
        {
            #region ЗАПОЛНЕНО ДЛЯ ПРИМЕРА

            var img = new BitmapImage(new Uri("../Resources/Images/TestImage.jpg", UriKind.Relative));
            var height = 230;
            var width = 160;
            var cornerRadius = 2;

            Elements = new ObservableCollection<MainViewElement>
            {
                new MainViewElement("Категории", img, height, width, cornerRadius,MainViewElementsIdentifiers.Categories),
                new MainViewElement("Пользователи", img, height, width, cornerRadius,MainViewElementsIdentifiers.Users),
                new MainViewElement("Производители", img, height, width, cornerRadius,MainViewElementsIdentifiers.Manufacturers),
                new MainViewElement("Организации", img, height, width, cornerRadius,MainViewElementsIdentifiers.Organizations),
                new MainViewElement("Торговые точки", img, height, width, cornerRadius,MainViewElementsIdentifiers.TradePoints),
                new MainViewElement("Товары", img, height, width, cornerRadius,MainViewElementsIdentifiers.Products),
                new MainViewElement("Типы свойств", img, height, width, cornerRadius,MainViewElementsIdentifiers.PropertyTypes),
                new MainViewElement("Группы свойств", img, height, width, cornerRadius,MainViewElementsIdentifiers.PropertyGroups),
            };

            #endregion

            //Подписываемся на событие клика по элементу (Обязательно надо подписываться что быиметь возможность реагировать на данное событие)
            foreach (var element in Elements)
                element.ElementCommandExecuted += ElementClicked;
        }

        /// <summary>
        /// Отображает главное меню
        /// </summary>
        public void RefreshView()
        {
            DisplayView(this, new DisplayViewArgs(new MainView(), this));
            MainWindowManager.Instance.SetWindowTitle("Главное меню");
            //если нужно будет перестраивать отображаемые элементы то нужно добавить сюда AddMainViewElements()
        }
        #endregion
    }
}
