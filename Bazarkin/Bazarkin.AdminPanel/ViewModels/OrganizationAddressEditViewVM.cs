﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bazarkin.AdminPanel.Classes;
using Bazarkin.AdminPanel.CommonViewModels;

namespace Bazarkin.AdminPanel.ViewModels
{
    public class OrganizationAddressEditViewVM : CommonEditViewVM<AddressItem>
    {
        public OrganizationAddressEditViewVM(AddressItem editedItem)
            : base(editedItem) { }
        public OrganizationAddressEditViewVM()
            : base() { }
    }
}
