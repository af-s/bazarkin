﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bazarkin.AdminPanel.Classes;
using Bazarkin.AdminPanel.CommonViewModels;
using Bazarkin.AdminPanel.EventArgs;
using Bazarkin.AdminPanel.Views;
using GalaSoft.MvvmLight.Command;

namespace Bazarkin.AdminPanel.ViewModels
{
    public class ProductsEditViewVM : CommonEditViewVM<ProductItem>
    {

        #region Properties

        #region SelectedProductModification

        private ProductModificationItem mSelectedProductModification;

        /// <summary>
        /// Выбранная модификация
        /// </summary>
        public ProductModificationItem SelectedProductModification
        {
            get { return mSelectedProductModification; }
            set
            {
                if (mSelectedProductModification == value)
                    return;
                mSelectedProductModification = value;
                RaisePropertyChanged(() => SelectedProductModification);
            }
        }

        #endregion

        #region Commands

        public RelayCommand EditCommand { get; set; }
        public RelayCommand DeleteCommand { get; set; }
        public RelayCommand AddModificationCommand { get; set; }

        #endregion

        #endregion

        #region Constructors

        public ProductsEditViewVM(ProductItem editedItem)
            : base(editedItem) { }
        public ProductsEditViewVM()
            : base() { }

        #endregion

        #region Methods

        protected override void InitializeCommands()
        {
            EditCommand = new RelayCommand(OnEditCommand);
            DeleteCommand = new RelayCommand(OnDeleteCommand);
            AddModificationCommand = new RelayCommand(OnAddModificationCommand);
            base.InitializeCommands();
        }

        private void EditDialogCallback(object sender, EditWindowClosedArgs e)
        {

        }

        #endregion

        #region Handlers

        private void OnDeleteCommand()
        {
            EditedItem.Modifications.Remove(SelectedProductModification);
        }

        private void OnEditCommand()
        {
            var editVm = new ModificationEditViewVM(SelectedProductModification);
            editVm.DialogCallback += EditDialogCallback;
            new ModificationEditView(editVm).ShowDialog();
        }

        private void OnAddModificationCommand()
        {
            var editVm = new ModificationEditViewVM();
            editVm.DialogCallback += EditDialogCallback;
            new ModificationEditView(editVm).ShowDialog();
        }

        

        #endregion
    }
}
