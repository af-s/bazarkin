﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bazarkin.AdminPanel.Classes;
using Bazarkin.AdminPanel.CommonViewModels;
using Bazarkin.Domain;

namespace Bazarkin.AdminPanel.ViewModels
{
    public class PropertyTypesEditViewVM : CommonEditViewVM<PropertyTypeItem>
    {
        EFDbContext db = new EFDbContext();

        #region Constructors

        public PropertyTypesEditViewVM(PropertyTypeItem editedItem)
            : base(editedItem) { }
        public PropertyTypesEditViewVM()
            : base() { }

        #endregion

        protected override void OnOkCommand()
        {
            PropertyType propertyType;
            if (EditedItem.ID == 0)
            {
                propertyType = new PropertyType();
            }
            else
            {
                propertyType = db.PropertyTypeSet.Where(x => x.Id == EditedItem.ID).FirstOrDefault();
            }

            propertyType.Name = EditedItem.Name.Trim();
            propertyType.Description = EditedItem.Description.Trim();
            propertyType.FilterType = EditedItem.FilterType;
            propertyType.ValueFormat = EditedItem.Format;

            if (propertyType.Id == 0)
            {
                db.PropertyTypeSet.Add(propertyType);
            }

            db.SaveChanges();

            CloseWindowAction.Invoke();
        }
    }
}
