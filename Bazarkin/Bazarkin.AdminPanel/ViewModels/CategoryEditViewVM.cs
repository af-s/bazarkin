﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using Bazarkin.AdminPanel.Classes;
using Bazarkin.AdminPanel.CommonViewModels;
using Bazarkin.AdminPanel.EventArgs;
using Bazarkin.AdminPanel.Interfaces;
using Bazarkin.AdminPanel.Views;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Bazarkin.Domain;
using Bazarkin.ClassLibrary;

namespace Bazarkin.AdminPanel.ViewModels
{
    public class CategoryEditViewVM : CommonEditViewVM<CategoriesViewItem>
    {
        #region Properties
        EFDbContext db = new EFDbContext();

        #region ExistedItems

        private ObservableCollection<CategoriesViewItem> mExistedItems;

        /// <summary>
        /// существующие записи для ComboBox "Родительская" 
        /// (Пока передаем в констректор коллекцию которая лежит в родительском окне, в будущем лучше грузить из БД
        /// дабы сохранить актуальность данных)
        /// </summary>
        public ObservableCollection<CategoriesViewItem> ExistedItems
        {
            get { return mExistedItems; }
            set
            {
                if (mExistedItems == value)
                    return;
                mExistedItems = value;
                RaisePropertyChanged(() => ExistedItems);
            }
        }

        #endregion

        #region Commands
        public RelayCommand EditCategoryProperties { get; set; }

        #endregion

        #region PropertiesOnView

        /// <summary>
        /// Отображает список свойств привязанных к данной категории
        /// </summary>
        public string PropertiesOnView
        {
            get
            {
                var result = "";
                foreach (var prop in EditedItem.Properties)
                {
                    result += string.Format(" {0} ", prop.Name);
                    foreach (var child in prop.Childs)
                    {
                        result += string.Format("{0}, ", child.Name);
                    }
                }
                return result;
            }
        }

        #endregion
        
        #endregion

        #region Constructors

        public CategoryEditViewVM(CategoriesViewItem editedItem, ObservableCollection<CategoriesViewItem> existedItems)
            : base(editedItem)
        {
            this.ExistedItems = existedItems;
        }

        public CategoryEditViewVM(ObservableCollection<CategoriesViewItem> existedItems)
            : base()
        {
            this.ExistedItems = existedItems;
        }

        #endregion

        #region Methods

        protected override void InitializeCommands()
        {
            EditCategoryProperties = new RelayCommand(OnEditCategoryProperties);
            base.InitializeCommands();
        }

        private void OnEditCategoryProperties()
        {
            var editVm = new CategoryPropertiesEditViewVM(EditedItem.Properties);
            editVm.DialogCallback += EditDialogCallback;
            new CategoryPropertiesEditView(editVm).ShowDialog();
        }

        private void EditDialogCallback(object sender, EditWindowClosedArgs e)
        {
            
        }


        #endregion

        protected override void OnOkCommand()
        {
            Category category;
            if (EditedItem.ID == 0)
            {
                category = new Category();
            }
            else
            {
                category = db.CategorySet.Where(x => x.Id == EditedItem.ID).FirstOrDefault();
            }

            category.Name = EditedItem.Name;
            category.Description = EditedItem.Description;
            category.Alias = Translit.RuToTranslite(category.Name);
            //category.GoodHear = EditedItem.IsFinal;
            //category.Image = EditedItem.ID;
            category.Parent = db.CategorySet.Where(x => x.Id == EditedItem.Parent).FirstOrDefault();

            if (category.Id == 0)
            {
                db.CategorySet.Add(category);
            }

            db.SaveChanges();

            CloseWindowAction.Invoke();
        }
    }
}
