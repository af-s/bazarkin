﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bazarkin.AdminPanel.Classes;
using Bazarkin.AdminPanel.CommonClasses;
using Bazarkin.AdminPanel.CommonViewModels;
using Bazarkin.AdminPanel.EventArgs;
using Bazarkin.AdminPanel.Interfaces;
using Bazarkin.AdminPanel.Views;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Bazarkin.Domain;

namespace Bazarkin.AdminPanel.ViewModels
{
    public class OrganizationEditViewVM : CommonEditViewVM<OrganizationsViewItem>
    {
        #region Properties
        EFDbContext db = new EFDbContext();

        #region SelectedOrganizationPoint

        private TradePointItem mSelectedOrganizationPoint;

        /// <summary>
        /// Выбранная торговая точка
        /// </summary>
        public TradePointItem SelectedOrganizationPoint
        {
            get { return mSelectedOrganizationPoint; }
            set
            {
                if (mSelectedOrganizationPoint == value)
                    return;
                mSelectedOrganizationPoint = value;
                RaisePropertyChanged(() => SelectedOrganizationPoint);
            }
        }

        #endregion

        #region SelectedOrganizationPhone

        private PhoneItem mSelectedOrganizationPhone;

        /// <summary>
        /// Выбранный телефон
        /// </summary>
        public PhoneItem SelectedOrganizationPhone
        {
            get { return mSelectedOrganizationPhone; }
            set
            {
                if (mSelectedOrganizationPhone == value)
                    return;
                mSelectedOrganizationPhone = value;
                RaisePropertyChanged(() => SelectedOrganizationPhone);
            }
        }

        #endregion

        #region SelectedOrganizationAddress

        private AddressItem mSelectedOrganizationAddress;

        /// <summary>
        /// Выбранный адрес
        /// </summary>
        public AddressItem SelectedOrganizationAddress
        {
            get { return mSelectedOrganizationAddress; }
            set
            {
                if (mSelectedOrganizationAddress == value)
                    return;
                mSelectedOrganizationAddress = value;
                RaisePropertyChanged(() => SelectedOrganizationAddress);
            }
        }

        #endregion

        #region SelectedOrganizationMail

        private MailItem mSelectedOrganizationMail;

        /// <summary>
        /// Выбранная почта
        /// </summary>
        public MailItem SelectedOrganizationMail
        {
            get { return mSelectedOrganizationMail; }
            set
            {
                if (mSelectedOrganizationMail == value)
                    return;
                mSelectedOrganizationMail = value;
                RaisePropertyChanged(() => SelectedOrganizationMail);
            }
        }

        #endregion

        #region Commands

        public RelayCommand EditPointCommand { get; set; }
        public RelayCommand DeletePointCommand { get; set; }

        public RelayCommand AddPhoneCommand { get; set; }
        public RelayCommand EditPhoneCommand { get; set; }
        public RelayCommand DeletePhoneCommand { get; set; }

        public RelayCommand AddAddressCommand { get; set; }
        public RelayCommand EditAddressCommand { get; set; }
        public RelayCommand DeleteAddressCommand { get; set; }

        public RelayCommand AddMailCommand { get; set; }
        public RelayCommand EditMailCommand { get; set; }
        public RelayCommand DeleteMailCommand { get; set; }

        #endregion

        #endregion

        #region Constructors

        public OrganizationEditViewVM(OrganizationsViewItem editedItem)
            : base(editedItem)
        {
            Organization organization = db.OrganizationSet.Where(x => x.Id == editedItem.ID).FirstOrDefault();

            // Торговый точки
            List<OrganizationUnit> organizationUnitSet = db.OrganizationUnitSet.Where(x => x.Organization.Id == organization.Id).ToList();
            foreach(var organizationUnit in organizationUnitSet)
            {
                TradePointItem tradePointItem = new TradePointItem();
                tradePointItem.ID = organizationUnit.Id;
                //tradePointItem.Name= organizationUnit.Name;
                tradePointItem.Description= organizationUnit.Description; 

                EditedItem.Points.Add(tradePointItem);
            }

            // Телефоны
            List<Phone> phoneSet = db.PhoneSet.Where(x => x.ContactGroup.Id == organization.ContactGroup.Id).OrderBy(x => x.Description).ToList();
            foreach (var phone in phoneSet)
            {
                PhoneItem phoneItem = new PhoneItem();
                phoneItem.ID = phone.Id;
                phoneItem.Number = phone.Number;
                phoneItem.Description = phone.Description;
                phoneItem.IsVisible = phone.View;

                EditedItem.Phones.Add(new PhoneItem());
            }

            // Адреса
            List<Address> addressSet = db.AddressSet.Where(x => x.ContactGroup.Id == organization.ContactGroup.Id).OrderBy(x => x.Description).ToList();
            foreach (var address in addressSet)
            {
                AddressItem addressItem = new AddressItem();
                addressItem.ID = address.Id;
                addressItem.Country = address.Country;
                addressItem.Region = address.Region;
                addressItem.City = address.City;
                addressItem.Street = address.Street;
                addressItem.Building = address.Building;
                addressItem.Structure = address.Structure;
                addressItem.Apartment = address.Room;
                addressItem.ShoppingCenter = address.ShoppingCenter;
                addressItem.Sector = address.Sector;
                addressItem.Row = address.Row;
                addressItem.Pavilion = address.Pavilion;
                addressItem.Index = address.Index;
                addressItem.Description = address.Description;
                addressItem.IsVisible = address.View;

                EditedItem.Phones.Add(new PhoneItem());
            }

            // Емайлы
            List<Mail> mailSet = db.MailSet.Where(x => x.ContactGroup.Id == organization.ContactGroup.Id).OrderBy(x => x.Description).ToList();
            foreach (var mail in mailSet)
            {
                MailItem mailItem = new MailItem();
                mailItem.ID = mail.Id;
                mailItem.Mail = mail.Value;
                mailItem.Description = mail.Description;

                EditedItem.Mails.Add(new MailItem());
            }
        }

        public OrganizationEditViewVM()
            : base() { }

        #endregion

        #region Handlers

        /// <summary>
        /// Обрабочтик события кнопки "Удалить торговую точку"
        /// </summary>
        private void OnDeletePointCommand()
        {
            EditedItem.Points.Remove(SelectedOrganizationPoint);
        }

        /// <summary>
        /// Обрабочтик события кнопки "Редактировать торговую точку"
        /// </summary>
        private void OnEditPointCommand()
        {
            var editVm = new TradePointEditViewVM(SelectedOrganizationPoint);
            editVm.DialogCallback += EditDialogCallback;
            new TradePointEditView(editVm).ShowDialog();
        }

        /// <summary>
        /// Обрабочтик события кнопки "Удалить телефон"
        /// </summary>
        private void OnDeletePhoneCommand()
        {
            EditedItem.Phones.Remove(SelectedOrganizationPhone);
        }

        /// <summary>
        /// Обрабочтик события кнопки "Редактировать телефон"
        /// </summary>
        private void OnEditPhoneCommand()
        {
            var editVm = new PhoneEditViewVM(SelectedOrganizationPhone);
            editVm.DialogCallback += EditDialogCallback;
            new PhoneEditView(editVm).ShowDialog();
        }

        /// <summary>
        /// Обрабочтик события кнопки "Удалить адрес"
        /// </summary>
        private void OnDeleteAddressCommand()
        {
            EditedItem.Addresses.Remove(SelectedOrganizationAddress);
        }

        /// <summary>
        /// Обрабочтик события кнопки "Редактировать адрес"
        /// </summary>
        private void OnEditAddressCommand()
        {
            var editVm = new OrganizationAddressEditViewVM(SelectedOrganizationAddress);
            editVm.DialogCallback += EditDialogCallback;
            new OrganizationAddressEditView(editVm).ShowDialog();
        }

        /// <summary>
        /// Обрабочтик события кнопки "Удалить почту"
        /// </summary>
        private void OnDeleteMailCommand()
        {
            EditedItem.Mails.Remove(SelectedOrganizationMail);
        }

        /// <summary>
        /// Обрабочтик события кнопки "Редактировать почту"
        /// </summary>
        private void OnEditMailCommand()
        {
            var editVm = new OrganizationMailEditViewVM(SelectedOrganizationMail);
            editVm.DialogCallback += EditDialogCallback;
            new OrganizationMailEditView(editVm).ShowDialog();
        }

        private void OnAddMailCommand()
        {
            var editVm = new OrganizationMailEditViewVM();
            editVm.DialogCallback += EditDialogCallback;
            new OrganizationMailEditView(editVm).ShowDialog();
        }

        private void OnAddAddressCommand()
        {
            var editVm = new OrganizationAddressEditViewVM();
            editVm.DialogCallback += EditDialogCallback;
            new OrganizationAddressEditView(editVm).ShowDialog();
        }

        private void OnAddPhoneCommand()
        {
            var editVm = new PhoneEditViewVM();
            editVm.DialogCallback += EditDialogCallback;
            new PhoneEditView(editVm).ShowDialog();
        }


        #endregion

        #region Methods

        protected override void InitializeCommands()
        {
            EditPointCommand = new RelayCommand(OnEditPointCommand);
            DeletePointCommand = new RelayCommand(OnDeletePointCommand);
            EditPhoneCommand = new RelayCommand(OnEditPhoneCommand);
            DeletePhoneCommand = new RelayCommand(OnDeletePhoneCommand);
            EditAddressCommand = new RelayCommand(OnEditAddressCommand);
            DeleteAddressCommand = new RelayCommand(OnDeleteAddressCommand);
            EditMailCommand = new RelayCommand(OnEditMailCommand);
            DeleteMailCommand = new RelayCommand(OnDeleteMailCommand);
            AddPhoneCommand = new RelayCommand(OnAddPhoneCommand);
            AddAddressCommand = new RelayCommand(OnAddAddressCommand);
            AddMailCommand = new RelayCommand(OnAddMailCommand);
            base.InitializeCommands();
        }

        /// <summary>
        /// Срабатывает по завершении редактирования записи
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditDialogCallback(object sender, EditWindowClosedArgs e)
        {
            if (!e.DialogResult) return;
            switch (((Type)e.EditedItem.GetType()).Name)
            {
                case "PhoneItem":
                    break;
                case "MailItem": 
                    break;
                case "OrganizationPointItem": 
                    break;
                case "OrganizationAddressItem": 
                    break;
            }
        }

        #endregion

        protected override void OnOkCommand()
        {
            Organization organization;
            if (EditedItem.ID == 0)
            {
                organization = new Organization();
            }
            else
            {
                organization = db.OrganizationSet.Where(x => x.Id == EditedItem.ID).FirstOrDefault();
            }

            organization.Name = EditedItem.Name;
            organization.Description = EditedItem.Description;
            organization.Active = EditedItem.IsActive;

            if (organization.Id == 0)
            {
                db.OrganizationSet.Add(organization);
            }

            db.SaveChanges();

            CloseWindowAction.Invoke();
        }
    }
}
