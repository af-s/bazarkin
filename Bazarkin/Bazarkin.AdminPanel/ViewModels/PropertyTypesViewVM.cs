﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bazarkin.AdminPanel.Classes;
using Bazarkin.AdminPanel.CommonViewModels;
using Bazarkin.AdminPanel.EventArgs;
using Bazarkin.AdminPanel.Views;
using Bazarkin.Domain;

namespace Bazarkin.AdminPanel.ViewModels
{
    public class PropertyTypesViewVM : CommonMainItemVM<PropertyTypeItem>
    {
        #region Constructors

        public PropertyTypesViewVM()
            : base("Типы свойств")
        {
        }

        #endregion

        #region Methods

        protected override void FillDisplayedCollection()
        {
            EFDbContext db = new EFDbContext();
            List<PropertyType> propertyTypeSet = db.PropertyTypeSet.OrderBy(x => x.Name).ToList();

            DisplayedCollection = new ObservableCollection<PropertyTypeItem>();

            foreach(var propertyType in propertyTypeSet)
            {
                var item = new PropertyTypeItem
                {
                    ID = propertyType.Id,
                    Description = propertyType.Description,
                    Name = propertyType.Name,
                    FilterType = propertyType.FilterType,
                    Format = propertyType.ValueFormat
                };
                DisplayedCollection.Add(item);
            }
        }

        protected override void EditDialogCallback(object sender, EditWindowClosedArgs e)
        {

        }
        #endregion

        #region Handlers

        protected override void OnAddCommand()
        {
            var editVm = new PropertyTypesEditViewVM();
            editVm.DialogCallback += EditDialogCallback;
            new PropertyTypesEditView(editVm).ShowDialog();
        }

        protected override void OnFindCommand()
        {
        
        }

        protected override void OnEditCommand()
        {
            var editVm = new PropertyTypesEditViewVM(SelectedItem);
            editVm.DialogCallback += EditDialogCallback;
            new PropertyTypesEditView(editVm).ShowDialog();
        }
        #endregion
    }
}
