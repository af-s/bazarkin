﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bazarkin.AdminPanel.CommonClasses;
using Bazarkin.AdminPanel.CommonViewModels;

namespace Bazarkin.AdminPanel.ViewModels
{
    public class PhoneEditViewVM : CommonEditViewVM<PhoneItem>
    {
          public PhoneEditViewVM(PhoneItem editedItem)
            : base(editedItem) { }

          public PhoneEditViewVM()
            : base() { }
    }
}
