﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bazarkin.AdminPanel.Classes;
using Bazarkin.AdminPanel.CommonViewModels;
using Bazarkin.AdminPanel.EventArgs;
using Bazarkin.AdminPanel.Interfaces;
using Bazarkin.AdminPanel.Views;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Bazarkin.Domain;

namespace Bazarkin.AdminPanel.ViewModels
{
    public class ManufacturersViewVM : CommonMainItemVM<ManufacturersViewItem>
    {
 

        #region Constructors

        public ManufacturersViewVM()
            : base("Производители")
        {
        }

        #endregion

        #region Methods

        protected override void FillDisplayedCollection()
        {
            EFDbContext db = new EFDbContext();
            List<Manufacturer> manufacturerSet = db.ManufacturerSet.OrderBy(x=>x.Name).ToList();

            DisplayedCollection = new ObservableCollection<ManufacturersViewItem>();

            foreach(var manufacturer in manufacturerSet)
            {
                DisplayedCollection.Add(new ManufacturersViewItem
                {
                    ID = manufacturer.Id,
                    Name = manufacturer.Name,
                    Description = manufacturer.Description,
                    Avatar = manufacturer.Avatar
                });
            }

            //for (int x = 0; x < 20; x++)
            //{
            //    DisplayedCollection.Add(new ManufacturersViewItem
            //    {
            //        ID = x,
            //        Name = "Name" + x,
            //        Description = "Description" + x,
            //        Avatar = "Avatar"+x
            //    });
            //}
        }

        protected override void EditDialogCallback(object sender, EditWindowClosedArgs e)
        {

        }

        #endregion

        #region Handlers

        protected override void OnAddCommand()
        {
            var editVm = new ManufacturerEditViewVM();
            editVm.DialogCallback += EditDialogCallback;
            new ManufacturerEditView(editVm).ShowDialog();
        }

        protected override void OnFindCommand()
        {

        }

        protected override void OnEditCommand()
        {
            var editVm = new ManufacturerEditViewVM(SelectedItem);
            editVm.DialogCallback += EditDialogCallback;
            new ManufacturerEditView(editVm).ShowDialog();
        }

        #endregion


    }
}
