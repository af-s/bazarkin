﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using Bazarkin.AdminPanel.Classes;
using Bazarkin.AdminPanel.Enums;
using Bazarkin.AdminPanel.EventArgs;
using Bazarkin.AdminPanel.Views;
using GalaSoft.MvvmLight;

namespace Bazarkin.AdminPanel.ViewModels
{
    public class MainWindowVM : ViewModelBase
    {
        #region Properties

        #region MainWindowContent

        private UserControl mMainWindowContent;

        /// <summary>
        /// Контент заполняющий основное окно (MainWindow)
        /// </summary>
        public UserControl MainWindowContent
        {
            get { return mMainWindowContent; }
            set
            {
                if (mMainWindowContent == value)
                    return;
                mMainWindowContent = value;
                RaisePropertyChanged(() => MainWindowContent);
            }
        }

        #endregion

        #endregion

        #region Constructos

        public MainWindowVM()
        {
            InitializeAuthorizationView();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Инициализация формы авторизации
        /// </summary>
        private void InitializeAuthorizationView()
        {
            var authVm = new AuthorizationViewVM();
            authVm.UserAuthorizationResult += UserAuthorizationResultHandler;
            MainWindowContent = new AuthorizationView() { DataContext = authVm };
            MainWindowManager.Instance.SetWindowTitle("Авторизация");
        }

        /// <summary>
        /// Обработчик события авторизации
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserAuthorizationResultHandler(object sender, AuthorizationViewLoginArgs e)
        {
            if (!e.LoginResult) return;

            MainWindowManager.Instance.SetWindowSize(WindowSizes.MainViewSize);

            var mainViewVm = new MainViewVM();
            mainViewVm.DisplayView += DisplayView;
            mainViewVm.RefreshView();
        }

        /// <summary>
        /// Отобразить новое View на форме
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DisplayView(object sender, DisplayViewArgs e)
        {
            MainWindowContent = e.ViewToDisplay;
        }

        #endregion
    }
}
