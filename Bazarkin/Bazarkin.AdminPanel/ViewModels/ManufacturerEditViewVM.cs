﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bazarkin.AdminPanel.Classes;
using Bazarkin.AdminPanel.CommonViewModels;
using Bazarkin.AdminPanel.EventArgs;
using Bazarkin.AdminPanel.Interfaces;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Bazarkin.Domain;
using Bazarkin.ClassLibrary;

namespace Bazarkin.AdminPanel.ViewModels
{
    public class ManufacturerEditViewVM : CommonEditViewVM<ManufacturersViewItem>
    {
        EFDbContext db = new EFDbContext();

        #region Constructors

        public ManufacturerEditViewVM(ManufacturersViewItem editedItem)
            : base(editedItem) { }
        public ManufacturerEditViewVM()
            : base() { }

        #endregion
        protected override void OnOkCommand()
        {
            Manufacturer manufacturer;
            if (EditedItem.ID == 0)
            {
                manufacturer = new Manufacturer();
            }
            else
            {
                manufacturer = db.ManufacturerSet.Where(x => x.Id == EditedItem.ID).FirstOrDefault();
            }

            manufacturer.Name = EditedItem.Name.Trim();
            manufacturer.Description = EditedItem.Description.Trim();
            manufacturer.Avatar = (EditedItem.Avatar == null || EditedItem.Avatar.Trim() == string.Empty) ? null : EditedItem.Avatar.Trim();
            manufacturer.Alias = Translit.RuToTranslite(manufacturer.Name);

            if (manufacturer.Id == 0)
            {
                db.ManufacturerSet.Add(manufacturer);
            }

            db.SaveChanges();

            CloseWindowAction.Invoke();
        }
    }
}
