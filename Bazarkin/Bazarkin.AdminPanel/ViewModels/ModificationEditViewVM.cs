﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bazarkin.AdminPanel.Classes;
using Bazarkin.AdminPanel.EventArgs;
using Bazarkin.AdminPanel.Interfaces;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Bazarkin.AdminPanel.ViewModels
{
    public class ModificationEditViewVM : ViewModelBase, IEditWindowClosed
    {
        #region Properties

        public Action CloseWindowAction { get; set; }
        public EventHandler<EditWindowClosedArgs> DialogCallback { get; set; }

        #region Commands

        public RelayCommand OkCommand { get; set; }
        public RelayCommand CancelCommand { get; set; }

        #endregion

        #region DisplayedTree

        private ObservableCollection<PropertyGroupAndProperties> mDisplayedTree;

        /// <summary>
        /// Отображаемое дерево
        /// </summary>
        public ObservableCollection<PropertyGroupAndProperties> DisplayedTree
        {
            get { return mDisplayedTree; }
            set
            {
                if (mDisplayedTree == value)
                    return;
                mDisplayedTree = value;
                RaisePropertyChanged(() => DisplayedTree);
            }
        }

        #endregion

        #endregion

        #region Constructors

        public ModificationEditViewVM(ProductModificationItem modification)
        {
            InitializeCommands();
            FillDisplayedTree();
        }

        public ModificationEditViewVM()
        {
            InitializeCommands();
            FillDisplayedTree(); 
        }

        #endregion

        #region Methods

        private void InitializeCommands()
        {
            OkCommand = new RelayCommand(OnOkCommand);
            CancelCommand = new RelayCommand(OnCancelCommand);
        }

        private EditWindowClosedArgs GetDialogResultArgs(bool dialogResult)
        {
            return dialogResult ? new EditWindowClosedArgs(dialogResult, DisplayedTree) : new EditWindowClosedArgs(dialogResult);
        }

        /// <summary>
        /// Заполняет дерево
        /// </summary>
        private void FillDisplayedTree()
        {
            DisplayedTree= new ObservableCollection<PropertyGroupAndProperties>();
            for (int i = 0; i < 10; i++)
            {
                var item = new PropertyGroupAndProperties
                {
                    PropertyGroup = new PropertyGroupItem {ID = i, Name = "GoupName" + i},
                    PropertyAndValues = new ObservableCollection<PropertyAndValues>()
                };
                for (int j = 0; j < 5; j++)
                {
                    var propItem = new PropertyAndValues
                    {
                        Property = new PropertyItem{ID = j,Name = "PropertyName"+j,PropertyGroup =i},
                        PropertyValues = new ObservableCollection<string>()
                    };
                    for (int k = 0; k < 6; k++)
                    {
                        propItem.PropertyValues.Add("Value" + k);
                    }
                    item.PropertyAndValues.Add(propItem);
                }
                DisplayedTree.Add(item);
            }
        }

        #endregion

        #region Handlers

        private void OnOkCommand()
        {
            DialogCallback.Invoke(this, GetDialogResultArgs(true));
            CloseWindowAction.Invoke();
        }

        private void OnCancelCommand()
        {
            DialogCallback.Invoke(this, GetDialogResultArgs(false));
            CloseWindowAction.Invoke();
        }

        #endregion


    }
}
