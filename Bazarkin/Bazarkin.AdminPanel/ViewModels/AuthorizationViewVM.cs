﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Bazarkin.AdminPanel.EventArgs;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Bazarkin.AdminPanel.ViewModels
{
    public class AuthorizationViewVM : ViewModelBase
    {
        #region Properties

        #region LoginCommandOk
        /// <summary>
        /// Событие будет возвращаеть в MainWindowVM результаты авторизации
        /// </summary>
        public EventHandler<AuthorizationViewLoginArgs> UserAuthorizationResult;

        #endregion

        #region Login

        private string mLogin;

        /// <summary>
        /// Логин
        /// </summary>
        public string Login
        {
            get { return mLogin; }
            set
            {
                if (mLogin == value)
                    return;
                mLogin = value;
                RaisePropertyChanged(() => Login);
                LoginCommand.RaiseCanExecuteChanged();
            }
        }

        #endregion

        #region Password
        /// <summary>
        /// Пароль
        /// </summary>
        private string _password;

        #endregion

        #region Info

        private string mInfo;

        /// <summary>
        /// Информация 
        /// </summary>
        public string Info
        {
            get { return mInfo; }
            set
            {
                if (mInfo == value)
                    return;
                mInfo = value;
                RaisePropertyChanged(() => Info);
            }
        }

        #endregion

        #endregion

        #region Commands
        public RelayCommand LoginCommand { get; set; }

        #endregion

        #region Constructors

        public AuthorizationViewVM()
        {
            LoginCommand = new RelayCommand(OnLoginCommand, LoginCommandCanExecute);
        }

        #endregion

        #region Handlers

        /// <summary>
        /// Проверяем заполнены ли Логин и Пароль
        /// </summary>
        /// <returns></returns>
        private bool LoginCommandCanExecute()
        {
            return true;
            //return !string.IsNullOrEmpty(Login) && !string.IsNullOrEmpty(_password);
        }

        /// <summary>
        /// Кнопка входа нажата
        /// </summary>
        private void OnLoginCommand()
        {
            LoginCommandExecuted(CheckUserAccount() && UserAuthorizationResult != null);
        }

        #endregion

        #region Methods

        /// <summary>
        /// В будущем метод предполагет криптографию пароля (предлагаю создать для этого отдельный класс и вызывать его тут),
        /// пока что просто заполняет поле Password (можно использовать HMACSHA256)
        /// </summary>
        /// <param name="password"></param>
        public void SetPassword(string password)
        {
            this._password = password;
            LoginCommand.RaiseCanExecuteChanged();
        }

        /// <summary>
        /// Предполагается проверка учетных данных пользователя
        /// </summary>
        private bool CheckUserAccount()
        {
            return true;
        }

        /// <summary>
        /// Возвращаем в MainWindowVM результаты авторизации
        /// </summary>
        /// <param name="result"></param>
        private void LoginCommandExecuted(bool result)
        {
            var args = new AuthorizationViewLoginArgs(result);
            UserAuthorizationResult.Invoke(this, args);
        }

        #endregion
    }
}
