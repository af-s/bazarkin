﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bazarkin.AdminPanel.Classes;
using Bazarkin.AdminPanel.EventArgs;
using Bazarkin.AdminPanel.Interfaces;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Bazarkin.AdminPanel.ViewModels
{
    public class CategoryPropertiesEditViewVM : ViewModelBase, IEditWindowClosed
    {
        #region Properties

        public Action CloseWindowAction { get; set; }
        public EventHandler<EditWindowClosedArgs> DialogCallback { get; set; }

        #region DisplayedTree

        private ObservableCollection<CategoryPropertiesTreeViewItem> mDisplayedTree;

        /// <summary>
        /// Отображаемое дерево в CategoryPropertiesEditView
        /// </summary>
        public ObservableCollection<CategoryPropertiesTreeViewItem> DisplayedTree
        {
            get { return mDisplayedTree; }
            set
            {
                if (mDisplayedTree == value)
                    return;
                mDisplayedTree = value;
                RaisePropertyChanged(() => DisplayedTree);
            }
        }

        #endregion

        #region Commands

        public RelayCommand OkCommand { get; set; }
        public RelayCommand CancelCommand { get; set; }

        #endregion
         
        #endregion

        #region Constructors

        public CategoryPropertiesEditViewVM(ObservableCollection<CategoryPropertiesTreeViewItem> properties)
        {
            InitializeCommands();
            DisplayedTree = properties;
        }

        #endregion

        #region Methods

        private void InitializeCommands()
        {
            OkCommand = new RelayCommand(OnOkCommand);
            CancelCommand = new RelayCommand(OnCancelCommand);
        }

        private EditWindowClosedArgs GetDialogResultArgs(bool dialogResult)
        {
            return dialogResult ? new EditWindowClosedArgs(dialogResult, DisplayedTree) : new EditWindowClosedArgs(dialogResult);
        }

        #endregion

        #region Handlers

        private void OnOkCommand()
        {
            DialogCallback.Invoke(this, GetDialogResultArgs(true));
            CloseWindowAction.Invoke();
        }

        private void OnCancelCommand()
        {
            DialogCallback.Invoke(this, GetDialogResultArgs(false));
            CloseWindowAction.Invoke();
        }

        #endregion
    }
}
