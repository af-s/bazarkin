﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bazarkin.AdminPanel.Classes;
using Bazarkin.AdminPanel.CommonViewModels;

namespace Bazarkin.AdminPanel.ViewModels
{
    public class PropertyEditViewVM : CommonEditViewVM<PropertyItem>
    {
        #region Properties

        #region PropertyGroups

        private ObservableCollection<PropertyGroupItem> mPropertyGroups;

        /// <summary>
        /// Группы свойств
        /// </summary>
        public ObservableCollection<PropertyGroupItem> PropertyGroups
        {
            get { return mPropertyGroups; }
            set
            {
                if (mPropertyGroups == value)
                    return;
                mPropertyGroups = value;
                RaisePropertyChanged(() => PropertyGroups);
            }
        }

        #endregion

        #region PropertyTypes

        private ObservableCollection<PropertyTypeItem> mPropertyTypes;

        /// <summary>
        /// Типы свойств
        /// </summary>
        public ObservableCollection<PropertyTypeItem> PropertyTypes
        {
            get { return mPropertyTypes; }
            set
            {
                if (mPropertyTypes == value)
                    return;
                mPropertyTypes = value;
                RaisePropertyChanged(() => PropertyTypes);
            }
        }

        #endregion

        #endregion

        #region Constructors

        public PropertyEditViewVM(PropertyItem editedItem)
            : base(editedItem)
        {
            GetGroups();
            GetTypes();
        }

        public PropertyEditViewVM()
            : base()
        {
            GetGroups();
            GetTypes();
        }

        #endregion

        #region Methods

        private void GetGroups()
        {
            PropertyGroups= new ObservableCollection<PropertyGroupItem>();
            for (int x = 0; x < 20; x++)
            {
                var item = new PropertyGroupItem
                {
                    ID = x,
                    Description = "Description" + x,
                    Name = "Name" + x
                };
                PropertyGroups.Add(item);
            }
        }

        private void GetTypes()
        {
            PropertyTypes=new ObservableCollection<PropertyTypeItem>();
            for (int x = 0; x < 20; x++)
            {
                var item = new PropertyTypeItem
                {
                    ID = x,
                    Description = "Description" + x,
                    Name = "Name" + x,
                    FilterType = "FilterType" + x,
                    Format = "Format" + x
                };
                PropertyTypes.Add(item);
            }
        }
        #endregion
    }
}
