﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bazarkin.AdminPanel.Classes;
using Bazarkin.AdminPanel.CommonClasses;
using Bazarkin.AdminPanel.CommonViewModels;
using Bazarkin.AdminPanel.EventArgs;
using Bazarkin.AdminPanel.Interfaces;
using Bazarkin.AdminPanel.Views;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Bazarkin.Domain;

namespace Bazarkin.AdminPanel.ViewModels
{
    public class TradePointsViewVM :CommonMainItemVM<TradePointItem>
    {
        #region Constructors

        public TradePointsViewVM()
            : base("Торговые точки")
        {
        }

        #endregion

        #region Methods

        protected override void FillDisplayedCollection()
        {
            EFDbContext db = new EFDbContext();
            List<OrganizationUnit> organizationUnitSet = db.OrganizationUnitSet.ToList();

            DisplayedCollection = new ObservableCollection<TradePointItem>();

            foreach(var organizationUnit in organizationUnitSet)
            {
                var tradeItem = new TradePointItem();
                tradeItem.ID= organizationUnit.Id;
                //tradeItem.Name= organizationUnit.Name;
                //tradeItem.Description= organizationUnit.Description;
                tradeItem.IsActive= organizationUnit.Active;
                tradeItem.PaidTo= organizationUnit.PayerDate.GetValueOrDefault();
                DisplayedCollection.Add(tradeItem);
            }

            //bool b = false;
            //for (int x = 0; x < 20; x++)
            //{
            //    var tradeItem = new TradePointItem
            //    {
            //        ID = x,
            //        IsActive = b,
            //        Addresses = new ObservableCollection<AddressItem>(),
            //        PaidTo = DateTime.Now,
            //        Description = "Description" + x,
            //        Name = "Name" + x,
            //        Phones = new ObservableCollection<PhoneItem>(),
            //    };
            //    var s = false;
            //    for (int k = 0; k < 5; k++)
            //    {
            //        tradeItem.Phones.Add(new PhoneItem
            //        {
            //            ID = k,
            //            Description = "Description" + k,
            //            Number = "+7xxxxxxxxx" + k,
            //            IsVisible = s
            //        });
            //        s = !s;
            //    }
            //    for (int i = 0; i < 3; i++)
            //    {
            //        tradeItem.Addresses.Add(new AddressItem
            //       {
            //           ID = i,
            //           City = "City"+i
            //       }); 
            //    }
            //    DisplayedCollection.Add(tradeItem);
            //    b = !b;
            //}
        }

        protected override void EditDialogCallback(object sender, EditWindowClosedArgs e)
        {
            
        }

        #endregion

        #region Handlers

        protected override void OnAddCommand()
        {
            var editVm = new TradePointEditViewVM();
            editVm.DialogCallback += EditDialogCallback;
            new TradePointEditView(editVm).ShowDialog();
        }

        protected override void OnFindCommand()
        {
            
        }


        protected override void OnEditCommand()
        {
            var editVm = new TradePointEditViewVM(SelectedItem);
            editVm.DialogCallback += EditDialogCallback;
            new TradePointEditView(editVm).ShowDialog();
        }

        #endregion


    }
}
