﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bazarkin.AdminPanel.OtherClasses
{
    /*
        <!--На случай если из enum(а) надо будет получать значение(1,2) а не наименование-->
        <!--<x:Array x:Key="FilterTypesArray" Type="{x:Type otherClasses:NameValueType}">
            <otherClasses:NameValueType Value="{x:Static enums:FilterTypes.list}" Name="list"/>
            <otherClasses:NameValueType Value="{x:Static enums:FilterTypes.interval}" Name="interval"/>
        </x:Array>-->
    */
    public class NameValueType
    {
        public string Name { get; set; }
        public int Value { get; set; }
    }
}
