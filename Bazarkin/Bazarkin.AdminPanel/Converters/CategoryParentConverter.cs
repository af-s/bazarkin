﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using Bazarkin.AdminPanel.Classes;

namespace Bazarkin.AdminPanel.Converters
{
    /// <summary>
    /// Конвертируем идентификатор родителя для категории в наименование
    /// </summary>
    public class CategoryParentConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var collection = values.FirstOrDefault(x => x is IEnumerable<CategoriesViewItem>);
            var identifier = values.FirstOrDefault(x => x is int);

            if (collection == null || identifier == null)
                return null;

            var currentCategory = ((IEnumerable<CategoriesViewItem>) collection).FirstOrDefault(x => x.ID == (int) identifier);

            return currentCategory == null ? null : currentCategory.Name;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
