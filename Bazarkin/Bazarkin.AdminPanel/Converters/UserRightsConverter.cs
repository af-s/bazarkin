﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using Bazarkin.AdminPanel.Classes;

namespace Bazarkin.AdminPanel.Converters
{
    public class UserRightsConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var tmp = value as ObservableCollection<RightsItem>;
            if (tmp == null) return null;

            var result = "";
            foreach (var item in tmp)
            {
                if (string.IsNullOrEmpty(result))
                    result += item.Name;
                else
                    result += ", " + item.Name;
            }
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
