﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using Bazarkin.AdminPanel.Classes;

namespace Bazarkin.AdminPanel.Converters
{
    public class OrganizationPointItemsToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var collection = value as ObservableCollection<TradePointItem>;
            if (collection == null) return null;

            var result = "";
            foreach (var point in collection)
            {
                if (string.IsNullOrEmpty(result))
                {
                    result += point.Addresses;
                    continue;
                }
                result += string.Format(", {0}", point.Addresses);
            }
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
