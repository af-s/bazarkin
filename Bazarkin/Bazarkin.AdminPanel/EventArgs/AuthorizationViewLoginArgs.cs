﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bazarkin.AdminPanel.EventArgs
{
    public class AuthorizationViewLoginArgs:System.EventArgs
    {
        /// <summary>
        /// Результат авторизации
        /// </summary>
        public bool LoginResult { get; set; }

        public AuthorizationViewLoginArgs(bool result)
        {
            this.LoginResult = result;
        }
    }
}
