﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bazarkin.AdminPanel.EventArgs
{
    public class ValueChangedArgs : System.EventArgs
    {
        public dynamic PropertyValue { get; set; }
        public string PropertyName { get; set; }
        public ValueChangedArgs(dynamic propertyValue, string propertyName)
        {
            this.PropertyName = propertyName;
            this.PropertyValue = propertyValue;
        }
    }
}
