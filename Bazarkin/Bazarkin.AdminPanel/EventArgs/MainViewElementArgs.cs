﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using Bazarkin.AdminPanel.Enums;
using Bazarkin.AdminPanel.ViewModels;
using Bazarkin.AdminPanel.Views;

namespace Bazarkin.AdminPanel.EventArgs
{
    public class MainViewElementArgs : System.EventArgs
    {
        #region Properties

        /// <summary>
        /// Предполагается использовать для идентификации элемента в главном меню, который вызвал событие нажатия по нему.
        /// </summary>
        public MainViewElementsIdentifiers ElementIdentity { get; set; }

        /// <summary>
        /// Представление которое отобразится по нажатии на элемент в главном меню
        /// </summary>
        public UserControl ElementView { get; set; }

        /// <summary>
        /// ViewModel для ElementView ↑
        /// </summary>
        public dynamic ElementViewModel { get; set; }

        #endregion

        #region Constructors

        public MainViewElementArgs(MainViewElementsIdentifiers elementIdentity)
        {
            this.ElementIdentity = elementIdentity;
            SetViewAndViewModel();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Устанавливаем View и ViewModel которые будем использовать на форме
        /// </summary>
        private void SetViewAndViewModel()
        {
            switch (ElementIdentity)
            {
                case MainViewElementsIdentifiers.Categories:
                    ElementView = new CategoriesView();
                    ElementViewModel = new CategoriesViewVM();
                    break;

                case MainViewElementsIdentifiers.Users:
                    ElementView = new UsersView();
                    ElementViewModel = new UsersViewVM();
                    break;

                case MainViewElementsIdentifiers.Manufacturers:
                    ElementView = new ManufacturersView();
                    ElementViewModel = new ManufacturersViewVM();
                    break;

                case MainViewElementsIdentifiers.Organizations:
                    ElementView = new OrganizationsView();
                    ElementViewModel = new OrganizationsViewVM();
                    break;

                case MainViewElementsIdentifiers.TradePoints:
                    ElementView = new TradePointsView();
                    ElementViewModel = new TradePointsViewVM();
                    break;

                case MainViewElementsIdentifiers.Products:
                    ElementView = new ProductsView();
                    ElementViewModel = new ProductsViewVM();
                    break;

                case MainViewElementsIdentifiers.PropertyTypes:
                    ElementView = new PropertyTypesView();
                    ElementViewModel = new PropertyTypesViewVM();
                    break;

                case MainViewElementsIdentifiers.PropertyGroups:
                    ElementView = new PropertyGroupsView();
                    ElementViewModel = new PropertyGroupsViewVM();
                    break;
            }
        }

        #endregion
    }
}
