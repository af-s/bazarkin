﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Bazarkin.AdminPanel.EventArgs
{
    public class DisplayViewArgs : System.EventArgs
    {
        public UserControl ViewToDisplay { get; set; }

        /// <summary>
        /// Заполняем аргументы для view которая будут отображаться 
        /// </summary>
        /// <param name="view">View отображаемая по клику на элемент</param>
        /// <param name="viewModel">ViewModel для элемента</param>
        /// <param name="returnPreviousViewCommand">Событие для возврата в предыдущее окно</param>
        public DisplayViewArgs(UserControl view, dynamic viewModel, EventHandler returnPreviousViewCommand = null)
        {
            var control = view;
            SetReturnPreviousViewActionToViewModel(viewModel,returnPreviousViewCommand);
            control.DataContext = viewModel;
            ViewToDisplay = control;
        }

        private void SetReturnPreviousViewActionToViewModel(dynamic viewModel, EventHandler action)
        {
            if (viewModel == null || action == null) return;
            Type type = viewModel.GetType();
            var property = type.GetProperties().FirstOrDefault(x => x.Name.Contains("ReturnPreviousView"));
            if (property != null)
                viewModel.ReturnPreviousView += action;
        }
    }
}
