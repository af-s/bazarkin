﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bazarkin.AdminPanel.EventArgs
{
    public class EditWindowClosedArgs : System.EventArgs
    {
        #region Properties

        public bool DialogResult { get; set; }

        public dynamic EditedItem { get; set; }

        #endregion

        #region Constructors

        public EditWindowClosedArgs(bool dialogResult, dynamic editedItem = null)
        {
            this.DialogResult = dialogResult;
            this.EditedItem = editedItem;
        }

        #endregion
    }
}
