﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bazarkin.AdminPanel.EventArgs;
using Bazarkin.AdminPanel.Interfaces;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Bazarkin.AdminPanel.CommonViewModels
{
    /// <summary>
    /// Generic для окон редактирования записей
    /// </summary>
    /// <typeparam name="T">Тип редактируемой записи</typeparam>
    public class CommonEditViewVM<T> : ViewModelBase, IEditWindowClosed where T : class
    {
        #region Properties

        public Action CloseWindowAction { get; set; }
        public EventHandler<EditWindowClosedArgs> DialogCallback { get; set; }

        #region EditedItem

        private T mEditedItem;

        /// <summary>
        /// Редактируемая или вновь создаваемая запись
        /// </summary>
        public T EditedItem
        {
            get { return mEditedItem; }
            set
            {
                if (mEditedItem == value)
                    return;
                mEditedItem = value;
                RaisePropertyChanged(() => EditedItem);
            }
        }

        #endregion

        #region Commands

        public RelayCommand OkCommand { get; set; }
        public RelayCommand CancelCommand { get; set; }

        #endregion

        #endregion

        #region Constructors

        public CommonEditViewVM(T editedItem)
        {
            /*Данная приблуда вызывает конструктор пришелшего типа,т.к. generic типы не умеют этого делать, и передает в него параметры*/
            EditedItem = Activator.CreateInstance(typeof(T), editedItem) as T;
            InitializeCommands();
        }

        public CommonEditViewVM()
        {
            /*Аналогичная хрень конструктору выше, только без параметров*/
            EditedItem = Activator.CreateInstance(typeof(T)) as T;
            InitializeCommands();
        }

        #endregion

        #region Methods

        protected virtual void InitializeCommands()
        {
            OkCommand = new RelayCommand(OnOkCommand, OkCommandCanExecute);
            CancelCommand = new RelayCommand(OnCancelCommand);
        }

        private EditWindowClosedArgs GetDialogResultArgs(bool dialogResult)
        {
            return dialogResult ? new EditWindowClosedArgs(dialogResult, EditedItem) : new EditWindowClosedArgs(dialogResult);
        }

        #endregion

        #region Handlers ВО ВСЕХ РЕАЛИЗОВАННЫХ ОБРАБОТЧИКАХ СЛОВО "virtual" СТОИТ НЕ ПРОСТО ТАК! (Ваш капитан очевидность =))

        /// <summary>
        /// Обработчик события нажатия на кнопку "Отмена"
        /// </summary>
        protected virtual void OnCancelCommand()
        {
            DialogCallback.Invoke(this, GetDialogResultArgs(false));
            CloseWindowAction.Invoke();
        }

        /// <summary>
        /// Запретить нажатие кнопки Сохранить пока не заполнены все необходимые поля!!!!!
        /// </summary>
        /// <returns></returns>
        protected virtual bool OkCommandCanExecute()
        {
            return true;
        }

        /// <summary>
        /// Обработчик события нажания на кнопку "Сохранить"
        /// </summary>
        protected virtual void OnOkCommand()
        {
            DialogCallback.Invoke(this, GetDialogResultArgs(true));
            CloseWindowAction.Invoke();
        }

        /// <summary>
        /// Загрузка редактируемого элемента
        /// </summary>
        protected virtual void LoadItem()
        {
            
        }

        #endregion
    }
}
