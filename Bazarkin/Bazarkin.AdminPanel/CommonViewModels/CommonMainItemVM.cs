﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bazarkin.AdminPanel.Classes;
using Bazarkin.AdminPanel.EventArgs;
using Bazarkin.AdminPanel.Interfaces;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Bazarkin.AdminPanel.CommonViewModels
{
    /// <summary>
    /// generic для элементов главного меню 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class CommonMainItemVM<T> : ViewModelBase, IReturnPreviousView
        where T : class
    {
        #region Properties
        public EventHandler ReturnPreviousView { get; set; }

        #region Command

        public RelayCommand EditCommand { get; set; }
        public RelayCommand DeleteCommand { get; set; }
        public RelayCommand FindCommand { get; set; }
        public RelayCommand AddCommand { get; set; }

        public RelayCommand BackCommand { get; set; }

        #endregion

        #region DisplayedCollection

        private ObservableCollection<T> mDisplayedCollection;

        /// <summary>
        /// Отображаемая коллекция
        /// </summary>
        public ObservableCollection<T> DisplayedCollection
        {
            get { return mDisplayedCollection; }
            set
            {
                if (mDisplayedCollection == value)
                    return;
                mDisplayedCollection = value;
                RaisePropertyChanged(() => DisplayedCollection);
            }
        }

        #endregion

        #region SelectedItem

        private T mSelectedItem;

        /// <summary>
        /// Выделеный элемент отображаемой коллекции
        /// </summary>
        public T SelectedItem
        {
            get { return mSelectedItem; }
            set
            {
                if (mSelectedItem == value)
                    return;
                mSelectedItem = value;
                RaisePropertyChanged(() => SelectedItem);
            }
        }

        #endregion

        #endregion

        #region Constructors

        public CommonMainItemVM(string windowTitle)
        {
            InitializeCommands();
            MainWindowManager.Instance.SetWindowTitle(windowTitle);
            FillDisplayedCollection();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Заполнить коллекцию отображаемых элементов
        /// </summary>
        protected abstract void FillDisplayedCollection();

        /// <summary>
        /// Срабатывает по завершении редактирования записи 
        /// Задуманно так что, отредактированный или вновь созданный элемент будет 
        /// возвращаться из окна редактирования в этот метод
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected abstract void EditDialogCallback(object sender, EditWindowClosedArgs e);

        protected virtual void InitializeCommands()
        {
            EditCommand = new RelayCommand(OnEditCommand);
            DeleteCommand = new RelayCommand(OnDeleteCommand);
            FindCommand = new RelayCommand(OnFindCommand);
            AddCommand = new RelayCommand(OnAddCommand);
            BackCommand = new RelayCommand(OnBackCommand);
        }

        #endregion

        #region Handlers

        /// <summary>
        /// Обработчик события кнопки "Добавить +"
        /// </summary>
        protected abstract void OnAddCommand();

        /// <summary>
        /// Обработчик события кнопки "Найти"
        /// </summary>
        protected abstract void OnFindCommand();
        /// <summary>
        /// Обработчик события кнопки "Редактировать запись"
        /// </summary>
        protected abstract void OnEditCommand();

        /// <summary>
        /// Обработчик события кнопки "Назад"
        /// </summary>
        protected virtual void OnBackCommand()
        {
            ReturnPreviousView.Invoke(this, System.EventArgs.Empty);
        }

        /// <summary>
        /// Обработчик события кнопки "Удалить запись"
        /// </summary>
        protected virtual void OnDeleteCommand()
        {
            DisplayedCollection.Remove(SelectedItem);
        }

        #endregion
    }
}
