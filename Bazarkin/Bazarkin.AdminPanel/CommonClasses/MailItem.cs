﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;

namespace Bazarkin.AdminPanel.CommonClasses
{
    public class MailItem : ObservableObject
    {
        #region Properties

        #region ID

        private int mID;

        /// <summary>
        /// Идентификатор
        /// </summary>
        public int ID
        {
            get { return mID; }
            set
            {
                if (mID == value)
                    return;
                mID = value;
                RaisePropertyChanged(() => ID);
            }
        }

        #endregion

        #region Mail

        private string mMail;

        /// <summary>
        /// Почта
        /// </summary>
        public string Mail
        {
            get { return mMail; }
            set
            {
                if (mMail == value)
                    return;
                mMail = value;
                RaisePropertyChanged(() => Mail);
            }
        }

        #endregion

        #region Description

        private string mDescription;

        /// <summary>
        /// Описание
        /// </summary>
        public string Description
        {
            get { return mDescription; }
            set
            {
                if (mDescription == value)
                    return;
                mDescription = value;
                RaisePropertyChanged(() => Description);
            }
        }

        #endregion

        #endregion

        #region Constructors

        public MailItem(MailItem item)
        {
            this.ID = item.ID;
            this.Mail = item.Mail;
            this.Description = item.Description;
        }

        public MailItem()
        {

        }

        #endregion
    }
}
