﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;

namespace Bazarkin.AdminPanel.CommonClasses
{
    public class PhoneItem : ObservableObject
    {
        #region Properties

        #region ID

        private int mID;

        /// <summary>
        /// Идентификатор
        /// </summary>
        public int ID
        {
            get { return mID; }
            set
            {
                if (mID == value)
                    return;
                mID = value;
                RaisePropertyChanged(() => ID);
            }
        }

        #endregion

        #region Number

        private string mNumber;

        /// <summary>
        /// Номер телефона
        /// </summary>
        public string Number
        {
            get { return mNumber; }
            set
            {
                if (mNumber == value)
                    return;
                mNumber = value;
                RaisePropertyChanged(() => Number);
            }
        }

        #endregion

        #region Description

        private string mDescription;

        /// <summary>
        /// Описание
        /// </summary>
        public string Description
        {
            get { return mDescription; }
            set
            {
                if (mDescription == value)
                    return;
                mDescription = value;
                RaisePropertyChanged(() => Description);
            }
        }

        #endregion

        #region IsVisible

        private bool mIsVisible;

        /// <summary>
        /// Видимость
        /// </summary>
        public bool IsVisible
        {
            get { return mIsVisible; }
            set
            {
                if (mIsVisible == value)
                    return;
                mIsVisible = value;
                RaisePropertyChanged(() => IsVisible);
            }
        }

        #endregion

        #endregion

        #region Constructors

        public PhoneItem(PhoneItem item)
        {
            this.ID = item.ID;
            this.Number = item.Number;
            this.Description = item.Description;
            this.IsVisible = item.IsVisible;
        }

        public PhoneItem()
        {

        }

        #endregion
    }
}
