﻿namespace Bazarkin.AdminPanel.Enums
{
    public enum WindowSizes
    {
        AuthorizationViewSize = 1,
        MainViewSize = 2
    }
}
