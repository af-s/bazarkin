﻿using System;

namespace Bazarkin.AdminPanel.Enums
{
    /// <summary>
    /// Идентификаторы для элементов "Главного меню"
    /// </summary>
    public enum MainViewElementsIdentifiers
    {
        Categories = 1,
        Users = 2,
        Manufacturers = 3,
        Organizations = 4,
        TradePoints = 5,
        Products = 6,
        PropertyTypes = 7,
        PropertyGroups = 8
    }
}



