﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;

namespace Bazarkin.AdminPanel.Classes
{
    public class PropertyItem : ObservableObject
    {
        #region Properties

        #region ID

        private int mID;

        /// <summary>
        /// Идентификатор
        /// </summary>
        public int ID
        {
            get { return mID; }
            set
            {
                if (mID == value)
                    return;
                mID = value;
                RaisePropertyChanged(() => ID);
            }
        }

        #endregion

        #region Name

        private string mName;

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name
        {
            get { return mName; }
            set
            {
                if (mName == value)
                    return;
                mName = value;
                RaisePropertyChanged(() => Name);
            }
        }

        #endregion

        #region Description

        private string mDescription;

        /// <summary>
        /// Описание
        /// </summary>
        public string Description
        {
            get { return mDescription; }
            set
            {
                if (mDescription == value)
                    return;
                mDescription = value;
                RaisePropertyChanged(() => Description);
            }
        }

        #endregion

        #region PropertyType

        private int mPropertyType;

        /// <summary>
        /// Тип свойства
        /// </summary>
        public int PropertyType
        {
            get { return mPropertyType; }
            set
            {
                if (mPropertyType == value)
                    return;
                mPropertyType = value;
                RaisePropertyChanged(() => PropertyType);
            }
        }

        #endregion

        #region PropertyGroup

        private int mPropertyGroup;

        /// <summary>
        /// Группа свойства
        /// </summary>
        public int PropertyGroup
        {
            get { return mPropertyGroup; }
            set
            {
                if (mPropertyGroup == value)
                    return;
                mPropertyGroup = value;
                RaisePropertyChanged(() => PropertyGroup);
            }
        }

        #endregion

        #endregion

        #region Constructors

        public PropertyItem(PropertyItem item)
        {
            this.ID = item.ID;
            this.Name = item.Name;
            this.Description = item.Description;
            this.PropertyGroup = item.PropertyGroup;
            this.PropertyType = item.mPropertyType;
        }

        public PropertyItem()
        {

        }

        #endregion
    }
}
