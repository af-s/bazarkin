﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using Bazarkin.Domain;

namespace Bazarkin.AdminPanel.Classes
{
    public class ManufacturersViewItem : ObservableObject
    {
        #region Properties

        #region ID

        private int mID;

        /// <summary>
        /// Идентификатор
        /// </summary>
        public int ID
        {
            get { return mID; }
            set
            {
                if (mID == value)
                    return;
                mID = value;
                RaisePropertyChanged(() => ID);
            }
        }

        #endregion

        #region Name

        private string mName;

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name
        {
            get { return mName; }
            set
            {
                if (mName == value)
                    return;
                mName = value;
                RaisePropertyChanged(() => Name);
            }
        }

        #endregion

        #region Description

        private string mDescription;

        /// <summary>
        /// Описание
        /// </summary>
        public string Description
        {
            get { return mDescription; }
            set
            {
                if (mDescription == value)
                    return;
                mDescription = value;
                RaisePropertyChanged(() => Description);
            }
        }

        #endregion

        #region Avatar

        private string mAvatar;

        /// <summary>
        /// Аватар
        /// </summary>
        public string Avatar
        {
            get { return mAvatar; }
            set
            {
                if (mAvatar == value)
                    return;
                mAvatar = value;
                RaisePropertyChanged(() => Avatar);
            }
        }

        #endregion

        #endregion

        #region Constructors

        public ManufacturersViewItem(ManufacturersViewItem item)
        {
            EFDbContext db = new EFDbContext();

            Manufacturer manufacturer = db.ManufacturerSet.Where(x => x.Id == item.ID).FirstOrDefault();

            this.ID = manufacturer.Id;
            this.Name = manufacturer.Name;
            this.Description = manufacturer.Description;
            this.Avatar = manufacturer.Avatar;
        }

        public ManufacturersViewItem()
        {
            
        }

        #endregion
    }
}
