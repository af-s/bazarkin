﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Bazarkin.AdminPanel.EventArgs;
using GalaSoft.MvvmLight;

namespace Bazarkin.AdminPanel.Classes
{
    public class UsersViewItem : ObservableObject
    {
        #region Properties

        #region ID

        private int mID;

        /// <summary>
        /// Идентификатор
        /// </summary>
        public int ID
        {
            get { return mID; }
            set
            {
                if (mID == value)
                    return;
                mID = value;
                RaisePropertyChanged(() => ID);
            }
        }

        #endregion

        #region Name

        private string mName;

        /// <summary>
        /// ФИО
        /// </summary>
        public string Name
        {
            get { return mName; }
            set
            {
                if (mName == value)
                    return;
                mName = value;
                RaisePropertyChanged(() => Name);
            }
        }

        #endregion

        #region Patronymic

        private string mPatronymic;

        /// <summary>
        /// Отчество
        /// </summary>
        public string Patronymic
        {
            get { return mPatronymic; }
            set
            {
                if (mPatronymic == value)
                    return;
                mPatronymic = value;
                RaisePropertyChanged(() => Patronymic);
            }
        }

        #endregion

        #region Surname

        private string mSurname;

        /// <summary>
        /// Фамилия
        /// </summary>
        public string Surname
        {
            get { return mSurname; }
            set
            {
                if (mSurname == value)
                    return;
                mSurname = value;
                RaisePropertyChanged(() => Surname);
            }
        }

        #endregion

        #region Login

        private string mLogin;

        /// <summary>
        /// Телефон
        /// </summary>
        public string Login
        {
            get { return mLogin; }
            set
            {
                if (mLogin == value)
                    return;
                mLogin = value;
                RaisePropertyChanged(() => Login);
            }
        }

        #endregion

        #region Password

        private string mPassword;

        /// <summary>
        /// Пароль пользователя
        /// </summary>
        public string Password
        {
            get { return mPassword; }
            set
            {
                if (mPassword == value)
                    return;
                mPassword = value;
                RaisePropertyChanged(() => Password);
            }
        }

        #endregion

        #region Rights

        private ObservableCollection<RightsItem> mRights;

        /// <summary>
        /// Права (Пока что string. В будущем будет коллекция элементов типа "Права")
        /// </summary>
        public ObservableCollection<RightsItem> Rights
        {
            get { return mRights; }
            set
            {
                if (mRights == value)
                    return;
                mRights = value;
                RaisePropertyChanged(() => Rights);
            }
        }

        #endregion

        #region Key

        private string mKey;

        /// <summary>
        /// Ключ
        /// </summary>
        public string Key
        {
            get { return mKey; }
            set
            {
                if (mKey == value)
                    return;
                mKey = value;
                RaisePropertyChanged(() => Key);
            }
        }

        #endregion

        #region Status

        private bool mStatus;

        /// <summary>
        /// Статус / Подтвержден
        /// </summary>
        public bool Status
        {
            get { return mStatus; }
            set
            {
                if (mStatus == value)
                    return;
                mStatus = value;
                RaisePropertyChanged(() => Status);
            }
        }

        #endregion

        #endregion

        #region Constructors

        public UsersViewItem(UsersViewItem item)
        {
            this.ID = item.ID;
            this.Name = item.Name;
            this.Patronymic = item.Patronymic;
            this.Surname = item.Surname;
            this.Login = item.Login;
            this.Password = item.Password;
            this.Rights = item.Rights;
            this.Key = item.Key;
            this.Status = item.Status;
        }

        public UsersViewItem()
        {
            
        }
        #endregion
    }
}
