﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bazarkin.AdminPanel.CommonClasses;
using GalaSoft.MvvmLight;
using Bazarkin.Domain;

namespace Bazarkin.AdminPanel.Classes
{
    public class OrganizationsViewItem : ObservableObject
    {
        #region Properties

        #region ID

        private int mID;

        /// <summary>
        /// Идентификатор
        /// </summary>
        public int ID
        {
            get { return mID; }
            set
            {
                if (mID == value)
                    return;
                mID = value;
                RaisePropertyChanged(() => ID);
            }
        }

        #endregion

        #region Name

        private string mName;

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name
        {
            get { return mName; }
            set
            {
                if (mName == value)
                    return;
                mName = value;
                RaisePropertyChanged(() => Name);
            }
        }

        #endregion

        #region Description

        private string mDescription;

        /// <summary>
        /// Описание
        /// </summary>
        public string Description
        {
            get { return mDescription; }
            set
            {
                if (mDescription == value)
                    return;
                mDescription = value;
                RaisePropertyChanged(() => Description);
            }
        }

        #endregion

        #region IsActive

        private bool mIsActive;

        /// <summary>
        /// Активность
        /// </summary>
        public bool IsActive
        {
            get { return mIsActive; }
            set
            {
                if (mIsActive == value)
                    return;
                mIsActive = value;
                RaisePropertyChanged(() => IsActive);
            }
        }

        #endregion

        #region Points

        private ObservableCollection<TradePointItem> mPoints;

        /// <summary>
        /// Торговые точки
        /// </summary>
        public ObservableCollection<TradePointItem> Points
        {
            get { return mPoints; }
            set
            {
                if (mPoints == value)
                    return;
                mPoints = value;
                RaisePropertyChanged(() => Points);
            }
        }

        #endregion

        #region Phones

        private ObservableCollection<PhoneItem> mPhones;

        /// <summary>
        /// Телефоны
        /// </summary>
        public ObservableCollection<PhoneItem> Phones
        {
            get { return mPhones; }
            set
            {
                if (mPhones == value)
                    return;
                mPhones = value;
                RaisePropertyChanged(() => Phones);
            }
        }

        #endregion

        #region Addresses

        private ObservableCollection<AddressItem> mAddresses;

        /// <summary>
        /// Адреса
        /// </summary>
        public ObservableCollection<AddressItem> Addresses
        {
            get { return mAddresses; }
            set
            {
                if (mAddresses == value)
                    return;
                mAddresses = value;
                RaisePropertyChanged(() => Addresses);
            }
        }

        #endregion

        #region Mails

        private ObservableCollection<MailItem> mMails;

        /// <summary>
        /// Почтовые адреса
        /// </summary>
        public ObservableCollection<MailItem> Mails
        {
            get { return mMails; }
            set
            {
                if (mMails == value)
                    return;
                mMails = value;
                RaisePropertyChanged(() => Mails);
            }
        }

        #endregion

        #endregion

        #region Constructors

        public OrganizationsViewItem(OrganizationsViewItem item)
        {
            EFDbContext db = new EFDbContext();

            Organization organization = db.OrganizationSet.Where(x => x.Id == item.ID).FirstOrDefault();

            this.ID = organization.Id;
            this.Name = organization.Name;
            this.Description = organization.Description;
            this.IsActive = organization.Active;
            this.Points = item.Points != null ? new ObservableCollection<TradePointItem>(item.Points) : new ObservableCollection<TradePointItem>();
            this.Phones = item.Phones != null ? new ObservableCollection<PhoneItem>(item.Phones) : new ObservableCollection<PhoneItem>();
            this.Addresses = item.Addresses!=null ? new ObservableCollection<AddressItem>(item.Addresses) : new ObservableCollection<AddressItem>();
            this.Mails = item.Mails != null ? new ObservableCollection<MailItem>(item.Mails) : new ObservableCollection<MailItem>();
        }

        public OrganizationsViewItem()
        {

        }

        #endregion
    }
}
