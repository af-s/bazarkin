﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;

namespace Bazarkin.AdminPanel.Classes
{
    public class AddressItem : ObservableObject
    {
        #region Properties

        #region ID

        private int mID;

        /// <summary>
        /// Идентификатор
        /// </summary>
        public int ID
        {
            get { return mID; }
            set
            {
                if (mID == value)
                    return;
                mID = value;
                RaisePropertyChanged(() => ID);
            }
        }

        #endregion

        #region Country

        private string mCountry;

        /// <summary>
        /// Страна
        /// </summary>
        public string Country
        {
            get { return mCountry; }
            set
            {
                if (mCountry == value)
                    return;
                mCountry = value;
                RaisePropertyChanged(() => Country);
            }
        }

        #endregion

        #region Region

        private string mRegion;

        /// <summary>
        /// Регион
        /// </summary>
        public string Region
        {
            get { return mRegion; }
            set
            {
                if (mRegion == value)
                    return;
                mRegion = value;
                RaisePropertyChanged(() => Region);
            }
        }

        #endregion

        #region City

        private string mCity;

        /// <summary>
        /// Город
        /// </summary>
        public string City
        {
            get { return mCity; }
            set
            {
                if (mCity == value)
                    return;
                mCity = value;
                RaisePropertyChanged(() => City);
            }
        }

        #endregion

        #region Street

        private string mStreet;

        /// <summary>
        /// Улица
        /// </summary>
        public string Street
        {
            get { return mStreet; }
            set
            {
                if (mStreet == value)
                    return;
                mStreet = value;
                RaisePropertyChanged(() => Street);
            }
        }

        #endregion

        #region Building

        private string mBuilding;

        /// <summary>
        /// Здание
        /// </summary>
        public string Building
        {
            get { return mBuilding; }
            set
            {
                if (mBuilding == value)
                    return;
                mBuilding = value;
                RaisePropertyChanged(() => Building);
            }
        }

        #endregion

        #region Structure

        private string mStructure;

        /// <summary>
        /// Строение
        /// </summary>
        public string Structure
        {
            get { return mStructure; }
            set
            {
                if (mStructure == value)
                    return;
                mStructure = value;
                RaisePropertyChanged(() => Structure);
            }
        }

        #endregion

        #region Apartment

        private string mApartment;

        /// <summary>
        /// Квартира
        /// </summary>
        public string Apartment
        {
            get { return mApartment; }
            set
            {
                if (mApartment == value)
                    return;
                mApartment = value;
                RaisePropertyChanged(() => Apartment);
            }
        }

        #endregion

        #region ShoppingCenter

        private string mShoppingCenter;

        /// <summary>
        /// Торговый центр
        /// </summary>
        public string ShoppingCenter
        {
            get { return mShoppingCenter; }
            set
            {
                if (mShoppingCenter == value)
                    return;
                mShoppingCenter = value;
                RaisePropertyChanged(() => ShoppingCenter);
            }
        }

        #endregion

        #region Sector

        private string mSector;

        /// <summary>
        /// Сектор
        /// </summary>
        public string Sector
        {
            get { return mSector; }
            set
            {
                if (mSector == value)
                    return;
                mSector = value;
                RaisePropertyChanged(() => Sector);
            }
        }

        #endregion

        #region Row

        private string mRow;

        /// <summary>
        /// Ряд
        /// </summary>
        public string Row
        {
            get { return mRow; }
            set
            {
                if (mRow == value)
                    return;
                mRow = value;
                RaisePropertyChanged(() => Row);
            }
        }

        #endregion

        #region Pavilion

        private string mPavilion;

        /// <summary>
        /// Павильон
        /// </summary>
        public string Pavilion
        {
            get { return mPavilion; }
            set
            {
                if (mPavilion == value)
                    return;
                mPavilion = value;
                RaisePropertyChanged(() => Pavilion);
            }
        }

        #endregion

        #region Index

        private string mIndex;

        /// <summary>
        /// Индекс
        /// </summary>
        public string Index
        {
            get { return mIndex; }
            set
            {
                if (mIndex == value)
                    return;
                mIndex = value;
                RaisePropertyChanged(() => Index);
            }
        }

        #endregion

        #region IsVisible

        private bool mIsVisible;

        /// <summary>
        /// Видимость
        /// </summary>
        public bool IsVisible
        {
            get { return mIsVisible; }
            set
            {
                if (mIsVisible == value)
                    return;
                mIsVisible = value;
                RaisePropertyChanged(() => IsVisible);
            }
        }

        #endregion

        #region Description

        private string mDescription;

        /// <summary>
        /// Описание
        /// </summary>
        public string Description
        {
            get { return mDescription; }
            set
            {
                if (mDescription == value)
                    return;
                mDescription = value;
                RaisePropertyChanged(() => Description);
            }
        }

        #endregion

        #endregion

        #region Constructors

        public AddressItem(AddressItem item)
        {
            this.ID = item.ID;
            this.Country = item.Country;
            this.Region = item.Region;
            this.City = item.City;
            this.Street = item.Street;
            this.Building = item.Building;
            this.Structure = item.Structure;
            this.Apartment = item.Apartment;
            this.ShoppingCenter = item.ShoppingCenter;
            this.Sector = item.Sector;
            this.Row = item.Row;
            this.Pavilion = item.Pavilion;
            this.Index = item.Index;
            this.IsVisible = item.IsVisible;
            this.Description = item.Description;
        }

        public AddressItem()
        {

        }

        #endregion
    }
}
