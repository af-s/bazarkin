﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using Bazarkin.Domain;

namespace Bazarkin.AdminPanel.Classes
{
    public class CategoriesViewItem : ObservableObject
    {
        #region Properties

        #region ID

        private int mID;

        /// <summary>
        /// Идентификатор
        /// </summary>
        public int ID
        {
            get { return mID; }
            set
            {
                if (mID == value)
                    return;
                mID = value;
                RaisePropertyChanged(() => ID);
            }
        }

        #endregion

        #region Name

        private string mName;

        /// <summary>
        /// Имя
        /// </summary>
        public string Name
        {
            get { return mName; }
            set
            {
                if (mName == value)
                    return;
                mName = value;
                RaisePropertyChanged(() => Name);
            }
        }

        #endregion

        #region Description

        private string mDescription;

        /// <summary>
        /// Описание
        /// </summary>
        public string Description
        {
            get { return mDescription; }
            set
            {
                if (mDescription == value)
                    return;
                mDescription = value;
                RaisePropertyChanged(() => Description);
            }
        }

        #endregion

        #region Parent

        private int? mParent;

        /// <summary>
        /// Родительская 
        /// (Возможно тут имеет смысл использовать идентификатор родительской записи,
        ///  а в отображении конвертер запилить который будет по идентификатору отображать наименование)
        /// </summary>
        public int? Parent
        {
            get { return mParent; }
            set
            {
                if (mParent == value)
                    return;
                mParent = value;
                RaisePropertyChanged(() => Parent);
            }
        }

        #endregion

        #region IsFinal

        private bool mIsFinal;

        /// <summary>
        /// Конечная
        /// </summary>
        public bool IsFinal
        {
            get { return mIsFinal; }
            set
            {
                if (mIsFinal == value)
                    return;
                mIsFinal = value;
                RaisePropertyChanged(() => IsFinal);
            }
        }

        #endregion

        #region Properties

        private ObservableCollection<CategoryPropertiesTreeViewItem> mProperties;

        /// <summary>
        /// Свойства в категории
        /// </summary>
        public ObservableCollection<CategoryPropertiesTreeViewItem> Properties
        {
            get { return mProperties; }
            set
            {
                if (mProperties == value)
                    return;
                mProperties = value;
                RaisePropertyChanged(() => Properties);
            }
        }

        #endregion

        #endregion

        #region Constructors

        public CategoriesViewItem(CategoriesViewItem item)
        {
            EFDbContext db = new EFDbContext();

            Category category = db.CategorySet.Where(x => x.Id == item.ID).FirstOrDefault();

            this.ID = category.Id;
            this.Name = category.Name;
            this.Description = category.Description;
            this.Parent = (category.Parent==null) ? 0 : category.Parent.Id;
            //this.IsFinal = category.GoodHear;
            this.Properties = item.Properties != null
                ? new ObservableCollection<CategoryPropertiesTreeViewItem>(item.Properties)
                : new ObservableCollection<CategoryPropertiesTreeViewItem>();
        }

        public CategoriesViewItem()
        {
            
        }


        #endregion
    }
}
