﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using Bazarkin.Domain;

namespace Bazarkin.AdminPanel.Classes
{
    public class PropertyTypeItem : ObservableObject
    {
        #region Properties

        #region ID

        private int mID;

        /// <summary>
        /// Идентификатор
        /// </summary>
        public int ID
        {
            get { return mID; }
            set
            {
                if (mID == value)
                    return;
                mID = value;
                RaisePropertyChanged(() => ID);
            }
        }

        #endregion

        #region Name

        private string mName;

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name
        {
            get { return mName; }
            set
            {
                if (mName == value)
                    return;
                mName = value;
                RaisePropertyChanged(() => Name);
            }
        }

        #endregion

        #region Format

        private string mFormat;

        /// <summary>
        /// Формат
        /// </summary>
        public string Format
        {
            get { return mFormat; }
            set
            {
                if (mFormat == value)
                    return;
                mFormat = value;
                RaisePropertyChanged(() => Format);
            }
        }

        #endregion

        #region Description

        private string mDescription;

        /// <summary>
        /// Описание
        /// </summary>
        public string Description
        {
            get { return mDescription; }
            set
            {
                if (mDescription == value)
                    return;
                mDescription = value;
                RaisePropertyChanged(() => Description);
            }
        }

        #endregion

        #region FilterType

        private string mFilterType;

        /// <summary>
        /// Тип фильтра
        /// </summary>
        public string FilterType
        {
            get { return mFilterType; }
            set
            {
                if (mFilterType == value)
                    return;
                mFilterType = value;
                RaisePropertyChanged(() => FilterType);
            }
        }

        #endregion

        #endregion

        #region Constructors

        public PropertyTypeItem(PropertyTypeItem item)
        {
            EFDbContext db = new EFDbContext();
            PropertyType propertyType = db.PropertyTypeSet.Where(x => x.Id == item.ID).FirstOrDefault();

            this.ID = propertyType.Id;
            this.Name = propertyType.Name;
            this.Format = propertyType.ValueFormat;
            this.Description = propertyType.Description;
            this.FilterType = propertyType.FilterType;
        }

        public PropertyTypeItem()
        {

        }

        #endregion
    }
}
