﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;

namespace Bazarkin.AdminPanel.Classes
{
    /// <summary>
    /// Права в редактировании пользователя
    /// </summary>
    public class RightsItem : ObservableObject
    {
        #region Properties

        #region ID

        private int mID;

        /// <summary>
        /// Идентификатор
        /// </summary>
        public int ID
        {
            get { return mID; }
            set
            {
                if (mID == value)
                    return;
                mID = value;
                RaisePropertyChanged(() => ID);
            }
        }

        #endregion

        #region Name

        private string mName;

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name
        {
            get { return mName; }
            set
            {
                if (mName == value)
                    return;
                mName = value;
                RaisePropertyChanged(() => Name);
            }
        }

        #endregion

        #region IsActive

        private bool mIsActive;

        /// <summary>
        /// Добавлена пользователю (Этот флаг участвует только в работе интерфейса)
        /// </summary>
        public bool IsActive
        {
            get { return mIsActive; }
            set
            {
                if (mIsActive == value)
                    return;
                mIsActive = value;
                RaisePropertyChanged(() => IsActive);
            }
        }

        #endregion

        #endregion

        #region Constructors

        public RightsItem(RightsItem item)
        {
            this.ID = item.ID;
            this.Name = item.Name;
        }

        public RightsItem()
        {
            
        }
        #endregion
    }
}
