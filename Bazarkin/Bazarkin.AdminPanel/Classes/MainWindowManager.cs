﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Bazarkin.AdminPanel.Enums;

namespace Bazarkin.AdminPanel.Classes
{
    /// <summary>
    /// Мэнеджер для манипулирования главным окном
    /// </summary>
    public class MainWindowManager
    {
        #region Singleton

        private static MainWindowManager instance;
        public static MainWindowManager Instance
        {
            get { return instance ?? (instance = new MainWindowManager()); }
        }
        public MainWindowManager() { }

        #endregion

        #region Properties

        #region MainWindow

        /// <summary>
        /// Возвращает окно MainWindow
        /// </summary>
        public Window MainWindow
        {
            get { return Application.Current.MainWindow; }
        }

        #endregion

        #region Sizes

        /// <summary>
        /// Размеры для окна используемые для разных View 
        /// { Наименование значений, {высота, ширина} }
        /// </summary>
        public Dictionary<WindowSizes, int[]> Sizes
        {
            get
            {
                return new Dictionary<WindowSizes, int[]>
                {
                    {WindowSizes.AuthorizationViewSize, new[] {350, 300}},
                    {WindowSizes.MainViewSize, new[] {600, 1000}}
                };
            }
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Установить размер окна
        /// </summary>
        /// <param name="size">размер окна из перечисления</param>
        public void SetWindowSize(WindowSizes size)
        {
            var currentSize = Sizes.First(x => x.Key == size);

            MainWindow.Height = currentSize.Value[0];
            MainWindow.Width = currentSize.Value[1];
        }

        /// <summary>
        /// Установить наименование окна
        /// </summary>
        /// <param name="title"></param>
        public void SetWindowTitle(string title)
        {
            MainWindow.Title = title;
        }

        #endregion
    }
}
