﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Bazarkin.AdminPanel.Classes
{
    public class PropertyGroupAndProperties : ObservableObject
    {
        #region Properties

        #region PropertyGroup

        private PropertyGroupItem mPropertyGroup;

        /// <summary>
        /// Группа свойств
        /// </summary>
        public PropertyGroupItem PropertyGroup
        {
            get { return mPropertyGroup; }
            set
            {
                if (mPropertyGroup == value)
                    return;
                mPropertyGroup = value;
                RaisePropertyChanged(() => PropertyGroup);
            }
        }

        #endregion

        #region PropertyAndValues

        private ObservableCollection<PropertyAndValues> mPropertyAndValues;

        /// <summary>
        /// Свойства и их значения
        /// </summary>
        public ObservableCollection<PropertyAndValues> PropertyAndValues
        {
            get { return mPropertyAndValues; }
            set
            {
                if (mPropertyAndValues == value)
                    return;
                mPropertyAndValues = value;
                RaisePropertyChanged(() => PropertyAndValues);
            }
        }

        #endregion

        #region SelectedProperty

        private PropertyAndValues mSelectedProperty;

        /// <summary>
        /// Выделенное свойство в гриде отображаемом как дочерний элемент дерева(редактирование Модификаций товара)
        /// </summary>
        public PropertyAndValues SelectedProperty
        {
            get { return mSelectedProperty; }
            set
            {
                if (mSelectedProperty == value)
                    return;
                mSelectedProperty = value;
                RaisePropertyChanged(() => SelectedProperty);
            }
        }

        #endregion

        public RelayCommand AddPropertyValueCommand { get; set; }
        #endregion

        #region Constructors

        public PropertyGroupAndProperties()
        {
            InitializeCommands();
        }

        #endregion

        #region Methods

        private void InitializeCommands()
        {
            AddPropertyValueCommand = new RelayCommand(OnAddPropertyValueCommand);
        }

        #endregion

        #region Handlers
        /// <summary>
        /// Обработчик события нажатия на кнопку добавить в дереве
        /// </summary>
        private void OnAddPropertyValueCommand()
        {
            /*Предполагается что откроется окно в котором можно будет ввести новое свойство,
            после чего обновить данные в БД,  и уже после этого добавить элемент в коллекцию выбранного свойства*/
            SelectedProperty.PropertyValues.Add("Новое значение свойства");
        }  

        #endregion
    }
}
