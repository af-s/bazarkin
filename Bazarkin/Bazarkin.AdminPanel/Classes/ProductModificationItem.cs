﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;

namespace Bazarkin.AdminPanel.Classes
{
    public class ProductModificationItem : ObservableObject
    {
        #region Properties

        #region ID

        private int mID;

        /// <summary>
        /// Идентификатор
        /// </summary>
        public int ID
        {
            get { return mID; }
            set
            {
                if (mID == value)
                    return;
                mID = value;
                RaisePropertyChanged(() => ID);
            }
        }

        #endregion

        #region ProductId

        private int mProductId;

        /// <summary>
        /// Идентификатор товара к которому принадлежит модификация
        /// </summary>
        public int ProductId
        {
            get { return mProductId; }
            set
            {
                if (mProductId == value)
                    return;
                mProductId = value;
                RaisePropertyChanged(() => ProductId);
            }
        }

        #endregion

        #region Article

        private string mArticle;

        /// <summary>
        /// Артикул
        /// </summary>
        public string Article
        {
            get { return mArticle; }
            set
            {
                if (mArticle == value)
                    return;
                mArticle = value;
                RaisePropertyChanged(() => Article);
            }
        }

        #endregion

        #region Modification

        private string mModification;

        /// <summary>
        /// Модификация
        /// </summary>
        public string Modification
        {
            get { return mModification; }
            set
            {
                if (mModification == value)
                    return;
                mModification = value;
                RaisePropertyChanged(() => Modification);
            }
        }

        #endregion

        #endregion

        #region Constructors

        public ProductModificationItem(ProductModificationItem item)
        {
            this.ID = item.ID;
            this.Article = item.Article;
            this.Modification = item.Modification;
        }

        public ProductModificationItem()
        {

        }

        #endregion

    }
}
