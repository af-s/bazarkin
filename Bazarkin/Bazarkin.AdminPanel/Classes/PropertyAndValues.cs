﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;

namespace Bazarkin.AdminPanel.Classes
{
    public class PropertyAndValues : ObservableObject
    {
        #region Properties

        #region Property

        private PropertyItem mProperty;

        /// <summary>
        /// Свойство
        /// </summary>
        public PropertyItem Property
        {
            get { return mProperty; }
            set
            {
                if (mProperty == value)
                    return;
                mProperty = value;
                RaisePropertyChanged(() => Property);
            }
        }

        #endregion

        #region PropertyValues

        private ObservableCollection<string> mPropertyValues;

        /// <summary>
        /// Значения свойства
        /// </summary>
        public ObservableCollection<string> PropertyValues
        {
            get { return mPropertyValues; }
            set
            {
                if (mPropertyValues == value)
                    return;
                mPropertyValues = value;
                RaisePropertyChanged(() => PropertyValues);
            }
        }

        #endregion

        #endregion
    }
}
