﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bazarkin.AdminPanel.CommonClasses;
using GalaSoft.MvvmLight;
using Bazarkin.Domain;

namespace Bazarkin.AdminPanel.Classes
{
    public class TradePointItem : ObservableObject
    {
        #region Properties

        #region ID

        private int mID;

        /// <summary>
        /// Идентификатор
        /// </summary>
        public int ID
        {
            get { return mID; }
            set
            {
                if (mID == value)
                    return;
                mID = value;
                RaisePropertyChanged(() => ID);
            }
        }

        #endregion

        #region Name

        private string mName;

        /// <summary>
        /// Название
        /// </summary>
        public string Name
        {
            get { return mName; }
            set
            {
                if (mName == value)
                    return;
                mName = value;
                RaisePropertyChanged(() => Name);
            }
        }

        #endregion

        #region Description

        private string mDescription;

        /// <summary>
        /// Описание
        /// </summary>
        public string Description
        {
            get { return mDescription; }
            set
            {
                if (mDescription == value)
                    return;
                mDescription = value;
                RaisePropertyChanged(() => Description);
            }
        }

        #endregion

        #region Address

        private ObservableCollection<AddressItem> mAddresses;

        /// <summary>
        /// Адрес
        /// </summary>
        public ObservableCollection<AddressItem> Addresses
        {
            get { return mAddresses; }
            set
            {
                if (mAddresses == value)
                    return;
                mAddresses = value;
                RaisePropertyChanged(() => Addresses);
            }
        }

        #endregion

        #region IsActive

        private bool mIsActive;

        /// <summary>
        /// Является активной
        /// </summary>
        public bool IsActive
        {
            get { return mIsActive; }
            set
            {
                if (mIsActive == value)
                    return;
                mIsActive = value;
                RaisePropertyChanged(() => IsActive);
            }
        }

        #endregion

        #region PaidTo

        private DateTime mPaidTo;

        /// <summary>
        /// Оплачена до
        /// </summary>
        public DateTime PaidTo
        {
            get { return mPaidTo; }
            set
            {
                if (mPaidTo == value)
                    return;
                mPaidTo = value;
                RaisePropertyChanged(() => PaidTo);
            }
        }

        #endregion

        #region Phones

        private ObservableCollection<PhoneItem> mPhones;

        /// <summary>
        /// Телефоны
        /// </summary>
        public ObservableCollection<PhoneItem> Phones
        {
            get { return mPhones; }
            set
            {
                if (mPhones == value)
                    return;
                mPhones = value;
                RaisePropertyChanged(() => Phones);
            }
        }

        #endregion


        #endregion

        #region Constructors

        public TradePointItem(TradePointItem item)
        {
            EFDbContext db = new EFDbContext();

            OrganizationUnit organizationUnit = db.OrganizationUnitSet.Where(x => x.Id == item.ID).FirstOrDefault();

            this.ID = organizationUnit.Id;
            this.IsActive = organizationUnit.Active;
            this.PaidTo = organizationUnit.PayerDate.GetValueOrDefault();
            //this.Name = organizationUnit.Name;
            this.Description = organizationUnit.Description;
            this.Phones = item.Phones != null ? new ObservableCollection<PhoneItem>(item.Phones) : new ObservableCollection<PhoneItem>();
            this.Addresses = item.Addresses != null ? new ObservableCollection<AddressItem>(item.Addresses) : new ObservableCollection<AddressItem>();
        }

        public TradePointItem()
        {

        }

        #endregion
    }
}
