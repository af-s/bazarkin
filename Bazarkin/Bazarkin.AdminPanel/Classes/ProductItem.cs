﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;

namespace Bazarkin.AdminPanel.Classes
{
    public class ProductItem : ObservableObject
    {
        #region Properties

        #region ID

        private int mID;

        /// <summary>
        /// Идентификатор
        /// </summary>
        public int ID
        {
            get { return mID; }
            set
            {
                if (mID == value)
                    return;
                mID = value;
                RaisePropertyChanged(() => ID);
            }
        }

        #endregion

        #region Name

        private string mName;

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name
        {
            get { return mName; }
            set
            {
                if (mName == value)
                    return;
                mName = value;
                RaisePropertyChanged(() => Name);
            }
        }

        #endregion

        #region Description

        private string mDescription;

        /// <summary>
        /// Описание
        /// </summary>
        public string Description
        {
            get { return mDescription; }
            set
            {
                if (mDescription == value)
                    return;
                mDescription = value;
                RaisePropertyChanged(() => Description);
            }
        }

        #endregion

        #region IsActive

        private bool mIsActive;

        /// <summary>
        /// Активность
        /// </summary>
        public bool IsActive
        {
            get { return mIsActive; }
            set
            {
                if (mIsActive == value)
                    return;
                mIsActive = value;
                RaisePropertyChanged(() => IsActive);
            }
        }

        #endregion

        #region Modifications

        private ObservableCollection<ProductModificationItem> mModifications;

        /// <summary>
        /// Модификации
        /// </summary>
        public ObservableCollection<ProductModificationItem> Modifications
        {
            get { return mModifications; }
            set
            {
                if (mModifications == value)
                    return;
                mModifications = value;
                RaisePropertyChanged(() => Modifications);
            }
        }

        #endregion

        #endregion

        #region Constructors

        public ProductItem(ProductItem item)
        {
            this.ID = item.ID;
            this.Name = item.Name;
            this.Description = item.Description;
            this.IsActive = item.IsActive;
            this.Modifications = item.Modifications != null ? new ObservableCollection<ProductModificationItem>(item.Modifications) : new ObservableCollection<ProductModificationItem>();
        }

        public ProductItem()
        {

        }

        #endregion
    }
}
