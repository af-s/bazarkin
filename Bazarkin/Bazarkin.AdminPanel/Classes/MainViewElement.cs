﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Bazarkin.AdminPanel.Enums;
using Bazarkin.AdminPanel.EventArgs;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace Bazarkin.AdminPanel.Classes
{
    /// <summary>
    /// Элемент отображаемый в "Главном меню"
    /// </summary>
    public class MainViewElement : ObservableObject
    {
        #region Properties

        #region ElementCommandExecuted
        /// <summary>
        /// Событие реагирует на нажатие элемента и сигнализирует об этом в MainViewVM
        /// Предполагается что идентификация элемента вызвавшего событие будет регулироваться MainViewElementArgs
        /// </summary>
        public EventHandler<MainViewElementArgs> ElementCommandExecuted;

        #endregion

        #region ElementImage

        private BitmapImage mImage;

        /// <summary>
        /// Рисунок отображаемый на элементе в главном меню
        /// </summary>
        public BitmapImage Image
        {
            get { return mImage; }
            set
            {
                if (mImage == value)
                    return;
                mImage = value;
                RaisePropertyChanged(() => Image);
            }
        }

        #endregion

        #region Content

        private string mContent;

        /// <summary>
        /// Надпись под кнопкой (В будущем сюда можно будет добавлять элементы не только типа string)
        /// </summary>
        public string Content
        {
            get { return mContent; }
            set
            {
                if (mContent == value)
                    return;
                mContent = value;
                RaisePropertyChanged(() => Content);
            }
        }

        #endregion

        #region Width

        private int mWidth;

        /// <summary>
        /// Ширина элемента
        /// </summary>
        public int Width
        {
            get { return mWidth; }
            set
            {
                if (mWidth == value)
                    return;
                mWidth = value;
                RaisePropertyChanged(() => Width);
            }
        }

        #endregion

        #region Height

        private int mHeight;

        /// <summary>
        /// Высота элемента
        /// </summary>
        public int Height
        {
            get { return mHeight; }
            set
            {
                if (mHeight == value)
                    return;
                mHeight = value;
                RaisePropertyChanged(() => Height);
            }
        }

        #endregion

        #region CornerRadius

        private int mCornerRadius;

        /// <summary>
        /// Угол сглаживания краев
        /// </summary>
        /// 
        public int CornerRadius
        {
            get { return mCornerRadius; }
            set
            {
                if (mCornerRadius == value)
                    return;
                mCornerRadius = value;
                RaisePropertyChanged(() => CornerRadius);
            }
        }

        #endregion

        #region ElementIdentity

        private MainViewElementsIdentifiers _elementIdentity;
        #endregion

        #region Commands
        public RelayCommand ElementCommand { get; set; }

        #endregion

        #endregion

        #region Constructors

        /// <summary>
        /// Инициализирует новый элемент главного меню
        /// </summary>
        /// <param name="content">Контент элемента</param>
        /// <param name="image">Рисунок отображаемого элемента</param>
        /// <param name="height">Высота элемента</param>
        /// <param name="width">Ширина элемента</param>
        /// <param name="cornerRadius">Угол сглаживания краев элемента</param>
        /// <param name="elementIdentity">Идентификатор элемента (Задуманно так что бы сначала заполнять, если требуется, соответствующий enum, а уже после всоздавать объект)</param>
        public MainViewElement(string content, BitmapImage image, int height, int width, int cornerRadius, MainViewElementsIdentifiers elementIdentity)
        {
            this.Content = content;
            this.Image = image;
            this.Height = height;
            this.Width = width;
            this.CornerRadius = cornerRadius;
            this._elementIdentity = elementIdentity;
            ElementCommand = new RelayCommand(OnElementCommand, ElementCommandCanExecute);
        }

        #endregion

        #region Handlers

        /// <summary>
        /// Можно ли использовать элемент главного меню (В расчете на разные уровни прав пользователей)
        /// </summary>
        /// <returns></returns>
        private bool ElementCommandCanExecute()
        {
            return true;
        }

        /// <summary>
        /// Обработчик события нажатия на элемент в "Главном меню"
        /// </summary>
        private void OnElementCommand()
        {
            if(ElementCommandExecuted==null) return;
            var args = new MainViewElementArgs(_elementIdentity);
            ElementCommandExecuted.Invoke(this, args);
        }

        #endregion
    }
}
