﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Bazarkin.AdminPanel.EventArgs;
using GalaSoft.MvvmLight;

namespace Bazarkin.AdminPanel.Classes
{
    public class CategoryPropertiesChildTreeViewItem : ObservableObject
    {
        public EventHandler<ValueChangedArgs> ValueChangedEvent { get; set; }

        #region IsActive

        private bool mIsActive;

        /// <summary>
        /// 
        /// </summary>
        public bool IsActive
        {
            get { return mIsActive; }
            set
            {
                if (mIsActive == value)
                    return;
                mIsActive = value;
                RaisePropertyChanged(() => IsActive);
                ValueChanged(IsActive, "IsActive");
            }
        }

        #endregion

        #region Name

        private string mName;

        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get { return mName; }
            set
            {
                if (mName == value)
                    return;
                mName = value;
                RaisePropertyChanged(() => Name);
                ValueChanged(Name,"Name");
            }
        }

        #endregion

        #region Value

        private int mValue;

        /// <summary>
        /// 
        /// </summary>
        public int Value
        {
            get { return mValue; }
            set
            {
                if (mValue == value)
                    return;
                mValue = value;
                RaisePropertyChanged(() => Value);
                ValueChanged(Value, "Value");
            }
        }

        #endregion

        #region Description

        private string mDescription;

        /// <summary>
        /// 
        /// </summary>
        public string Description
        {
            get { return mDescription; }
            set
            {
                if (mDescription == value)
                    return;
                mDescription = value;
                RaisePropertyChanged(() => Description);
                ValueChanged(Description, "Description");
            }
        }

        #endregion

        private void ValueChanged(dynamic propertyValue, string propertyName)
        {
            //Hello
            if (ValueChangedEvent != null)
                ValueChangedEvent.Invoke(this, new ValueChangedArgs(propertyValue, propertyName));
        }
    }
}
