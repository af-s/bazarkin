﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bazarkin.AdminPanel.EventArgs;
using GalaSoft.MvvmLight;

namespace Bazarkin.AdminPanel.Classes
{
    public class CategoryPropertiesTreeViewItem : ObservableObject
    {
        #region Name

        private string mName;

        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get { return mName; }
            set
            {
                if (mName == value)
                    return;
                mName = value;
                RaisePropertyChanged(() => Name);
            }
        }

        #endregion

        #region ChildsCount

        public int ChildsCount
        {
            get { return Childs.Count; }
        }

        #endregion

        #region ActiveChildsCount

        public int ActiveChildsCount
        {
            get { return Childs.Count(x => x.IsActive); }
        }

        #endregion

        #region Childs

        private ObservableCollection<CategoryPropertiesChildTreeViewItem> mChilds;

        /// <summary>
        /// Детишки кочерыжки
        /// </summary>
        public ObservableCollection<CategoryPropertiesChildTreeViewItem> Childs
        {
            get { return mChilds; }
            set
            {
                if (mChilds == value)
                    return;
                mChilds = value;
                RaisePropertyChanged(() => Childs);
            }
        }

        #endregion

        public void ChildPropertyChanged(object sender, ValueChangedArgs e)
        {
            RaisePropertyChanged(() => ChildsCount);
            RaisePropertyChanged(() => ActiveChildsCount);
        }
    }
}
