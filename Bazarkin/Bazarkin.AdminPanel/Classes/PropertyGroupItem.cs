﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bazarkin.AdminPanel.CommonViewModels;
using GalaSoft.MvvmLight;
using Bazarkin.Domain;

namespace Bazarkin.AdminPanel.Classes
{
    public class PropertyGroupItem : ObservableObject
    {

        #region Properties

        #region ID

        private int mID;

        /// <summary>
        /// Идентификатор
        /// </summary>
        public int ID
        {
            get { return mID; }
            set
            {
                if (mID == value)
                    return;
                mID = value;
                RaisePropertyChanged(() => ID);
            }
        }

        #endregion

        #region Name

        private string mName;

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name
        {
            get { return mName; }
            set
            {
                if (mName == value)
                    return;
                mName = value;
                RaisePropertyChanged(() => Name);
            }
        }

        #endregion

        #region Description

        private string mDescription;

        /// <summary>
        /// Описание
        /// </summary>
        public string Description
        {
            get { return mDescription; }
            set
            {
                if (mDescription == value)
                    return;
                mDescription = value;
                RaisePropertyChanged(() => Description);
            }
        }

        #endregion

        #region Properties

        private ObservableCollection<PropertyItem> mProperties;

        /// <summary>
        /// Свойства
        /// </summary>
        public ObservableCollection<PropertyItem> Properties
        {
            get { return mProperties; }
            set
            {
                if (mProperties == value)
                    return;
                mProperties = value;
                RaisePropertyChanged(() => Properties);
            }
        }

        #endregion

        #endregion

        #region Constructors

        public PropertyGroupItem(PropertyGroupItem item)
        {
            EFDbContext db = new EFDbContext();
            PropertyGroup propertyGroup = db.PropertyGroupSet.Where(x => x.Id == item.ID).FirstOrDefault();

            this.ID = propertyGroup.Id;
            this.Name = propertyGroup.Name;
            this.Description = propertyGroup.Description;
            this.Properties = item.Properties != null ? new ObservableCollection<PropertyItem>(item.Properties) : new ObservableCollection<PropertyItem>();
        }

        public PropertyGroupItem()
        {

        }

        #endregion
    }
}
