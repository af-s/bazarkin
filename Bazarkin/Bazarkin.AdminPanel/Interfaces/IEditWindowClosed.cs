﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bazarkin.AdminPanel.EventArgs;

namespace Bazarkin.AdminPanel.Interfaces
{
    public interface IEditWindowClosed
    {
        Action CloseWindowAction { get; set; }

        EventHandler<EditWindowClosedArgs> DialogCallback { get; set; }
    }
}
