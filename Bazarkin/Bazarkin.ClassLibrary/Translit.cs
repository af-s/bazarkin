﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bazarkin.ClassLibrary
{
    public class Translit
    {
        /// <summary>
        /// Транслитерация текста
        /// </summary>
        /// <param name="text">Текст</param>
        /// <returns>Текст транслитом</returns>
        public static string RuToTranslite(string text)
        {
            Dictionary<string, string> transliter = new Dictionary<string, string>();
            transliter.Add("а", "a");
            transliter.Add("б", "b");
            transliter.Add("в", "v");
            transliter.Add("г", "g");
            transliter.Add("д", "d");
            transliter.Add("е", "e");
            transliter.Add("ё", "e");
            transliter.Add("ж", "j");
            transliter.Add("з", "z");
            transliter.Add("и", "i");
            transliter.Add("й", "i");
            transliter.Add("к", "k");
            transliter.Add("л", "l");
            transliter.Add("м", "m");
            transliter.Add("н", "n");
            transliter.Add("о", "o");
            transliter.Add("п", "p");
            transliter.Add("р", "r");
            transliter.Add("с", "s");
            transliter.Add("т", "t");
            transliter.Add("у", "u");
            transliter.Add("ф", "f");
            transliter.Add("х", "h");
            transliter.Add("ц", "c");
            transliter.Add("ч", "ch");
            transliter.Add("ш", "sh");
            transliter.Add("щ", "sc");
            transliter.Add("ы", "y");
            transliter.Add("э", "e");
            transliter.Add("ю", "iu");
            transliter.Add("я", "ia");
            transliter.Add(" ", "-");
            transliter.Add("-", "-");

            text = text.ToLower();

            StringBuilder translitText = new StringBuilder();
            for (int i = 0; i < text.Length; i++)
            {
                if (transliter.ContainsKey(text[i].ToString()))
                {
                    translitText.Append(transliter[text[i].ToString()]);
                }
                else if (text[i] >= 'a' && text[i] <= 'z' || text[i] >= '0' && text[i] <= '9')
                {
                    translitText.Append(text[i].ToString());
                }
            }
            var str = translitText.ToString();
            while (str.IndexOf("--") != -1)
            {
                translitText.Replace("--", "-");
                str = translitText.ToString();
            }
            while (str.FirstOrDefault().Equals('-'))
            {
                translitText.Remove(0, 1);
                str = translitText.ToString();
            }
            while (str.LastOrDefault().Equals('-'))
            {
                translitText.Remove(translitText.Length - 1, 1);
                str = translitText.ToString();
            }
            return str;
        }
    }
}
