﻿using Bazarkin.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bazarkin.ClassLibrary
{
    public class GoodLib
    {
        /// <summary>
        /// Строковое название модификации
        /// </summary>
        /// <param name="good">Товар</param>
        /// <returns>Словарь Id-Модификация</returns>
        public static Dictionary<int, string> GetModificationNameDict(Good good)
        {
            EFDbContext db = new EFDbContext();

            List<ModificationGood> modificationGoodList = db.ModificationGoodSet.Where(x => x.Good.Id == good.Id).ToList();

            List<PropertyCategory> propertyCategoryList = db.PropertyCategorySet.Where(x => x.Category.Id == good.Category.Id).OrderBy(x => x.OrderIndex).ToList();
            List<Property> propertyList = new List<Property>();
            foreach (var propertyCategory in propertyCategoryList)
            {
                Property property = db.PropertySet.Where(x => x.Id == propertyCategory.Property.Id).FirstOrDefault();

                List<PropertyValueGroup> propertyValueGroupList = new List<PropertyValueGroup>();
                foreach(var modificationGood in modificationGoodList)
                {
                    PropertyValueGroup propertyValueGroup = db.PropertyValueGroupSet.Where(x => x.ModificationGood.Id == modificationGood.Id && x.Property.Id == property.Id).FirstOrDefault();
                    if (propertyValueGroup != null)
                    {
                        propertyValueGroupList.Add(propertyValueGroup);
                    }
                }

                List<PropertyValue> propertyValue = new List<PropertyValue>();
                foreach(var propertyValueGroup in propertyValueGroupList)
                {
                    propertyValue.Add(db.PropertyValueSet.Where(x => x.Id == propertyValueGroup.PropertyValue.Id).FirstOrDefault());
                }
                propertyValue = propertyValue.Distinct().ToList();
                if (propertyValue.Count > 1)
                {
                    propertyList.Add(property);
                }
            }

            Dictionary<int, string> modificationNameDict = new Dictionary<int, string>();

            foreach (var modificationGood in modificationGoodList)
            {
                string modificationStr = string.Empty;
                foreach(var property in propertyList)
                {
                    PropertyValueGroup propertyValueGroup = db.PropertyValueGroupSet.Where(x => x.ModificationGood.Id == modificationGood.Id && x.Property.Id == property.Id).FirstOrDefault();
                    if (propertyValueGroup != null)
                    {
                        if (modificationStr != string.Empty)
                        {
                            modificationStr += " | ";
                        }
                        modificationStr += db.PropertyValueSet.Where(x => x.Id == propertyValueGroup.PropertyValue.Id).FirstOrDefault().Value;
                    }
                }
                modificationNameDict.Add(modificationGood.Id, modificationStr);
            }
            return modificationNameDict;
        }
    }
}
