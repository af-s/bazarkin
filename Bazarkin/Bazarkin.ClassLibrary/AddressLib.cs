﻿using Bazarkin.Domain;

namespace Bazarkin.ClassLibrary
{
    public class AddressLib
    {
        /// <summary>
        /// Формирование строки адреса
        /// </summary>
        /// <param name="address">Адрес объект</param>
        /// <returns>Адрес в строково представлении</returns>
        public static string ObjectToString(Address address)
        {
            string addressStr = string.Empty;

            if (address.Country != null)
            {
                if (addressStr != string.Empty)
                {
                    addressStr += ", ";
                }
                addressStr += address.Country;
            }
            if (address.Region != null)
            {
                if (addressStr != string.Empty)
                {
                    addressStr += ", ";
                }
                addressStr += address.Region;
            }
            if (address.City != null)
            {
                if (addressStr != string.Empty)
                {
                    addressStr += ", ";
                }
                addressStr += address.City;
            }
            if (address.Street != null)
            {
                if (addressStr != string.Empty)
                {
                    addressStr += ", ";
                }
                addressStr += address.Street;
            }
            if (address.Building != null)
            {
                if (addressStr != string.Empty)
                {
                    addressStr += ", ";
                }
                addressStr += address.Building;
            }
            if (address.Structure != null)
            {
                //if (item != string.Empty)
                //{
                //    item += ", ";
                //}
                addressStr += "к" + address.Structure;
            }
            if (address.Room != null)
            {
                if (addressStr != string.Empty)
                {
                    addressStr += ", ";
                }
                addressStr += address.Room;
            }
            if (address.ShoppingCenter != null)
            {
                if (addressStr != string.Empty)
                {
                    addressStr += ", ";
                }
                addressStr += address.ShoppingCenter;
            }
            if (address.Row != null)
            {
                if (addressStr != string.Empty)
                {
                    addressStr += ", ";
                }
                addressStr += "ряд " + address.Row;
            }
            if (address.Sector != null)
            {
                if (addressStr != string.Empty)
                {
                    addressStr += ", ";
                }
                addressStr += "сектор " + address.Sector;
            }
            if (address.Pavilion != null)
            {
                if (addressStr != string.Empty)
                {
                    addressStr += ", ";
                }
                addressStr += "павильон " + address.Pavilion;
            }
            if (address.Index != null)
            {
                if (addressStr != string.Empty)
                {
                    addressStr += ", ";
                }
                addressStr += "индекс " + address.Index;
            }

            return addressStr;
        }
    }
}
