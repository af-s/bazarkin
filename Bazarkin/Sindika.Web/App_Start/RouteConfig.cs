﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Sindika.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            // Catalog -----------------------------------------------------------------------------------------------------------------------------------

            routes.MapRoute(
                name: "Category",
                url: "Kategoriya/{categoryName}",
                defaults: new { controller = "Category", action = "Index", categoryName = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Good",
                url: "Tovar/{goodId}",
                defaults: new { controller = "Good", action = "Index", goodName = UrlParameter.Optional }
            );

            // Organization ------------------------------------------------------------------------------------------------------------------------------

            routes.MapRoute(
                name: "Organization",
                url: "org{organizationID}",
                defaults: new { controller = "Organization", action = "Index" }
            );

            routes.MapRoute(
                name: "OrganizationCategorySet",
                url: "org{organizationID}/kategoriya/{categoryName}",
                defaults: new { controller = "OrganizationCategory", action = "Index" }
            );

            routes.MapRoute(
                name: "OrganizationGood",
                url: "org{organizationID}/Tovar/{goodId}",
                defaults: new { controller = "OrganizationGood", action = "Index" }
            );

            routes.MapRoute(
                name: "OrganizationList",
                url: "magazini",
                defaults: new { controller = "OrganizationList", action = "Index" }
            );

            // Mark -----------------------------------------------------------------------------------------------------------------------------------

            routes.MapRoute(
                name: "Mark",
                url: "zakladki",
                defaults: new { controller = "Mark", action = "Index" }
            );

            // Cart -----------------------------------------------------------------------------------------------------------------------------------

            routes.MapRoute(
                name: "Cart",
                url: "korzina",
                defaults: new { controller = "Cart", action = "Index" }
            );

            // Default -----------------------------------------------------------------------------------------------------------------------------------

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}",
                defaults: new { controller = "Category", action = "Index" }
            );
        }
    }
}
