﻿using Bazarkin.Domain;
using Sindika.Web.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sindika.Web.Models
{
    public class OrganizationCategoryView
    {
        public CategoryView categoryView;
        public Organization organization;
        public string organizationName;
        public string organizationPhone;

        public BreadCrumbs breadCrumbs;

        public OrganizationCategoryView(EFDbContext db, Organization organization, Category category, List<ModificationGood> modificationGoodFilterSet, List<ModificationGood> modificationGoodViewSet)
        {
            this.organization = organization;
            ContactGroup contactGroup = db.ContactGroupSet.Where(x => x.Id == organization.ContactGroup.Id).FirstOrDefault();
            organizationName = organization.Name;
            organizationPhone = "8800200600";
            categoryView = new CategoryView(db, category, modificationGoodFilterSet, modificationGoodViewSet);
            breadCrumbs = new BreadCrumbs(db, organization, category);
        }
    }
}