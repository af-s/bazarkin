﻿using Bazarkin.ClassLibrary;
using Bazarkin.Domain;
using Sindika.Web.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sindika.Web.Models
{
    public class GoodView
    {
        public Good good;
        public Manufacturer manufacturer;
        public List<ImageGood> imageGoodList;
        public List<ModificationAndPrice> modificationAndPriceSet;
        public List<PropertyGroupAndValue> propertyGroupAndValueSet;
        public List<OrganizationUnitAndPrice> organizationUnitAndPriceList;
        public BreadCrumbs breadCrumbs;

        public GoodView() { }

        public GoodView(EFDbContext db, List<ModificationGood> modificationGoodList, ModificationGood modificationGoodActive, Organization organization)
        {
            int goodId = modificationGoodList[0].Good.Id;
            good = db.GoodSet.Where(x => x.Id == goodId).FirstOrDefault();
            manufacturer = db.ManufacturerSet.Where(x => x.Id == good.Manufacturer.Id).FirstOrDefault();

            breadCrumbs = new BreadCrumbs(db, good);

            // Изображения

            if (modificationGoodActive == null)
            {
                imageGoodList = db.ImageGoodSet.Where(x => x.Good.Id == good.Id).ToList();
                if (imageGoodList != null)
                {
                    imageGoodList = imageGoodList.OrderBy(x => x.OrderIndex).ToList();
                }
            }
            else
            {
                List<ImageModificationGood> imageModificationGoodList = db.ImageModificationGoodSet.Where(x => x.ModificationGood.Id == modificationGoodActive.Id).ToList();

                if (imageModificationGoodList != null)
                {
                    imageGoodList = new List<ImageGood>();
                    foreach (var imageModificationGood in imageModificationGoodList)
                    {
                        imageGoodList.Add(db.ImageGoodSet.Where(x => x.Id == imageModificationGood.ImageGood.Id).FirstOrDefault());
                    }
                    imageGoodList = imageGoodList.OrderBy(x => x.OrderIndex).ToList();
                }
            }

            // Модификации и цены

            Dictionary<int, string> modificationNameDict = GoodLib.GetModificationNameDict(good);

            modificationAndPriceSet = new List<ModificationAndPrice>();
            foreach (var modificationGood in modificationGoodList)
            {
                modificationAndPriceSet.Add(new ModificationAndPrice(db, modificationGood, modificationNameDict.Where(x => x.Key == modificationGood.Id).FirstOrDefault().Value));
            }

            // Торговые точки и цены

            organizationUnitAndPriceList = new List<OrganizationUnitAndPrice>();            
            List<OrganizationUnit> organizationUnitList = new List<OrganizationUnit>();

            List<Price> priceList = new List<Price>();
            foreach (var modificationGood in modificationGoodList)
            {
                priceList.AddRange(db.PriceSet.Where(x => x.ModificationGood.Id == modificationGood.Id).ToList());
            }

            foreach (var price in priceList)
            {
                organizationUnitList.Add(db.OrganizationUnitSet.Where(x => x.Id == price.OrganizationUnit.Id).First());
            }
            organizationUnitList = organizationUnitList.Distinct().ToList();

            foreach (var organizationUnit in organizationUnitList)
            {
                organizationUnitAndPriceList.Add(new OrganizationUnitAndPrice(db, organizationUnit, modificationGoodList));
            }

            // Свойства и значения

            propertyGroupAndValueSet = new List<PropertyGroupAndValue>();
            List<PropertyValueGroup> propertyValueGroupSet = new List<PropertyValueGroup>();
            foreach (var modificationGood in modificationGoodList)
            {
                propertyValueGroupSet.AddRange(db.PropertyValueGroupSet.Where(x => x.ModificationGood.Id == modificationGood.Id));
            }
            List<PropertyGroup> propertyGroupList = new List<PropertyGroup>();
            foreach (var propertyValueGroup in propertyValueGroupSet)
            {
                propertyGroupList.Add(db.PropertyGroupSet.Where(x => x.Id == propertyValueGroup.Property.PropertyGroup.Id).FirstOrDefault());
            }
            propertyGroupList = propertyGroupList.Distinct().ToList();
            foreach (var propertyGroup in propertyGroupList)
            {
                propertyGroupAndValueSet.Add(new PropertyGroupAndValue(db, propertyGroup, propertyValueGroupSet, good));
            }
        }

        public class ModificationAndPrice
        {
            public ModificationGood modificationGood;
            public List<Price> priceSet;
            public string modificationString;

            public ModificationAndPrice() { }

            public ModificationAndPrice(EFDbContext db, ModificationGood modificationGood, string modificationString)
            {
                this.modificationGood = modificationGood;
                priceSet = db.PriceSet.Where(x => x.ModificationGood.Id == modificationGood.Id).ToList();

                this.modificationString = modificationString;
            }
        }

        public class PropertyGroupAndValue
        {
            public PropertyGroup propertyGroup;
            public List<PropertyAndValue> propertyAndValueSet;

            public PropertyGroupAndValue(EFDbContext db, PropertyGroup propertyGroup, List<PropertyValueGroup> propertyValueGroupSet, Good good)
            {
                this.propertyGroup = propertyGroup;

                Category category = db.CategorySet.Where(x => x.Id == good.Category.Id).FirstOrDefault();
                List<PropertyCategory> propertyCategoryList = db.PropertyCategorySet.Where(x => x.Category.Id == category.Id).ToList();

                List<Property> propertySet = new List<Property>();
                foreach(var propertyCategory in propertyCategoryList)
                {
                    Property property = db.PropertySet.Where(x => x.Id == propertyCategory.Property.Id && x.PropertyGroup.Id == propertyGroup.Id).FirstOrDefault();
                    if (property != null)
                    {
                        propertySet.Add(property);
                    }
                }
                propertySet = propertySet.OrderBy(x => x.Name).ToList();

                propertyAndValueSet = new List<PropertyAndValue>();
                foreach (var property in propertySet)
                {
                    propertyAndValueSet.Add(new PropertyAndValue(db, property, propertyValueGroupSet));
                }
            }
        }

        public class PropertyAndValue
        {
            public Property property;
            public string propertyValueString;

            public PropertyAndValue(EFDbContext db, Property property, List<PropertyValueGroup> propertyValueGroupSet)
            {
                this.property = property;
                this.propertyValueString = string.Empty;
                propertyValueGroupSet = propertyValueGroupSet.Where(x => x.PropertyValue.Property.Id == property.Id).ToList();
                List<PropertyValue> propertyValueSet = new List<PropertyValue>();
                foreach (var propertyValueGroup in propertyValueGroupSet)
                {
                    propertyValueSet.Add(db.PropertyValueSet.Where(x => x.Id == propertyValueGroup.PropertyValue.Id).FirstOrDefault());
                }
                propertyValueSet = propertyValueSet.Distinct().ToList();
                foreach (var propertyValue in propertyValueSet)
                {
                    if (propertyValueString != string.Empty)
                    {
                        propertyValueString += " | ";
                    }
                    propertyValueString += propertyValue.Value;
                }
            }
        }

        public class OrganizationUnitAndPrice
        {
            public Organization organization;
            public OrganizationUnit organizationUnit;
            public List<Price> priceSet;
            public string addressStr;

            public OrganizationUnitAndPrice() { }

            public OrganizationUnitAndPrice(EFDbContext db, OrganizationUnit organizationUnit, List<ModificationGood> modificationGoodSet)
            {
                this.organizationUnit = organizationUnit;
                organization = db.OrganizationSet.Where(x => x.Id == organizationUnit.Organization.Id).FirstOrDefault();
                priceSet = new List<Price>();

                foreach (var modificationGood in modificationGoodSet)
                {
                    Price price = db.PriceSet.Where(x => x.ModificationGood.Id == modificationGood.Id && x.OrganizationUnit.Id == organizationUnit.Id).FirstOrDefault();
                    if (price != null)
                    {
                        priceSet.Add(price);
                    }
                }

                ContactGroup cg = db.ContactGroupSet.Where(x => x.Id == organizationUnit.ContactGroup.Id).FirstOrDefault();
                Address address = db.AddressSet.Where(x => x.ContactGroup.Id == cg.Id).FirstOrDefault();
                addressStr = AddressLib.ObjectToString(address);
            }
        }
    }
}