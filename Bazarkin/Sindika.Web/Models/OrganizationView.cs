﻿using Bazarkin.ClassLibrary;
using Bazarkin.Domain;
using Sindika.Web.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sindika.Web.Models
{
    public class OrganizationView
    {
        public OrganizationAndInfo organizationAndInfo;
        public List<OrganizationUnitAndInfo> organizationUnitAndInfoSet;
        public List<Category> categoryList;

        public string organizationName;
        public string organizationPhone;

        public BreadCrumbs breadCrumbs;

        public string type;

        public OrganizationView(EFDbContext db, Organization organization)
        {
            type = "Organization";

            this.organizationAndInfo = new OrganizationAndInfo(db, organization);

            List<OrganizationUnit> organizationUnitSet = db.OrganizationUnitSet.Where(x => x.Organization.Id == organization.Id).ToList();
            this.organizationUnitAndInfoSet = new List<OrganizationUnitAndInfo>();
            foreach (var organizationUnit in organizationUnitSet)
            {
                organizationUnitAndInfoSet.Add(new OrganizationUnitAndInfo(db, organizationUnit));
            }

            this.categoryList = new List<Category>();

            foreach (var organizationUnit in organizationUnitSet)
            {
                List<Price> priceSet = db.PriceSet.Where(x=>x.OrganizationUnit.Id==organizationUnit.Id).ToList();
                foreach(var price in priceSet)
                {
                    this.categoryList.Add(db.CategorySet.Where(x => x.Id == price.ModificationGood.Good.Category.Id).FirstOrDefault());
                }
            }
            this.categoryList = this.categoryList.Distinct().ToList();

            organizationName = organization.Name;
            if (organizationAndInfo.phoneSet.Count() > 0)
            {
                organizationPhone = organizationAndInfo.phoneSet.FirstOrDefault().Number;
            }

            this.breadCrumbs = new BreadCrumbs(db, organization);
        }

        public class OrganizationAndInfo
        {
            public Organization organization;
            public List<Phone> phoneSet;
            public List<AddressAndDescription> addressAndDescriptionSet;
            //public List<OrganizationUnitAndInfo> organizationUnitAndInfoSet;

            public OrganizationAndInfo(EFDbContext db, Organization organization)
            {
                this.organization = organization;
                List<Address> addressSet = db.AddressSet.Where(x => x.ContactGroup.Id == organization.ContactGroup.Id).ToList();
                this.phoneSet = db.PhoneSet.Where(x => x.ContactGroup.Id == organization.ContactGroup.Id).ToList();
                addressAndDescriptionSet = new List<AddressAndDescription>();
                foreach (var address in addressSet)
                {
                    addressAndDescriptionSet.Add(new AddressAndDescription(address));
                }

                //organizationUnitAndInfoSet = new List<OrganizationUnitAndInfo>();
                //List<OrganizationUnit> organizationUnitSet = db.OrganizationUnitSet.Where(x => x.Organization.Id == organization.Id).ToList();
                //foreach (var organizationUnit in organizationUnitSet)
                //{
                //    organizationUnitAndInfoSet.Add(new OrganizationUnitAndInfo(db, organizationUnit));
                //}
            }
        }

        public class OrganizationUnitAndInfo
        {
            public OrganizationUnit organizationUnit;
            public List<Phone> phoneSet;
            public List<AddressAndDescription> addressAndDescriptionSet;

            public OrganizationUnitAndInfo(EFDbContext db, OrganizationUnit organizationUnit)
            {
                this.organizationUnit = organizationUnit;
                List<Address> addressSet = db.AddressSet.Where(x => x.ContactGroup.Id == organizationUnit.ContactGroup.Id).ToList();
                this.phoneSet = db.PhoneSet.Where(x => x.ContactGroup.Id == organizationUnit.ContactGroup.Id).ToList();
                addressAndDescriptionSet = new List<AddressAndDescription>();
                foreach (var address in addressSet)
                {
                    addressAndDescriptionSet.Add(new AddressAndDescription(address));
                }
            }
        }

        public class AddressAndDescription
        {
            public Address address;
            public string addressStr;

            public AddressAndDescription(Address address)
            {
                this.address = address;
                addressStr = AddressLib.ObjectToString(address);
            }
        }
    }
}