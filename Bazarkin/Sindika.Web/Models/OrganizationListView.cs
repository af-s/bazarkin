﻿using Bazarkin.Domain;
using Sindika.Web.BusinessLogic;
using System.Collections.Generic;
using System.Linq;

namespace Sindika.Web.Models
{
    public class OrganizationListView
    {
        private EFDbContext db = new EFDbContext();
        public List<OrganizationAndInfo> OrganizationAndInfoList;
        public BreadCrumbs breadCrumbs;

        public OrganizationListView()
        {
            breadCrumbs = new BreadCrumbs(db, "Магазины");
            List<Organization> organizationList = db.OrganizationSet.OrderBy(x => x.Name).ToList();

            OrganizationAndInfoList = new List<OrganizationAndInfo>();
            foreach(var organization in organizationList)
            {
                OrganizationAndInfoList.Add(new OrganizationAndInfo(db, organization));
            }
        }

        public class OrganizationAndInfo
        {
            public Organization organization;
            public List<Category> categoryList;

            public OrganizationAndInfo(EFDbContext db, Organization organization)
            {
                this.organization = organization;
                List<Price> priceList = db.PriceSet.Where(x => x.OrganizationUnit.Organization.Id == organization.Id).ToList();

                categoryList = new List<Category>();

                while (priceList.Count != 0)
                {
                    int categoryId = priceList.First().ModificationGood.Good.Category.Id;
                    categoryList.Add(db.CategorySet.Where(x => x.Id == categoryId).FirstOrDefault());

                    priceList.RemoveAll(x => x.ModificationGood.Good.Category.Id == categoryId);
                }
            }
        }
    }
}