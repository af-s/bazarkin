﻿using Bazarkin.Domain;
using Sindika.Web.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sindika.Web.Models
{
    /// <summary>
    /// Модель данных для представления Каталог
    /// </summary>
    public class CatalogView
    {
        public List<CatalogCategory> catalogCategorySet;
        public BreadCrumbs breadCrumbs;

        public CatalogView(EFDbContext db)
        {
            breadCrumbs = new BreadCrumbs(db, "Каталог");

            catalogCategorySet = new List<CatalogCategory>();

            List<Category> categorySet = db.CategorySet.Where(x => x.Parent == null).OrderBy(x => x.Name).ToList();
            foreach (var categoryItem in categorySet)
            {
                CatalogCategory catalogCategory = new CatalogCategory(db, categoryItem);
                catalogCategorySet.Add(catalogCategory);
            }
        }
    }

    public class CatalogCategory
    {
        public Category category;
        public List<CatalogCategory> catalogCategorySet;

        public CatalogCategory(EFDbContext db, Category category)
        {
            catalogCategorySet = new List<CatalogCategory>();
            this.category = category;
            List<Category> categoryList = db.CategorySet.Where(x => x.Parent.Id == category.Id).OrderBy(x => x.Name).ToList();
            if (categoryList.Count != 0)
            {
                foreach (var categoryItem in categoryList)
                {
                    CatalogCategory catalogCategory = new CatalogCategory(db, categoryItem);
                    catalogCategorySet.Add(catalogCategory);
                }
            }
        }
    }
}