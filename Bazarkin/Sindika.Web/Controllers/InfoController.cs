﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Bazarkin.Web.Controllers
{
    public class InfoController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult OKomplekse()
        {
            ViewBag.Title = String.Format("Синдика | О комплексе");
            return View();
        }

        public ActionResult Akcii()
        {
            ViewBag.Title = String.Format("Синдика | Акции");
            return View();
        }

        public ActionResult Arendatoram()
        {
            ViewBag.Title = String.Format("Синдика | Арендаторам");
            return View();
        }

        public ActionResult Contacti()
        {
            ViewBag.Title = String.Format("Синдика | Контакты");
            return View();
        }

        public ActionResult ShemaKompleksa()
        {
            ViewBag.Title = String.Format("Синдика | Схема комплекса");
            return View();
        }

        public ActionResult Novosti()
        {
            ViewBag.Title = String.Format("Синдика | Новости");
            return View();
        }
    }
}