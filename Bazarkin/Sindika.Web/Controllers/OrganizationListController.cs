﻿using Sindika.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sindika.Web.Controllers
{
    public class OrganizationListController : Controller
    {
        // GET: OrganizationList
        public ActionResult Index()
        {
            ViewBag.Title = String.Format("Синдика | Магазины");
            OrganizationListView organizationListView = new OrganizationListView();
            return View(organizationListView);
        }
    }
}