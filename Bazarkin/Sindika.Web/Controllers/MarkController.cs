﻿using Bazarkin.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sindika.Web.Controllers
{
    public class MarkController : Controller
    {
        private EFDbContext db = new EFDbContext();

        // GET: Mark
        public ActionResult Index()
        {
            List<int> goodIdList = new List<int>() { 1046, 1048, 1060, 1161, 1150, 1057, 1065 };
            List<Models.CategoryView.GoodCard> goodCardList = new List<Models.CategoryView.GoodCard>();
            foreach(var goodId in goodIdList)
            {
                goodCardList.Add(new Models.CategoryView.GoodCard(db, db.GoodSet.Where(x => x.Id == goodId).FirstOrDefault(), db.ModificationGoodSet.Where(x => x.Good.Id == goodId).ToList()));
            }
            ViewBag.Title = String.Format("Синдика | Закладки");
            return View(goodCardList);
        }
    }
}