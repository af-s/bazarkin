﻿using Bazarkin.Domain;
using Sindika.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sindika.Web.Controllers
{
    public class OrganizationCategoryController : Controller
    {
        private EFDbContext db = new EFDbContext();

        public ActionResult Index()
        {
            int organizationId;
            string catrgoryAlias;
            string orgId = RouteData.Values["organizationID"].ToString();
            if (RouteData.Values["organizationID"] != null && RouteData.Values["categoryName"] != null)
            {
                organizationId = Convert.ToInt32(RouteData.Values["organizationID"]);
                catrgoryAlias = RouteData.Values["categoryName"].ToString();

                Organization organization = db.OrganizationSet.Where(x => x.Id == organizationId).FirstOrDefault();
                Category category = db.CategorySet.Where(x => x.Alias == catrgoryAlias).FirstOrDefault();

                if(organization!=null && category != null)
                {
                    ViewBag.Title = "Синдика | " + organization.Name + " | " + category.Name;

                    List<Price> priceList = db.PriceSet.Where(x => x.OrganizationUnit.Organization.Id == organization.Id && x.ModificationGood.Good.Category.Id == category.Id).ToList();
                    // проверка что цена последняя не нулл
                    List<ModificationGood> modificationGoodList = new List<ModificationGood>();
                    foreach(var price in priceList)
                    {
                        if (priceList.Where(x => x.ModificationGood.Id == price.ModificationGood.Id).Last().Value != null)
                        {
                            modificationGoodList.Add(db.ModificationGoodSet.Where(x => x.Id == price.ModificationGood.Id).FirstOrDefault());
                        }
                    }

                    modificationGoodList = modificationGoodList.Distinct().ToList();

                    OrganizationCategoryView organizationCategory = new OrganizationCategoryView(db, organization, category, modificationGoodList, modificationGoodList);

                    return View(organizationCategory);
                }
            }

            
            // 404
            return View();
        }
    }
}