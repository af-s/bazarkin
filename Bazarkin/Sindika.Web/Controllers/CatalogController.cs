﻿using Bazarkin.Domain;
using Sindika.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sindika.Web.Controllers
{
    /// <summary>
    /// Каталог всех групп с подгруппами
    /// </summary>
    public class CatalogController : Controller
    {
        private EFDbContext db = new EFDbContext();
        public ActionResult Index()
        {
            ViewBag.Title = String.Format("Синдика | Каталог");
            CatalogView catalogView = new CatalogView(db);

            return View(catalogView);
        }
    }
}