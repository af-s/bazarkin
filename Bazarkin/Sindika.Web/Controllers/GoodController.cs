﻿using Bazarkin.Domain;
using Sindika.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Bazarkin.Web.Controllers
{
    /// <summary>
    /// Страница товара
    /// </summary>
    public class GoodController : Controller
    {
        private EFDbContext db = new EFDbContext();

        [HttpGet]
        public ActionResult Index()
        {
            int goodId;
            if (RouteData.Values["goodId"] != null)
            {
                goodId = Convert.ToInt32(RouteData.Values["goodId"]);
                Good good = db.GoodSet.Where(x => x.Id == goodId).FirstOrDefault();

                if (good != null)
                {
                    Manufacturer manufacturer = db.ManufacturerSet.Where(x => x.Id == good.Manufacturer.Id).FirstOrDefault();
                    ViewBag.Title = String.Format("Синдика | {0} {1}", manufacturer.Name, good.Name);

                    List<ModificationGood> modificationGoodSet = db.ModificationGoodSet.Where(x => x.Good.Id == good.Id).ToList();
                    GoodView goodItemView = new GoodView(db, modificationGoodSet, null, null);
                    return View("Index", goodItemView);
                }
            }

            return View();
        }

        [HttpPost]
        public ActionResult Index(int? modificationId, int? PriceId)
        {
            List<ModificationGood> modificationGoodSet = db.ModificationGoodSet.Where(x => x.Id == modificationId).ToList();
            //GoodView goodItemView = new GoodView(db, modificationGoodSet, null);
            return View("Index"/*, goodItemView*/);
        }
    }
}