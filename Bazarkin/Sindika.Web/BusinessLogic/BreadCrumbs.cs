﻿using Bazarkin.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sindika.Web.BusinessLogic
{
    public class BreadCrumbs
    {
        public List<BreadCrumbsItem> BreadCrumbsItemSet;

        public BreadCrumbs(EFDbContext db, Category category)
        {
            BreadCrumbsItemSet = new List<BreadCrumbsItem>();
            while (true)
            {
                if (category != null)
                {
                    BreadCrumbsItemSet.Add(new BreadCrumbsItem(category));
                    if (category.Parent!=null)
                    {
                        category = db.CategorySet.Where(x => x.Id == category.Parent.Id).FirstOrDefault();
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            BreadCrumbsItemSet.Reverse();
        }
        public BreadCrumbs(EFDbContext db, Good good)
        {
            BreadCrumbsItemSet = new List<BreadCrumbsItem>();
            Manufacturer manufacturer = db.ManufacturerSet.Where(x => x.Id == good.Manufacturer.Id).FirstOrDefault();
            Category category = db.CategorySet.Where(x => x.Id == good.Category.Id).FirstOrDefault();
            BreadCrumbsItemSet.Add(new BreadCrumbsItem(good, manufacturer));
            while (true)
            {
                if (category != null)
                {
                    BreadCrumbsItemSet.Add(new BreadCrumbsItem(category));
                    if (category.Parent != null)
                    {
                        category = db.CategorySet.Where(x => x.Id == category.Parent.Id).FirstOrDefault();
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            BreadCrumbsItemSet.Reverse();
        }

        public BreadCrumbs(EFDbContext db, string text)
        {
            BreadCrumbsItemSet = new List<BreadCrumbsItem>();
            BreadCrumbsItemSet.Add(new BreadCrumbsItem(text));
        }

        public BreadCrumbs(EFDbContext db, Organization organization)
        {
            BreadCrumbsItemSet = new List<BreadCrumbsItem>();
            BreadCrumbsItemSet.Add(new BreadCrumbsItem(organization));
        }

        public BreadCrumbs(EFDbContext db, Organization organization, Category category)
        {
            BreadCrumbsItemSet = new List<BreadCrumbsItem>();
            BreadCrumbsItemSet.Add(new BreadCrumbsItem(organization));
            BreadCrumbsItemSet.Add(new BreadCrumbsItem(category));
        }

        /// <summary>
        /// Эллемен хлебной крошки
        /// </summary>
        public class BreadCrumbsItem
        {
            public string name;
            public string href;

            public BreadCrumbsItem() { }

            public BreadCrumbsItem(Category category)
            {
                this.name = category.Name;
                this.href = "/kategoriya/"+category.Alias;
            }

            public BreadCrumbsItem(Good good, Manufacturer manufacturer)
            {
                this.name = string.Format("{0} {1}", manufacturer.Name, good.Name);
                this.href = "/tovar/" + good.Id;
            }

            public BreadCrumbsItem(string text)
            {
                this.name = text;
                this.href = "/#";
            }

            public BreadCrumbsItem(Organization organization)
            {
                this.name = organization.Name;
                this.href = "/org" + organization.Id;
            }
        }
    }
}