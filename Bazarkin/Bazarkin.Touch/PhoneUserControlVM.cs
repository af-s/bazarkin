﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bazarkin.Touch
{
    public class PhoneUserControlVM : ViewModelBase
    {
        #region PhoneNumber

        private string mPhoneNumber;
        /// <summary>
        /// Номер телефона
        /// </summary>
        public string PhoneNumber
        {
            get { return mPhoneNumber; }
            set
            {
                if (mPhoneNumber == value)
                    return;
                mPhoneNumber = value;
                RaisePropertyChanged(() => PhoneNumber);
            }
        }

        #endregion

        #region PhoneDescription

        private string mPhoneDescription;
        /// <summary>
        /// Описание для номера телефона
        /// </summary>
        public string PhoneDescription
        {
            get { return mPhoneDescription; }
            set
            {
                if (mPhoneDescription == value)
                    return;
                mPhoneDescription = value;
                RaisePropertyChanged(() => PhoneDescription);
            }
        }

        #endregion

        #region PhoneDescription

        private bool mPhoneVisibility;
        /// <summary>
        /// Показывать на сайте
        /// </summary>
        public bool PhoneVisibility
        {
            get { return mPhoneVisibility; }
            set
            {
                if (mPhoneVisibility == value)
                    return;
                mPhoneVisibility = value;
                RaisePropertyChanged(() => PhoneVisibility);
            }
        }

        #endregion
    }
}
