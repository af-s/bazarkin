﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using System.Windows.Controls;
using GalaSoft.MvvmLight.Command;
using System.Windows.Media;
using Bazarkin.Domain;
using System.Xml.Linq;

namespace Bazarkin.Touch
{
    public class OrganizationUnitVM : ViewModelBase
    {
        public RelayCommand AddPhoneCommand { get; set; }

        public RelayCommand SaveCommand { get; set; }

        private bool PhoneBackgroundFlag;

        #region PhonView
        private StackPanel mPhonView;
        /// <summary>
        /// View для телефонов
        /// </summary>
        public StackPanel PhonView
        {
            get { return mPhonView; }
            set
            {
                if (mPhonView == value)
                    return;
                mPhonView = value;
                RaisePropertyChanged(() => PhonView);
            }
        }
        #endregion

        #region AddressShopingCenter

        private string mAddressShopingCenter;
        /// <summary>
        /// Торговый центр
        /// </summary>
        public string AddressShopingCenter
        {
            get { return mAddressShopingCenter; }
            set
            {
                if (mAddressShopingCenter == value)
                    return;
                mAddressShopingCenter = value;
                RaisePropertyChanged(() => AddressShopingCenter);
            }
        }

        #endregion

        #region AddressRow

        private string mAddressRow;
        /// <summary>
        /// Ряд
        /// </summary>
        public string AddressRow
        {
            get { return mAddressRow; }
            set
            {
                if (mAddressRow == value)
                    return;
                mAddressRow = value;
                RaisePropertyChanged(() => AddressRow);
            }
        }

        #endregion

        #region AddressPavilion

        private string mAddressPavilion;
        /// <summary>
        /// Павильон
        /// </summary>
        public string AddressPavilion
        {
            get { return mAddressPavilion; }
            set
            {
                if (mAddressPavilion == value)
                    return;
                mAddressPavilion = value;
                RaisePropertyChanged(() => AddressPavilion);
            }
        }

        #endregion

        #region OrganizationUnitName

        private string mOrganizationUnitName;
        /// <summary>
        /// Наименование торговой точки
        /// </summary>
        public string OrganizationUnitName
        {
            get { return mOrganizationUnitName; }
            set
            {
                if (mOrganizationUnitName == value)
                    return;
                mOrganizationUnitName = value;
                RaisePropertyChanged(() => OrganizationUnitName);
            }
        }

        #endregion

        #region OrganizationUnitDescription

        private string mOrganizationUnitDescription;
        /// <summary>
        /// Описание торговой точки
        /// </summary>
        public string OrganizationUnitDescription
        {
            get { return mOrganizationUnitDescription; }
            set
            {
                if (mOrganizationUnitDescription == value)
                    return;
                mOrganizationUnitDescription = value;
                RaisePropertyChanged(() => OrganizationUnitDescription);
            }
        }

        #endregion

        #region EmailValue

        private string mEmailValue;
        /// <summary>
        /// e-mail
        /// </summary>
        public string EmailValue
        {
            get { return mEmailValue; }
            set
            {
                if (mEmailValue == value)
                    return;
                mEmailValue = value;
                RaisePropertyChanged(() => EmailValue);
            }
        }

        #endregion

        #region EmailDescription

        private string mEmailDescription;
        /// <summary>
        /// e-mail описание
        /// </summary>
        public string EmailDescription
        {
            get { return mEmailDescription; }
            set
            {
                if (mEmailDescription == value)
                    return;
                mEmailDescription = value;
                RaisePropertyChanged(() => EmailDescription);
            }
        }

        #endregion

        #region OrganizationName

        private string mOrganizationName;
        /// <summary>
        /// Юридическое название организации
        /// </summary>
        public string OrganizationName
        {
            get { return mOrganizationName; }
            set
            {
                if (mOrganizationName == value)
                    return;
                mOrganizationName = value;
                RaisePropertyChanged(() => OrganizationName);
            }
        }

        #endregion

        #region OrganizationDescription

        private string mOrganizationDescription;
        /// <summary>
        /// организация описание
        /// </summary>
        public string OrganizationDescription
        {
            get { return mOrganizationDescription; }
            set
            {
                if (mOrganizationDescription == value)
                    return;
                mOrganizationDescription = value;
                RaisePropertyChanged(() => OrganizationDescription);
            }
        }

        #endregion

        #region ResultDecscription

        private string mResultDecscription;
        /// <summary>
        /// результат описание
        /// </summary>
        public string ResultDecscription
        {
            get { return mResultDecscription; }
            set
            {
                if (mResultDecscription == value)
                    return;
                mResultDecscription = value;
                RaisePropertyChanged(() => ResultDecscription);
            }
        }

        #endregion

        #region Statuses

        private List<string> mStatuses;
        /// <summary>
        /// Статусы
        /// </summary>
        public List<string> Statuses
        {
            get { return mStatuses; }
            set
            {
                if (mStatuses == value)
                    return;
                mStatuses = value;
                RaisePropertyChanged(() => Statuses);
            }
        }

        #endregion

        #region SelectedStatus

        private string mSelectedStatus;
        /// <summary>
        /// Выбранный результат
        /// </summary>
        public string SelectedStatus
        {
            get { return mSelectedStatus; }
            set
            {
                if (mSelectedStatus == value)
                    return;
                mSelectedStatus = value;
                RaisePropertyChanged(() => SelectedStatus);
            }
        }

        #endregion

        #region UserName

        private string mUserName;
        /// <summary>
        /// Выбранный результат
        /// </summary>
        public string UserName
        {
            get { return mUserName; }
            set
            {
                if (mUserName == value)
                    return;
                mUserName = value;
                RaisePropertyChanged(() => UserName);
            }
        }

        #endregion

        public EventHandler CloseWindow;

        #region Constructors

        public OrganizationUnitVM()
        {
            GeneratePhoneView();
            AddPhoneCommand = new RelayCommand(OnAddPhoneCommand);
            SaveCommand = new RelayCommand(OnSaveCommand, SaveCommandCanExecute);
            CloseWindow += CloseWindowCommand;

            List<string> stat = new List<string>();
            stat.Add("Не указано");
            stat.Add("Отказался от плтатной подписки");
            stat.Add("Согласен на плтатную подписку");
            Statuses = stat;
            SelectedStatus = "Не указано";

            string fileName = "info.xml";
            XDocument xdoc = XDocument.Load(fileName);

            var userNameStr = xdoc.Element("users").Elements("user").FirstOrDefault().Element("name").Value;
            UserName = userNameStr;
        }

        #endregion

        #region Handlers

        /// <summary>
        /// Событие закрытия окна
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseWindowCommand(object sender, EventArgs e)
        {
            
        }

        /// <summary>
        /// Кнопка добавить телефон
        /// </summary>
        private void OnAddPhoneCommand()
        {
            PhoneBackgroundFlag = !PhoneBackgroundFlag;
            var control = new PhoneUserControl { DataContext = new PhoneUserControlVM() };
            if (PhoneBackgroundFlag)
                control.Background = Brushes.LightGray;
            PhonView.Children.Add(control);
        }

        /// <summary>
        /// Можно ли нажимать кнопку сохранить
        /// </summary>
        /// <returns></returns>
        private bool SaveCommandCanExecute()
        {
            return true;
            //Если хочешь поменять свойство из кода то после валидации надо вызвать
            //SaveCommand.RaiseCanExecuteChanged();
        }

        /// <summary>
        /// Обработчик события сохранить
        /// </summary>
        private void OnSaveCommand()
        {
            XMLAdd();
            ClearAdd();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Генерируем грид для телефонов
        /// </summary>
        private void GeneratePhoneView()
        {
            PhonView = new StackPanel();
            PhonView.Children.Add(new PhoneUserControl { DataContext = new PhoneUserControlVM() });
        }

        /// <summary>
        /// Добавление информации в XML
        /// </summary>
        private void XMLAdd()
        {
            string fileName = "base.xml";

            // Торговая точка
            XElement xOrganizationUnit = new XElement("organizationUnit");
            XAttribute xOrganizationUnitName = OrganizationUnitName != null ? new XAttribute("name", OrganizationUnitName) : new XAttribute("name", string.Empty);
            XAttribute xOrganizationUnitDescription = OrganizationUnitDescription != null ? new XAttribute("description", OrganizationUnitDescription) : new XAttribute("description", string.Empty);
            xOrganizationUnit.Add(xOrganizationUnitName);
            xOrganizationUnit.Add(xOrganizationUnitDescription);

            // Адрес
            XElement xAddress = new XElement("address");
            XAttribute xAddressShoppingCenter = AddressShopingCenter != null ? new XAttribute("shoppingCenter", AddressShopingCenter) : new XAttribute("shoppingCenter", string.Empty);
            XAttribute xAddressRow = AddressRow != null ? new XAttribute("row", AddressRow) : new XAttribute("row", string.Empty);
            XAttribute xAddressPavilion = AddressPavilion != null ? new XAttribute("pavilion", AddressPavilion) : new XAttribute("pavilion", string.Empty);
            xAddress.Add(xAddressShoppingCenter);
            xAddress.Add(xAddressRow);
            xAddress.Add(xAddressPavilion);

            // Почта
            XElement xMail = new XElement("mail");
            XAttribute xMailValue = EmailValue != null ? new XAttribute("value", EmailValue) : new XAttribute("value", string.Empty);
            XAttribute xMailDescription = EmailDescription != null ? new XAttribute("description", EmailDescription) : new XAttribute("description", string.Empty);
            xMail.Add(xMailValue);
            xMail.Add(xMailDescription);

            // Информация
            XElement xInfo = new XElement("info");
            XAttribute xInfoDescription = ResultDecscription != null ? new XAttribute("description", ResultDecscription) : new XAttribute("description", string.Empty);
            XAttribute xInfoStatus = SelectedStatus != null ? new XAttribute("status", SelectedStatus) : new XAttribute("status", string.Empty);
            xInfo.Add(xInfoDescription);
            xInfo.Add(xInfoStatus);

            // Список телефонов
            XElement xPhoneSet = new XElement("phoneSet");
            foreach (PhoneUserControl child in PhonView.Children)
            {
                //Получаем ViewModel для каждого child(а) она является PhoneUserControlVM
                var context = child.DataContext;
                //Приводим тип и получаем список значений (Ваш КЭП)
                if(context!= null)
                {
                    if (((PhoneUserControlVM)context).PhoneNumber != null || ((PhoneUserControlVM)context).PhoneDescription != null)
                    {
                        XElement xPhone = new XElement("phone");
                        XAttribute xPhoneNumber = ((PhoneUserControlVM)context).PhoneNumber != null ? new XAttribute("number", ((PhoneUserControlVM)context).PhoneNumber) : new XAttribute("number", string.Empty);
                        XAttribute xPhoneDescription = ((PhoneUserControlVM)context).PhoneDescription != null ? new XAttribute("description", ((PhoneUserControlVM)context).PhoneDescription) : new XAttribute("description", string.Empty);
                        XAttribute xPhoneView =  new XAttribute("view", ((PhoneUserControlVM)context).PhoneVisibility);

                        xPhone.Add(xPhoneNumber);
                        xPhone.Add(xPhoneDescription);
                        xPhone.Add(xPhoneView);
                        xPhoneSet.Add(xPhone);
                    }
                }
            }

            XElement xUser = new XElement("user");
            XAttribute xName = new XAttribute("name", UserName);
            xUser.Add(xName);
            
            xOrganizationUnit.Add(xAddress);
            xOrganizationUnit.Add(xMail);
            xOrganizationUnit.Add(xInfo);
            xOrganizationUnit.Add(xPhoneSet);

            xOrganizationUnit.Add(xUser);

            XDocument xdoc = XDocument.Load(fileName);

            xdoc.Root.Add(xOrganizationUnit);

            xdoc.Save(fileName);
        }

        /// <summary>
        /// Отчистка полей
        /// </summary>
        private void ClearAdd()
        {
            
            OrganizationUnitName = string.Empty;
            OrganizationUnitDescription = string.Empty;
            AddressShopingCenter = string.Empty;
            AddressRow = string.Empty;
            AddressPavilion = string.Empty;
            EmailValue = string.Empty;
            EmailDescription = string.Empty;
            OrganizationName = string.Empty;
            OrganizationDescription = string.Empty;
            ResultDecscription = string.Empty;

            PhonView.Children.Clear();
            GeneratePhoneView();

            SelectedStatus = "Не указано";
        }
        #endregion
    }
}
