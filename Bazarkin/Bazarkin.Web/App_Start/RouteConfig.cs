﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Bazarkin.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            // Catalog -----------------------------------------------------------------------------------------------------------------------------------

            routes.MapRoute(
                name: "Category",
                url: "Kategoriya/{categoryName}",
                defaults: new { controller = "Kategoriya", action = "Index", categoryName = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Good",
                url: "Tovar/{goodId}",
                defaults: new { controller = "Tovar", action = "Index", goodName = UrlParameter.Optional }
            );

            // Organization ------------------------------------------------------------------------------------------------------------------------------

            routes.MapRoute(
                name: "Organization",
                url: "org{organizationID}",
                defaults: new { controller = "Organization", action = "Index" }
            );

            routes.MapRoute(
                name: "OrganizationCategorySet",
                url: "org{organizationID}/kategoriya/{categoryName}",
                defaults: new { controller = "OrganizationKategoriya", action = "Index" }
            );

            routes.MapRoute(
                name: "OrganizationGood",
                url: "org{organizationID}/Tovar/{goodId}",
                defaults: new { controller = "OrganizationTovar", action = "Index" }
            );

            // Default -----------------------------------------------------------------------------------------------------------------------------------

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}",
                defaults: new { controller = "Kategoriya", action = "Index" }
            );
        }
    }
}
