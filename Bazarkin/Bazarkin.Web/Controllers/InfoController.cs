﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Bazarkin.Web.Controllers
{
    public class InfoController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AboutPortal()
        {
            return View();
        }

        public ActionResult Partners()
        {
            return View();
        }

        public ActionResult Contacts()
        {
            return View();
        }

        public ActionResult Help()
        {
            return View();
        }

        public ActionResult Apps()
        {
            return View();
        }

        public ActionResult Agreement()
        {
            return View();
        }
        //-------------------------------------------------------
        public ActionResult Markets()
        {
            return View();
        }

        public ActionResult Delivery()
        {
            return View();
        }

        public ActionResult HowToOrder()
        {
            return View();
        }

        public ActionResult HowToPay()
        {
            return View();
        }

        public ActionResult HowToGet()
        {
            return View();
        }

        public ActionResult Bonuses()
        {
            return View();
        }
        
        public ActionResult ReturnsAndWarranty()
        {
            return View();
        }
    }
}