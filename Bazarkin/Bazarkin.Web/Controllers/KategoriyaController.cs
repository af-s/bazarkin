﻿using Bazarkin.Domain;
using Bazarkin.Web.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Bazarkin.Web.Controllers
{
    public class KategoriyaController : Controller
    {
        private EFDbContext db = new EFDbContext();

        [HttpGet]
        public ActionResult Index()
        {
            Category categoryParent;

            if (RouteData.Values["categoryName"] != null)
            {
                string categoryName = RouteData.Values["categoryName"].ToString();
                categoryParent = db.CategorySet.Where(x => x.Alias == categoryName.ToString()).FirstOrDefault();
                if (categoryParent!=null)
                {
                    ViewBag.Title = "Bazarkin | " + categoryParent.Name;
                }
                else
                {
                    // Редирект на ненайденую страницу
                }
            }
            else
            {
                categoryParent = null;
                ViewBag.Title = "Bazarkin | Главная";
                ViewBag.White = true;
            }

            CategoryView categoryView;
            if (categoryParent != null)
            {
                List<ModificationGood> modificationGoodList = db.ModificationGoodSet.Where(x => x.Good.Category.Id == categoryParent.Id).ToList();
                categoryView = new CategoryView(db, categoryParent, modificationGoodList, modificationGoodList);
            }
            else
            {
                categoryView = new CategoryView(db, categoryParent, null, null);
            }
            
            return View("Index", categoryView);
        }
    }
}