﻿using Bazarkin.Domain;
using Bazarkin.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Bazarkin.Web.Controllers
{
    public class Kategoriya2Controller : Controller
    {
        private EFDbContext db = new EFDbContext();

        [HttpGet]
        public ActionResult Index()
        {
            string catalogName;
            if (RouteData.Values["categoryName"] != null)
            {
                catalogName = RouteData.Values["categoryName"].ToString();
            }
            else
            {
                catalogName = "root";
                ViewBag.Title = "Bazarkin | Главная";
                ViewBag.White = true;
            }
            Category category = db.CategorySet.Where(x => x.Alias == catalogName).FirstOrDefault();
            if(category!=null && category.Name!="root")
            {
                ViewBag.Title = "Bazarkin | " + category.Name;
            }

            if (category != null)
            {
                //if (category.GoodHear)
                //{
                //    List<ModificationGood> modificationGoodViewSet = db.ModificationGoodSet.Where(x => x.Good.Category.Id == category.Id).ToList();
                //    List<ModificationGood> modificationGoodFilterSet = db.ModificationGoodSet.Where(x => x.Good.Category.Id == category.Id).ToList();
                //    GoodListView goodListView = new GoodListView(db, category, modificationGoodViewSet, modificationGoodFilterSet);
                //    return View("GoodList", goodListView);
                //}
                //else
                //{
                //    CategoryListView categoryListView = new CategoryListView(db, category);
                //    return View("CategoryList", categoryListView);
                //}
            }
            return View();
        }

        [HttpPost]
        public ActionResult Index(GoodListView goodListView)
        {
            Category category = db.CategorySet.Where(x => x.Id == goodListView.category.Id).FirstOrDefault();

            List<ModificationGood> modificationGoodViewSet = db.ModificationGoodSet.Where(x => x.Good.Category.Id == category.Id).ToList();
            List<ModificationGood> modificationGoodFilterSet = db.ModificationGoodSet.Where(x => x.Good.Category.Id == category.Id).ToList();

            #region price
            if (goodListView.priceMin != null)
            {
                List<Price> priceSet = new List<Price>();
                decimal priceMin = Convert.ToDecimal(goodListView.priceMin);
                foreach (var modificationGood in modificationGoodViewSet)
                {
                    priceSet.AddRange(db.PriceSet.Where(x => x.Value >= priceMin && x.ModificationGood.Id == modificationGood.Id));
                }
                List<ModificationGood> modificationGFSet = new List<ModificationGood>();
                foreach (var price in priceSet)
                {
                    modificationGFSet.Add(modificationGoodViewSet.Where(x => x.Id == price.ModificationGood.Id).FirstOrDefault());
                }
                modificationGoodViewSet = modificationGFSet;
            }
            if (goodListView.priceMax != null)
            {
                List<Price> priceSet = new List<Price>();
                decimal priceMax = Convert.ToDecimal(goodListView.priceMax);
                foreach (var modificationGood in modificationGoodViewSet)
                {
                    priceSet.AddRange(db.PriceSet.Where(x => x.Value <= priceMax && x.ModificationGood.Id == modificationGood.Id));
                }
                List<ModificationGood> modificationGFSet = new List<ModificationGood>();
                foreach (var price in priceSet)
                {
                    modificationGFSet.Add(modificationGoodViewSet.Where(x => x.Id == price.ModificationGood.Id).FirstOrDefault());
                }
                modificationGoodViewSet = modificationGFSet;
            }
            #endregion

            #region manufacturer
            bool manufacturerFilter = false;
            foreach (var manufacturerFlag in goodListView.manufacturerFlagSet)
            {
                if (manufacturerFlag.flag)
                {
                    manufacturerFilter = true;
                    break;
                }
            }
            if (manufacturerFilter)
            {
                foreach (var manufacturerFlag in goodListView.manufacturerFlagSet)
                {
                    if (!manufacturerFlag.flag)
                    {
                        modificationGoodViewSet = modificationGoodViewSet.Where(x => x.Good.Manufacturer.Id != manufacturerFlag.manufacturer.Id).ToList();
                    }
                }
            }
            #endregion

            #region filter
            foreach (var filter in goodListView.filterSet)
            {
                foreach (var filterItem in filter.filterItemSet)
                {
                    if (filterItem.propertyType.FilterType == "list")
                    {
                        List<PropertyValueGroup> propertyValueGroupSet = new List<PropertyValueGroup>();
                        foreach (var modificationGoodView in modificationGoodViewSet)
                        {
                            propertyValueGroupSet.AddRange(db.PropertyValueGroupSet.Where(
                                x => x.ModificationGood.Id == modificationGoodView.Id
                                && x.PropertyValue.Property.Id == filterItem.property.Id));
                        }
                        bool flag = false;

                        foreach (var propertyValueFlag in filterItem.propertyValueFlagSet)
                        {
                            if (propertyValueFlag.flag)
                            {
                                flag = true;
                                break;
                            }
                        }
                        if (flag)
                        {
                            foreach (var propertyValueFlag in filterItem.propertyValueFlagSet)
                            {
                                if(!propertyValueFlag.flag)
                                {
                                    propertyValueGroupSet = propertyValueGroupSet.Where(x => x.PropertyValue.Id != propertyValueFlag.propertyValue.Id).ToList();
                                }
                            }
                        }
                        List<ModificationGood> modificationGFSet = new List<ModificationGood>();
                        foreach (var propertyValueGroup in propertyValueGroupSet)
                        {
                            modificationGFSet.Add(modificationGoodViewSet.Where(x => x.Id == propertyValueGroup.ModificationGood.Id).FirstOrDefault());
                        }
                        modificationGoodViewSet = modificationGFSet;

                    }
                    else if (filterItem.propertyType.FilterType == "interval")
                    {
                        if (filterItem.valueMin != null)
                        {
                            List<PropertyValueGroup> propertyValueGroupSet = new List<PropertyValueGroup>();
                            foreach (var modificationGoodView in modificationGoodViewSet)
                            {
                                decimal filterMin = Convert.ToDecimal(filterItem.valueMin);
                                PropertyValueGroup ropertyValueGroup = db.PropertyValueGroupSet.Where(
                                    x => x.ModificationGood.Id == modificationGoodView.Id
                                    && x.PropertyValue.Property.Id == filterItem.property.Id).FirstOrDefault();
                                if (Convert.ToDecimal(ropertyValueGroup.PropertyValue.Value) >= filterMin)
                                {
                                    propertyValueGroupSet.Add(ropertyValueGroup);
                                }
                            }
                            List<ModificationGood> modificationGFSet = new List<ModificationGood>();
                            foreach (var propertyValueGroup in propertyValueGroupSet)
                            {
                                modificationGFSet.Add(modificationGoodViewSet.Where(x => x.Id == propertyValueGroup.ModificationGood.Id).FirstOrDefault());
                            }
                            modificationGoodViewSet = modificationGFSet;
                        }
                        if (filterItem.valueMax != null)
                        {
                            List<PropertyValueGroup> propertyValueGroupSet = new List<PropertyValueGroup>();
                            foreach (var modificationGoodView in modificationGoodViewSet)
                            {
                                decimal filterMax = Convert.ToDecimal(filterItem.valueMax);
                                PropertyValueGroup ropertyValueGroup = db.PropertyValueGroupSet.Where(
                                    x => x.ModificationGood.Id == modificationGoodView.Id
                                    && x.PropertyValue.Property.Id == filterItem.property.Id).FirstOrDefault();
                                if (Convert.ToDecimal(ropertyValueGroup.PropertyValue.Value) <= filterMax)
                                {
                                    propertyValueGroupSet.Add(ropertyValueGroup);
                                }
                            }
                            List<ModificationGood> modificationGFSet = new List<ModificationGood>();
                            foreach (var propertyValueGroup in propertyValueGroupSet)
                            {
                                modificationGFSet.Add(modificationGoodViewSet.Where(x => x.Id == propertyValueGroup.ModificationGood.Id).FirstOrDefault());
                            }
                            modificationGoodViewSet = modificationGFSet;
                        }
                    }
                }
            }
            #endregion

            GoodListView goodListViewFilter = new GoodListView(db, category, modificationGoodViewSet, modificationGoodFilterSet);
            return View("GoodList", goodListViewFilter);
        }
    }
}