﻿using Bazarkin.Domain;
using Bazarkin.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Bazarkin.Web.Controllers
{
    /// <summary>
    /// Каталог всех групп с подгруппами
    /// </summary>
    public class CatalogController : Controller
    {
        private EFDbContext db = new EFDbContext();
        public ActionResult Index()
        {
            CatalogView catalogView = new CatalogView(db);

            return View(catalogView);
        }
    }
}