﻿using Bazarkin.Domain;
using Bazarkin.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Bazarkin.Web.Controllers
{
    public class OrganizationKategoriyaController : Controller
    {
        private EFDbContext db = new EFDbContext();

        public ActionResult Index()
        {
            int organizationID;
            string categoryName;

            Organization organization;
            Category category;

            if (RouteData.Values["categoryName"] != null && RouteData.Values["organizationID"] != null)
            {
                organizationID = Convert.ToInt32(RouteData.Values["organizationID"]);
                categoryName = RouteData.Values["categoryName"].ToString();

                organization = db.OrganizationSet.Where(x => x.Id == organizationID).FirstOrDefault();
                category = db.CategorySet.Where(x => x.Alias == categoryName).FirstOrDefault();

                if (organization != null && category != null)
                {
                    ViewBag.Title = "Bazarkin | " + organization.Name + " - " + category.Name;

                    //List<ModificationGood> modificationGoodViewSet = db.ModificationGoodSet.Where(x => x.Good.Category.Id == category.Id).ToList();
                    //List<ModificationGood> modificationGoodFilterSet = db.ModificationGoodSet.Where(x => x.Good.Category.Id == category.Id).ToList();

                    List<OrganizationUnit> organizationUnitSet = db.OrganizationUnitSet.Where(x=>x.Organization.Id==organization.Id).ToList();
                    List<Price> priceSet = new List<Price>();
                    foreach(var organizationUnit in organizationUnitSet)
                    {
                        priceSet.AddRange(db.PriceSet.Where(x=>x.OrganizationUnit.Id==organizationUnit.Id));
                    }
                    List<ModificationGood> modificationGoodSet = new List<ModificationGood>();
                    foreach (var price in priceSet)
                    {
                        modificationGoodSet.AddRange(db.ModificationGoodSet.Where(x => x.Id == price.ModificationGood.Id));
                    }
                    modificationGoodSet = modificationGoodSet.Distinct().ToList();

                    OrganizationKategoriya organizationKategoriya = new OrganizationKategoriya(db, organization, category, modificationGoodSet, modificationGoodSet);

                    return View(organizationKategoriya);
                }
            }
            else
            {
                // return 404
            }

            return View();
        }
    }
}