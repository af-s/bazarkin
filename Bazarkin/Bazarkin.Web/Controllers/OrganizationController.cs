﻿using Bazarkin.Domain;
using Bazarkin.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Bazarkin.Web.Controllers
{
    public class OrganizationController : Controller
    {
        private EFDbContext db = new EFDbContext();

        public ActionResult Index()
        {
            int organizationId=0;
            if (RouteData.Values["organizationID"] != null)
            {
                organizationId = Convert.ToInt32(RouteData.Values["organizationID"]);
            }

            Organization organization = db.OrganizationSet.Where(x => x.Id == organizationId).FirstOrDefault();

            ViewBag.Title = "Bazarkin | " + organization.Name;

            OrganizationView organizationView = new OrganizationView(db, organization);

            return View(organizationView);
        }
    }
}