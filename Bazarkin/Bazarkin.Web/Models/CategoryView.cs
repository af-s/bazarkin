﻿using Bazarkin.Domain;
using Bazarkin.Web.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bazarkin.Web.Models
{
    public class CategoryView
    {
        public Category categoryParent { get; set; }
        public List<Filter> filterList { get; set; }
        public List<GoodCard> goodCardList { get; set; }
        public List<ManufacturerFlag> manufacturerFlagList { get; set; }
        public string priceMin { get; set; }
        public string priceMax { get; set; }

        public List<Price> priceList;
        public List<Category> categoryList;
        public BreadCrumbs breadCrumbs;

        public CategoryView() { }

        public CategoryView(EFDbContext db, Category categoryParent, List<ModificationGood> modificationGoodFilterList, List<ModificationGood> modificationGoodViewList)
        {
            if (categoryParent == null)
            {
                categoryList = db.CategorySet.Where(x => x.Parent == null).OrderBy(y => y.Name).ToList();
            }
            else
            {
                this.categoryParent = categoryParent;
                categoryList = db.CategorySet.Where(x => x.Parent.Id == categoryParent.Id).OrderBy(y => y.Name).ToList();
                breadCrumbs = new BreadCrumbs(db, categoryParent);
            }

            if (modificationGoodFilterList != null)
            {
                // Фильтры
                List<PropertyValueGroup> propertyValueGroupList = new List<PropertyValueGroup>();
                foreach(var modificationGoodFilter in modificationGoodFilterList)
                {
                    propertyValueGroupList.AddRange(db.PropertyValueGroupSet.Where(x => x.ModificationGood.Id == modificationGoodFilter.Id));
                }

                List<PropertyGroup> propertyGroupList = new List<PropertyGroup>();
                foreach(var propertyValueGroup in propertyValueGroupList)
                {
                    propertyGroupList.Add(db.PropertyGroupSet.Where(x => x.Id == propertyValueGroup.Property.PropertyGroup.Id).FirstOrDefault());
                }
                propertyGroupList = propertyGroupList.Distinct().ToList();

                filterList = new List<Filter>();
                foreach (var propertyGroup in propertyGroupList)
                {
                    filterList.Add(new Filter(db, modificationGoodFilterList, propertyGroup, propertyValueGroupList));
                }

                // Карочки товара
                goodCardList = new List<GoodCard>();
                List<Good> goodViewSet = new List<Good>();
                foreach (var modificationGoodView in modificationGoodViewList)
                {
                    goodViewSet.Add(db.GoodSet.Where(x => x.Id == modificationGoodView.Good.Id).FirstOrDefault());
                }
                goodViewSet = goodViewSet.Distinct().ToList();
                foreach (var goodView in goodViewSet)
                {
                    goodCardList.Add(new GoodCard(db, goodView, modificationGoodViewList));
                }

                // Производители
                manufacturerFlagList = new List<ManufacturerFlag>();
                List<Manufacturer> manufacturerSet = new List<Manufacturer>();
                foreach (var modificationGoodFilter in modificationGoodFilterList)
                {
                    manufacturerSet.Add(db.ManufacturerSet.Where(x => x.Id == modificationGoodFilter.Good.Manufacturer.Id).FirstOrDefault());
                }
                manufacturerSet = manufacturerSet.Distinct().ToList();
                foreach (var manufacturer in manufacturerSet)
                {
                    manufacturerFlagList.Add(new ManufacturerFlag(manufacturer));
                }

                // Цена
                priceList = new List<Price>();
                foreach (var modificationGoodFilter in modificationGoodFilterList)
                {
                    priceList.AddRange(db.PriceSet.Where(x => x.ModificationGood.Id == modificationGoodFilter.Id));
                }
            }
        }
        public class Filter
        {
            public PropertyGroup propertyGroup { get; set; }
            public List<FilterItem> filterItemSet { get; set; }

            public Filter() { }

            public Filter(EFDbContext db, List<ModificationGood> modificationGoodFilterSet, PropertyGroup propertyGroup, List<PropertyValueGroup> propertyValueGroupList)
            {
                this.propertyGroup = propertyGroup;

                List<Property> propertyList = new List<Property>();
                propertyValueGroupList = propertyValueGroupList.Where(x => x.Property.PropertyGroup.Id == propertyGroup.Id).ToList();
                foreach (var propertyValueGroup in propertyValueGroupList)
                {
                    propertyList.Add(db.PropertySet.Where(x => x.Id == propertyValueGroup.Property.Id).FirstOrDefault());
                }
                propertyList = propertyList.Distinct().ToList();

                filterItemSet = new List<FilterItem>();
                foreach (var property in propertyList)
                {
                    filterItemSet.Add(new FilterItem(db, modificationGoodFilterSet, property, propertyValueGroupList));
                }
            }
        }

        public class FilterItem
        {
            public Property property { get; set; }
            public List<PropertyValueFlag> propertyValueFlagSet { get; set; }
            public PropertyValue propertyValue { get; set; }
            public PropertyType propertyType { get; set; }

            public string valueMin { get; set; }
            public string valueMax { get; set; }

            public string valueMinInfo;
            public string valueMaxInfo;

            public FilterItem() { }

            public FilterItem(EFDbContext db, List<ModificationGood> modificationGoodFilterSet, Property property, List<PropertyValueGroup> propertyValueGroupList)
            {
                this.property = property;
                this.propertyType = db.PropertyTypeSet.Where(x => x.Id == property.PropertyType.Id).FirstOrDefault();
                propertyValueFlagSet = new List<PropertyValueFlag>();

                propertyValueGroupList = propertyValueGroupList.Where(x => x.Property.Id == property.Id).ToList();
                List<PropertyValue> propertyValueList = new List<PropertyValue>();
                foreach (var propertyValueGroup in propertyValueGroupList)
                {
                    propertyValueList.Add(db.PropertyValueSet.Where(x => x.Id == propertyValueGroup.PropertyValue.Id).FirstOrDefault());
                }
                propertyValueList = propertyValueList.Distinct().ToList();

                foreach (var propertyValue in propertyValueList)
                {
                    propertyValueFlagSet.Add(new PropertyValueFlag(propertyValue));
                }

                if (propertyType.FilterType == "interval")
                {
                    propertyValue = db.PropertyValueSet.Where(x => x.Property.Id == property.Id).FirstOrDefault();
                    valueMinInfo = propertyValueList.Min(x => x.Value);
                    valueMaxInfo = propertyValueList.Max(x => x.Value);
                }
            }
        }

        public class PropertyValueFlag
        {
            public PropertyValue propertyValue { get; set; }
            public bool flag { get; set; }

            public PropertyValueFlag() { }

            public PropertyValueFlag(PropertyValue propertyValue)
            {
                this.propertyValue = propertyValue;
            }
        }

        public class GoodCard
        {
            public Good good;
            public string image;
            public int modificationGoodCount;
            public Manufacturer manufacturer;
            public string propertyValueLine;
            public List<Price> priceSet;

            private int propertyValueLineLength = 100;

            public GoodCard(EFDbContext db, Good good, List<ModificationGood> modificationGoodViewSet)
            {
                propertyValueLine = string.Empty;

                this.good = good;
                List<ImageGood> imageGoodList = db.ImageGoodSet.Where(x => x.Good.Id == good.Id).ToList();
                if (imageGoodList.Count != 0)
                {
                    this.image = imageGoodList.OrderBy(x => x.OrderIndex).FirstOrDefault().Name;
                }
                else
                {
                    image = "fileNull";
                }
                List<ModificationGood> modificationGoodSet=modificationGoodViewSet.Where(x => x.Good.Id == good.Id).ToList();
                this.modificationGoodCount = modificationGoodSet.Count();
                this.manufacturer = db.ManufacturerSet.Where(x => x.Id == good.Manufacturer.Id).FirstOrDefault();

                List<PropertyCategory> propertyCategorySet = db.PropertyCategorySet.Where(x => x.Category.Id == good.Category.Id).OrderBy(y => y.OrderIndex).ToList();

                List<Property> propertySet = new List<Property>();
                foreach (var propertyCategory in propertyCategorySet)
                {
                    propertySet.Add(db.PropertySet.Where(x => x.Id == propertyCategory.Property.Id).FirstOrDefault());
                }

                List<PropertyValueGroup> propertyValueGroupSet = new List<PropertyValueGroup>();
                foreach (var modificationGood in modificationGoodSet)
                {
                    propertyValueGroupSet.AddRange(db.PropertyValueGroupSet.Where(x => x.ModificationGood.Id == modificationGood.Id));
                }
                List<PropertyValue> propertyValueSet = new List<PropertyValue>();
                foreach (var propertyValueGroup in propertyValueGroupSet)
                {
                    propertyValueSet.AddRange(db.PropertyValueSet.Where(x => x.Id == propertyValueGroup.PropertyValue.Id));
                }
                propertyValueSet = propertyValueSet.Distinct().ToList();
                foreach (var property in propertySet)
                {
                    if (propertyValueLine.Length < propertyValueLineLength)
                    {
                        if (propertyValueLine != string.Empty)
                        {
                            propertyValueLine += ", ";
                        }
                        PropertyType propertyType = db.PropertyTypeSet.Where(x => x.Id == property.PropertyType.Id).FirstOrDefault();
                        List<PropertyValue> propertyValueInfoSet = new List<PropertyValue>();
                        propertyValueInfoSet.AddRange(propertyValueSet.Where(x => x.Property.Id == property.Id));

                        for (int i = 0; i < propertyValueInfoSet.Count(); i++)
                        {
                            if (i != 0)
                            {
                                propertyValueLine += " | ";
                            }
                            propertyValueLine += string.Format(propertyType.ValueFormat, propertyValueInfoSet[i].Value);
                        }
                    }
                    else
                    {
                        propertyValueLine = propertyValueLine.Remove(propertyValueLineLength - 3) + "...";
                        break;
                    }
                }

                this.priceSet = new List<Price>();
                foreach (var modificationGood in modificationGoodSet)
                {
                    this.priceSet.AddRange(db.PriceSet.Where(x => x.ModificationGood.Id == modificationGood.Id));
                }
            }
        }

        public class ManufacturerFlag
        {
            public Manufacturer manufacturer { get; set; }
            public bool flag { get; set; }
            public ManufacturerFlag() { }
            public ManufacturerFlag(Manufacturer manufacturer)
            {
                this.manufacturer = manufacturer;
            }
        }
    }
}