﻿using Bazarkin.Domain;
using Bazarkin.Web.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bazarkin.Web.Models
{
    public class OrganizationView
    {
        public OrganizationAndInfo organizationAndInfo;
        public List<OrganizationUnitAndInfo> organizationUnitAndInfoSet;
        public List<Category> categorySet;

        public string organizationName;
        public string organizationPhone;

        public BreadCrumbs breadCrumbs;

        public OrganizationView(EFDbContext db, Organization organization)
        {
            this.organizationAndInfo = new OrganizationAndInfo(db, organization);

            List<OrganizationUnit> organizationUnitSet = db.OrganizationUnitSet.Where(x => x.Organization.Id == organization.Id).ToList();
            this.organizationUnitAndInfoSet = new List<OrganizationUnitAndInfo>();
            foreach (var organizationUnit in organizationUnitSet)
            {
                organizationUnitAndInfoSet.Add(new OrganizationUnitAndInfo(db, organizationUnit));
            }

            this.categorySet = new List<Category>();

            foreach (var organizationUnit in organizationUnitSet)
            {
                List<Price> priceSet = db.PriceSet.Where(x=>x.OrganizationUnit.Id==organizationUnit.Id).ToList();
                foreach(var price in priceSet)
                {
                    this.categorySet.Add(db.CategorySet.Where(x => x.Id == price.ModificationGood.Good.Category.Id).FirstOrDefault());
                }
            }
            this.categorySet = this.categorySet.Distinct().ToList();

            organizationName = organization.Name;
            organizationPhone = organizationAndInfo.phoneSet.FirstOrDefault().Number;

            this.breadCrumbs = new BreadCrumbs(db, organization);
        }

        public class OrganizationAndInfo
        {
            public Organization organization;
            public List<Phone> phoneSet;
            public List<AddressAndDescription> addressAndDescriptionSet;
            //public List<OrganizationUnitAndInfo> organizationUnitAndInfoSet;

            public OrganizationAndInfo(EFDbContext db, Organization organization)
            {
                this.organization = organization;
                List<Address> addressSet = db.AddressSet.Where(x => x.ContactGroup.Id == organization.ContactGroup.Id).ToList();
                this.phoneSet = db.PhoneSet.Where(x => x.ContactGroup.Id == organization.ContactGroup.Id).ToList();
                addressAndDescriptionSet = new List<AddressAndDescription>();
                foreach (var address in addressSet)
                {
                    addressAndDescriptionSet.Add(new AddressAndDescription(address));
                }

                //organizationUnitAndInfoSet = new List<OrganizationUnitAndInfo>();
                //List<OrganizationUnit> organizationUnitSet = db.OrganizationUnitSet.Where(x => x.Organization.Id == organization.Id).ToList();
                //foreach (var organizationUnit in organizationUnitSet)
                //{
                //    organizationUnitAndInfoSet.Add(new OrganizationUnitAndInfo(db, organizationUnit));
                //}
            }
        }

        public class OrganizationUnitAndInfo
        {
            public OrganizationUnit organizationUnit;
            public List<Phone> phoneSet;
            public List<AddressAndDescription> addressAndDescriptionSet;

            public OrganizationUnitAndInfo(EFDbContext db, OrganizationUnit organizationUnit)
            {
                this.organizationUnit = organizationUnit;
                List<Address> addressSet = db.AddressSet.Where(x => x.ContactGroup.Id == organizationUnit.ContactGroup.Id).ToList();
                this.phoneSet = db.PhoneSet.Where(x => x.ContactGroup.Id == organizationUnit.ContactGroup.Id).ToList();
                addressAndDescriptionSet = new List<AddressAndDescription>();
                foreach (var address in addressSet)
                {
                    addressAndDescriptionSet.Add(new AddressAndDescription(address));
                }
            }
        }

        public class AddressAndDescription
        {
            public Address address;
            public string addressStr;

            public AddressAndDescription(Address address)
            {
                this.address = address;
                addressStr = string.Empty;

                if (address.Country != null)
                {
                    if (addressStr != string.Empty)
                    {
                        addressStr += ", ";
                    }
                    addressStr += address.Country;
                }
                if (address.Region != null)
                {
                    if (addressStr != string.Empty)
                    {
                        addressStr += ", ";
                    }
                    addressStr += address.Region;
                }
                if (address.City != null)
                {
                    if (addressStr != string.Empty)
                    {
                        addressStr += ", ";
                    }
                    addressStr += address.City;
                }
                if (address.Street != null)
                {
                    if (addressStr != string.Empty)
                    {
                        addressStr += ", ";
                    }
                    addressStr += address.Street;
                }
                if (address.Building != null)
                {
                    if (addressStr != string.Empty)
                    {
                        addressStr += ", ";
                    }
                    addressStr += address.Building;
                }
                if (address.Structure != null)
                {
                    //if (item != string.Empty)
                    //{
                    //    item += ", ";
                    //}
                    addressStr += "к" + address.Structure;
                }
                if (address.Room != null)
                {
                    if (addressStr != string.Empty)
                    {
                        addressStr += ", ";
                    }
                    addressStr += address.Room;
                }
                if (address.ShoppingCenter != null)
                {
                    if (addressStr != string.Empty)
                    {
                        addressStr += ", ";
                    }
                    addressStr += address.ShoppingCenter;
                }
                if (address.Row != null)
                {
                    if (addressStr != string.Empty)
                    {
                        addressStr += ", ";
                    }
                    addressStr += "ряд " + address.Row;
                }
                if (address.Sector != null)
                {
                    if (addressStr != string.Empty)
                    {
                        addressStr += ", ";
                    }
                    addressStr += "сектор " + address.Sector;
                }
                if (address.Pavilion != null)
                {
                    if (addressStr != string.Empty)
                    {
                        addressStr += ", ";
                    }
                    addressStr += "павильон " + address.Pavilion;
                }
                if (address.Index != null)
                {
                    if (addressStr != string.Empty)
                    {
                        addressStr += ", ";
                    }
                    addressStr += "индекс " + address.Index;
                }
            }
        }
    }
}