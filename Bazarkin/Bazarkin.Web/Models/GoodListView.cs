﻿using Bazarkin.Domain;
using Bazarkin.Web.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bazarkin.Web.Models
{
    public class GoodListView
    {
        public Category category { get; set; }

        public List<Filter> filterSet { get; set; }

        public List<GoodCard> goodCardSet { get; set; }
        public BreadCrumbs breadCrumbs { get; set; }

        public List<ManufacturerFlag> manufacturerFlagSet { get; set; }
        public string priceMin { get; set; }
        public string priceMax { get; set; }

        public List<Price> priceSet;

        public GoodListView() { }

        public GoodListView(EFDbContext db, Category category, List<ModificationGood> modificationGoodViewSet, List<ModificationGood> modificationGoodFilterSet)
        {
            this.category = category;

            // Фильтры
            filterSet = new List<Filter>();
            List<PropertyValueGroup> propertyValueGroupSet = new List<PropertyValueGroup>();
            foreach (var modificationGoodFilter in modificationGoodFilterSet)
            {
                propertyValueGroupSet.AddRange(db.PropertyValueGroupSet.Where(x => x.ModificationGood.Id == modificationGoodFilter.Id));
            }
            List<PropertyValue> propertyValueSet = new List<PropertyValue>();
            foreach (var propertyValueGroup in propertyValueGroupSet)
            {
                propertyValueSet.AddRange(db.PropertyValueSet.Where(x => x.Id == propertyValueGroup.PropertyValue.Id));
            }
            propertyValueSet = propertyValueSet.Distinct().ToList();
            List<Property> propertySet = new List<Property>();
            foreach (var propertyValue in propertyValueSet)
            {
                propertySet.AddRange(db.PropertySet.Where(x => x.Id == propertyValue.Property.Id));
            }
            propertySet = propertySet.Distinct().ToList();
            List<PropertyGroup> propertyGroupSet = new List<PropertyGroup>();
            foreach (var property in propertySet)
            {
                propertyGroupSet.Add(db.PropertyGroupSet.Where(x => x.Id == property.PropertyGroup.Id).FirstOrDefault());
            }
            propertyGroupSet = propertyGroupSet.Distinct().ToList();
            foreach (var propertyGroup in propertyGroupSet)
            {
                filterSet.Add(new Filter(db, modificationGoodFilterSet, propertyGroup));
            }

            // Карочки товара
            goodCardSet = new List<GoodCard>();
            List<Good> goodViewSet = new List<Good>();
            foreach (var modificationGoodView in modificationGoodViewSet)
            {
                goodViewSet.Add(db.GoodSet.Where(x => x.Id == modificationGoodView.Good.Id).FirstOrDefault());
            }
            goodViewSet = goodViewSet.Distinct().ToList();
            foreach (var goodView in goodViewSet)
            {
                goodCardSet.Add(new GoodCard(db, goodView, modificationGoodViewSet));
            }

            // Хлебные крошки
            this.breadCrumbs = new BreadCrumbs(db, category);

            // Производители
            manufacturerFlagSet = new List<ManufacturerFlag>();
            List<Manufacturer> manufacturerSet = new List<Manufacturer>();
            foreach (var modificationGoodFilter in modificationGoodFilterSet)
            {
                manufacturerSet.Add(db.ManufacturerSet.Where(x => x.Id == modificationGoodFilter.Good.Manufacturer.Id).FirstOrDefault());
            }
            manufacturerSet = manufacturerSet.Distinct().ToList();
            foreach (var manufacturer in manufacturerSet)
            {
                manufacturerFlagSet.Add(new ManufacturerFlag(manufacturer));
            }

            // Цена
            priceSet=new List<Price>();
            foreach (var modificationGoodFilter in modificationGoodFilterSet)
            {
                priceSet.AddRange(db.PriceSet.Where(x => x.ModificationGood.Id == modificationGoodFilter.Id));
            }
        }
    }

    public class Filter
    {
        public PropertyGroup propertyGroup { get; set; }
        public List<FilterItem> filterItemSet { get; set; }

        public Filter() { }

        public Filter(EFDbContext db, List<ModificationGood> modificationGoodFilterSet, PropertyGroup propertyGroup)
        {
            this.propertyGroup = propertyGroup;
            List<PropertyValueGroup> propertyValueGroupSet = new List<PropertyValueGroup>();
            foreach (var modificationGoodFilter in modificationGoodFilterSet)
            {
                propertyValueGroupSet.AddRange(db.PropertyValueGroupSet.Where(x => x.ModificationGood.Id == modificationGoodFilter.Id && x.PropertyValue.Property.PropertyGroup.Id==propertyGroup.Id));
            }
            List<PropertyValue> propertyValueSet = new List<PropertyValue>();
            foreach (var propertyValueGroup in propertyValueGroupSet)
            {
                propertyValueSet.AddRange(db.PropertyValueSet.Where(x => x.Id == propertyValueGroup.PropertyValue.Id));
            }
            List<Property> propertySet = new List<Property>();
            foreach (var propertyValue in propertyValueSet)
            {
                propertySet.AddRange(db.PropertySet.Where(x => x.Id == propertyValue.Property.Id));
            }
            propertySet = propertySet.Distinct().ToList();

            filterItemSet = new List<FilterItem>();
            foreach (var property in propertySet)
            {
                filterItemSet.Add(new FilterItem(db, modificationGoodFilterSet, property));
            }
        }
    }

    public class FilterItem
    {
        public Property property { get; set; }
        public List<PropertyValueFlag> propertyValueFlagSet { get; set; }
        public PropertyValue propertyValue { get; set; }
        public PropertyType propertyType { get; set; }

        public string valueMin { get; set; }
        public string valueMax { get; set; }

        public string valueMinInfo;
        public string valueMaxInfo;

        public FilterItem() { }

        public FilterItem(EFDbContext db, List<ModificationGood> modificationGoodFilterSet, Property property)
        {
            this.property = property;
            this.propertyType = db.PropertyTypeSet.Where(x => x.Id == property.PropertyType.Id).FirstOrDefault();
            propertyValueFlagSet = new List<PropertyValueFlag>();
            List<PropertyValue> propertyValueSet = new List<PropertyValue>();

            List<PropertyValueGroup> propertyValueGroupSet = new List<PropertyValueGroup>();
            foreach (var modificationGoodFilter in modificationGoodFilterSet)
            {
                propertyValueGroupSet.AddRange(db.PropertyValueGroupSet.Where(x => x.ModificationGood.Id == modificationGoodFilter.Id));
            }
            foreach (var propertyValueGroup in propertyValueGroupSet)
            {
                propertyValueSet.AddRange(db.PropertyValueSet.Where(x => x.Id == propertyValueGroup.PropertyValue.Id && x.Property.Id == property.Id));
            }
            propertyValueSet = propertyValueSet.Distinct().ToList();

            foreach (var propertyValue in propertyValueSet)
            {
                propertyValueFlagSet.Add(new PropertyValueFlag(propertyValue));
            }

            if (propertyType.FilterType == "interval")
            {
                propertyValue = db.PropertyValueSet.Where(x => x.Property.Id == property.Id).FirstOrDefault();
                valueMinInfo = propertyValueSet.Min(x => x.Value);
                valueMaxInfo = propertyValueSet.Max(x => x.Value);
            }
        }
    }

    public class PropertyValueFlag
    {
        public PropertyValue propertyValue { get; set; }
        public bool flag { get; set; }

        public PropertyValueFlag() { }

        public PropertyValueFlag(PropertyValue propertyValue)
        {
            this.propertyValue = propertyValue;
        }
    }

    public class GoodCard
    {
        public Good good;
        public string image;
        public int modificationGoodCount;
        public Manufacturer manufacturer;
        public string propertyValueLine;
        public List<Price> priceSet;

        private int propertyValueLineLength = 100;

        public GoodCard(EFDbContext db, Good good, List<ModificationGood> modificationGoodViewSet)
        {
            //propertyValueLine = string.Empty;

            //this.good = good;
            //this.image = db.ImageGoodSet.Where(x => x.Good.Id == good.Id && x.Index == 1).FirstOrDefault().Name;
            //List<ModificationGood> modificationGoodSet=modificationGoodViewSet.Where(x => x.Good.Id == good.Id).ToList();
            //this.modificationGoodCount = modificationGoodSet.Count();
            //this.manufacturer = db.ManufacturerSet.Where(x => x.Id == good.Manufacturer.Id).FirstOrDefault();

            //List<PropertyCategory> propertyCategorySet = db.PropertyCategorySet.Where(x => x.Category.Id == good.Category.Id).OrderBy(y => y.Index).ToList();

            //List<Property> propertySet = new List<Property>();
            //foreach (var propertyCategory in propertyCategorySet)
            //{
            //    propertySet.Add(db.PropertySet.Where(x => x.Id == propertyCategory.Property.Id).FirstOrDefault());
            //}

            //List<PropertyValueGroup> propertyValueGroupSet = new List<PropertyValueGroup>();
            //foreach (var modificationGood in modificationGoodSet)
            //{
            //    propertyValueGroupSet.AddRange(db.PropertyValueGroupSet.Where(x => x.ModificationGood.Id == modificationGood.Id));
            //}
            //List<PropertyValue> propertyValueSet = new List<PropertyValue>();
            //foreach (var propertyValueGroup in propertyValueGroupSet)
            //{
            //    propertyValueSet.AddRange(db.PropertyValueSet.Where(x => x.Id == propertyValueGroup.PropertyValue.Id));
            //}
            //propertyValueSet = propertyValueSet.Distinct().ToList();
            //foreach (var property in propertySet)
            //{
            //    if (propertyValueLine.Length < propertyValueLineLength)
            //    {
            //        if (propertyValueLine != string.Empty)
            //        {
            //            propertyValueLine += ", ";
            //        }
            //        PropertyType propertyType = db.PropertyTypeSet.Where(x => x.Id == property.PropertyType.Id).FirstOrDefault();
            //        List<PropertyValue> propertyValueInfoSet = new List<PropertyValue>();
            //        propertyValueInfoSet.AddRange(propertyValueSet.Where(x => x.Property.Id == property.Id));

            //        for (int i = 0; i < propertyValueInfoSet.Count(); i++)
            //        {
            //            if (i != 0)
            //            {
            //                propertyValueLine += " | ";
            //            }
            //            propertyValueLine += string.Format(propertyType.ValueFormat, propertyValueInfoSet[i].Value);
            //        }
            //    }
            //    else
            //    {
            //        propertyValueLine = propertyValueLine.Remove(propertyValueLineLength - 3) + "...";
            //        break;
            //    }
            //}

            //this.priceSet = new List<Price>();
            //foreach (var modificationGood in modificationGoodSet)
            //{
            //    this.priceSet.AddRange(db.PriceSet.Where(x => x.ModificationGood.Id == modificationGood.Id));
            //}
        }
    }

    public class ManufacturerFlag
    {
        public Manufacturer manufacturer { get; set; }
        public bool flag { get; set; }
        public ManufacturerFlag() { }
        public ManufacturerFlag(Manufacturer manufacturer)
        {
            this.manufacturer = manufacturer;
        }
    }
}