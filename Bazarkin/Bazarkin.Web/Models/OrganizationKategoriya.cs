﻿using Bazarkin.Domain;
using Bazarkin.Web.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bazarkin.Web.Models
{
    public class OrganizationKategoriya
    {
        public OrganizationView organizationView;
        public GoodListView goodListView;

        public string organizationName;
        public string organizationPhone;

        public BreadCrumbs breadCrumbs;

        public OrganizationKategoriya(EFDbContext db, Organization organization, Category category, List<ModificationGood> modificationGoodViewSet, List<ModificationGood> modificationGoodFilterSet)
        {
            organizationView = new OrganizationView(db, organization);
            goodListView = new GoodListView(db, category, modificationGoodViewSet, modificationGoodFilterSet);
            organizationName = organizationView.organizationName;
            organizationPhone = organizationView.organizationPhone;
            breadCrumbs = new BreadCrumbs(db, organization, category);
        }
    }
}