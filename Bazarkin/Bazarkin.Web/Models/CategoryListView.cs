﻿using Bazarkin.Domain;
using Bazarkin.Web.BusinessLogic;
using System.Collections.Generic;
using System.Linq;

namespace Bazarkin.Web.Models
{
    public class CategoryListView
    {
        public Category categoryParent;
        public List<Category> categorySet;
        public BreadCrumbs breadCrumbs;

        public CategoryListView(EFDbContext db, Category category)
        {
            this.categoryParent = category;
            this.categorySet = db.CategorySet.Where(x => x.Parent.Id == category.Id).OrderBy(y => y.Name).ToList();
            this.breadCrumbs = new BreadCrumbs(db, category);
        }
    }
}