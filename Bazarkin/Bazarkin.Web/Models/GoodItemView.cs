﻿using Bazarkin.Domain;
using Bazarkin.Web.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bazarkin.Web.Models
{
    public class GoodItemView
    {
        public Good good;
        public Manufacturer manufacturer;
        public List<ModificationAndPrice> modificationAndPriceSet;
        public List<PropertyGroupAndValue> propertyGroupAndValueSet;
        public List<OrganizationUnitAndPrice> organizationUnitAndPriceSet;
        public BreadCrumbs breadCrumbs;

        public GoodItemView() { }

        public GoodItemView(EFDbContext db, List<ModificationGood> modificationGoodSet, Organization organization)
        {
            int item = modificationGoodSet.First().Good.Id;
            this.good = db.GoodSet.Where(x => x.Id == item).FirstOrDefault();
            this.manufacturer = db.ManufacturerSet.Where(x => x.Id == good.Manufacturer.Id).FirstOrDefault();

            this.breadCrumbs = new BreadCrumbs(db, good);

            List<PropertyCategory> propertyCategorySet = db.PropertyCategorySet.Where(x => x.Category.Id == good.Category.Id).OrderBy(x => x.OrderIndex).ToList();

            List<Property> propertySet = new List<Property>();
            foreach (var propertyCategory in propertyCategorySet)
            {
                propertySet.AddRange(db.PropertySet.Where(x => x.Id == propertyCategory.Property.Id));
                //propertySet = db.PropertySet.Where(x => x.Id == propertyCategory.Property.Id).ToList();
            }
            List<PropertyValueGroup> propertyValueGroupSet = new List<PropertyValueGroup>();
            foreach (var modificationGood in modificationGoodSet)
            {
                propertyValueGroupSet.AddRange(db.PropertyValueGroupSet.Where(x => x.ModificationGood.Id == modificationGood.Id));
            }

            this.modificationAndPriceSet = new List<ModificationAndPrice>();
            foreach (var modificationGood in modificationGoodSet)
            {
                List<PropertyValueGroup> propertyValueGroupModificationSet = new List<PropertyValueGroup>();
                foreach (var property in propertySet)
                {
                    List<PropertyValue> propertyValueList = new List<PropertyValue>();
                    foreach(var propertyValueGroup in propertyValueGroupSet)
                    {
                        PropertyValue propertyValue = db.PropertyValueSet.Where(x => x.Id == propertyValueGroup.PropertyValue.Id&&x.Property.Id==property.Id).FirstOrDefault();
                        if (propertyValue != null)
                        {
                            propertyValueList.Add(propertyValue);
                        }
                    }
                    propertyValueList = propertyValueList.Distinct().ToList();

                    if (propertyValueList.Count() > 1)
                    {
                        propertyValueGroupModificationSet.Add(propertyValueGroupSet.Where(x => x.PropertyValue.Property.Id == property.Id && x.ModificationGood.Id == modificationGood.Id).FirstOrDefault());
                    }
                }

                this.modificationAndPriceSet.Add(new ModificationAndPrice(db, modificationGood, propertyValueGroupModificationSet));
            }

            // ----------------------------------------------------------------

            this.propertyGroupAndValueSet = new List<PropertyGroupAndValue>();
            List<PropertyGroup> propertyGroupSet = new List<PropertyGroup>();
            foreach (var property in propertySet)
            {
                propertyGroupSet.Add(db.PropertyGroupSet.Where(x => x.Id == property.PropertyGroup.Id).FirstOrDefault());
            }
            propertyGroupSet = propertyGroupSet.Distinct().ToList();
            foreach (var propertyGroup in propertyGroupSet)
            {
                propertyGroupAndValueSet.Add(new PropertyGroupAndValue(db, propertyGroup, propertyValueGroupSet));
            }

            // -----------------------------------------------------------------

            this.organizationUnitAndPriceSet = new List<OrganizationUnitAndPrice>();

            List<Price> priceSet = new List<Price>();
            foreach (var modificationGoodActiv in modificationGoodSet)
            {
                priceSet.AddRange(db.PriceSet.Where(x => x.ModificationGood.Id == modificationGoodActiv.Id).ToList());
            }
            List<OrganizationUnit> organizationUnitSet = new List<OrganizationUnit>();
            foreach (var price in priceSet)
            {
                organizationUnitSet.Add(db.OrganizationUnitSet.Where(x => x.Id == price.OrganizationUnit.Id).First());
            }
            organizationUnitSet = organizationUnitSet.Distinct().ToList();

            foreach (var organizationUnit in organizationUnitSet)
            {
                organizationUnitAndPriceSet.Add(new OrganizationUnitAndPrice(db, organizationUnit, modificationGoodSet));
            }
        }

        public class ModificationAndPrice
        {
            public ModificationGood modificationGood;
            public List<Price> priceSet;
            public string modificationString;

            public ModificationAndPrice() { }

            public ModificationAndPrice(EFDbContext db, ModificationGood modificationGood, List<PropertyValueGroup> propertyValueGroupSet)
            {
                this.modificationGood = modificationGood;
                this.priceSet = db.PriceSet.Where(x => x.ModificationGood.Id == modificationGood.Id).ToList();

                this.modificationString = string.Empty;


                foreach (var propertyValueGroup in propertyValueGroupSet)
                {
                    PropertyType propertyType = db.PropertyTypeSet.Where(x => x.Id == propertyValueGroup.PropertyValue.Property.PropertyType.Id).FirstOrDefault();
                    if (this.modificationString != string.Empty)
                    {
                        this.modificationString += " | ";
                    }
                    PropertyValue propertyValue = db.PropertyValueSet.Where(x => x.Id == propertyValueGroup.PropertyValue.Id).FirstOrDefault();
                    this.modificationString += string.Format(propertyType.ValueFormat, propertyValue.Value);
                }
            }
        }

        public class PropertyGroupAndValue
        {
            public PropertyGroup propertyGroup;
            public List<PropertyAndValue> propertyAndValueSet;

            public PropertyGroupAndValue(EFDbContext db, PropertyGroup propertyGroup, List<PropertyValueGroup> propertyValueGroupSet)
            {
                this.propertyGroup = propertyGroup;

                List<Property> propertySet = db.PropertySet.Where(x => x.PropertyGroup.Id == propertyGroup.Id).OrderBy(y => y.Name).ToList();
                propertySet = propertySet.Distinct().ToList();

                propertyAndValueSet = new List<PropertyAndValue>();
                foreach (var property in propertySet)
                {
                    propertyAndValueSet.Add(new PropertyAndValue(db, property, propertyValueGroupSet));
                }
            }
        }

        public class PropertyAndValue
        {
            public Property property;
            public string propertyValueString;

            public PropertyAndValue(EFDbContext db, Property property, List<PropertyValueGroup> propertyValueGroupSet)
            {
                this.property = property;
                this.propertyValueString = string.Empty;
                propertyValueGroupSet = propertyValueGroupSet.Where(x => x.PropertyValue.Property.Id == property.Id).ToList();
                List<PropertyValue> propertyValueSet = new List<PropertyValue>();
                foreach (var propertyValueGroup in propertyValueGroupSet)
                {
                    propertyValueSet.Add(db.PropertyValueSet.Where(x => x.Id == propertyValueGroup.PropertyValue.Id).FirstOrDefault());
                }
                foreach (var propertyValue in propertyValueSet)
                {
                    if (propertyValueString != string.Empty)
                    {
                        propertyValueString += " | ";
                    }
                    propertyValueString += propertyValue.Value;
                }
            }
        }

        public class OrganizationUnitAndPrice
        {
            public Organization organization;
            public OrganizationUnit organizationUnit;
            public List<Price> priceSet;
            public string addressStr;

            public OrganizationUnitAndPrice() { }

            public OrganizationUnitAndPrice(EFDbContext db, OrganizationUnit organizationUnit, List<ModificationGood> modificationGoodSet)
            {
                this.organizationUnit = organizationUnit;
                this.organization = db.OrganizationSet.Where(x => x.Id == organizationUnit.Organization.Id).FirstOrDefault();
                this.priceSet = new List<Price>();

                foreach (var modificationGood in modificationGoodSet)
                {
                    Price price = db.PriceSet.Where(x => x.ModificationGood.Id == modificationGood.Id && x.OrganizationUnit.Id == organizationUnit.Id).FirstOrDefault();
                    if (price != null)
                    {
                        priceSet.Add(price);
                    }
                }

                ContactGroup cg = db.ContactGroupSet.Where(x=>x.Id ==organizationUnit.ContactGroup.Id).FirstOrDefault();
                Address address = db.AddressSet.Where(x => x.ContactGroup.Id == cg.Id).FirstOrDefault();
                addressStr = string.Empty;

                if (address.Country != null)
                {
                    if (addressStr != string.Empty)
                    {
                        addressStr += ", ";
                    }
                    addressStr += address.Country;
                }
                if (address.Region != null)
                {
                    if (addressStr != string.Empty)
                    {
                        addressStr += ", ";
                    }
                    addressStr += address.Region;
                }
                if (address.City != null)
                {
                    if (addressStr != string.Empty)
                    {
                        addressStr += ", ";
                    }
                    addressStr += address.City;
                }
                if (address.Street != null)
                {
                    if (addressStr != string.Empty)
                    {
                        addressStr += ", ";
                    }
                    addressStr += address.Street;
                }
                if (address.Building != null)
                {
                    if (addressStr != string.Empty)
                    {
                        addressStr += ", ";
                    }
                    addressStr += address.Building;
                }
                if (address.Structure != null)
                {
                    //if (item != string.Empty)
                    //{
                    //    item += ", ";
                    //}
                    addressStr += "к" + address.Structure;
                }
                if (address.Room != null)
                {
                    if (addressStr != string.Empty)
                    {
                        addressStr += ", ";
                    }
                    addressStr += address.Room;
                }
                if (address.ShoppingCenter != null)
                {
                    if (addressStr != string.Empty)
                    {
                        addressStr += ", ";
                    }
                    addressStr += address.ShoppingCenter;
                }
                if (address.Row != null)
                {
                    if (addressStr != string.Empty)
                    {
                        addressStr += ", ";
                    }
                    addressStr += "ряд " + address.Row;
                }
                if (address.Sector != null)
                {
                    if (addressStr != string.Empty)
                    {
                        addressStr += ", ";
                    }
                    addressStr += "сектор " + address.Sector;
                }
                if (address.Pavilion != null)
                {
                    if (addressStr != string.Empty)
                    {
                        addressStr += ", ";
                    }
                    addressStr += "павильон " + address.Pavilion;
                }
                if (address.Index != null)
                {
                    if (addressStr != string.Empty)
                    {
                        addressStr += ", ";
                    }
                    addressStr += "индекс " + address.Index;
                }
            }
        }
    }
}